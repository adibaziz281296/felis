/*
Author: Ahmed
Date: August 2017
*/
localStorage.removeItem('latt');
localStorage.removeItem('lonn');
localStorage.removeItem('zoomm');
//var ipaddress = '150.242.180.128';
var ipaddress = 'www.gis.myetapp.gov.my';


$("#basic_legend").click(function() {
     $("#wrapper").toggleClass("toggled");
  return false;
});

$(".sidebar-brand").click(function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
});

$("#menu-toggle").click(function(e) {
	$("#wrapper").toggleClass("toggled");
});


//===============================================================//
//							ARCGIS								 //
//===============================================================//

var map,symbol, geomTask;
var printInfo;
var app = {};
app.map = null; app.printer = null;
var carryValue;
var gviewClient, Pano, Glocator;
var viewPoint;
var viewStatus = false;
var viewAddress = "";

var jkptgLegend, ndcdbLegend;

var search_pemohonan_layer, search_qt_layer, search_ft_layer, search_rezab_layer;
var negeri_layer, daerah_layer, mukim_layer;
var storeStateLot, storeDistrictLot, storeMukimLot, storeSeksyenLot;
var storeStateProject, storeDistrictProject, storeMukimProject, storeSeksyenProject;
var storeStateSementara, storeDistrictSementara, storeMukimSementara, storeSeksyenSementara;
var storeStateMilik, storeDistrictMilik, storeMukimMilik, storeSeksyenMilik;
var selectStateIdLot, selectDistrictIdLot, selectMukimIdLot, selectSeksyenIdLot;
var selectStateIdProject, selectDistrictIdProject, selectMukimIdProject, selectSeksyenIdProject;
var selectStateIdSementara, selectDistrictIdSementara, selectMukimIdSementara, selectSeksyenIdSementara;
var selectStateIdMilik, selectDistrictIdMilik, selectMukimIdMilik, selectSeksyenIdMilik;

		require([
				"dojo/i18n!esri/nls/jsapi",
				"esri/map",
				"esri/dijit/Print",
				"esri/tasks/PrintTemplate",
				"esri/tasks/LegendLayer",
				"esri/request",
				"esri/config",
				"esri/geometry/Extent",
				"esri/dijit/HomeButton",
				"esri/layers/ArcGISDynamicMapServiceLayer",
				"esri/layers/FeatureLayer",
				"agsjs/dijit/TOC", // file TOC.js dlm folder agsjs/dijit/
				"esri/SpatialReference",
				"esri/layers/ImageParameters",
				
				"esri/geometry/webMercatorUtils", // display coordinate
				"esri/dijit/Search", // google search map
				"esri/dijit/OverviewMap",
				"esri/dijit/BasemapGallery",
			//	"esri/dijit/BasemapToggle", // Basemap 2 choices
				"esri/dijit/Scalebar",
				// for Bookmark
				"esri/dijit/Bookmarks",
				"dojo/cookie",
				
				"esri/InfoTemplate",
				"esri/tasks/IdentifyTask",
				"esri/tasks/IdentifyParameters",
				"esri/dijit/Popup",	
				"esri/dijit/PopupTemplate",				
				"esri/symbols/SimpleFillSymbol",
				"esri/symbols/SimpleLineSymbol",
				
				"dojox/layout/FloatingPane",
				"dijit/layout/TabContainer",
				"dijit/form/ComboBox", 
				"dijit/form/Button",
				"dojox/layout/Dock",
				
				"esri/tasks/QueryTask",
				"esri/tasks/query",
				"dojo/data/ItemFileReadStore",
				"dojo/data/ItemFileWriteStore",
				"dojox/grid/EnhancedGrid",
				"dojox/grid/enhanced/plugins/Pagination",
				
				"esri/symbols/SimpleMarkerSymbol",	
				
				"esri/tasks/GeometryService", // for buffer
				"esri/toolbars/draw",
				"esri/graphic",
				"esri/tasks/BufferParameters",
				"dojo/dom",
				"dojo/on",
				"esri/geometry/normalizeUtils",
				"esri/SpatialReference",
				"dgrid/OnDemandGrid", 
				"dojo/store/Memory",
				"dijit/TooltipDialog",
				
				"esri/dijit/Measurement", // for measure
				"esri/urlUtils",
				// for get directions
				"esri/dijit/Directions", 
				"esri/geometry/Point", // for search coordinate			
				"esri/dijit/LocateButton",
				 "esri/layers/LabelClass",
				 "esri/symbols/TextSymbol",
				
				//Growler
				'js/Growler',
				
				// For StreetView
				"esri/symbols/PictureMarkerSymbol", // for flying man streetview
				"esri/symbols/PictureFillSymbol", 
				"esri/symbols/CartographicLineSymbol",
				
				// For Route with Barrier
				"esri/tasks/RouteTask", 
				"esri/tasks/RouteParameters", 
				"esri/tasks/FeatureSet",
				//"esri/SpatialReference",
				"esri/units",
				//"dojo/_base/Color",	
				"dojo/_base/connect", 
				
				"dojo/parser", // scan page for widgets	
				"dojo/_base/array",
				"esri/Color",
				"dojo/query",
				"dojo/promise/all",
				"dojo/dom-class",
				"dojo/dom-construct",
				"dojo/domReady!"
      ], 
	  function (
				esriBundle, Map, Print, PrintTemplate, LegendLayer, esriRequest, esriConfig, Extent, HomeButton, ArcGISDynamicMapServiceLayer, FeatureLayer, TOC, SpatialReference, ImageParameters,
				webMercatorUtils, Search, OverviewMap, BasemapGallery, //BasemapToggle, 
				Scalebar, Bookmarks, cookie,
				InfoTemplate, IdentifyTask, IdentifyParameters, Popup, PopupTemplate,
				SimpleFillSymbol, SimpleLineSymbol,
				FloatingPane, TabContainer, ComboBox, Button, Dock,
				QueryTask, Query, ItemFileReadStore, ItemFileWriteStore, EnhancedGrid, pagination,
				SimpleMarkerSymbol,
				GeometryService, Draw, Graphic, BufferParameters, dom, on, normalizeUtils, SpatialReference, OnDemandGrid, Memory, TooltipDialog,
				Measurement, urlUtils, Directions, Point, LocateButton, LabelClass, TextSymbol, Growler, 
				PictureMarkerSymbol, PictureFillSymbol, CartographicLineSymbol, 
				RouteTask, RouteParameters, FeatureSet, Units, conn,
				parser, arrayUtils, Color, query, All, domClass, domConstruct
      ){

		parser.parse(); 

		// ---------------------- RENAME WIDGET'S DEFAULT NAME ---------------------
		esriBundle.widgets.Search.main.placeholder = "Search an address or location";
		esriBundle.widgets.homeButton.home.title = "Full Extent";
		//esriBundle.toolbars.draw.addPoint = "Add a new tree to the map";
		
		esri.bundle.toolbars.draw.addPoint = "Click on the map to draw a point";
		esri.bundle.toolbars.draw.addLine = "Click on the map to draw a line";
		esri.bundle.toolbars.draw.start = "Click to start drawing";
		esri.bundle.toolbars.draw.resume = "Click to continue to draw";
		esri.bundle.toolbars.draw.finish = "Click to continue drawing or double-click to finish";
		esri.bundle.toolbars.draw.complete = "Click to continue drawing or double-click to finish";

			
		// ---------------------- GLOBAL VARIABLES DECLARATION -------------------------------
		var identifyParams, identifyTask, tb;
		var center = [108.75,5.391]; // lon, lat
		
        var clickmap;
		var dynaLayer1, dynaLayer2, dynaLayer3, dynaLayer4;
		
		// ---------------------- INFO POPUP PROPS -------------------------------
        var popup = new Popup({ fillSymbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
											new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
											new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25]))
					}, domConstruct.create("div"));
		
 	
		// popup theme
        //dojo.addClass(popup.domNode, "modernGrey");
		 domClass.add(popup.domNode, "dark");
		 
		var latitude = phpVars["value1"];
		var longitude = phpVars["value2"];
		var zoomin = phpVars["value3"];
		var stateCode = phpVars["value4"];
		var userCategory = phpVars["value5"];
		var spatialReference = new SpatialReference({wkid:4742});
          
        loading = dom.byId("loadingImg");  //loading image. id  
		
		//create the growler
        var growler = new Growler({}, "growlerDijit");
        growler.startup();
		
		 //configure map animation to be faster
 //       esriConfig.defaults.map.panDuration = 1; // time in milliseconds, default panDuration: 350
 //       esriConfig.defaults.map.panRate = 1; // default panRate: 25
 //       esriConfig.defaults.map.zoomDuration = 100; // default zoomDuration: 500
 //       esriConfig.defaults.map.zoomRate = 1; // default zoomRate: 25

		 map = new Map("mapWrapper", { 
			center: [latitude, longitude],
			zoom: zoomin,
			basemap: "satellite",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false*/
			sliderStyle: "large",
			infoWindow: popup,
			showAttribution: false
		});
		
		 // Available growl params:
        // {
        //   title: "Title",
        //   message: "Message",
        //   level: "default", //can be: 'default', 'warning', 'info', 'error', 'success', if empty will be 'default'
        //   timeout: 10000, //set to 0 for no timeout
        //   opacity: 1.0,
        // }

  /*      growler.growl({
          title: "Warning!",
          message: "Best check yo self, you're not looking too good.",
          level: "warning"
        });
        growler.growl({
          title: "Oh snap!",
          message: "Change a few things up and try submitting again.",
          level: "error"
        });
        growler.growl({
          title: "Well done!",
          message: "You successfully read this important alert message.",
          level: "success"
        });*/
		
		on(dom.byId('oninfo'), 'click', function() {
          growler.growl({
            title: "Info",
            message: "Your info window has been deactivate.",
            level: "error",
            timeout: 10000,
          });
        });
		
		on(dom.byId('offinfo'), 'click', function() {
          growler.growl({
            title: "Info",
            message: "Your info window has been activate.",
            level: "success",
            timeout: 10000,
          });
        });
		
        growler.growl({
          title: "Info",
          message: "Your info window has been activate.",
          level: "success"
        });
 /*       growler.growl({
          title: "Default!",
          message: "Need higher contrast? Got you covered."
        }); */
		
		dojo.connect(map, "onLoad", function() {          
			// dojo.connect(map,"onMouseDragStart",onMouseDragStart);
			// dojo.connect(map,"onMouseDrag",onMouseDrag);
			// dojo.connect(map,"onMouseDragEnd",onMouseDragEnd);
			// dojo.connect(map, "onClick", runIdentifies); 
			tocRun(); // <-- put here to avoid register widget 2 times (during importing data)
		});
				

		// ---------------------- DEFINE SERVICES -------------------------------	
		//esriConfig.defaults.io.proxyUrl = "/proxy/";
		esriConfig.defaults.io.proxyUrl = "proxy/PHP/proxy.php";
        esriConfig.defaults.io.alwaysUseProxy = false;
		
		// SETUP PROXY for Buffer, Measure
	//	esriConfig.defaults.io.proxyUrl = 'dist/proxy-1.1.0/PHP/proxy.php';
	//	esriConfig.defaults.io.alwaysUseProxy = false;
//			esriConfig.defaults.io.corsDetection = false; // to get rid of HTTPS (share services in applications that utilize HTTPS)
		esriConfig.defaults.io.corsEnabledServers.push("https://www.gis.myetapp.gov.my/myetappgis");
		esriConfig.defaults.io.corsEnabledServers.push("https://www.gis.myetapp.gov.my");
		esriConfig.defaults.io.corsEnabledServers.push("https://1malaysiamap.mygeoportal.gov.my");
//			esriConfig.request.corsEnabledServers.push("www.gis.myetapp.gov.my");
//			esriConfig.request.corsEnabledServers.push("http://150.242.180.128");
	
		
		//Use the ImageParameters to set map service layer definitions and map service visible layers before adding to the client map.
          var imageParameters = new ImageParameters();

          //ImageParameters.layerDefinitions takes an array.  The index of the array corresponds to the layer id.
          //In the sample below an element is added in the array at 3, 4, and 5 indexes.
          //Those array elements correspond to the layer id within the remote ArcGISDynamicMapServiceLayer
          var layerDefs = [];
          if (userCategory == 'Staff') {
          	if (stateCode == '16' || stateCode == '15' || stateCode == '14') {
          		layerDefs[0] = "STATUS_TERKINI='2' and (KOD_NEGERI ='16' or KOD_NEGERI ='15' or KOD_NEGERI ='14')";
				layerDefs[1] = "STATUS_TERKINI='2' and (KOD_NEGERI ='16' or KOD_NEGERI ='15' or KOD_NEGERI ='14')";
				layerDefs[2] = "KOD_NEGERI ='16' or KOD_NEGERI ='15' or KOD_NEGERI ='14'";
				layerDefs[3] = "STATUS_TERKINI='2' and (KOD_NEGERI ='16' or KOD_NEGERI ='15' or KOD_NEGERI ='14')";
          	}else{ 
          		layerDefs[0] = "STATUS_TERKINI='2' and KOD_NEGERI ='"+stateCode+"'";
				layerDefs[1] = "STATUS_TERKINI='2' and KOD_NEGERI ='"+stateCode+"'";
				layerDefs[2] = "KOD_NEGERI ='"+stateCode+"'";
				layerDefs[3] = "STATUS_TERKINI='2' and KOD_NEGERI ='"+stateCode+"'";
	        }
			
          }else{
			layerDefs[0] = "STATUS_TERKINI='2'";
			layerDefs[1] = "STATUS_TERKINI='2'";
			layerDefs[2] = "STATUS_TERKINI='3'";
			layerDefs[3] = "STATUS_TERKINI='2'";
          }
          imageParameters.layerDefinitions = layerDefs;
		  
		  //I want layers 5,4, and 3 to be visible
          imageParameters.layerIds = [0, 1, 2, 3];
          imageParameters.layerOption = ImageParameters.LAYER_OPTION_SHOW;
          imageParameters.transparent = true;

          // LOT NDCDB

          //Use the ImageParameters to set map service layer definitions and map service visible layers before adding to the client map.
          var imageParametersLot = new ImageParameters();

          //ImageParameters.layerDefinitions takes an array.  The index of the array corresponds to the layer id.
          //In the sample below an element is added in the array at 3, 4, and 5 indexes.
          //Those array elements correspond to the layer id within the remote ArcGISDynamicMapServiceLayer
          var layerDefsLot = [];
          if (userCategory == 'Staff') {
			layerDefsLot[0] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[1] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[2] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[3] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[4] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[5] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[6] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[7] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[8] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[9] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[10] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[11] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[12] = "NEGERI ='"+stateCode+"'";
			layerDefsLot[13] = "NEGERI ='"+stateCode+"'";
			// layerDefs[1] = "STATUS_TERKINI='2' and KOD_NEGERI ='"+stateCode+"'";
			// layerDefs[2] = "KOD_NEGERI ='"+stateCode+"'";
			// layerDefs[3] = "STATUS_TERKINI='2' and KOD_NEGERI ='"+stateCode+"'";
          }else{
			layerDefsLot[0] = "";
			layerDefsLot[1] = "";
			layerDefsLot[2] = "";
			layerDefsLot[3] = "";
			layerDefsLot[4] = "";
			layerDefsLot[5] = "";
			layerDefsLot[6] = "";
			layerDefsLot[7] = "";
			layerDefsLot[8] = "";
			layerDefsLot[9] = "";
			layerDefsLot[10] = "";
			layerDefsLot[11] = "";
			layerDefsLot[12] = "";
			layerDefsLot[13] = "";
          }
          imageParametersLot.layerDefinitions = layerDefsLot;
		  
		  //I want layers 5,4, and 3 to be visible
          imageParametersLot.layerIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
          imageParametersLot.layerOption = ImageParameters.LAYER_OPTION_SHOW;
          imageParametersLot.transparent = true;
		
		
			  dynaLayer1 = new ArcGISDynamicMapServiceLayer("https://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/sani/1MyMap/MapServer", {
                opacity: 0.8
              });
			  
			 /* dynaLayer2 = new ArcGISDynamicMapServiceLayer("http://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer", {
                opacity: 0.8
              }); */
			  
			    //construct ArcGISDynamicMapServiceLayer with imageParameters from above
				dynaLayer2 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer",{
				id:"jkptg",
				opacity: 0.8,
				"imageParameters": imageParameters
				});

				jkptgLegend = new LegendLayer();
                jkptgLegend.layerId = "jkptg";
			  
			  dynaLayer3 = new ArcGISDynamicMapServiceLayer("https://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/jalan_jkr/Rangkaian_Jalan_JKR/MapServer", {
                opacity: 0.8
              });

                
			  
			  dynaLayer4 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb_legend/MapServer", {
                id:"ndcdb",
                opacity: 0.8,
                "imageParameters": imageParametersLot
              });

              ndcdbLegend = new LegendLayer();
                ndcdbLegend.layerId = "ndcdb";
			  
			  dynaLayer5 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer", {
                opacity: 0.8
              });
			  
			  dynaLayer6 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/sempadan/MapServer", {
                opacity: 0.8
              });

			  map.addLayers([dynaLayer6 ,dynaLayer4 , dynaLayer5 ,dynaLayer2, dynaLayer1, dynaLayer3]);


              function tocRun(evt){ // 
				console.log("TOC");
                // overwrite the default visibility of service.
                // TOC will honor the overwritten value.
				
			   
                //try {
                // 1. Charting +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				toc_dynaLayer2 = new TOC({
					map: map,
					layerInfos: [{
						layer: dynaLayer2,
						title: "Charting", 
						// collapsed: true,
						slider: true
					}]
				}, 'tocCharting'); 
				  
				toc_dynaLayer2.startup();
				console.log("start toc Charting Layer");
				
				toc_dynaLayer2.on('load', function(){
					if (console) 
						console.log('TOC Charting Layer loaded');				
				});
				  
				toc_dynaLayer2.on('toc-node-checked', function(evt){
					if (console) {
						console.log("TOCNodeChecked, rootLayer:"
						+(evt.rootLayer?evt.rootLayer.id:'NULL')
						+", serviceLayer:"+(evt.serviceLayer?evt.serviceLayer.id:'NULL')
						+ " Checked:"+evt.checked);
						if (evt.checked && evt.rootLayer && evt.serviceLayer){
							//evt.rootLayer.setVisibleLayers([evt.serviceLayer.id])
							//console.log(evt.serviceLayer);
						}
					}
				});

				// 2. 1 Malaysia Map +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				// toc_dynaLayer1 = new TOC({
				// 	map: map,
				// 	layerInfos: [{
				// 		layer: dynaLayer1,
				// 		title: "1 Malaysia Map", 
				// 		collapsed: true,
				// 		slider: false
				// 	}]
				// }, 'toc1MalaysiaMap'); 
				  
				// toc_dynaLayer1.startup();
				// console.log("start toc 1 Malaysia Map Layer");
				
				// toc_dynaLayer1.on('load', function(){
				// 	if (console) 
				// 		console.log('TOC 1 Malaysia Map Layer loaded');				
				// });
				  
				// toc_dynaLayer1.on('toc-node-checked', function(evt){
				// 	if (console) {
				// 		console.log("TOCNodeChecked, rootLayer:"
				// 		+(evt.rootLayer?evt.rootLayer.id:'NULL')
				// 		+", serviceLayer:"+(evt.serviceLayer?evt.serviceLayer.id:'NULL')
				// 		+ " Checked:"+evt.checked);
				// 		if (evt.checked && evt.rootLayer && evt.serviceLayer){
				// 			//evt.rootLayer.setVisibleLayers([evt.serviceLayer.id])
				// 			//console.log(evt.serviceLayer);
				// 		}
				// 	}
				// });

				// 3. JKR Road +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				// toc_dynaLayer3 = new TOC({
				// 	map: map,
				// 	layerInfos: [{
				// 		layer: dynaLayer3,
				// 		title: "JKR Road", 
				// 		collapsed: true,
				// 		slider: false
				// 	}]
				// }, 'tocJKRRoad'); 
				  
				// toc_dynaLayer3.startup();
				// console.log("start toc JKR Road Layer");
				
				// toc_dynaLayer3.on('load', function(){
				// 	if (console) 
				// 		console.log('TOC JKR Road Layer loaded');				
				// });
				  
				// toc_dynaLayer3.on('toc-node-checked', function(evt){
				// 	if (console) {
				// 		console.log("TOCNodeChecked, rootLayer:"
				// 		+(evt.rootLayer?evt.rootLayer.id:'NULL')
				// 		+", serviceLayer:"+(evt.serviceLayer?evt.serviceLayer.id:'NULL')
				// 		+ " Checked:"+evt.checked);
				// 		if (evt.checked && evt.rootLayer && evt.serviceLayer){
				// 			//evt.rootLayer.setVisibleLayers([evt.serviceLayer.id])
				// 			//console.log(evt.serviceLayer);
				// 		}
				// 	}
				// });

				// 4. Index Map +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				toc_dynaLayer5 = new TOC({
					map: map,
					layerInfos: [{
						layer: dynaLayer5,
						title: "Index Map", 
						collapsed: true,
						slider: false
					}]
				}, 'tocIndexMap'); 
				  
				toc_dynaLayer5.startup();
				console.log("start toc Index Map Layer");
				
				toc_dynaLayer5.on('load', function(){
					if (console) 
						console.log('TOC Index Map Layer loaded');				
				});
				  
				toc_dynaLayer5.on('toc-node-checked', function(evt){
					if (console) {
						console.log("TOCNodeChecked, rootLayer:"
						+(evt.rootLayer?evt.rootLayer.id:'NULL')
						+", serviceLayer:"+(evt.serviceLayer?evt.serviceLayer.id:'NULL')
						+ " Checked:"+evt.checked);
						if (evt.checked && evt.rootLayer && evt.serviceLayer){
							//evt.rootLayer.setVisibleLayers([evt.serviceLayer.id])
							//console.log(evt.serviceLayer);
						}
					}
				});

				// 5. Lot NDCDB +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				toc_dynaLayer4 = new TOC({
					map: map,
					layerInfos: [{
						layer: dynaLayer4,
						title: "Lot NDCDB", 
						collapsed: true,
						slider: false
					}]
				}, 'tocLotNDCDB'); 
				  
				toc_dynaLayer4.startup();
				console.log("start toc Lot NDCDB Layer");
				
				toc_dynaLayer4.on('load', function(){
					if (console) 
						console.log('TOC Lot NDCDB Layer loaded');				
				});
				  
				toc_dynaLayer4.on('toc-node-checked', function(evt){
					if (console) {
						console.log("TOCNodeChecked, rootLayer:"
						+(evt.rootLayer?evt.rootLayer.id:'NULL')
						+", serviceLayer:"+(evt.serviceLayer?evt.serviceLayer.id:'NULL')
						+ " Checked:"+evt.checked);
						if (evt.checked && evt.rootLayer && evt.serviceLayer){
							//evt.rootLayer.setVisibleLayers([evt.serviceLayer.id])
							//console.log(evt.serviceLayer);
						}
					}
				});

				// 5. Lot NDCDB +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				toc_dynaLayer6 = new TOC({
					map: map,
					layerInfos: [{
						layer: dynaLayer6,
						title: "Sempadan", 
						collapsed: true,
						slider: false
					}]
				}, 'tocSempadan'); 
				  
				toc_dynaLayer6.startup();
				console.log("start toc Sempadan Layer");
				
				toc_dynaLayer6.on('load', function(){
					if (console) 
						console.log('TOC Sempadan Layer loaded');				
				});
				  
				toc_dynaLayer6.on('toc-node-checked', function(evt){
					if (console) {
						console.log("TOCNodeChecked, rootLayer:"
						+(evt.rootLayer?evt.rootLayer.id:'NULL')
						+", serviceLayer:"+(evt.serviceLayer?evt.serviceLayer.id:'NULL')
						+ " Checked:"+evt.checked);
						if (evt.checked && evt.rootLayer && evt.serviceLayer){
							//evt.rootLayer.setVisibleLayers([evt.serviceLayer.id])
							//console.log(evt.serviceLayer);
						}
					}
				});
			             
			  // map.on('layers-add-result', function(evt){
                // overwrite the default visibility of service.
                // TOC will honor the overwritten value.
				
			   // SET LAYER VISIBILITY ================================================
				dynaLayer1.setVisibility(false); // to default unchecked
				dynaLayer3.setVisibility(false); // to default unchecked
				dynaLayer4.setVisibility(false); // to default unchecked
				dynaLayer5.setVisibility(false); // to default unchecked
				dynaLayer6.setVisibility(false); // to default unchecked

				dynaLayer1.setVisibleLayers([]);
				dynaLayer3.setVisibleLayers([]);
				dynaLayer2.setVisibleLayers([]);
				dynaLayer4.setVisibleLayers([]);
				dynaLayer5.setVisibleLayers([]);
				dynaLayer6.setVisibleLayers([]);
			}
                //try {
     //              toc = new TOC({
     //                map: map,
     //                layerInfos: [
					// /*{
     //                  layer: featLayer1,
     //                  title: "FeatureLayer1"
     //                },*/
					
					// {
     //                  layer: dynaLayer2,
     //                  title: "Charting",
     //                  //collapsed: false, // whether this root layer should be collapsed initially, default false.
     //                  slider: true // whether to display a transparency slider.
     //                },
					
					// {
     //                  layer: dynaLayer1,
     //                  title: "1 Malaysia Map"
     //                  //collapsed: false, // whether this root layer should be collapsed initially, default false.
     //                  //slider: false // whether to display a transparency slider.
     //                },
										
					// {
     //                  layer: dynaLayer3,
     //                  title: "JKR Road"
     //                  //collapsed: false, // whether this root layer should be collapsed initially, default false.
     //                  //slider: false // whether to display a transparency slider.
     //                },
					// {
     //                  layer: dynaLayer5,
     //                  title: "Index Map"
     //                  //collapsed: false, // whether this root layer should be collapsed initially, default false.
     //                  //slider: false // whether to display a transparency slider.
     //                },
					// {
     //                  layer: dynaLayer4,
     //                  title: "Lot NDCDB"
     //                  //collapsed: false, // whether this root layer should be collapsed initially, default false.
     //                  //slider: false // whether to display a transparency slider.
     //                },
					// {
     //                  layer: dynaLayer6,
     //                  title: "Sempadan"
     //                  //collapsed: false, // whether this root layer should be collapsed initially, default false.
     //                  //slider: false // whether to display a transparency slider.
     //                }	
					// ]
     //              }, 'tocDiv');
     //              toc.startup();
         
     //            //} catch (e) {  alert(e); }
     //          });


		// For Measure
		esriConfig.defaults.geometryService = new GeometryService("https://"+ipaddress+"/arcgis/rest/services/Utilities/Geometry/GeometryServer");
		var geometryService = new GeometryService("https://"+ipaddress+"/arcgis/rest/services/Utilities/Geometry/GeometryServer"); 
		
		// Create empty store for each search result's grid
		var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});

		// Search (Query Task) service
		search_pemohonan_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer/0");
		search_qt_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer/1");
		search_ft_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer/2");
		search_rezab_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer/3");
		
		// Search (Query Task) Lot NDCDB service
	
		// var johor_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG/ndcdb/MapServer/0");
		var johor_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/0");
		var kedah_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/1");
		var kelantan_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/2");
		var kl_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/11");
		var labuan_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/12");
		var melaka_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/3");
		var n9_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/4");
		var pahang_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/5");
		var perak_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/6");
		var perlis_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/7");
		var ppinang_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/8");
		var putrajaya_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/13");
		var selangor_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/9");
		var terengganu_ndcdb_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/ndcdb/MapServer/10");
		
		// Search (Query Task) index map service
	
		var johor_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/0");
		var kedah_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/1");
		var kelantan_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/2");		
		var melaka_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/3");
		var n9_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/4");
		var pahang_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/5");
		var perak_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/6");
		var perlis_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/7");
		var ppinang_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/8");
		var selangor_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/9");
		var terengganu_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/10");
		var kl_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/11");
		var putrajaya_indexmap_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer/12");
		

		negeri_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/sempadan/MapServer/2");
		daerah_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/sempadan/MapServer/1");
		mukim_layer = new QueryTask("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/sempadan/MapServer/0");



		// ---------------------- INITIALIZE MAP --------------------------------	
		/*var extentMalaysia = new Extent({
			xmin: 98.947096,
			ymin: 0.358901,
			xmax: 119.689284,
			ymax: 8.414430,
			spatialReference: {
				wkid: 4326
			}
		});*/
						
		/*map = new Map("mapWrapper", { 
			extent: extentMalaysia,
			basemap: "hybrid",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false
			infoWindow: popup,
			showAttribution: false,
			zoom: 6
		});*/
		
		/*map = new Map("mapWrapper", { 
			center: [107.988844, 4.550888],
			zoom: 6,
			basemap: "osm",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false
			infoWindow: popup,
			showAttribution: false
		});*/
		
		//"streets" | "satellite" | "hybrid" | "topo" | "gray" | "dark-gray" | "oceans"| "national-geographic" | "terrain" | "osm" | "dark-gray-vector" | "gray-vector" | "streets-vector" | "streets-night-vector" | "streets-relief-vector" | "streets-navigation-vector" | "topo-vector"
		
		//if there are url params zoom to location   
		var coords, zoomLevel; 
		var paparTitle;		
		var urlObject = esri.urlToObject(document.location.href);  
	
		// Infowindows
		map.infoWindow.resize(550, 250);			

		// Resize map
		map.on('resize', function (evt) {
			var pnt = evt.target.extent.getCenter();
			setTimeout(function () { evt.target.centerAt(pnt);}, 500);
		});
		
		// ---------------------- Activate HOME button ----------------------
		var home = new HomeButton({ map: map}, "HomeButton");      	
		home.title = "This is the new title"; 
		home.startup();	
		
		map.on("load", function() {
          //after map loads, connect to listen to mouse move & drag events
          map.on("mouse-move", showCoordinates);
          map.on("mouse-drag", showCoordinates);
	
        });
          
        // ---------------------- Show Loading ----------------------
        on(map, "update-start", showLoading);
		on(map, "update-end", hideLoading);
		
		 function showLoading() {
			esri.show(loading);
			map.disableMapNavigation();
			map.hideZoomSlider();
		  }

		 function hideLoading(error) {
			esri.hide(loading);
			map.enableMapNavigation();
			map.showZoomSlider();
		  }  
		

		// function connectInfo(){
		// 	console.log("connect info");
		// 	klikInfo = map.on("click", executeIdentifyTask);				
		// }

		// function disconnectInfo(){
		// 	console.log("disconnect info");	
		// 	dojo.disconnect(klikInfo);
		// 	klikInfo.remove();
		// }
		
		// ---------------------- ERASE button ----------------------
		on(dom.byId("EraseButton"), "click", function(){
			if(map){ 
				map.graphics.clear(); 
//				connectInfo();
				// stop measure
				measurement.setTool("area",false); 
				measurement.setTool("distance",false);
				measurement.setTool("location",false);
				measurement.clearResult(); 		
				clickmap.remove();

			}
        }); 
		
		// ---------------------- LOCATE/Geolocation button ----------------------
		geoLocate = new LocateButton({
			map: map
		}, "LocateButton");
		geoLocate.startup();

		// ---------------------- Show Coordinates ----------------------
        function showCoordinates(evt) {
			//the map is in web mercator but display coordinates in geographic (lat, long)
			var mp = webMercatorUtils.webMercatorToGeographic(evt.mapPoint);
			//display mouse coordinates
			dom.byId("coordinate_info").innerHTML = "X: "+ mp.x.toFixed(6) + ", Y: " + mp.y.toFixed(6);
        }
		
		// ---------------------- SCALEBAR --------------------------------
		var scalebar = new Scalebar({
			map: map,
			attachTo: 'bottom-left',
			scalebarStyle: 'line',
			scalebarUnit: 'dual'
		});

		// ------------------------ SEARCH MAP LOCATION TASK ----------------------------
		var search = new Search({
			map: map,   
		}, "searchBox");
	
		search.sources[0].countryCode = "MY";
		search.startup();
		
		// ------------------------------- OVERVIEW MAP-------- -----------------------
		var overviewMapDijit = new OverviewMap({
			map: map,
			attachTo: "bottom-right",
			/*color:"#fdf874",*/
			height : 200,
			width : 225,
			opacity: .40
        });
		
        overviewMapDijit.startup();
				
	/* =====================================================================================================
											WIDGETS: BASIC FUNCTIONS 													  
	======================================================================================================== */
				
		// ---------------------- BASEMAP TOGGLE ---------------------
		/*var toggle = new BasemapToggle({
			map: map,
			basemap: "osm"
		}, "BasemapToggle");
      
		toggle.startup();*/

		// ---------------------- LOCATOR COORDINATE ---------------------
		// Open Locator Pane
 		locator_button = dojo.byId('basic_locator');
		dojo.connect(locator_button, "onclick", open_locatorPane);
		function open_locatorPane() { dijit.byId('locatorPane').show(); }  
		
		// Connection so that the dijit button can read the function inside require.
		dijit.byId("search_coordinate").on("click",  locate_Coordinate);
		dijit.byId("clear_coordinate").on("click",  clear_Coordinate);		
		
		function locate_Coordinate(){
			console.log("got here 2")  
			var lat_value = document.getElementById("latitude").value  
			var long_value = document.getElementById("longitude").value 
			
			xmin =  98.947096;
			ymin =  0.358901;
			xmax = 119.689284;
			ymax = 8.414430;
			
			
			if(lat_value == "" || long_value == "")
				alert ("Please enter X and Y values (Malaysia's extent)");
			else {
				// check for values
				if ( (long_value >= xmin && long_value <= xmax ) && (lat_value >= ymin && lat_value <= ymax ) ) {
					var mp = new Point(long_value, lat_value);  
					var graphic = new esri.Graphic(mp, symbol_coordinates);  
					map.graphics.add(graphic);  
					map.centerAndZoom(mp, 18);
				}
				
				else {
					alert ("Please enter valid X and Y values");
				}
			}
		}	
		
		function clear_Coordinate() {			
			// Remove all graphics on the maps graphics layer
			map.graphics.clear();
						
			// Remove previous search results
			document.getElementById("latitude").value = "";
			document.getElementById("longitude").value = "";
			map.centerAndZoom(center, 5);
		}
		
		// ---------------------- DRAW ---------------------
		// ---------------------- PRINT ---------------------
		
	/* =====================================================================================================
											WIDGETS: ANALYSIS													  
	======================================================================================================== */	

	// ---------------------- BASEMAP ---------------------
		//add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
		var basemapGallery = new BasemapGallery({
			showArcGISBasemaps: true,
			map: map
		}, "basemapGallery");
		basemapGallery.startup();

		basemapGallery.on("error", function(msg) {
			console.log("basemap gallery error:  ", msg);
		});
				
		// Open Basemap Pane
 		basemap_button = dojo.byId('basemap');
		dojo.connect(basemap_button, "onclick", open_basemapPane);
		function open_basemapPane() { 
			dijit.byId('basemapGalleryPane').show(); 
		} 
		
        // Close Basemap Pane
		dojo.connect(dijit.byId('basemapGalleryPane'), "hide");
		
		// ---------------------- MEASURE ---------------------
		var measurement = new Measurement({
			map: map
		}, dom.byId("measurementDiv"));		
		measurement.startup();
				
		// Open Measure Pane
 		measure_button = dojo.byId('analysis_measure');
		dojo.connect(measure_button, "onclick", open_measurePane);
		function open_measurePane() { 
			dijit.byId('measurePane').show(); 
//			disconnectInfo(); 
//			dijit.byId('streetFormView').hide();
			map.graphics.clear();
			clickmap.remove();
		} 
		
		//Setup button click handlers -stop measure
        on(dom.byId("clearMeasure"), "click", function(){
			if(map){ 
				map.graphics.clear(); 
				measurement.setTool("area",false); 
				measurement.setTool("distance",false);
				measurement.setTool("location",false);
				measurement.clearResult(); 
			}
        }); 
		         
        //Stop function measure when panel measure hide
		dojo.connect(dijit.byId('measurePane'), "hide", exitMeasure);
		
		function exitMeasure() {   
			map.graphics.clear(); 
            measurement.setTool("area",false); 
            measurement.setTool("distance",false);
            measurement.setTool("location",false);
            measurement.clearResult();
//			connectInfo();
		}
		// ---------------------- CLOSE MEASURE -----------------------
		

		// ---------------------- STREET VIEW -------------------------
		
		// Open Search Form
 		street_view_button = dojo.byId('street_view');
		dojo.connect(street_view_button, "onclick", open_streetViewPane);
		function open_streetViewPane() {
			dijit.byId('streetFormView').show();
			clickmap = map.on("click", doStreetView);			
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
//			disconnectInfo(); 
//			dijit.byId('measurePane').hide();
//			map.graphics.clear(); 
			measurement.setTool("area",false); 
			measurement.setTool("distance",false);
			measurement.setTool("location",false);
			measurement.clearResult(); 
		}
		
		dojo.connect(dijit.byId('streetFormView'), "hide", exitStreetview);
		
		function exitStreetview() {
           // dijit.byId('streetFormView').hide();
			clickmap.remove();
            map.graphics.clear();
//			connectInfo();
		}

		symbol = new PictureMarkerSymbol("assets/img/flying_man.png", 32, 34);
        //set up street view
        Glocator = new google.maps.Geocoder();
        gviewClient = new google.maps.DirectionsService();
		sv = new google.maps.StreetViewService();
        Pano = new google.maps.StreetViewPanorama(document.getElementById("streetview"));
        google.maps.event.addListener(Pano, "error", handleNoFlash);
			
		function resizeMap() {
            //resize the map when the browser resizes - view the 'Resizing and repositioning the map' section in  
            //the following help topic for more details http://help.esri.com/EN/webapi/javascript/arcgis/help/jshelp_start.htm#jshelp/inside_faq.htm 
            var resizeTimer;
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                map.resize();
                map.reposition();
            }, 500);
        }

        function doStreetView(evt) {
            map.graphics.clear();           
            viewPoint = esri.geometry.webMercatorToGeographic(evt.mapPoint);
            sv.getPanoramaByLocation(new google.maps.LatLng(viewPoint.y, viewPoint.x), 100, showStreetView);

        }

        function handleNoFlash(errorCode) {
            if (errorCode == FLASH_UNAVAILABLE) {
                alert("Error: Flash doesn't appear to be supported by your browser");
                return;
            }
        }
		
		function showStreetView(data, status) {
			if(status == google.maps.StreetViewStatus.OK) {   //street view is available 
				map.infoWindow.hide(); 
				// alert("clicked");
                viewStatus = true;
                viewAddress = data.location.description;
                google.maps.event.addListener(Pano, 'initialized', streetViewChanged); // add an event handling street view changes
				var markerPanoID = data.location.pano;
      			// Set the Pano to use the passed panoID
      			Pano.setPano(markerPanoID);
                Pano.setPov(data.location.latlng, { heading:34, pitch:10 }); //push the pano to a this location
				Pano.setVisible(true);
                //add graphic
                var graphic = new esri.Graphic(esri.geometry.geographicToWebMercator(viewPoint), symbol);
                map.graphics.add(graphic);
            }
            else{ //no street view for this location
				alert("No street view for this location");
                //if (Pano != null) Pano.hide(); //hide the street view
                viewStatus = false;
                viewAddress = "";
            }
            var locationpoint = new google.maps.LatLng(viewPoint.y, viewPoint.x);
            Glocator.geocode({'latLng': locationpoint}, function(results, status) {
			var addr_type = results[0].types[0];	// type of address inputted that was geocoded
			if ( status == google.maps.GeocoderStatus.OK ) 
				showStreetAddress(locationpoint, results[0].formatted_address, addr_type );
			else     
				alert("Geocode was not successful for the following reason: " + status);        
				}); //show street address info
        }
		
        function showStreetAddress(latlng, address, addr_type) {
			// alert("masuk doh");
            var descContent = "";
            descContent = "<table width='95%'><tr><td><b>View Point Address:</b></td><td>" + address + "</td></tr><tr><td><b>View Point Lat/Lon:</b></td><td>"
            descContent += latlng + "</td></tr><tr><td>";
            descContent += "<b>Address type:</b></td><td>" + addr_type + "<td><tr></table>";
            document.getElementById("Desc").innerHTML = descContent;
        }

        function getlocalAddress(Addr) {
            var index = Addr.indexOf(',');
            return Addr.substring(0, index);
        }

        function streetViewChanged(streetLocation) {
            viewStatus = true;
            viewAddress = streetLocation.description;
            var point = new esri.geometry.Point({"x": streetLocation.latlng.lng(), "y": streetLocation.latlng.lat(), " spatialReference": { " wkid": 4326} });
            //add to the map
            map.graphics.clear();
            var symbolPoint = esri.geometry.geographicToWebMercator(point);
            map.graphics.add(new esri.Graphic(symbolPoint, symbol));
            map.centerAt(symbolPoint);
            Glocator.getLocations(streetLocation.latlng, showStreetAddress);
        }
		
			// ---------------------- CLOSE STREET VIEW -------------------------
          		
		// ===============================================================================
		// 									QUERY TASK 
		// ===============================================================================	
		// ---------------------- STYLE for searched features ----------------------
		// Create symbol for search features
		/*symbolSearch = new SimpleMarkerSymbol();
		symbolSearch.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
		symbolSearch.setSize(10);
		symbolSearch.setColor(new dojo.Color([255,255,0,0.5]));*/
		
		var symbol_coordinates = new SimpleMarkerSymbol(  
			SimpleMarkerSymbol.STYLE_CIRCLE, 15,  
			new SimpleLineSymbol( "solid", new Color([0, 0, 255, 0.5]), 8),  
			new Color([0, 0, 255])  
		);  
		
		//symbolSearch = new PictureMarkerSymbol("assets/img/map_pin.png", 16, 32);
		symbolSearch = new SimpleFillSymbol( "solid", new SimpleLineSymbol("solid", new Color([225,225,0]), 2), new Color([225,225,0,0.25]));
		
		polygonsymbol = new SimpleFillSymbol( "solid", new SimpleLineSymbol("solid", new Color([232,104,80]), 2), new Color([232,104,80,0.25]));

		function get_negeri_zoom(negeri){
			var query_negeri = new Query();
			query_negeri.returnGeometry = true;
			query_negeri.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni
			query_negeri.outSpatialReference = { "wkid": 102100 };
			query_negeri.where = "KOD_NEGERI = '"+ negeri +"'";
			var queryTask = negeri_layer; 
			queryTask.execute(query_negeri, zoom_negeri);
			function zoom_negeri(featureSet) {
				// Remove all graphics on the maps graphics layer
				map.graphics.clear();
				var resultFeatures = featureSet.features;
				//Loop through each feature returned
				dojo.forEach(featureSet.features, function (feature) {  
					var graphic = feature;  
					graphic.setSymbol(symbolSearch);  
					map.graphics.add(graphic);  
					// items_Lot.push(feature.attributes);						
				}); // close loop forEach
				var selectedId;
				// alert(clickedId);	    
				for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
					var currentGraphic = map.graphics.graphics[i];
					if ((currentGraphic.attributes) && currentGraphic.attributes.KOD_NEGERI == negeri) {
						selectedId = currentGraphic;
						break;
					}
				}
							   
				var feature = selectedId;
				console.log('selected kod= ' + negeri);	
				// Get extent then zoom in
				getFeatureExtent(feature);
				var resultExtent = esri.graphicsExtent(resultFeatures);
				map.setExtent(resultExtent);
			}
		}

		function get_daerah_zoom(negeri, daerah){
			var query_daerah = new Query();
			query_daerah.returnGeometry = true;
			query_daerah.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni
			query_daerah.outSpatialReference = { "wkid": 102100 };
			query_daerah.where = "KOD_NEGERI = '"+ negeri +"' AND KOD_DAERAH = '"+ daerah +"'";
			var queryTask = daerah_layer; 
			queryTask.execute(query_daerah, zoom_daerah);
			function zoom_daerah(featureSet) {
				// Remove all graphics on the maps graphics layer
				map.graphics.clear();
				var resultFeatures = featureSet.features;
				//Loop through each feature returned
				dojo.forEach(featureSet.features, function (feature) {  
					var graphic = feature;  
					graphic.setSymbol(symbolSearch);  
					map.graphics.add(graphic);  
					// items_Lot.push(feature.attributes);						
				}); // close loop forEach
				var selectedId;
				// alert(clickedId);	    
				for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
					var currentGraphic = map.graphics.graphics[i];
					if ((currentGraphic.attributes) && currentGraphic.attributes.KOD_NEGERI == negeri && currentGraphic.attributes.KOD_DAERAH == daerah) {
						selectedId = currentGraphic;
						break;
					}
				}
							   
				var feature = selectedId;	
				// Get extent then zoom in
				getFeatureExtent(feature);
				var resultExtent = esri.graphicsExtent(resultFeatures);
				map.setExtent(resultExtent);
			}
		}

		function get_mukim_zoom(negeri, daerah, mukim){
			var query_mukim = new Query();
			query_mukim.returnGeometry = true;
			query_mukim.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni
			query_mukim.outSpatialReference = { "wkid": 102100 };
			query_mukim.where = "KOD_NEGERI = '"+ negeri +"' AND KOD_DAERAH = '"+ daerah +"' AND KOD_MUKIM = '"+ mukim +"'";
			var queryTask = mukim_layer; 
			queryTask.execute(query_mukim, zoom_mukim);
			function zoom_mukim(featureSet) {
				// Remove all graphics on the maps graphics layer
				map.graphics.clear();
				var resultFeatures = featureSet.features;
				//Loop through each feature returned
				dojo.forEach(featureSet.features, function (feature) {  
					var graphic = feature;  
					graphic.setSymbol(symbolSearch);  
					map.graphics.add(graphic);  
					// items_Lot.push(feature.attributes);						
				}); // close loop forEach
				var selectedId;
				// alert(clickedId);	    
				for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
					var currentGraphic = map.graphics.graphics[i];
					if ((currentGraphic.attributes) && currentGraphic.attributes.KOD_NEGERI == negeri && currentGraphic.attributes.KOD_DAERAH == daerah && currentGraphic.attributes.KOD_MUKIM == mukim) {
						selectedId = currentGraphic;
						break;
					}
				}
							   
				var feature = selectedId;	
				// Get extent then zoom in
				getFeatureExtent(feature);
				var resultExtent = esri.graphicsExtent(resultFeatures);
				map.setExtent(resultExtent);
			}
		}
		  
		// ---------------------- 1. SEARCH: LOT ------------------------- //
		dijit.byId("select_state_lot").on("change",  function() {
			if (userCategory == 'Super Admin') {
				selectStateIdLot = storeStateLot.getValue(dijit.byId("select_state_lot").item, "id");
				populateList_District_setbyState_lot(selectStateIdLot);
			}else if (userCategory == 'Admin') {
				if(stateCode == '16'){
					selectStateIdLot = storeStateLot.getValue(dijit.byId("select_state_lot").item, "id");
					populateList_District_setbyState_lot(selectStateIdLot);
				}else{
					var check = dojo.byId('select_state_lot').value;
					if (check != 'Select state...') {
						if (check == 'Wilayah Persekutuan Kuala Lumpur') {
							selectStateIdLot = '14';
						}else if (check == 'Wilayah Persekutuan Labuan') {
							selectStateIdLot = '15';
						}else if (check == 'Wilayah Persekutuan Putrajaya') {
							selectStateIdLot = '16';
						}else{
							selectStateIdLot = stateCode;
						}
						populateList_District_setbyState_lot(selectStateIdLot);
					}
				}
			}else{
				var check = dojo.byId('select_state_lot').value;
				// alert(check);
				if (check != 'Select state...') {
					selectStateIdLot = stateCode;
					populateList_District_setbyState_lot(selectStateIdLot);
				}
			}
		});
		dijit.byId("select_district_lot").on("change",  function() {
			selectDistrictIdLot = storeDistrictLot.getValue(dijit.byId("select_district_lot").item, "id");
			populateList_Mukim_setbyDistrict_lot(selectStateIdLot,selectDistrictIdLot);
		});
		dijit.byId("select_mukim_lot").on("change",  function() {
			selectMukimIdLot = storeMukimLot.getValue(dijit.byId("select_mukim_lot").item, "id");
			populateList_Mukim_selected_lot(selectStateIdLot,selectDistrictIdLot,selectMukimIdLot);
		});
		// The SEARCH FORM
		// Open Search Form
 		search_lot_button = dojo.byId('search_lot');
		dojo.connect(search_lot_button, "onclick", function() { 
			populateList_State_setbyService_lot();
			dijit.byId('searchFormLot').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormPANDCDB').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();

			dojo.byId('select_state_lot').value = "Select state...";
			dojo.byId('select_district_lot').value = "Select district...";
			dojo.byId('select_mukim_lot').value = "Select sub district...";
			dojo.byId('seksyen_name').value = "";
			dojo.byId('lot_number').value = "";
		});

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_lot() {
			if (userCategory == 'Super Admin') { 
				document.getElementById('imageLoadingLot').innerHTML = '<img src="loading.gif" />'; 
				// D. Initialize Query Task
				var state_list_lot = new Query();
				state_list_lot.where = "OBJECTID <> ''";
				state_list_lot.returnGeometry = false;
				state_list_lot.outFields = ["*"];

				negeri_layer.execute(state_list_lot, populateList_state_lot);
			}else{
				if (userCategory == 'Admin') {
					if (stateCode == '16') {
						document.getElementById('imageLoadingLot').innerHTML = '<img src="loading.gif" />'; 
						// D. Initialize Query Task
						var state_list_lot = new Query();
						state_list_lot.where = "OBJECTID <> ''";
						state_list_lot.returnGeometry = false;
						state_list_lot.outFields = ["*"];

						negeri_layer.execute(state_list_lot, populateList_state_lot);
					}
				}
			}
		}		

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_lot(stateId) {
			document.getElementById('imageLoadingLot').innerHTML = '<img src="loading.gif" />'; 
			get_negeri_zoom(stateId);
			// alert(stateId);
			// D. Initialize Query Task
			var district_list_lot = new Query();
			district_list_lot.where = "KOD_NEGERI = '" + stateId + "'";
			district_list_lot.outFields = ["KOD_NEGERI","KOD_DAERAH", "NAM"];

			daerah_layer.execute(district_list_lot, populateList_district_byState_lot);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_lot(stateId,districtId) {
			var selectedState_lot = dojo.byId('select_state_lot').value;
			var selectedDistrict_lot = dojo.byId('select_district_lot').value;

			document.getElementById('imageLoadingLot').innerHTML = '<img src="loading.gif" />';
			get_daerah_zoom(stateId, districtId);
			
			var mukim_list_lot = new Query();
			mukim_list_lot.where = "KOD_NEGERI = '" + stateId + "' AND KOD_DAERAH = '" + districtId + "'";
			mukim_list_lot.returnGeometry = false;
			mukim_list_lot.outFields = ["NAM", "KOD_NEGERI", "KOD_DAERAH", "KOD_MUKIM"];

			mukim_layer.execute(mukim_list_lot, populateList_mukim_byStateDistrict_lot);
		}

		function populateList_Mukim_selected_lot(stateId,districtId,mukimId){
			get_mukim_zoom(stateId, districtId, mukimId);
		}
    		
		query_Lot = new Query();
		query_Lot.returnGeometry = true;
		query_Lot.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Lot = new EnhancedGrid({
			structure: gridLot_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 14,
					/*position of the pagination bar*/
					position: "bottom"
			  	}
			}
		}, 'gridLot');	
			
		gridResult_Lot.set("store",clearStore); 
		gridResult_Lot.startup();  

		var gridResult_Lot_QT = new EnhancedGrid({
			structure: gridLot_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 14,
					/*position of the pagination bar*/
					position: "bottom"
			  	}
			}
		}, 'gridLotQT');	
			
		gridResult_Lot_QT.set("store",clearStore); 
		gridResult_Lot_QT.startup();

		var gridResult_Lot_FT = new EnhancedGrid({
			structure: gridLot_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 14,
					/*position of the pagination bar*/
					position: "bottom"
			  	}
			}
		}, 'gridLotFT');	
			
		gridResult_Lot_FT.set("store",clearStore); 
		gridResult_Lot_FT.startup();  

		var gridResult_Lot_Rizab = new EnhancedGrid({
			structure: gridLot_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 14,
					/*position of the pagination bar*/
					position: "bottom"
			  	}
			}
		}, 'gridLotRizab');	
			
		gridResult_Lot_Rizab.set("store",clearStore); 
		gridResult_Lot_Rizab.startup();  

		// ---------------------- 2. SEARCH: Project ------------------------- //
		dijit.byId("select_state_project").on("change",  function() {
			if (userCategory == 'Super Admin') {
				selectStateIdProject = storeStateProject.getValue(dijit.byId("select_state_project").item, "id");
				populateList_District_setbyState_project(selectStateIdProject);
			}else if (userCategory == 'Admin') {
				if(stateCode == '16'){
					selectStateIdProject = storeStateProject.getValue(dijit.byId("select_state_project").item, "id");
					populateList_District_setbyState_project(selectStateIdProject);
				}else{
					var check = dojo.byId('select_state_project').value;
					if (check != 'Select state...') {
						if (check == 'Wilayah Persekutuan Kuala Lumpur') {
							selectStateIdProject = '14';
						}else if (check == 'Wilayah Persekutuan Labuan') {
							selectStateIdProject = '15';
						}else if (check == 'Wilayah Persekutuan Putrajaya') {
							selectStateIdProject = '16';
						}else{
							selectStateIdProject = stateCode;
						}
						populateList_District_setbyState_project(selectStateIdProject);
					}
				}
			}else{
				var check = dojo.byId('select_state_project').value;
				// alert(check);
				if (check != 'Select state...') {
					selectStateIdProject = stateCode;
					populateList_District_setbyState_project(selectStateIdProject);
				}
			}
		});
		dijit.byId("select_district_project").on("change",  function() {
			selectDistrictIdProject = storeDistrictProject.getValue(dijit.byId("select_district_project").item, "id");
			populateList_Mukim_setbyDistrict_project(selectStateIdProject,selectDistrictIdProject);
		});
		dijit.byId("select_mukim_project").on("change",  function() {
			selectMukimIdProject = storeMukimProject.getValue(dijit.byId("select_mukim_project").item, "id");
			populateList_Mukim_selected_project(selectStateIdProject,selectDistrictIdProject,selectMukimIdProject);
		});

		// The SEARCH FORM
		// Open Search Form
 		search_project_button = dojo.byId('search_project');
		dojo.connect(search_project_button, "onclick", function() {
			populateList_State_setbyService_project(); 
			dijit.byId('searchFormProject').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormPANDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();

			dojo.byId('select_state_project').value = "Select state...";
			dojo.byId('select_district_project').value = "Select district...";
			dojo.byId('select_mukim_project').value = "Select sub district...";
			dojo.byId('tanah').value = "";
		});

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_project() {
			if (userCategory == 'Super Admin') { 
				document.getElementById('imageLoadingProject').innerHTML = '<img src="loading.gif" />'; 
				// D. Initialize Query Task
				var state_list_project = new Query();
				state_list_project.where = "OBJECTID <> ''";
				state_list_project.returnGeometry = false;
				state_list_project.outFields = ["*"];

				negeri_layer.execute(state_list_project, populateList_state_project);
			}else{
				if (userCategory == 'Admin') {
					if (stateCode == '16') {
						document.getElementById('imageLoadingProject').innerHTML = '<img src="loading.gif" />'; 
						// D. Initialize Query Task
						var state_list_project = new Query();
						state_list_project.where = "OBJECTID <> ''";
						state_list_project.returnGeometry = false;
						state_list_project.outFields = ["*"];

						negeri_layer.execute(state_list_project, populateList_state_project);
					}
				}
			}
		}

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_project(stateId) {
			document.getElementById('imageLoadingProject').innerHTML = '<img src="loading.gif" />';
			get_negeri_zoom(stateId);

			var district_list_project = new Query();
			district_list_project.where = "KOD_NEGERI  = '" + stateId + "'";
			district_list_project.outFields = ["KOD_NEGERI","KOD_DAERAH", "NAM"];

			daerah_layer.execute(district_list_project, populateList_district_byState_project);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_project(stateId,districtId) { 
			document.getElementById('imageLoadingProject').innerHTML = '<img src="loading.gif" />';
			get_daerah_zoom(stateId, districtId);

			var mukim_list_project = new Query();
			mukim_list_project.where = "KOD_NEGERI = '" + stateId + "' AND KOD_DAERAH = '" + districtId + "'";
			mukim_list_project.returnGeometry = false;
			mukim_list_project.outFields = ["NAM", "KOD_NEGERI", "KOD_DAERAH", "KOD_MUKIM"];

			mukim_layer.execute(mukim_list_project, populateList_mukim_byStateDistrict_project);
		}

		function populateList_Mukim_selected_project(stateId,districtId,mukimId){
			get_mukim_zoom(stateId,districtId,mukimId);
		}
		
		// D. Initialize Query Task    		
		query_Project = new Query();
		query_Project.returnGeometry = true;
		query_Project.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		var gridResult_Project_Permohonan = new EnhancedGrid({
			structure: gridProject_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 4,
					/*position of the pagination bar*/
					position: "bottom"
			  }
			}
		}, 'gridProjectPermohonan');	
			
		gridResult_Project_Permohonan.set("store",clearStore); 
		gridResult_Project_Permohonan.startup();

		var gridResult_Project_QT = new EnhancedGrid({
			structure: gridProject_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 4,
					/*position of the pagination bar*/
					position: "bottom"
			  }
			}
		}, 'gridProjectQT');	
			
		gridResult_Project_QT.set("store",clearStore); 
		gridResult_Project_QT.startup(); 

		var gridResult_Project_FT = new EnhancedGrid({
			structure: gridProject_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 4,
					/*position of the pagination bar*/
					position: "bottom"
			  }
			}
		}, 'gridProjectFT');	
			
		gridResult_Project_FT.set("store",clearStore); 
		gridResult_Project_FT.startup(); 

		var gridResult_Project_Rizab = new EnhancedGrid({
			structure: gridProject_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 4,
					/*position of the pagination bar*/
					position: "bottom"
			  }
			}
		}, 'gridProjectRizab');
			
		gridResult_Project_Rizab.set("store",clearStore); 
		gridResult_Project_Rizab.startup(); 

		// ---------------------- 3. SEARCH: Hak Milik Sementara ----------------------- //
		dijit.byId("select_state_sementara").on("change",  function() {
			if (userCategory == 'Super Admin') {
				selectStateIdSementara = storeStateSementara.getValue(dijit.byId("select_state_sementara").item, "id");
				populateList_District_setbyState_sementara(selectStateIdSementara);
			}else if (userCategory == 'Admin') {
				if(stateCode == '16'){
					selectStateIdSementara = storeStateSementara.getValue(dijit.byId("select_state_sementara").item, "id");
					populateList_District_setbyState_sementara(selectStateIdSementara);
				}else{
					var check = dojo.byId('select_state_sementara').value;
					if (check != 'Select state...') {
						if (check == 'Wilayah Persekutuan Kuala Lumpur') {
							selectStateIdSementara = '14';
						}else if (check == 'Wilayah Persekutuan Labuan') {
							selectStateIdSementara = '15';
						}else if (check == 'Wilayah Persekutuan Putrajaya') {
							selectStateIdSementara = '16';
						}else{
							selectStateIdSementara = stateCode;
						}
						populateList_District_setbyState_sementara(selectStateIdSementara);
					}
				}
			}else{
				var check = dojo.byId('select_state_sementara').value;
				// alert(check);
				if (check != 'Select state...') {
					selectStateIdSementara = stateCode;
					populateList_District_setbyState_sementara(selectStateIdSementara);
				}
			}
		});
		dijit.byId("select_district_sementara").on("change",  function() {
			selectDistrictIdSementara = storeDistrictSementara.getValue(dijit.byId("select_district_sementara").item, "id");
			populateList_Mukim_setbyDistrict_sementara(selectStateIdSementara,selectDistrictIdSementara);
		});
		dijit.byId("select_mukim_sementara").on("change",  function() {
			selectMukimIdSementara = storeMukimSementara.getValue(dijit.byId("select_mukim_sementara").item, "id");
			populateList_Mukim_selected_sementara(selectStateIdSementara,selectDistrictIdSementara,selectMukimIdSementara);
		});
		// The SEARCH FORM
		// Open Search Form
 		search_sementara_button = dojo.byId('search_sementara');
		dojo.connect(search_sementara_button, "onclick", function() { 
			populateList_State_setbyService_sementara();
			dijit.byId('searchFormSementara').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormPANDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();

			dojo.byId('select_state_sementara').value = "Select state...";
			dojo.byId('select_district_sementara').value = "Select district...";
			dojo.byId('select_mukim_sementara').value = "Select sub district...";
			dojo.byId('sementara').value = "";	
		});

		// A. Initial STATE Dropdown query	
		// var state_list_sementara = new Query();
		// state_list_sementara.where = "NAMA_NEGERI <> ''";
		// state_list_sementara.returnGeometry = false;
		// state_list_sementara.outFields = ["NAMA_NEGERI"];
		// console.log("sini ok");
		// var state_ddown_sementara = search_qt_layer;
		// state_ddown_sementara.execute(state_list_sementara, populateList_state_sementara);

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_sementara() {
			if (userCategory == 'Super Admin') { 
				document.getElementById('imageLoadingSementara').innerHTML = '<img src="loading.gif" />'; 
				// D. Initialize Query Task
				var state_list_sementara = new Query();
				state_list_sementara.where = "OBJECTID <> ''";
				state_list_sementara.returnGeometry = false;
				state_list_sementara.outFields = ["*"];

				negeri_layer.execute(state_list_sementara, populateList_state_sementara);
			}else{
				if (userCategory == 'Admin') {
					if (stateCode == '16') {
						document.getElementById('imageLoadingSementara').innerHTML = '<img src="loading.gif" />'; 
						// D. Initialize Query Task
						var state_list_sementara = new Query();
						state_list_sementara.where = "OBJECTID <> ''";
						state_list_sementara.returnGeometry = false;
						state_list_sementara.outFields = ["*"];

						negeri_layer.execute(state_list_sementara, populateList_state_sementara);
					}
				}
			}
		}

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_sementara(stateId) {
			document.getElementById('imageLoadingSementara').innerHTML = '<img src="loading.gif" />';
			get_negeri_zoom(stateId);

			var district_list_sementara = new Query();
			district_list_sementara.where = "KOD_NEGERI  = '" + stateId + "'";
			district_list_sementara.outFields = ["KOD_NEGERI","KOD_DAERAH", "NAM"];

			daerah_layer.execute(district_list_sementara, populateList_district_byState_sementara);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_sementara(stateId,districtId) { 
			document.getElementById('imageLoadingSementara').innerHTML = '<img src="loading.gif" />';
			get_daerah_zoom(stateId, districtId);

			var mukim_list_sementara = new Query();
			mukim_list_sementara.where = "KOD_NEGERI = '" + stateId + "' AND KOD_DAERAH = '" + districtId + "'";
			mukim_list_sementara.returnGeometry = false;
			mukim_list_sementara.outFields = ["NAM", "KOD_NEGERI", "KOD_DAERAH", "KOD_MUKIM"];

			mukim_layer.execute(mukim_list_sementara, populateList_mukim_byStateDistrict_sementara);
		}

		function populateList_Mukim_selected_sementara(stateId,districtId,mukimId){
			get_mukim_zoom(stateId,districtId,mukimId);
		}
		
		// D. Initialize Query Task
		queryTask_Sementara =  search_qt_layer;     		
		query_Sementara = new Query();
		query_Sementara.returnGeometry = true;
		query_Sementara.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		var gridResult_Sementara_Permohonan = new EnhancedGrid({
			structure: gridSementara_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100","All"],
				  	description: true,
				  	sizeSwitch: true,
				  	pageStepper: true,
				  	gotoButton: true,
				  	/*page step to be displayed*/
				  	maxPageStep: 4,
				  	/*position of the pagination bar*/
				  	position: "bottom"
			  	}
			}
		}, 'gridSementaraPermohonan');	
			
		gridResult_Sementara_Permohonan.set("store",clearStore); 
		gridResult_Sementara_Permohonan.startup();

		var gridResult_Sementara_QT = new EnhancedGrid({
			structure: gridSementara_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100","All"],
				  	description: true,
				  	sizeSwitch: true,
				  	pageStepper: true,
				  	gotoButton: true,
				  	/*page step to be displayed*/
				  	maxPageStep: 4,
				  	/*position of the pagination bar*/
				  	position: "bottom"
			  	}
			}
		}, 'gridSementaraQT');	
			
		gridResult_Sementara_QT.set("store",clearStore); 
		gridResult_Sementara_QT.startup();

		var gridResult_Sementara_FT = new EnhancedGrid({
			structure: gridSementara_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100","All"],
				  	description: true,
				  	sizeSwitch: true,
				  	pageStepper: true,
				  	gotoButton: true,
				  	/*page step to be displayed*/
				  	maxPageStep: 4,
				  	/*position of the pagination bar*/
				  	position: "bottom"
			  	}
			}
		}, 'gridSementaraFT');	
			
		gridResult_Sementara_FT.set("store",clearStore); 
		gridResult_Sementara_FT.startup();

		var gridResult_Sementara_Rizab = new EnhancedGrid({
			structure: gridSementara_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100","All"],
				  	description: true,
				  	sizeSwitch: true,
				  	pageStepper: true,
				  	gotoButton: true,
				  	/*page step to be displayed*/
				  	maxPageStep: 4,
				  	/*position of the pagination bar*/
				  	position: "bottom"
			  	}
			}
		}, 'gridSementaraRizab');	
			
		gridResult_Sementara_Rizab.set("store",clearStore); 
		gridResult_Sementara_Rizab.startup();

// ---------------------- 4. SEARCH: Hak Milik ------------------------- //
		dijit.byId("select_state_milik").on("change",  function() {
			if (userCategory == 'Super Admin') {
				selectStateIdMilik = storeStateMilik.getValue(dijit.byId("select_state_milik").item, "id");
				populateList_District_setbyState_milik(selectStateIdMilik);
			}else if (userCategory == 'Admin') {
				if(stateCode == '16'){
					selectStateIdMilik = storeStateMilik.getValue(dijit.byId("select_state_milik").item, "id");
					populateList_District_setbyState_milik(selectStateIdMilik);
				}else{
					var check = dojo.byId('select_state_milik').value;
					if (check != 'Select state...') {
						if (check == 'Wilayah Persekutuan Kuala Lumpur') {
							selectStateIdMilik = '14';
						}else if (check == 'Wilayah Persekutuan Labuan') {
							selectStateIdMilik = '15';
						}else if (check == 'Wilayah Persekutuan Putrajaya') {
							selectStateIdMilik = '16';
						}else{
							selectStateIdMilik = stateCode;
						}
						populateList_District_setbyState_milik(selectStateIdMilik);
					}
				}
			}else{
				var check = dojo.byId('select_state_milik').value;
				// alert(check);
				if (check != 'Select state...') {
					selectStateIdMilik = stateCode;
					populateList_District_setbyState_milik(selectStateIdMilik);
				}
			}
		});
		dijit.byId("select_district_milik").on("change",  function() {
			selectDistrictIdMilik = storeDistrictMilik.getValue(dijit.byId("select_district_milik").item, "id");
			populateList_Mukim_setbyDistrict_milik(selectStateIdMilik,selectDistrictIdMilik);
		});
		dijit.byId("select_mukim_milik").on("change",  function() {
			selectMukimIdMilik = storeMukimMilik.getValue(dijit.byId("select_mukim_milik").item, "id");
			populateList_Mukim_selected_milik(selectStateIdMilik,selectDistrictIdMilik,selectMukimIdMilik);
		});
		// The SEARCH FORM
		// Open Search Form
 		search_milik_button = dojo.byId('search_milik');
		dojo.connect(search_milik_button, "onclick", function() {
			populateList_State_setbyService_milik(); 
			dijit.byId('searchFormMilik').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormPANDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();

			dojo.byId('select_state_milik').value = "Select state...";
			dojo.byId('select_district_milik').value = "Select district...";
			dojo.byId('select_mukim_milik').value = "Select sub district...";
			dojo.byId('milik').value = "";
					
		});

		// A. Initial STATE Dropdown query	

		// var state_list_milik = new Query();
		// state_list_milik.where = "NAMA_NEGERI <> ''";
		// state_list_milik.returnGeometry = false;
		// state_list_milik.outFields = ["NAMA_NEGERI"];
		// console.log("sini ok");
		// var state_ddown_milik = search_ft_layer;
		// state_ddown_milik.execute(state_list_milik, populateList_state_milik);

		function populateList_State_setbyService_milik() {
			if (userCategory == 'Super Admin') { 
				document.getElementById('imageLoadingMilik').innerHTML = '<img src="loading.gif" />'; 
				// D. Initialize Query Task
				var state_list_milik = new Query();
				state_list_milik.where = "OBJECTID <> ''";
				state_list_milik.returnGeometry = false;
				state_list_milik.outFields = ["*"];

				negeri_layer.execute(state_list_milik, populateList_state_milik);
			}else{
				if (userCategory == 'Admin') {
					if (stateCode == '16') {
						document.getElementById('imageLoadingMilik').innerHTML = '<img src="loading.gif" />'; 
						// D. Initialize Query Task
						var state_list_milik = new Query();
						state_list_milik.where = "OBJECTID <> ''";
						state_list_milik.returnGeometry = false;
						state_list_milik.outFields = ["*"];

						negeri_layer.execute(state_list_milik, populateList_state_milik);
					}
				}
			}
		}

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_milik(stateId) {
			document.getElementById('imageLoadingMilik').innerHTML = '<img src="loading.gif" />';
			get_negeri_zoom(stateId);

			var district_list_milik = new Query();
			district_list_milik.where = "KOD_NEGERI  = '" + stateId + "'";
			district_list_milik.outFields = ["KOD_NEGERI","KOD_DAERAH", "NAM"];

			daerah_layer.execute(district_list_milik, populateList_district_byState_milik);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_milik(stateId,districtId) { ;
			document.getElementById('imageLoadingMilik').innerHTML = '<img src="loading.gif" />';
			get_daerah_zoom(stateId, districtId); 
			
			var mukim_list_milik = new Query();
			mukim_list_milik.where = "KOD_NEGERI = '" + stateId + "' AND KOD_DAERAH = '" + districtId + "'";
			mukim_list_milik.returnGeometry = false;
			mukim_list_milik.outFields = ["NAM", "KOD_NEGERI", "KOD_DAERAH", "KOD_MUKIM"];

			mukim_layer.execute(mukim_list_milik, populateList_mukim_byStateDistrict_milik);
		}

		function populateList_Mukim_selected_milik(stateId,districtId,mukimId){
			get_mukim_zoom(stateId, districtId, mukimId);
		}
		
		// D. Initialize Query Task
		queryTask_Milik =  search_ft_layer;     		
		query_Milik = new Query();
		query_Milik.returnGeometry = true;
		query_Milik.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		var gridResult_Milik_Permohonan = new EnhancedGrid({
			structure: gridMilik_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 4,
					/*position of the pagination bar*/
					position: "bottom"
			  	}
			}
		}, 'gridMilikPermohonan');	
			
		gridResult_Milik_Permohonan.set("store",clearStore); 
		gridResult_Milik_Permohonan.startup();

		var gridResult_Milik_QT = new EnhancedGrid({
			structure: gridMilik_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 4,
					/*position of the pagination bar*/
					position: "bottom"
			  	}
			}
		}, 'gridMilikQT');	
			
		gridResult_Milik_QT.set("store",clearStore); 
		gridResult_Milik_QT.startup();

		var gridResult_Milik_FT = new EnhancedGrid({
			structure: gridMilik_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 4,
					/*position of the pagination bar*/
					position: "bottom"
			  	}
			}
		}, 'gridMilikFT');	
			
		gridResult_Milik_FT.set("store",clearStore); 
		gridResult_Milik_FT.startup();

		var gridResult_Milik_Rizab = new EnhancedGrid({
			structure: gridMilik_Structure, /* inside evacuationCentre.js */
			selectionMode: 'single',
			rowSelector: '20px',
			loadingMessage:'Loading results, please wait a moment.......',
			noDataMessage: "There are no items to display.",
			plugins: {
			  	pagination: {
					pageSizes: ["10", "25", "50", "100", "All"],
					description: true,
					sizeSwitch: true,
					pageStepper: true,
					gotoButton: true,
					/*page step to be displayed*/
					maxPageStep: 4,
					/*position of the pagination bar*/
					position: "bottom"
			  	}
			}
		}, 'gridMilikRizab');	
			
		gridResult_Milik_Rizab.set("store",clearStore); 
		gridResult_Milik_Rizab.startup();

		// ---------------------- 5. SEARCH: File Name -------------------------
		dijit.byId("select_service_file").on("change",  populateList_State_setbyService_file);
		// The SEARCH FORM
		// Open Search Form
 		search_file_button = dojo.byId('search_file');
		dojo.connect(search_file_button, "onclick", open_searchFileForm);
		function open_searchFileForm() { 
			dijit.byId('searchFormFile').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormPANDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormIndexmap').hide();

			dojo.byId('select_service_file').value = "Select service...";
			dojo.byId('select_state_file').value = "Select state...";
			dojo.byId('ktpk').value = "";

			var selectedService_file = dojo.byId('select_service_file').value;

			if (selectedService_file == 'Pemohonan') {
				queryTask_file =  search_pemohonan_layer;
				// alert('Pemohonan');
			}else{
				if (selectedService_file == 'FT') {
				 	queryTask_file =  search_ft_layer;
					// alert('FT');
				}else{
					if (selectedService_file == 'QT') {
						queryTask_file =  search_qt_layer;
						// alert('QT');
					}
					else{
						selectedService_file =  search_rezab_layer;
						// alert('Rezab');
					}
				} 	
			}
					
		}  
		var selectedService_file = dojo.byId('select_service_file').value;

		

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_file() { 
			var selectedService_file = dojo.byId('select_service_file').value;
			document.getElementById('imageLoadingFile').innerHTML = '<img src="loading.gif" />'; 

			var state_list_file = new Query();
			state_list_file.where = "NAMA_NEGERI <> ''";
			state_list_file.returnGeometry = false;
			state_list_file.outFields = ["NAMA_NEGERI"];
			console.log("sini ok");
			if (selectedService_file == 'Pemohonan') {
				var state_ddown_file = search_pemohonan_layer;
				queryTask_File =  search_pemohonan_layer;

			}else{
				if (selectedService_file == 'FT') {
					var state_ddown_file = search_ft_layer;
					queryTask_File =  search_ft_layer;
				}else{
					if (selectedService_file == 'QT') {
						var state_ddown_file = search_qt_layer;
						queryTask_File =  search_qt_layer;
					}
					else{
						var state_ddown_file =  search_rezab_layer;
						queryTask_File =  search_rezab_layer;
					}
				}
			}
			//var state_ddown_file = state_layer1;
			state_ddown_file.execute(state_list_file, populateList_state_file);

			// B. Initial DISTRICT Dropdown query 			
			/* var district_list = new Query();
			district_list.where = "DISTRICT >''";
			district_list.returnGeometry = false;
			district_list.outFields = ["DISTRICT"];
			
			district_ddown = mukim_layer;
			district_ddown.execute(district_list, populateList_district); */
		}

		
		
		// D. Initialize Query Task
		//queryTask_File =  search_project_layer;     		
		query_File = new Query();
		query_File.returnGeometry = true;
		query_File.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_File = new EnhancedGrid({
					structure: gridFile_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridFile');	
			
		gridResult_File.set("store",clearStore); 
		gridResult_File.startup();
		
		// ---------------------- 6. SEARCH: Lot NDCDB -------------------------

		dijit.byId("select_state_ndcdb").on("change",  function() {
			if (userCategory == 'Super Admin') {
				selectStateIdndcdb = storeStatendcdb.getValue(dijit.byId("select_state_ndcdb").item, "id");
				populateList_District_setbyState_ndcdb(selectStateIdndcdb);
			}else if (userCategory == 'Admin') {
				if(stateCode == '16'){
					selectStateIdndcdb = storeStatendcdb.getValue(dijit.byId("select_state_ndcdb").item, "id");
					populateList_District_setbyState_ndcdb(selectStateIdndcdb);
				}else{
					var check = dojo.byId('select_state_ndcdb').value;
					if (check != 'Select state...') {
						if (check == 'Wilayah Persekutuan Kuala Lumpur') {
							selectStateIdndcdb = '14';
						}else if (check == 'Wilayah Persekutuan Labuan') {
							selectStateIdndcdb = '15';
						}else if (check == 'Wilayah Persekutuan Putrajaya') {
							selectStateIdndcdb = '16';
						}else{
							selectStateIdndcdb = stateCode;
						}
						populateList_District_setbyState_ndcdb(selectStateIdndcdb);
					}
				}
			}else{
				var check = dojo.byId('select_state_ndcdb').value;
				// alert(check);
				if (check != 'Select state...') {
					selectStateIdndcdb = stateCode;
					populateList_District_setbyState_ndcdb(selectStateIdndcdb);
				}
			}
		});
		dijit.byId("select_district_ndcdb").on("change",  function() {
			selectDistrictIdndcdb = storeDistrictndcdb.getValue(dijit.byId("select_district_ndcdb").item, "id");
			populateList_Mukim_setbyDistrict_ndcdb(selectStateIdndcdb,selectDistrictIdndcdb);
		});
		dijit.byId("select_mukim_ndcdb").on("change",  function() {
			selectMukimIdndcdb = storeMukimndcdb.getValue(dijit.byId("select_mukim_ndcdb").item, "id");
			populateList_Mukim_selected_ndcdb(selectStateIdndcdb,selectDistrictIdndcdb,selectMukimIdndcdb);
		});
		// The SEARCH FORM
		// Open Search Form
 		search_ndcdb_button = dojo.byId('search_ndcdb');
		dojo.connect(search_ndcdb_button, "onclick", open_searchNDCDBForm);
		function open_searchNDCDBForm() { 
			populateList_State_setbyService_ndcdb();
			dijit.byId('searchFormNDCDB').show();
			dijit.byId('searchFormPANDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();

			dojo.byId('select_state_ndcdb').value = "Select state...";
			dojo.byId('select_district_ndcdb').value = "Select district...";
			dojo.byId('select_mukim_ndcdb').value = "Select sub district...";
			dojo.byId('ndcdb').value = "";
					
		}

		function populateList_State_setbyService_ndcdb() {
			if (userCategory == 'Super Admin') { 
				document.getElementById('imageLoadingNDCDB').innerHTML = '<img src="loading.gif" />'; 
				// D. Initialize Query Task
				var state_list_ndcdb = new Query();
				state_list_ndcdb.where = "OBJECTID <> ''";
				state_list_ndcdb.returnGeometry = false;
				state_list_ndcdb.outFields = ["*"];

				negeri_layer.execute(state_list_ndcdb, populateList_state_ndcdb);
			}else{
				if (userCategory == 'Admin') {
					if (stateCode == '16') {
						document.getElementById('imageLoadingNDCDB').innerHTML = '<img src="loading.gif" />'; 
						// D. Initialize Query Task
						var state_list_ndcdb = new Query();
						state_list_ndcdb.where = "OBJECTID <> ''";
						state_list_ndcdb.returnGeometry = false;
						state_list_ndcdb.outFields = ["*"];

						negeri_layer.execute(state_list_ndcdb, populateList_state_ndcdb);
					}
				}
			}
		}

		// A. Initial STATE Dropdown query	
		

		// B. Initial DISTRICT Dropdown query 			
		/* var district_list = new Query();
		district_list.where = "DISTRICT >''";
		district_list.returnGeometry = false;
		district_list.outFields = ["DISTRICT"];
			
		district_ddown = mukim_layer;
		district_ddown.execute(district_list, populateList_district); */

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_ndcdb(stateId) {
			document.getElementById('imageLoadingNDCDB').innerHTML = '<img src="loading.gif" />';
			get_negeri_zoom(stateId);

			var district_list_ndcdb = new Query();
			district_list_ndcdb.where = "KOD_NEGERI  = '" + stateId + "'";
			district_list_ndcdb.outFields = ["KOD_NEGERI","KOD_DAERAH", "NAM"];

			daerah_layer.execute(district_list_ndcdb, populateList_district_byState_ndcdb);

			// if (userCategory == 'Super Admin') {
			// 	var selectedState_ndcdb = storeStateLot.getValue(dijit.byId("select_state_ndcdb").item, "id");
			// }else{
			// 	var check = dojo.byId('select_state_ndcdb').value;
			// 	if (check != 'Select state...') {
			// 		var selectedState_ndcdb = stateCode;
			// 	}
			// }
			// document.getElementById('imageLoadingNDCDB').innerHTML = '<img src="loading.gif" />'; 
			// get_negeri_zoom(selectedState_ndcdb);
			// var district_list_ndcdb = new Query();
			// district_list_ndcdb.where = "KOD_NEGERI  = '" + selectedState_ndcdb + "'";
			// //district_list_milik.where = "Negeri  = 'Kedah'";
			// //district_list_milik.returnGeometry = false
			// district_list_ndcdb.outFields = ["NAMA_NEGERI", "NAMA_DAERAH"];
			
			// // var district_ddown_ndcdb = search_ft_layer;
			// if(selectedState_ndcdb == 'JOHOR'){
			// 	var district_ddown_ndcdb = johor_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'KEDAH'){
			// 	var district_ddown_ndcdb = kedah_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'KELANTAN'){
			// 	var district_ddown_ndcdb = kelantan_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'KUALA LUMPUR'){
			// 	var district_ddown_ndcdb = kl_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'LABUAN'){
			// 	var district_ddown_ndcdb = labuan_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'MELAKA'){
			// 	var district_ddown_ndcdb = melaka_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'NEGERI SEMBILAN'){
			// 	var district_ddown_ndcdb = n9_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PAHANG'){
			// 	var district_ddown_ndcdb = pahang_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PERAK'){
			// 	var district_ddown_ndcdb = perak_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PERLIS'){
			// 	var district_ddown_ndcdb = perlis_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PULAU PINANG'){
			// 	var district_ddown_ndcdb = ppinang_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PUTRAJAYA'){
			// 	var district_ddown_ndcdb = putrajaya_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'SELANGOR'){
			// 	var district_ddown_ndcdb = selangor_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'TERENGGANU'){
			// 	var district_ddown_ndcdb = terengganu_ndcdb_layer;

				
			// }
			// console.log(selectedState_ndcdb + "=" + district_ddown_ndcdb);
			// district_ddown_ndcdb.execute(district_list_ndcdb, populateList_district_byState_ndcdb);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_ndcdb(stateId,districtId) { ;
			document.getElementById('imageLoadingNDCDB').innerHTML = '<img src="loading.gif" />';
			get_daerah_zoom(stateId, districtId); 
			
			var mukim_list_ndcdb = new Query();
			mukim_list_ndcdb.where = "KOD_NEGERI = '" + stateId + "' AND KOD_DAERAH = '" + districtId + "'";
			mukim_list_ndcdb.returnGeometry = false;
			mukim_list_ndcdb.outFields = ["NAM", "KOD_NEGERI", "KOD_DAERAH", "KOD_MUKIM"];

			mukim_layer.execute(mukim_list_ndcdb, populateList_mukim_byStateDistrict_ndcdb);
		}

		// function populateList_Mukim_setbyDistrict_ndcdb() { 
		// 	var selectedState_ndcdb = dojo.byId('select_state_ndcdb').value;
		// 	var selectedDistrict_ndcdb = dojo.byId('select_district_ndcdb').value;
		// 	document.getElementById('imageLoadingNDCDB').innerHTML = '<img src="loading.gif" />';
		// 	get_daerah_zoom(selectedState_ndcdb, selectedDistrict_ndcdb);
			
		// 	var mukim_list_ndcdb = new Query();
		// 	mukim_list_ndcdb.where = "NAMA_NEGERI = '" + selectedState_ndcdb + "' AND NAMA_DAERAH = '" + selectedDistrict_ndcdb + "'";
		// 	mukim_list_ndcdb.returnGeometry = false;
		// 	mukim_list_ndcdb.outFields = ["NAMA_NEGERI", "NAMA_DAERAH", "NAMA_MUKIM"];
		// 	console.log(mukim_list_ndcdb.where);
			
		// 	// var mukim_ddown_ndcdb = search_ft_layer;
		// 	if(selectedState_ndcdb == 'JOHOR'){
		// 		var mukim_ddown_ndcdb = johor_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'KEDAH'){
		// 		var mukim_ddown_ndcdb = kedah_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'KELANTAN'){
		// 		var mukim_ddown_ndcdb = kelantan_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'KUALA LUMPUR'){
		// 		var mukim_ddown_ndcdb = kl_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'LABUAN'){
		// 		var mukim_ddown_ndcdb = labuan_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'MELAKA'){
		// 		var mukim_ddown_ndcdb = melaka_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'NEGERI SEMBILAN'){
		// 		var mukim_ddown_ndcdb = n9_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PAHANG'){
		// 		var mukim_ddown_ndcdb = pahang_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PERAK'){
		// 		var mukim_ddown_ndcdb = perak_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PERLIS'){
		// 		var mukim_ddown_ndcdb = perlis_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PULAU PINANG'){
		// 		var mukim_ddown_ndcdb = ppinang_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PUTRAJAYA'){
		// 		var mukim_ddown_ndcdb = putrajaya_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'SELANGOR'){
		// 		var mukim_ddown_ndcdb = selangor_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'TERENGGANU'){
		// 		var mukim_ddown_ndcdb = terengganu_ndcdb_layer;
		// 	}
		// 	mukim_ddown_ndcdb.execute(mukim_list_ndcdb, populateList_mukim_byStateDistrict_ndcdb);
		// }

		function populateList_Mukim_selected_ndcdb(stateId,districtId,mukimId){
			get_mukim_zoom(stateId, districtId, mukimId);
		}

		// function populateList_Mukim_selected_ndcdb(){
		// 	var selectedState_ndcdb = dojo.byId('select_state_ndcdb').value;
		// 	var selectedDistrict_ndcdb = dojo.byId('select_district_ndcdb').value;
		// 	var selectedMukim_ndcdb = dojo.byId('select_mukim_ndcdb').value;
			
		// 	get_mukim_zoom(selectedState_ndcdb, selectedDistrict_ndcdb, selectedMukim_ndcdb);
		// }
		
		// D. Initialize Query Task
		queryTask_NDCDB_johor =  johor_ndcdb_layer; 
		queryTask_NDCDB_kedah =  kedah_ndcdb_layer;
		queryTask_NDCDB_kelantan =  kelantan_ndcdb_layer; 
		queryTask_NDCDB_kl =  kl_ndcdb_layer; 
		queryTask_NDCDB_labuan =  labuan_ndcdb_layer; 
		queryTask_NDCDB_melaka =  melaka_ndcdb_layer; 
		queryTask_NDCDB_n9 =  n9_ndcdb_layer; 
		queryTask_NDCDB_pahang =  pahang_ndcdb_layer; 
		queryTask_NDCDB_perak =  perak_ndcdb_layer; 
		queryTask_NDCDB_perlis =  perlis_ndcdb_layer; 
		queryTask_NDCDB_ppinang =  ppinang_ndcdb_layer; 
		queryTask_NDCDB_putrajaya =  putrajaya_ndcdb_layer; 
		queryTask_NDCDB_selangor =  selangor_ndcdb_layer; 
		queryTask_NDCDB_terengganu =  terengganu_ndcdb_layer;

		query_NDCDB = new Query();
		query_NDCDB.returnGeometry = true;
		query_NDCDB.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_NDCDB = new EnhancedGrid({
					structure: gridNDCDB_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridNDCDB');	
			
		gridResult_NDCDB.set("store",clearStore); 
		gridResult_NDCDB.startup();


		// ---------------------- 7. SEARCH: PA NDCDB -------------------------

		dijit.byId("select_state_pandcdb").on("change",  function() {
			if (userCategory == 'Super Admin') {
				selectStateIdpandcdb = storeStatepandcdb.getValue(dijit.byId("select_state_pandcdb").item, "id");
				populateList_District_setbyState_pandcdb(selectStateIdpandcdb);
			}else if (userCategory == 'Admin') {
				if(stateCode == '16'){
					selectStateIdpandcdb = storeStatepandcdb.getValue(dijit.byId("select_state_pandcdb").item, "id");
					populateList_District_setbyState_pandcdb(selectStateIdpandcdb);
				}else{
					var check = dojo.byId('select_state_pandcdb').value;
					if (check != 'Select state...') {
						if (check == 'Wilayah Persekutuan Kuala Lumpur') {
							selectStateIdpandcdb = '14';
						}else if (check == 'Wilayah Persekutuan Labuan') {
							selectStateIdpandcdb = '15';
						}else if (check == 'Wilayah Persekutuan Putrajaya') {
							selectStateIdpandcdb = '16';
						}else{
							selectStateIdpandcdb = stateCode;
						}
						populateList_District_setbyState_pandcdb(selectStateIdpandcdb);
					}
				}
			}else{
				var check = dojo.byId('select_state_pandcdb').value;
				// alert(check);
				if (check != 'Select state...') {
					selectStateIdpandcdb = stateCode;
					populateList_District_setbyState_pandcdb(selectStateIdpandcdb);
				}
			}
		});
		dijit.byId("select_district_pandcdb").on("change",  function() {
			selectDistrictIdpandcdb = storeDistrictpandcdb.getValue(dijit.byId("select_district_pandcdb").item, "id");
			populateList_Mukim_setbyDistrict_pandcdb(selectStateIdpandcdb,selectDistrictIdpandcdb);
		});
		dijit.byId("select_mukim_pandcdb").on("change",  function() {
			selectMukimIdpandcdb = storeMukimpandcdb.getValue(dijit.byId("select_mukim_pandcdb").item, "id");
			populateList_Mukim_selected_pandcdb(selectStateIdpandcdb,selectDistrictIdpandcdb,selectMukimIdpandcdb);
		});
		// The SEARCH FORM
		// Open Search Form
 		search_pandcdb_button = dojo.byId('search_pandcdb');
		dojo.connect(search_pandcdb_button, "onclick", open_searchPANDCDBForm);
		function open_searchPANDCDBForm() { 
			populateList_State_setbyService_pandcdb();
			dijit.byId('searchFormPANDCDB').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();

			dojo.byId('select_state_pandcdb').value = "Select state...";
			dojo.byId('select_district_pandcdb').value = "Select district...";
			dojo.byId('select_mukim_pandcdb').value = "Select sub district...";
			dojo.byId('pandcdb').value = "";
					
		}

		function populateList_State_setbyService_pandcdb() {
			if (userCategory == 'Super Admin') { 
				document.getElementById('imageLoadingPANDCDB').innerHTML = '<img src="loading.gif" />'; 
				// D. Initialize Query Task
				var state_list_pandcdb = new Query();
				state_list_pandcdb.where = "OBJECTID <> ''";
				state_list_pandcdb.returnGeometry = false;
				state_list_pandcdb.outFields = ["*"];

				negeri_layer.execute(state_list_pandcdb, populateList_state_pandcdb);
			}else{
				if (userCategory == 'Admin') {
					if (stateCode == '16') {
						document.getElementById('imageLoadingPANDCDB').innerHTML = '<img src="loading.gif" />'; 
						// D. Initialize Query Task
						var state_list_pandcdb = new Query();
						state_list_pandcdb.where = "OBJECTID <> ''";
						state_list_pandcdb.returnGeometry = false;
						state_list_pandcdb.outFields = ["*"];

						negeri_layer.execute(state_list_pandcdb, populateList_state_pandcdb);
					}
				}
			}
		}

		// A. Initial STATE Dropdown query	
		

		// B. Initial DISTRICT Dropdown query 			
		/* var district_list = new Query();
		district_list.where = "DISTRICT >''";
		district_list.returnGeometry = false;
		district_list.outFields = ["DISTRICT"];
			
		district_ddown = mukim_layer;
		district_ddown.execute(district_list, populateList_district); */

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_pandcdb(stateId) {
			document.getElementById('imageLoadingPANDCDB').innerHTML = '<img src="loading.gif" />';
			get_negeri_zoom(stateId);

			var district_list_pandcdb = new Query();
			district_list_pandcdb.where = "KOD_NEGERI  = '" + stateId + "'";
			district_list_pandcdb.outFields = ["KOD_NEGERI","KOD_DAERAH", "NAM"];

			daerah_layer.execute(district_list_pandcdb, populateList_district_byState_pandcdb);

			// if (userCategory == 'Super Admin') {
			// 	var selectedState_ndcdb = storeStateLot.getValue(dijit.byId("select_state_ndcdb").item, "id");
			// }else{
			// 	var check = dojo.byId('select_state_ndcdb').value;
			// 	if (check != 'Select state...') {
			// 		var selectedState_ndcdb = stateCode;
			// 	}
			// }
			// document.getElementById('imageLoadingNDCDB').innerHTML = '<img src="loading.gif" />'; 
			// get_negeri_zoom(selectedState_ndcdb);
			// var district_list_ndcdb = new Query();
			// district_list_ndcdb.where = "KOD_NEGERI  = '" + selectedState_ndcdb + "'";
			// //district_list_milik.where = "Negeri  = 'Kedah'";
			// //district_list_milik.returnGeometry = false
			// district_list_ndcdb.outFields = ["NAMA_NEGERI", "NAMA_DAERAH"];
			
			// // var district_ddown_ndcdb = search_ft_layer;
			// if(selectedState_ndcdb == 'JOHOR'){
			// 	var district_ddown_ndcdb = johor_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'KEDAH'){
			// 	var district_ddown_ndcdb = kedah_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'KELANTAN'){
			// 	var district_ddown_ndcdb = kelantan_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'KUALA LUMPUR'){
			// 	var district_ddown_ndcdb = kl_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'LABUAN'){
			// 	var district_ddown_ndcdb = labuan_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'MELAKA'){
			// 	var district_ddown_ndcdb = melaka_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'NEGERI SEMBILAN'){
			// 	var district_ddown_ndcdb = n9_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PAHANG'){
			// 	var district_ddown_ndcdb = pahang_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PERAK'){
			// 	var district_ddown_ndcdb = perak_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PERLIS'){
			// 	var district_ddown_ndcdb = perlis_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PULAU PINANG'){
			// 	var district_ddown_ndcdb = ppinang_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'PUTRAJAYA'){
			// 	var district_ddown_ndcdb = putrajaya_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'SELANGOR'){
			// 	var district_ddown_ndcdb = selangor_ndcdb_layer;
			// }
			// if(selectedState_ndcdb == 'TERENGGANU'){
			// 	var district_ddown_ndcdb = terengganu_ndcdb_layer;

				
			// }
			// console.log(selectedState_ndcdb + "=" + district_ddown_ndcdb);
			// district_ddown_ndcdb.execute(district_list_ndcdb, populateList_district_byState_ndcdb);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_pandcdb(stateId,districtId) { ;
			document.getElementById('imageLoadingPANDCDB').innerHTML = '<img src="loading.gif" />';
			get_daerah_zoom(stateId, districtId); 
			
			var mukim_list_pandcdb = new Query();
			mukim_list_pandcdb.where = "KOD_NEGERI = '" + stateId + "' AND KOD_DAERAH = '" + districtId + "'";
			mukim_list_pandcdb.returnGeometry = false;
			mukim_list_pandcdb.outFields = ["NAM", "KOD_NEGERI", "KOD_DAERAH", "KOD_MUKIM"];

			mukim_layer.execute(mukim_list_pandcdb, populateList_mukim_byStateDistrict_pandcdb);
		}

		// function populateList_Mukim_setbyDistrict_ndcdb() { 
		// 	var selectedState_ndcdb = dojo.byId('select_state_ndcdb').value;
		// 	var selectedDistrict_ndcdb = dojo.byId('select_district_ndcdb').value;
		// 	document.getElementById('imageLoadingNDCDB').innerHTML = '<img src="loading.gif" />';
		// 	get_daerah_zoom(selectedState_ndcdb, selectedDistrict_ndcdb);
			
		// 	var mukim_list_ndcdb = new Query();
		// 	mukim_list_ndcdb.where = "NAMA_NEGERI = '" + selectedState_ndcdb + "' AND NAMA_DAERAH = '" + selectedDistrict_ndcdb + "'";
		// 	mukim_list_ndcdb.returnGeometry = false;
		// 	mukim_list_ndcdb.outFields = ["NAMA_NEGERI", "NAMA_DAERAH", "NAMA_MUKIM"];
		// 	console.log(mukim_list_ndcdb.where);
			
		// 	// var mukim_ddown_ndcdb = search_ft_layer;
		// 	if(selectedState_ndcdb == 'JOHOR'){
		// 		var mukim_ddown_ndcdb = johor_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'KEDAH'){
		// 		var mukim_ddown_ndcdb = kedah_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'KELANTAN'){
		// 		var mukim_ddown_ndcdb = kelantan_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'KUALA LUMPUR'){
		// 		var mukim_ddown_ndcdb = kl_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'LABUAN'){
		// 		var mukim_ddown_ndcdb = labuan_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'MELAKA'){
		// 		var mukim_ddown_ndcdb = melaka_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'NEGERI SEMBILAN'){
		// 		var mukim_ddown_ndcdb = n9_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PAHANG'){
		// 		var mukim_ddown_ndcdb = pahang_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PERAK'){
		// 		var mukim_ddown_ndcdb = perak_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PERLIS'){
		// 		var mukim_ddown_ndcdb = perlis_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PULAU PINANG'){
		// 		var mukim_ddown_ndcdb = ppinang_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'PUTRAJAYA'){
		// 		var mukim_ddown_ndcdb = putrajaya_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'SELANGOR'){
		// 		var mukim_ddown_ndcdb = selangor_ndcdb_layer;
		// 	}
		// 	if(selectedState_ndcdb == 'TERENGGANU'){
		// 		var mukim_ddown_ndcdb = terengganu_ndcdb_layer;
		// 	}
		// 	mukim_ddown_ndcdb.execute(mukim_list_ndcdb, populateList_mukim_byStateDistrict_ndcdb);
		// }

		function populateList_Mukim_selected_pandcdb(stateId,districtId,mukimId){
			get_mukim_zoom(stateId, districtId, mukimId);
		}

		// function populateList_Mukim_selected_ndcdb(){
		// 	var selectedState_ndcdb = dojo.byId('select_state_ndcdb').value;
		// 	var selectedDistrict_ndcdb = dojo.byId('select_district_ndcdb').value;
		// 	var selectedMukim_ndcdb = dojo.byId('select_mukim_ndcdb').value;
			
		// 	get_mukim_zoom(selectedState_ndcdb, selectedDistrict_ndcdb, selectedMukim_ndcdb);
		// }
		
		// D. Initialize Query Task
		queryTask_PANDCDB_johor =  johor_ndcdb_layer; 
		queryTask_PANDCDB_kedah =  kedah_ndcdb_layer;
		queryTask_PANDCDB_kelantan =  kelantan_ndcdb_layer; 
		queryTask_PANDCDB_kl =  kl_ndcdb_layer; 
		queryTask_PANDCDB_labuan =  labuan_ndcdb_layer; 
		queryTask_PANDCDB_melaka =  melaka_ndcdb_layer; 
		queryTask_PANDCDB_n9 =  n9_ndcdb_layer; 
		queryTask_PANDCDB_pahang =  pahang_ndcdb_layer; 
		queryTask_PANDCDB_perak =  perak_ndcdb_layer; 
		queryTask_PANDCDB_perlis =  perlis_ndcdb_layer; 
		queryTask_PANDCDB_ppinang =  ppinang_ndcdb_layer; 
		queryTask_PANDCDB_putrajaya =  putrajaya_ndcdb_layer; 
		queryTask_PANDCDB_selangor =  selangor_ndcdb_layer; 
		queryTask_PANDCDB_terengganu =  terengganu_ndcdb_layer;

		query_PANDCDB = new Query();
		query_PANDCDB.returnGeometry = true;
		query_PANDCDB.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_PANDCDB = new EnhancedGrid({
					structure: gridPANDCDB_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridPANDCDB');	
			
		gridResult_PANDCDB.set("store",clearStore); 
		gridResult_PANDCDB.startup();
		
		// ---------------------- 6. SEARCH: Index Map -------------------------

		// The SEARCH FORM
		// Open Search Form
 		search_indexmap_button = dojo.byId('search_indexmap');
		dojo.connect(search_indexmap_button, "onclick", open_searchIndexmapForm);
		function open_searchIndexmapForm() { 
			dijit.byId('searchFormIndexmap').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
		} 
		
		
		queryTask_Indexmap_johor =  johor_indexmap_layer; 
		queryTask_Indexmap_kedah =  kedah_indexmap_layer;
		queryTask_Indexmap_kelantan =  kelantan_indexmap_layer;  
		queryTask_Indexmap_melaka =  melaka_indexmap_layer; 
		queryTask_Indexmap_n9 =  n9_indexmap_layer; 
		queryTask_Indexmap_pahang =  pahang_indexmap_layer; 
		queryTask_Indexmap_perak =  perak_indexmap_layer; 
		queryTask_Indexmap_perlis =  perlis_indexmap_layer; 
		queryTask_Indexmap_ppinang =  ppinang_indexmap_layer; 
		queryTask_Indexmap_selangor =  selangor_indexmap_layer; 
		queryTask_Indexmap_terengganu =  terengganu_indexmap_layer; 
		queryTask_Indexmap_kl =  kl_indexmap_layer; 
		queryTask_Indexmap_putrajaya =  putrajaya_indexmap_layer; 
				

		query_Indexmap = new Query();
		query_Indexmap.returnGeometry = true;
		query_Indexmap.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create Indexmap empty grid
		  var gridResult_Indexmap = new EnhancedGrid({
					structure: gridIndexmap_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridIndexmap');	
			
		gridResult_Indexmap.set("store",clearStore); 
		gridResult_Indexmap.startup();	
		

		


        // ---------------------- Feedback -------------------------

        // Open Feedback 
 		feedback_button = dojo.byId('feedback');
		dojo.connect(feedback_button, "onclick", open_feedback);
		function open_feedback() { 
			
			var point = map.extent.getCenter();
			var currentZoom = map.getZoom();
  
            var newPoint = webMercatorUtils.webMercatorToGeographic(point);
            console.log("current map center is x: " + newPoint.x + ", y: " + newPoint.y + " Zoom: "+currentZoom);
			// alert(newPoint.x +', '+ newPoint.y+' zoom'+currentZoom);
			localStorage.setItem('latt',newPoint.x);
			localStorage.setItem('lonn',newPoint.y);
			localStorage.setItem('zoomm',currentZoom);

			// alert(localStorage.getItem('latt'));
			window.location.replace("https://"+ipaddress+"/myetappgis/Project-temp/index.php");
			// , { lat: newPoint.x, lon: newPoint.y, zoom: currentZoom};

		} 


        // ---------------------- Print map -------------------------
		       
			
			app.printUrl = "https://"+ipaddress+"/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";
			esriConfig.defaults.io.proxyUrl = "/proxy/";

	    /*      var mapOnlyIndex, templates;

	          var templateNames = ["A3 Landscape", "A3 Portrait", "A4 Landscape", "A4 Portrait", "Letter ANSI A Landscape", "Letter ANSI A Portrait", "Tabloid ANSI B Landscape", "Tabloid ANSI B Portrait", "MAP_ONLY"];
	          // remove the MAP_ONLY template then add it to the end of the list of templates 
	          mapOnlyIndex = arrayUtils.indexOf(templateNames, "MAP_ONLY");
	          if ( mapOnlyIndex > -1 ) {
	            var mapOnly = templateNames.splice(mapOnlyIndex, mapOnlyIndex + 1)[0];
	            templateNames.push(mapOnly);
	          }
	          // create a print template for each choice
	          templates = arrayUtils.map(templateNames, function(ch) {
	            var plate = new PrintTemplate();
	            plate.layout = plate.label = ch;
	            plate.format = "PDF";
	            plate.layoutOptions = { 
	              "authorText": "Made by:  JKPTG",
	              "copyrightText": "<copyright info here>",
	              "legendLayers": [], 
	              "titleText": paparTitle, 
	              "scalebarUnit": "Miles" 
	            };
	            return plate;
	          });

	          // create the print dijit
	          app.printer = new Print({
	            "map": map,
	            "templates": templates,
	            url: app.printUrl
	          }, dom.byId("printButton")); */
			  
	    // ---------------------- OPEN PRINT --------------------------
		
		// Open Print Pane
 		print_button = dojo.byId('print');
		dojo.connect(print_button, "onclick", open_printPane);
		function open_printPane() { 
			dijit.byId('printPane').show(); 
			
			//app.printer.startup();

		} 
		
		         
        // Panel print hide
		dojo.connect(dijit.byId('printPane'), "hide", exitPrint);
		
		function exitPrint() {   
		
		}
		


	  		
}); // CLOSE FUNCTIONS

		function enabledInfo(){
			//alert("ON");
			document.getElementById("oninfo").style.display = "block";
			document.getElementById("offinfo").style.display = "none";
			// klikInfo = map.on("click", executeIdentifyTask);				
		}

		function disabledInfo(){
			//alert("OFF");
			document.getElementById("oninfo").style.display = "none";
			document.getElementById("offinfo").style.display = "block";
			// dojo.disconnect(klikInfo);
			// klikInfo.remove();
		}
		
	  function myPDF ()
	  {	
//		alert(app.printer);
	  
        if (app.printer != null)
        {
		  app.printer.destroy(); 
        }    

		var PDFTitle = document.getElementById('pdfTitle').value;

		var mapOnlyIndex, templates;
	    var templateNames = ["A3 Landscape", "A3 Portrait", "A4 Landscape", "A4 Portrait", "Letter ANSI A Landscape", "Letter ANSI A Portrait", "Tabloid ANSI B Landscape", "Tabloid ANSI B Portrait", "MAP_ONLY"];
	          
        // create a print template for each choice
          templates = dojo.map(templateNames, function(ch) {          
          var plate = new esri.tasks.PrintTemplate();
//		  var legendLayer = new esri.tasks.LegendLayer();
//		  legendLayer.layerId = "dynamicLayer";		
//		  legendLayer.subLayerIds = [1,3,5,6,14,17,18,19,21,22];
		//alert(legendLayer);
          plate.layout = plate.label = ch;
          plate.format = "PDF";		  
          plate.layoutOptions = { 
            "authorText": "City of Fort Smith GIS",
            "copyrightText": "Copyright 2013, City of Fort Smith",
            "legendLayers": [jkptgLegend,ndcdbLegend], 
            "titleText": PDFTitle, 						
            "scalebarUnit": "Feet" 
          };		  
          return plate;
        });

        // create the print dijit
        app.printer = new esri.dijit.Print({
          "map": map,
          "templates": templates,
          url: app.printUrl
        }, dojo.byId("print_button"));			
        app.printer.startup();

		app.printer.on('print-start',function(){
			document.getElementById('imageLoadingPrint').innerHTML = '<img src="loading.gif" />';
			console.log('The print operation has started');					
		});
		
	   app.printer.on('print-complete',function(evt){
			document.getElementById('imageLoadingPrint').innerHTML = '';
			console.log('The url to the print image is : ' + evt.result.url);
		});
		
      }

      function handleError(err) {
        console.log("Something broke: ", err);
      }
	  ////printdijit