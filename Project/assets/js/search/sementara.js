
var prevfeature = null;
 
// Create Centre result grid
var gridSementara_Structure =[[
	// declare your structure here
	{name: 'No. Fail JKPTG', field: 'NO_JKPTG', width: '30%'},
	{name: 'Projek', field: 'GUNA_TANAH', width: '70%'},
	{name: 'Kementerian', field: 'KEMENTERIAN', width: '50%'},
	{name: 'Negeri', field: 'NAMA_NEGERI', width: '30%'},
	{name: 'Daerah', field: 'NAMA_DAERAH', width: '30%'},
	{name: 'Mukim', field: 'NAMA_MUKIM', width: '30%'},
	{name: 'Seksyen', field: 'NAMA_SEKSYEN', width: '30%'},
	{name: 'No. Hakmilik Sementara', field: 'HMLK_SMTARA', width: '32%'},
	{name: 'No. PT', field: 'NO_PT', width: '30%'}
]];

// Centre: REGION Dropdown 			
function populateList_state_sementara(results) {
	document.getElementById('imageLoadingSementara').innerHTML = '';

	var stateName, stateCode;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		stateName = feature.attributes.NAM;
		stateCode = feature.attributes.KOD_NEGERI;
		if (!testVals[stateName]) { 
			testVals[stateName] = true;
			values.push({name:stateName, id:stateCode});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "id", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	storeStateSementara = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_state_sementara").set('store', storeStateSementara);
}

// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_sementara(results) {
	document.getElementById('imageLoadingSementara').innerHTML = '';
	dijit.byId("select_district_sementara").reset(); 
	
	var districtName, districtCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		districtName = feature.attributes.NAM;
		districtCode = feature.attributes.KOD_DAERAH;
		if (!testVals[districtName]) {
			testVals[districtName] = true;
			values.push({name:districtName, id:districtCode});			
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	storeDistrictSementara = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_sementara").set('store', storeDistrictSementara);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_sementara(results) {
	document.getElementById('imageLoadingSementara').innerHTML = '';
	dijit.byId("select_mukim_sementara").reset(); 
	
	var mukimName, mukimCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukimName = feature.attributes.NAM;
		mukimCode = feature.attributes.KOD_MUKIM;
		if (!testVals[mukimName]) {
			testVals[mukimName] = true;
			values.push({name:mukimName, id:mukimCode});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	storeMukimSementara = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_sementara").set('store', storeMukimSementara);
}

// Execute Search
function executeQueryTask_Sementara() { console.log("execute task search Hak Milik Sementara");
	var state = dojo.byId('select_state_sementara').value;
	var district = dojo.byId('select_district_sementara').value;
	var mukim = dojo.byId('select_mukim_sementara').value;
	var sementara = dojo.byId('sementara').value;
		
	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(sementara);
	console.log("Negeri ID: " + selectStateIdSementara + ", Daerah ID: " + selectDistrictIdSementara + ", Mukim ID: " + selectMukimIdSementara);

	var kes;
		
	if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && sementara == "") kes = 1; // state
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && sementara == "") kes = 2; // state, district
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && sementara == "") kes = 3; // state, district, mukim
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && sementara != "") kes = 4; // state, district, mukim, sementara
	else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && sementara != "") kes = 5; // state, sementara
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && sementara != "") kes = 6; // state, district, sementara
	else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && sementara != "") kes = 7; // sementara
	else kes = 'default';
		
	console.log("kes = "+ kes);
		
	switch (kes){
			
		case 1:
			query_Sementara.where = "KOD_NEGERI = '" + selectStateIdSementara  + "' AND HMLK_SMTARA <> ''";
		break;
			
		case 2:
			query_Sementara.where = "KOD_NEGERI = '" + selectStateIdSementara  + "'  AND KOD_DAERAH = '" + selectDistrictIdSementara + "' AND HMLK_SMTARA <> ''";
		break;
			
		case 3:
			query_Sementara.where = "KOD_NEGERI = '" + selectStateIdSementara  + "'  AND KOD_DAERAH = '" + selectDistrictIdSementara + "' AND KOD_MUKIM = '" + selectMukimIdSementara + "' AND HMLK_SMTARA <> ''";
		break;

		case 4:
			query_Sementara.where = "KOD_NEGERI = '" + selectStateIdSementara  + "'  AND KOD_DAERAH = '" + selectDistrictIdSementara + "' AND KOD_MUKIM = '" + selectMukimIdSementara + "' AND HMLK_SMTARA LIKE '%" + sementara + "%'";
		break;

		case 5:
			query_Sementara.where = "KOD_NEGERI = '" + selectStateIdSementara  + "' AND HMLK_SMTARA LIKE '%" + sementara + "%'";
		break;

		case 6:
			query_Sementara.where = "KOD_NEGERI = '" + selectStateIdSementara  + "'  AND KOD_DAERAH = '" + selectDistrictIdSementara + "' AND HMLK_SMTARA LIKE '%" + sementara + "%'";
		break;

		case 7:
			query_Sementara.where = "HMLK_SMTARA LIKE '%" + sementara + "%'";
		break;
			
		default:
			query_Sementara.where = "HMLK_SMTARA <> ''";
		break;		
	}
		
	console.log(query_Sementara.where);

	// Execute query
	search_pemohonan_layer.execute(query_Sementara, showResults_Sementara_Permohonan);
	search_qt_layer.execute(query_Sementara, showResults_Sementara_QT);
	search_ft_layer.execute(query_Sementara, showResults_Sementara_FT);
	search_rezab_layer.execute(query_Sementara, showResults_Sementara_Rizab);

	showResultpane_Sementara();
} 

function showResultpane_Sementara() {  
	dijit.byId('searchResultSementara').show();
	dijit.byId('searchFormNDCDB').hide();
	dijit.byId('searchFormLot').hide();
	dijit.byId('searchFormProject').hide();
	dijit.byId('searchFormSementara').hide();
	dijit.byId('searchFormMilik').hide();
	dijit.byId('searchFormFile').hide();
	dijit.byId('searchFormIndexmap').hide();
}

// Show Search Result			
function showResults_Sementara_Permohonan(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Sementara_Permohonan = dijit.byId("gridSementaraPermohonan");				
	var items_Sementara_Permohonan = []; // all items to be stored in data store

	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Sementara_Permohonan.set("noDataMessage", 'Sorry, there are no results.');
		grid_Sementara_Permohonan.setStore(clearStore);				
	} else {								    			    
		prevfeature = null;
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;	
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicSementaraPermohonan = feature;  
			graphicSementaraPermohonan.setSymbol(symbolSearch);  
			map.graphics.add(graphicSementaraPermohonan);  
			items_Sementara_Permohonan.push(feature.attributes);  

			var info_sementara_permohonan = new esri.InfoTemplate();
			info_sementara_permohonan.setTitle("Carian Berdasarkan No Hak Milik Sementara");
			info_sementara_permohonan.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT}");

			graphicSementaraPermohonan.setInfoTemplate(info_sementara_permohonan); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gSementara = evt.graphic;
          map.infoWindow.setContent(gSementara.getContent());
          map.infoWindow.setTitle(gSementara.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
									
		//Create data object to be used in store
		var data_Sementara = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Sementara_Permohonan
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Sementara,
				clearOnClose: true
		});
												
		grid_Sementara_Permohonan.set("store", store);
		
		// Clear previous search results
		if(grid_Sementara_Permohonan.store.save) { grid_Sementara_Permohonan.store.save();}
		grid_Sementara_Permohonan.store.close();				
		grid_Sementara_Permohonan._refresh(); // or grid.store.fetch();								
		grid_Sementara_Permohonan.filter();
		
		grid_Sementara_Permohonan.on("rowClick", onRowClickHandler_grid_Sementara_Permohonan);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	}
}

// Show Search Result			
function showResults_Sementara_QT(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Sementara_QT = dijit.byId("gridSementaraQT");				
	var items_Sementara_QT = []; // all items to be stored in data store
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Sementara_QT.set("noDataMessage", 'Sorry, there are no results.');
		grid_Sementara_QT.setStore(clearStore);				
	} else {								    			    
		prevfeature = null;
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;	
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicSementaraQT = feature;  
			graphicSementaraQT.setSymbol(symbolSearch);  
			map.graphics.add(graphicSementaraQT);  
			items_Sementara_QT.push(feature.attributes);  

			var info_sementara_qt = new esri.InfoTemplate();
			info_sementara_qt.setTitle("Carian Berdasarkan No Hak Milik Sementara");
			info_sementara_qt.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT}");

			graphicSementaraQT.setInfoTemplate(info_sementara_qt); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gSementara = evt.graphic;
          map.infoWindow.setContent(gSementara.getContent());
          map.infoWindow.setTitle(gSementara.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
									
		//Create data object to be used in store
		var data_Sementara = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Sementara_QT
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Sementara,
				clearOnClose: true
		});
												
		grid_Sementara_QT.set("store", store);
		
		// Clear previous search results
		if(grid_Sementara_QT.store.save) { grid_Sementara_QT.store.save();}
		grid_Sementara_QT.store.close();				
		grid_Sementara_QT._refresh(); // or grid.store.fetch();								
		grid_Sementara_QT.filter();
		
		grid_Sementara_QT.on("rowClick", onRowClickHandler_grid_Sementara_QT);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	} 
}

// Show Search Result			
function showResults_Sementara_FT(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Sementara_FT = dijit.byId("gridSementaraFT");				
	var items_Sementara_FT = []; // all items to be stored in data store
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Sementara_FT.set("noDataMessage", 'Sorry, there are no results.');
		grid_Sementara_FT.setStore(clearStore);				
	} else {								    			    
		prevfeature = null;
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;	
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicSementaraFT = feature;  
			graphicSementaraFT.setSymbol(symbolSearch);  
			map.graphics.add(graphicSementaraFT);  
			items_Sementara_FT.push(feature.attributes);  

			var info_sementara_ft = new esri.InfoTemplate();
			info_sementara_ft.setTitle("Carian Berdasarkan No Hak Milik Sementara");
			info_sementara_ft.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT}");

			graphicSementaraFT.setInfoTemplate(info_sementara_ft); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gSementara = evt.graphic;
          map.infoWindow.setContent(gSementara.getContent());
          map.infoWindow.setTitle(gSementara.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
									
		//Create data object to be used in store
		var data_Sementara = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Sementara_FT
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Sementara,
				clearOnClose: true
		});
												
		grid_Sementara_FT.set("store", store);
		
		// Clear previous search results
		if(grid_Sementara_FT.store.save) { grid_Sementara_FT.store.save();}
		grid_Sementara_FT.store.close();				
		grid_Sementara_FT._refresh(); // or grid.store.fetch();								
		grid_Sementara_FT.filter();
		
		grid_Sementara_FT.on("rowClick", onRowClickHandler_grid_Sementara_FT);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	} 
}

// Show Search Result			
function showResults_Sementara_Rizab(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Sementara_Rizab = dijit.byId("gridSementaraRizab");				
	var items_Sementara_Rizab = []; // all items to be stored in data store
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Sementara_Rizab.set("noDataMessage", 'Sorry, there are no results.');
		grid_Sementara_Rizab.setStore(clearStore);				
	} else {								    			    
		prevfeature = null;
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;	
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicSementaraRizab = feature;  
			graphicSementaraRizab.setSymbol(symbolSearch);  
			map.graphics.add(graphicSementaraRizab);  
			items_Sementara_Rizab.push(feature.attributes);  

			var info_sementara_rizab = new esri.InfoTemplate();
			info_sementara_rizab.setTitle("Carian Berdasarkan No Hak Milik Sementara");
			info_sementara_rizab.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT}");

			graphicSementaraRizab.setInfoTemplate(info_sementara_rizab); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gSementara = evt.graphic;
          map.infoWindow.setContent(gSementara.getContent());
          map.infoWindow.setTitle(gSementara.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
									
		//Create data object to be used in store
		var data_Sementara = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Sementara_Rizab
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Sementara,
				clearOnClose: true
		});
												
		grid_Sementara_Rizab.set("store", store);
		
		// Clear previous search results
		if(grid_Sementara_Rizab.store.save) { grid_Sementara_Rizab.store.save();}
		grid_Sementara_Rizab.store.close();				
		grid_Sementara_Rizab._refresh(); // or grid.store.fetch();								
		grid_Sementara_Rizab.filter();
		
		grid_Sementara_Rizab.on("rowClick", onRowClickHandler_grid_Sementara_Rizab);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Sementara_Permohonan(evt){
	var grid = dijit.byId("gridSementaraPermohonan");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Sementara_QT(evt){
	var grid = dijit.byId("gridSementaraQT");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Sementara_FT(evt){
	var grid = dijit.byId("gridSementaraFT");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Sementara_Rizab(evt){
	var grid = dijit.byId("gridSementaraRizab");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_Sementara(){
	dojo.byId('select_state_sementara').value = "Select state...";
	dojo.byId('select_district_sementara').value = "Select district...";
	dojo.byId('select_mukim_sementara').value = "Select sub district...";
	dojo.byId('sementara').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridSementara");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridSementara"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultSementara').hide();
	
}