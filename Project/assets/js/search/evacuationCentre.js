/**
 * @author Fadhilah, Nov 2015
 * Search Evacuation Centre Props
 */
var prevfeature = null;
 
// Create Evacuation Centre result grid
var gridEvacuation_Structure =[[
	// declare your structure here
	{name: 'Center Name', field: 'nama', width: '20%'},
	{name: 'Type', field: 'jenis'},
	{name: 'Capacity', field: 'kapasiti'},	
	{name: 'Sub District', field: 'mukim'}
	/*{name: 'Daerah', field: 'daerah'},*/
]];

// Evacuation Centre: REGION Dropdown 			
function populateList_state_evacuation(results) {
	//console.log("populateList_state_evacuation");
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.STATE
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
   	dijit.byId("select_state_evacuation").set('store', store);
	//console.log(dijit.byId("select_state_evacuation"));
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */
	//console.log("finish");
}

// Evacuation Centre: Initial District Dropdown 
/*function populateList_district(results) {
	var district;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DISTRI;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Pilih Daerah...', id: '00' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_evacuation").set('store', store);
	
/* 		values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_district_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */
/*} */
		
// Evacuation Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_evacuation(results) {
	console.log("populateList_district_byState_evacuation");	
	dijit.byId("select_district_evacuation").reset(); 
	//dijit.byId("select_district_emergency").reset();  
	//document.getElementById('select_district_evacuation2').reset();
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DISTRICT;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_evacuation").set('store', store);
}

// Evacuation Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Evacuation Centre: Execute Search
function executeQueryTask_EvacuationCentre() { console.log("execute task");
	// Check if all values have been selected
	var state = dojo.byId('select_state_evacuation').value;
	var district = dojo.byId('select_district_evacuation').value;
	var evacuation_name = dojo.byId('evacuation_name').value;
	var evacuation_capacity = dojo.byId('evacuation_capacity').value;
	
	console.log(state);
	console.log(district);
	console.log(evacuation_name);
	console.log(evacuation_capacity);
	
	var kes;
	
	if(state == "Select state..." && district == "Select district..." && evacuation_name == "" && evacuation_capacity == "") kes = 1; // none
	else if(state != "Select state..." && district == "Select district..." && evacuation_name == "" && evacuation_capacity == "") kes = 2; // state
	else if(state != "Select state..." && district != "Select district..." && evacuation_name == "" && evacuation_capacity == "") kes = 3; // state & district
	else if(state != "Select state..." && district != "Select district..." && evacuation_name != "" && evacuation_capacity == "") kes = 4; // state, district, evacuation
	else if(state != "Select state..." && district != "Select district..." && evacuation_name == "" && evacuation_capacity != "") kes = 5; // state, district, capacity
	else if(state != "Select state..." && district == "Select district..." && evacuation_name != "" && evacuation_capacity == "") kes = 6; // state & evacuation
	else if(state != "Select state..." && district == "Select district..." && evacuation_name != "" && evacuation_capacity != "") kes = 7; // state, evacuation, capacity	
	else if(state != "Select state..." && district == "Select district..." && evacuation_name == "" && evacuation_capacity != "") kes = 8; // state & capacity
	else if(state != "Select state..." && district != "Select district..." && evacuation_name != "" && evacuation_capacity != "") kes = 9; // ALL
	else kes = 'default';
	
	console.log("kes = "+ kes);
		
	switch (kes){
		case 1:
			query_EvacuationCentre.where = "nama <> ''";
		break;
		
		case 2:
			query_EvacuationCentre.where = "negeri = '" + state  + "'";
		break;
		
		case 3:
			query_EvacuationCentre.where = "negeri = '" + state  + "' AND daerah = '" + district + "'";
		break;
		
		case 4:
			query_EvacuationCentre.where = "negeri = '" + state  + "' AND daerah = '" + district + "' AND nama LIKE '%" + evacuation_name + "%'";
		break;
		
		case 5:
			query_EvacuationCentre.where = "negeri = '" + state  + "' AND daerah = '" + district + "' AND kapasiti = '" + evacuation_capacity + "'";
		break;
		
		case 6:
			query_EvacuationCentre.where = "negeri = '" + state  + "' AND nama LIKE '%" + evacuation_name + "%'";
		break;
		
		case 7:
			query_EvacuationCentre.where = "negeri = '" + state  + "' AND nama LIKE '%" + evacuation_name + "%' AND kapasiti = '" + evacuation_capacity + "'";
		break;
		
		case 8:
			query_EvacuationCentre.where = "negeri = '" + state  + "' AND kapasiti = '%" + evacuation_capacity + "%'";
		break;
		
		case 9:
			query_EvacuationCentre.where = "negeri = '" + state  + "' AND daerah = '" + district + "' AND nama LIKE '%" + evacuation_name + "%' AND kapasiti = '" + evacuation_capacity + "'";
		break;
		
		default:
			query_EvacuationCentre.where = "nama <> ''";
		break;		
	}
	
	
/* 	if(state == "Pilih negeri..." || district == "Pilih daerah..." || evacuation_name == "")
		alert ("Sila pilih Negeri, Daerah dan masukkan Nama Pusat Pemindahan Sementara");
	else {
		// Set query based on what user typed in for population;
		query_EvacuationCentre.where = "DISTRICT = '" + district + "' AND NAME LIKE '%" + evacuation_name + "%'";
		console.log("DISTRICT = '" + district + "' AND NAME LIKE '%" + evacuation_name + "%'");
	
		// Execute query
		queryTask_EvacuationCentre.execute(query_EvacuationCentre,showResults_EvacuationCentre);
		showResultpane_EvacuationCentre();
	} */
	
	// Execute query
	queryTask_EvacuationCentre.execute(query_EvacuationCentre,showResults_EvacuationCentre);
	showResultpane_EvacuationCentre();
} 

function showResultpane_EvacuationCentre() {  
			dijit.byId('searchResultEvacuation').show();
			
			dijit.byId('searchFormEvacuation').hide(); 
			dijit.byId('searchFormEmergency').hide();
			dijit.byId('searchFormFacility').hide();
			dijit.byId('searchFormVictim').hide();
}

// Evacuation Centre: Show Search Result			
function showResults_EvacuationCentre(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_EvacuationCentre = dijit.byId("gridEvacuation");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_EvacuationCentre.set("noDataMessage", 'Sorry, there are no results.');
		grid_EvacuationCentre.setStore(clearStore); 					
	}
	
	else {								    			    
		prevfeature = null;
	
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_EvacuationCentre = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_EvacuationCentre.push(feature.attributes);  
		
			//Set the infoTemplate.
			var bil_mangsa_list = feature.attributes.bil_mangsa_daftar;
			var turun_senarai = '';
			if (bil_mangsa_list > 0) turun_senarai = "<a href='assets/flood_report/victim_list.xls' target='blank'>Download list</a>";
			else turun_senarai = "-Tiada-";
				
			var info_evacuation = new esri.InfoTemplate();
			info_evacuation.setTitle("Evacuation Centre");
			info_evacuation.setContent(
									"<table id='info'>" +
										"<tr><td><b>NAME</b></td><td><b>: </b>${nama}</td></tr>" +
										"<tr><td><b>CAPACITY</b></td><td><b>: </b>${kapasiti}</td></tr>" +
										"<tr><td><b>TYPE</b></td><td><b>: </b>${jenis}</td></tr>" +
										"<tr><td><b>SUB DISTRICT</b></td><td><b>: </b>${mukim}</td></tr>" +
										"<tr><td><b>CITY</b></td><td><b>: </b>${daerah}</td></tr>" +
										"<tr><td><b>NO. VICTIMS REGISTERED</b></td><td><b>: </b>"+bil_mangsa_list+"</td></tr>" +
										"<tr><td><b>LIST OF FLOOD VICTIMS</b></td><td><b>: </b>"+turun_senarai+"</td></tr>" +
										"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
										"<tr><td colspan='2' align='center'><img src='assets/img/Point Of Interest/${photos}' width='100%' height='50%'</td></tr>" +
									"</table>" 
			 ); 
			graphic.setInfoTemplate(info_evacuation); 
			//console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gEvacuation = evt.graphic;
          map.infoWindow.setContent(gEvacuation.getContent());
          map.infoWindow.setTitle(gEvacuation.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_EvacuationCentre = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_EvacuationCentre
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_EvacuationCentre,
				clearOnClose: true
		});
												
		grid_EvacuationCentre.set("store", store);
		
		// Clear previous search results
		if(grid_EvacuationCentre.store.save) { grid_EvacuationCentre.store.save();}
		grid_EvacuationCentre.store.close();				
		grid_EvacuationCentre._refresh(); // or grid.store.fetch();								
		grid_EvacuationCentre.filter();
		
		grid_EvacuationCentre.on("rowClick", onRowClickHandler_grid_EvacuationCentre);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_EvacuationCentre(evt){
	var grid = dijit.byId("gridEvacuation");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_EvacuationCentre(){
	dojo.byId('select_state_evacuation').value = "Select state...";
	dojo.byId('select_district_evacuation').value = "Select district...";
	dojo.byId('evacuation_name').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	clearQueryTask("gridEvacuation");
	dijit.byId('searchResultEvacuation').hide();
	
}