
var prevfeature = null;
 
// Create result grid
var gridProject_Structure =[[
	// declare your structure here
	{name: 'No. Fail JKPTG', field: 'NO_JKPTG', width: '30%'},
	{name: 'Projek', field: 'GUNA_TANAH', width: '70%'},
	{name: 'Kementerian', field: 'KEMENTERIAN', width: '50%'},
	{name: 'Negeri', field: 'NAMA_NEGERI', width: '30%'},
	{name: 'Daerah', field: 'NAMA_DAERAH', width: '30%'},
	{name: 'Mukim', field: 'NAMA_MUKIM', width: '30%'},
	{name: 'Seksyen', field: 'NAMA_SEKSYEN', width: '30%'},
	{name: 'No. Lot', field: 'NO_LOT'},
	{name: 'No. PA', field: 'NO_PA'}
]];

// REGION Dropdown 			
function populateList_state_project(results) {
	document.getElementById('imageLoadingProject').innerHTML = '';
	
	var stateName, stateCode;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		stateName = feature.attributes.NAM;
		stateCode = feature.attributes.KOD_NEGERI;
		if (!testVals[stateName]) { 
			testVals[stateName] = true;
			values.push({name:stateName, id:stateCode});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "id", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	storeStateProject = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_state_project").set('store', storeStateProject);
}

		
// DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_project(results) {
	document.getElementById('imageLoadingProject').innerHTML = '';
	dijit.byId("select_district_project").reset(); 
	
	var districtName, districtCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		districtName = feature.attributes.NAM;
		districtCode = feature.attributes.KOD_DAERAH;
		if (!testVals[districtName]) {
			testVals[districtName] = true;
			values.push({name:districtName, id:districtCode});			
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	storeDistrictProject = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_project").set('store', storeDistrictProject);
}

// MUKIM Dropdown by STATE & DISTRICT
function populateList_mukim_byStateDistrict_project(results) {
	document.getElementById('imageLoadingProject').innerHTML = '';
	dijit.byId("select_mukim_project").reset(); 
	
	var mukimName, mukimCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukimName = feature.attributes.NAM;
		mukimCode = feature.attributes.KOD_MUKIM;
		if (!testVals[mukimName]) {
			testVals[mukimName] = true;
			values.push({name:mukimName, id:mukimCode});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	storeMukimProject = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_project").set('store', storeMukimProject);
}

// Execute Search
function executeQueryTask_Project() { console.log("execute task search Project");
	var state = dojo.byId('select_state_project').value;
	var district = dojo.byId('select_district_project').value;
	var mukim = dojo.byId('select_mukim_project').value;
	var tanah = dojo.byId('tanah').value;
	
	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(tanah);
	console.log("Negeri ID: " + selectStateIdProject + ", Daerah ID: " + selectDistrictIdProject + ", Mukim ID: " + selectMukimIdProject);

	var kes;
	
	if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tanah == "") kes = 1; // state
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && tanah == "") kes = 2; // state, district
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && tanah == "") kes = 3; // state, district, mukim
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && tanah != "") kes = 4; // state, district, mukim, tanah
	else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tanah != "") kes = 5; // state, tanah
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && tanah != "") kes = 6; // state, district, tanah
	else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tanah != "") kes = 7; // tanah
	else kes = 'default';
	
	console.log("kes = "+ kes);
	
	switch (kes){
		case 1:
			query_Project.where = "KOD_NEGERI = '" + selectStateIdProject  + "' AND GUNA_TANAH <> ''";
		break;
		
		case 2:
			query_Project.where = "KOD_NEGERI = '" + selectStateIdProject  + "'  AND KOD_DAERAH = '" + selectDistrictIdProject + "'  AND GUNA_TANAH <> ''";
		break;
		
		case 3:
			query_Project.where = "KOD_NEGERI = '" + selectStateIdProject  + "'  AND KOD_DAERAH = '" + selectDistrictIdProject + "' AND KOD_MUKIM = '" + selectMukimIdProject + "'  AND GUNA_TANAH <> ''";
		break;

		case 4:
			query_Project.where = "KOD_NEGERI = '" + selectStateIdProject  + "'  AND KOD_DAERAH = '" + selectDistrictIdProject + "' AND KOD_MUKIM = '" + selectMukimIdProject + "' AND GUNA_TANAH LIKE '%" + tanah + "%'";
		break;

		case 5:
			query_Project.where = "KOD_NEGERI = '" + selectStateIdProject  + "' AND GUNA_TANAH LIKE '%" + tanah + "%'";
		break;

		case 6:
			query_Project.where = "KOD_NEGERI = '" + selectStateIdProject  + "'  AND KOD_DAERAH = '" + selectDistrictIdProject + "' AND GUNA_TANAH LIKE '%" + tanah + "%'";
		break;

		case 7:
			query_Project.where = "GUNA_TANAH LIKE '%" + tanah + "%'";
		break;
		
		default:
			query_Project.where = "GUNA_TANAH <> ''";
		break;		
	}
	
	console.log(query_Project.where);

	// Execute query
	search_pemohonan_layer.execute(query_Project, showResults_Project_Permohonan);
	search_qt_layer.execute(query_Project, showResults_Project_QT);
	search_ft_layer.execute(query_Project, showResults_Project_FT);
	search_rezab_layer.execute(query_Project, showResults_Project_Rizab);
	showResultpane_Project();
} 

function showResultpane_Project() {  
			dijit.byId('searchResultProject').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();
}

// Project: Show Search Result			
function showResults_Project_Permohonan(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Project_Permohonan = dijit.byId("gridProjectPermohonan");
	var items_Project_Permohonan = []; // Create items array to be added to store's data
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Project_Permohonan.set("noDataMessage", 'Sorry, there are no results.');
		grid_Project_Permohonan.setStore(clearStore); 				
	}
	
	else {								    			    
		prevfeature = null;

		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicProjectPermohonan = feature;  
			graphicProjectPermohonan.setSymbol(symbolSearch);  
			map.graphics.add(graphicProjectPermohonan);  
			items_Project_Permohonan.push(feature.attributes);  
		
			//Set the infoTemplate.	
			var info_project_permohonan = new esri.InfoTemplate();
			info_project_permohonan.setTitle("Carian Nama Projek Berstatus Pemohonan");
			info_project_permohonan.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS}  <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN}");

			graphicProjectPermohonan.setInfoTemplate(info_project_permohonan); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          	var gProject = evt.graphic;
          	map.infoWindow.setContent(gProject.getContent());
          	map.infoWindow.setTitle(gProject.getTitle());
          	map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
											
		//Create data object to be used in store
		var data_Project = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Project_Permohonan
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Project,
				clearOnClose: true
		});
												
		grid_Project_Permohonan.set("store", store);
		
		// Clear previous search results
		if(grid_Project_Permohonan.store.save) { grid_Project_Permohonan.store.save();}
		grid_Project_Permohonan.store.close();				
		grid_Project_Permohonan._refresh(); // or grid.store.fetch();								
		grid_Project_Permohonan.filter();
		
		grid_Project_Permohonan.on("rowClick", onRowClickHandler_grid_Project_Permohonan);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

// Project: Show Search Result			
function showResults_Project_QT(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Project_QT = dijit.byId("gridProjectQT");
	var items_Project_QT = []; // Create items array to be added to store's data
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Project_QT.set("noDataMessage", 'Sorry, there are no results.');
		grid_Project_QT.setStore(clearStore); 				
	}
	
	else {								    			    
		prevfeature = null;

		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicProjectQT = feature;  
			graphicProjectQT.setSymbol(symbolSearch);  
			map.graphics.add(graphicProjectQT);  
			items_Project_QT.push(feature.attributes);  
		
			//Set the infoTemplate.	
			var info_project_qt = new esri.InfoTemplate();
			info_project_qt.setTitle("Carian Nama Projek Berstatus QT");
			info_project_qt.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS}  <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN}");

			graphicProjectQT.setInfoTemplate(info_project_qt); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          	var gProject = evt.graphic;
          	map.infoWindow.setContent(gProject.getContent());
          	map.infoWindow.setTitle(gProject.getTitle());
          	map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
											
		//Create data object to be used in store
		var data_Project = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Project_QT
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Project,
				clearOnClose: true
		});
												
		grid_Project_QT.set("store", store);
		
		// Clear previous search results
		if(grid_Project_QT.store.save) { grid_Project_QT.store.save();}
		grid_Project_QT.store.close();				
		grid_Project_QT._refresh(); // or grid.store.fetch();								
		grid_Project_QT.filter();
		
		grid_Project_QT.on("rowClick", onRowClickHandler_grid_Project_QT);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

// Project: Show Search Result			
function showResults_Project_FT(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Project_FT = dijit.byId("gridProjectFT");
	var items_Project_FT = []; // Create items array to be added to store's data
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Project_FT.set("noDataMessage", 'Sorry, there are no results.');
		grid_Project_FT.setStore(clearStore); 				
	}
	
	else {								    			    
		prevfeature = null;

		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicProjectFT = feature;  
			graphicProjectFT.setSymbol(symbolSearch);  
			map.graphics.add(graphicProjectFT);  
			items_Project_FT.push(feature.attributes);  
		
			//Set the infoTemplate.	
			var info_project_ft = new esri.InfoTemplate();
			info_project_ft.setTitle("Carian Nama Projek Berstatus FT");
			info_project_ft.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS}  <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN}");

			graphicProjectFT.setInfoTemplate(info_project_ft); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          	var gProject = evt.graphic;
          	map.infoWindow.setContent(gProject.getContent());
          	map.infoWindow.setTitle(gProject.getTitle());
          	map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
											
		//Create data object to be used in store
		var data_Project = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Project_FT
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Project,
				clearOnClose: true
		});
												
		grid_Project_FT.set("store", store);
		
		// Clear previous search results
		if(grid_Project_FT.store.save) { grid_Project_FT.store.save();}
		grid_Project_FT.store.close();				
		grid_Project_FT._refresh(); // or grid.store.fetch();								
		grid_Project_FT.filter();
		
		grid_Project_FT.on("rowClick", onRowClickHandler_grid_Project_FT);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

// Project: Show Search Result			
function showResults_Project_Rizab(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Project_Rizab = dijit.byId("gridProjectRizab");
	var items_Project_Rizab = []; // Create items array to be added to store's data
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Project_Rizab.set("noDataMessage", 'Sorry, there are no results.');
		grid_Project_Rizab.setStore(clearStore); 				
	}
	
	else {								    			    
		prevfeature = null;

		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicProjectRizab = feature;  
			graphicProjectRizab.setSymbol(symbolSearch);  
			map.graphics.add(graphicProjectRizab);  
			items_Project_Rizab.push(feature.attributes);  
		
			//Set the infoTemplate.	
			var info_project_rizab = new esri.InfoTemplate();
			info_project_rizab.setTitle("Carian Nama Projek Berstatus Pemohonan");
			info_project_rizab.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS}  <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN}");

			graphicProjectRizab.setInfoTemplate(info_project_rizab); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          	var gProject = evt.graphic;
          	map.infoWindow.setContent(gProject.getContent());
          	map.infoWindow.setTitle(gProject.getTitle());
          	map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
											
		//Create data object to be used in store
		var data_Project = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Project_Rizab
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Project,
				clearOnClose: true
		});
												
		grid_Project_Rizab.set("store", store);
		
		// Clear previous search results
		if(grid_Project_Rizab.store.save) { grid_Project_Rizab.store.save();}
		grid_Project_Rizab.store.close();				
		grid_Project_Rizab._refresh(); // or grid.store.fetch();								
		grid_Project_Rizab.filter();
		
		grid_Project_Rizab.on("rowClick", onRowClickHandler_grid_Project_Rizab);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Project_Permohonan(evt){
	var grid = dijit.byId("gridProjectPermohonan");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;

	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Project_QT(evt){
	var grid = dijit.byId("gridProjectQT");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;

	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Project_FT(evt){
	var grid = dijit.byId("gridProjectFT");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;

	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Project_Rizab(evt){
	var grid = dijit.byId("gridProjectRizab");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				
function clearSearch_Project(){
	dojo.byId('select_service_project').value = "Select service...";
	dojo.byId('select_state_project').value = "Select state...";
	dojo.byId('select_district_project').value = "Select district...";
	dojo.byId('select_mukim_project').value = "Select sub district...";
	dojo.byId('tanah').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridProject");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridProject"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultProject').hide();
	
}