/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;
 
// Create Victim Centre result grid
var gridFloodVictims_Structure =[[
	// declare your structure here
	{name: 'Name', field: 'mangsaNama', width: '30%'},
	{name: 'Gender', field: 'jantina'},
	{name: 'Age', field: 'umur'},
	{name: 'IC No.', field: 'noIC'},
	{name: 'Evacuation Centre', field: 'pusat_pemindahan', width: '30%'},
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_victim(results) {
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.STATE
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_victim").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		
// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_victim(results) {
	dijit.byId("select_district_victim").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DISTRICT;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_victim").set('store', store);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_victim(results) {
	dijit.byId("select_mukim_victim").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukim = feature.attributes.NAME;
		if (!testVals[mukim]) {
			testVals[mukim] = true;
			values.push({name:mukim, id:mukim});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_victim").set('store', store);
}

// Victim Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Victim Centre: Execute Search
function executeQueryTask_FloodVictims() { console.log("execute task search victims");
	// Check if all values have been selected
	var state = dojo.byId('select_state_victim').value;
	var district = dojo.byId('select_district_victim').value;
	var mukim = dojo.byId('select_mukim_victim').value;
	var victim_name = dojo.byId('victim_name').value;
	
	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(victim_name);
	
	var kes;
	
	if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && victim_name == "") kes = 1; // none
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && victim_name == "") kes = 2; // state, district, mukim
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && victim_name != "") kes = 3; // ALL
	else kes = 'default';
	
	console.log(kes);
	
	switch (kes){
		case 1:
			query_FloodVictims.where = "nama <> ''";
		break;
		
		case 2:
			query_FloodVictims.where = "negeri = '" + state  + "'  AND daerah = '" + district + "' AND mukim = '" + mukim + "'";
		break;
		
		case 3:
			query_FloodVictims.where = "negeri = '" + state  + "'  AND daerah = '" + district + "' AND mukim = '" + mukim + "' AND nama LIKE '%" + victim_name + "%'";
		break;
			
		default:
			query_FloodVictims.where = "nama LIKE '%" + victim_name + "%'";
		break;		
	}
	
	console.log(query_FloodVictims.where);

	// Execute query
	queryTask_FloodVictims.execute(query_FloodVictims, showResults_FloodVictims);
	showResultpane_FloodVictims();
} 

function showResultpane_FloodVictims() {  
			dijit.byId('searchResultFloodVictims').show();
}

// Victim Centre: Show Search Result			
function showResults_FloodVictims(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_FloodVictims = dijit.byId("gridFloodVictims");	

	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	console.log("result length = " + featureSet.features.length);
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_FloodVictims.set("noDataMessage", 'Sorry, there are no results.');
		grid_FloodVictims.setStore(clearStore); 					
	}
	
	else {								    			    
		prevfeature = null;
	
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_Victim = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_Victim.push(feature.attributes);  
		
			//Set the infoTemplate.
			var info_Victim = new esri.InfoTemplate();
			info_Victim.setTitle("Flood victims");
			info_Victim.setContent(
					"<table id='info'>" +
						"<tr><td><b>The Evacuation Centre</b></td><td><b>: </b>${nama_pusat}</td></tr>" +
				    "</table>" 
			); 			         			      
			graphic.setInfoTemplate(info_Victim); 
			console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gFloodVictims = evt.graphic;
          map.infoWindow.setContent(gFloodVictims.getContent());
          map.infoWindow.setTitle(gFloodVictims.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_FloodVictims = {
			identifier : "OBJECTID_1", //This field needs to have unique values
			label : "OBJECTID_1", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Victim
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_FloodVictims,
				clearOnClose: true
		});
												
		grid_FloodVictims.set("store", store);
		
		// Clear previous search results
		if(grid_FloodVictims.store.save) { grid_FloodVictims.store.save();}
		grid_FloodVictims.store.close();				
		grid_FloodVictims._refresh(); // or grid.store.fetch();								
		grid_FloodVictims.filter();
		
		grid_FloodVictims.on("rowClick", onRowClickHandler_grid_FloodVictims);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_FloodVictims(evt){
	console.log('prevfeature = '+ prevfeature);	
	var grid = dijit.byId("gridFloodVictims");				
	var clickedId = grid.getItem(evt.rowIndex).objectid_12;
	console.log('clickedId = ' + clickedId);
	var selectedId;	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.objectid_12 == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;			
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_FloodVictims(){
	dojo.byId('select_state_victim').value = "Select state...";
	dojo.byId('select_district_victim').value = "Select district...";
	dojo.byId('select_mukim_victim').value = "Select sub district...";
	dojo.byId('victim_name').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridFloodVictims");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridFloodVictims"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultFloodVictims').hide();
	
}