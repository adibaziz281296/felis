/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;


// Create Victim Centre result grid
var gridNDCDB_Structure =[[
	// declare your structure here
	{name: 'Lot NDCDB', field: 'LOT', width: '30%'},
	{name: 'Negeri', field: 'NAMA_NEGERI', width: '30%'},
	{name: 'Daerah', field: 'NAMA_DAERAH', width: '30%'},
	{name: 'Mukim', field: 'NAMA_MUKIM', width: '30%'},
	{name: 'Seksyen', field: 'SEKSYEN', width: '30%'}
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_ndcdb(results) {
	document.getElementById('imageLoadingNDCDB').innerHTML = '';
	dijit.byId("select_district_ndcdb").reset();
	
	var stateName, stateCode;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		stateName = feature.attributes.NAM;
		stateCode = feature.attributes.KOD_NEGERI;
		if (!testVals[stateName]) { 
			testVals[stateName] = true;
			values.push({name:stateName, id:stateCode});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "id", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	storeStatendcdb = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_state_ndcdb").set('store', storeStatendcdb);
}

		
// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_ndcdb(results) {
	document.getElementById('imageLoadingNDCDB').innerHTML = '';
	dijit.byId("select_district_ndcdb").reset(); 
	
	var districtName, districtCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		districtName = feature.attributes.NAM;
		districtCode = feature.attributes.KOD_DAERAH;
		if (!testVals[districtName]) {
			testVals[districtName] = true;
			values.push({name:districtName, id:districtCode});			
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	storeDistrictndcdb = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_ndcdb").set('store', storeDistrictndcdb);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_ndcdb(results) {
	document.getElementById('imageLoadingNDCDB').innerHTML = '';
	dijit.byId("select_mukim_ndcdb").reset(); 
	
	var mukimName, mukimCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukimName = feature.attributes.NAM;
		mukimCode = feature.attributes.KOD_MUKIM;
		if (!testVals[mukimName]) {
			testVals[mukimName] = true;
			values.push({name:mukimName, id:mukimCode});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	storeMukimndcdb = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_ndcdb").set('store', storeMukimndcdb);
}



// Execute Search
function executeQueryTask_NDCDB() { 

	var state = dojo.byId('select_state_ndcdb').value;
	var district = dojo.byId('select_district_ndcdb').value;
	var mukim = dojo.byId('select_mukim_ndcdb').value;
//	var district = dojo.byId('select_district_ndcdb').value;
	var lotndcdb = dojo.byId('ndcdb').value;
	if (state == 'Wilayah Persekutuan Kuala Lumpur') {
		state = 'Kuala Lumpur';
	}

	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(lotndcdb);
	
	if (state != 'Select state...' && district != 'Select district...' && mukim != 'Select sub district...' && lotndcdb != ''){
		

		query_NDCDB.where = "NAMA_NEGERI = '" + state  + "'  AND NAMA_DAERAH = '" + district + "' AND NAMA_MUKIM = '" + mukim + "' AND LOT LIKE '%" + lotndcdb + "%'";		
		console.log(query_NDCDB.where);

		if(state == 'Johor'){
			queryTask_NDCDB_johor.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Kedah'){
			queryTask_NDCDB_kedah.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Kelantan'){
			queryTask_NDCDB_kelantan.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Kuala Lumpur'){
			queryTask_NDCDB_kl.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Wilayah Persekutuan Labuan'){
			queryTask_NDCDB_labuan.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Melaka'){
			queryTask_NDCDB_melaka.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Negeri Sembilan'){
			queryTask_NDCDB_n9.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Pahang'){
			queryTask_NDCDB_pahang.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Perak'){
			queryTask_NDCDB_perak.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Perlis'){
			queryTask_NDCDB_perlis.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Pulau Pinang'){
			queryTask_NDCDB_ppinang.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Wilayah Persekutuan Putrajaya'){
			queryTask_NDCDB_putrajaya.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Selangor'){
			queryTask_NDCDB_selangor.execute(query_NDCDB, showResults_NDCDB);
		}
		if(state == 'Terengganu'){
			queryTask_NDCDB_terengganu.execute(query_NDCDB, showResults_NDCDB);
		}
		
		
		showResultpane_NDCDB();
	}
	
	if (state == 'Select state...'){
		alert('Please select state!!!');
	}else{
		if (district == 'Select district...'){
			alert('Please select city!!!');
		}else{
			if (mukim == 'Select sub district...'){
				alert('Please select Sub District!!!');
			}else{
				if (lotndcdb == ''){
					alert('Please select lot ndcdb!!!');	
				}
			}
		}
	}
} 

function showResultpane_NDCDB() {  
			dijit.byId('searchResultNDCDB').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();
}

// File: Show Search Result			
function showResults_NDCDB(featureSet) {

	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_NDCDB = dijit.byId("gridNDCDB");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
//	alert(featureSet.features.length);
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_NDCDB.set("noDataMessage", 'Sorry, there are no results.');
		grid_NDCDB.setStore(clearStore); 	
		console.log("I'M HERE");				
	}
	
	else {								    			    
		prevfeature = null;
		console.log("I'M HERE TOO");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_NDCDB = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_NDCDB.push(feature.attributes);
		
			var info_ndcdb = new esri.InfoTemplate();
			
			info_ndcdb.setTitle("Carian Lot NDCDB");
			info_ndcdb.setContent("${*}");		
			graphic.setInfoTemplate(info_ndcdb); 
					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gNDCDB = evt.graphic;
          map.infoWindow.setContent(gNDCDB.getContent());
          map.infoWindow.setTitle(gNDCDB.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_NDCDB = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_NDCDB
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_NDCDB,
				clearOnClose: true
		});
												
		grid_NDCDB.set("store", store);
		
		// Clear previous search results
		if(grid_NDCDB.store.save) { grid_NDCDB.store.save();}
		grid_NDCDB.store.close();				
		grid_NDCDB._refresh(); // or grid.store.fetch();								
		grid_NDCDB.filter();
		
		grid_NDCDB.on("rowClick", onRowClickHandler_grid_NDCDB);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	
		//map.centerAndZoom(center, zoom);	
			

		//var extent = graphicsUtils.graphicsExtent(map.graphics.graphics).expand(4);
		//map.setExtent(extent);
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_NDCDB(evt){
	var grid = dijit.byId("gridNDCDB");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_NDCDB(){
	dojo.byId('select_state_ndcdb').value = "Select state...";
	//dojo.byId('select_district_ndcdb').value = "Select district...";
	//dojo.byId('select_mukim_ndcdb').value = "Select mukim...";
	dojo.byId('ndcdb').value = "";
	
	//clearQueryTask("gridFile");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridNDCDB"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultNDCDB').hide();
	
}