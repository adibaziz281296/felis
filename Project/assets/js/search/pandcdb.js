/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;


// Create Victim Centre result grid
var gridPANDCDB_Structure =[[
	// declare your structure here
	{name: 'PA', field: 'PA', width: '30%'},
	{name: 'Negeri', field: 'NAMA_NEGERI', width: '30%'},
	{name: 'Daerah', field: 'NAMA_DAERAH', width: '30%'},
	{name: 'Mukim', field: 'NAMA_MUKIM', width: '30%'},
	{name: 'Seksyen', field: 'SEKSYEN', width: '30%'}
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_pandcdb(results) {
	document.getElementById('imageLoadingPANDCDB').innerHTML = '';
	dijit.byId("select_district_pandcdb").reset();
	
	var stateName, stateCode;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		stateName = feature.attributes.NAM;
		stateCode = feature.attributes.KOD_NEGERI;
		if (!testVals[stateName]) { 
			testVals[stateName] = true;
			values.push({name:stateName, id:stateCode});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "id", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	storeStatepandcdb = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_state_pandcdb").set('store', storeStatepandcdb);
}

		
// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_pandcdb(results) {
	document.getElementById('imageLoadingPANDCDB').innerHTML = '';
	dijit.byId("select_district_pandcdb").reset(); 
	
	var districtName, districtCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		districtName = feature.attributes.NAM;
		districtCode = feature.attributes.KOD_DAERAH;
		if (!testVals[districtName]) {
			testVals[districtName] = true;
			values.push({name:districtName, id:districtCode});			
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	storeDistrictpandcdb = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_pandcdb").set('store', storeDistrictpandcdb);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_pandcdb(results) {
	document.getElementById('imageLoadingPANDCDB').innerHTML = '';
	dijit.byId("select_mukim_pandcdb").reset(); 
	
	var mukimName, mukimCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukimName = feature.attributes.NAM;
		mukimCode = feature.attributes.KOD_MUKIM;
		if (!testVals[mukimName]) {
			testVals[mukimName] = true;
			values.push({name:mukimName, id:mukimCode});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	storeMukimpandcdb = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_pandcdb").set('store', storeMukimpandcdb);
}



// Execute Search
function executeQueryTask_PANDCDB() { 

	var state = dojo.byId('select_state_pandcdb').value;
	var district = dojo.byId('select_district_pandcdb').value;
	var mukim = dojo.byId('select_mukim_pandcdb').value;
//	var district = dojo.byId('select_district_ndcdb').value;
	var pandcdb = dojo.byId('pandcdb').value;
	if (state == 'Wilayah Persekutuan Kuala Lumpur') {
		state = 'Kuala Lumpur';
	}

	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(pandcdb);
	
	if (state != 'Select state...' && district != 'Select district...' && mukim != 'Select sub district...' && pandcdb != ''){
		

		query_PANDCDB.where = "NAMA_NEGERI = '" + state  + "'  AND NAMA_DAERAH = '" + district + "' AND NAMA_MUKIM = '" + mukim + "' AND PA LIKE '%" + pandcdb + "%'";		
		console.log(query_NDCDB.where);

		if(state == 'Johor'){
			queryTask_PANDCDB_johor.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Kedah'){
			queryTask_PANDCDB_kedah.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Kelantan'){
			queryTask_PANDCDB_kelantan.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Kuala Lumpur'){
			queryTask_PANDCDB_kl.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Wilayah Persekutuan Labuan'){
			queryTask_PANDCDB_labuan.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Melaka'){
			queryTask_PANDCDB_melaka.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Negeri Sembilan'){
			queryTask_PANDCDB_n9.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Pahang'){
			queryTask_PANDCDB_pahang.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Perak'){
			queryTask_PANDCDB_perak.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Perlis'){
			queryTask_PANDCDB_perlis.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Pulau Pinang'){
			queryTask_PANDCDB_ppinang.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Wilayah Persekutuan Putrajaya'){
			queryTask_PANDCDB_putrajaya.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Selangor'){
			queryTask_PANDCDB_selangor.execute(query_PANDCDB, showResults_PANDCDB);
		}
		if(state == 'Terengganu'){
			queryTask_PANDCDB_terengganu.execute(query_PANDCDB, showResults_PANDCDB);
		}
		
		
		showResultpane_PANDCDB();
	}
	
	if (state == 'Select state...'){
		alert('Please select state!!!');
	}else{
		if (district == 'Select district...'){
			alert('Please select city!!!');
		}else{
			if (mukim == 'Select sub district...'){
				alert('Please select Sub District!!!');
			}else{
				if (pandcdb == ''){
					alert('Please insert PA!!!');	
				}
			}
		}
	}
} 

function showResultpane_PANDCDB() {  
			dijit.byId('searchResultPANDCDB').show();
			dijit.byId('searchFormPANDCDB').hide();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();
}

// File: Show Search Result			
function showResults_PANDCDB(featureSet) {

	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_PANDCDB = dijit.byId("gridPANDCDB");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
//	alert(featureSet.features.length);
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_PANDCDB.set("noDataMessage", 'Sorry, there are no results.');
		grid_PANDCDB.setStore(clearStore); 	
		console.log("I'M HERE");				
	}
	
	else {								    			    
		prevfeature = null;
		console.log("I'M HERE TOO");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_PANDCDB = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_PANDCDB.push(feature.attributes);
		
			var info_pandcdb = new esri.InfoTemplate();
			
			info_pandcdb.setTitle("Carian PA NDCDB");
			info_pandcdb.setContent("${*}");		
			graphic.setInfoTemplate(info_pandcdb); 
					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gPANDCDB = evt.graphic;
          map.infoWindow.setContent(gPANDCDB.getContent());
          map.infoWindow.setTitle(gPANDCDB.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_PANDCDB = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_PANDCDB
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_PANDCDB,
				clearOnClose: true
		});
												
		grid_PANDCDB.set("store", store);
		
		// Clear previous search results
		if(grid_PANDCDB.store.save) { grid_PANDCDB.store.save();}
		grid_PANDCDB.store.close();				
		grid_PANDCDB._refresh(); // or grid.store.fetch();								
		grid_PANDCDB.filter();
		
		grid_PANDCDB.on("rowClick", onRowClickHandler_grid_PANDCDB);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	
		//map.centerAndZoom(center, zoom);	
			

		//var extent = graphicsUtils.graphicsExtent(map.graphics.graphics).expand(4);
		//map.setExtent(extent);
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_PANDCDB(evt){
	var grid = dijit.byId("gridPANDCDB");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_PANDCDB(){
	dojo.byId('select_state_pandcdb').value = "Select state...";
	//dojo.byId('select_district_ndcdb').value = "Select district...";
	//dojo.byId('select_mukim_ndcdb').value = "Select mukim...";
	dojo.byId('pandcdb').value = "";
	
	//clearQueryTask("gridFile");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridPANDCDB"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultPANDCDB').hide();
	
}