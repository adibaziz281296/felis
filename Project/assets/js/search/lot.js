
var prevfeature = null;
 
// Create result grid
var gridLot_Structure =[[
	// declare your structure here
	{name: 'No.Fail', field: 'NO_JKPTG', width: '30%'},
	{name: 'Projek', field: 'GUNA_TANAH', width: '70%'},
	{name: 'Kementerian', field: 'KEMENTERIAN', width: '50%'},
	{name: 'Daerah', field: 'NAMA_DAERAH', width: '30%'},
	{name: 'Mukim', field: 'NAMA_MUKIM', width: '30%'},
	{name: 'Seksyen', field: 'NAMA_SEKSYEN', width: '30%'},
	{name: 'No. Lot', field: 'NO_LOT'},
	{name: 'No. PA', field: 'NO_PA'}
]];

// REGION Dropdown 			
function populateList_state_lot(results) {
	document.getElementById('imageLoadingLot').innerHTML = '';
	dijit.byId("select_district_lot").reset();
	
	var stateName, stateCode;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		stateName = feature.attributes.NAM;
		stateCode = feature.attributes.KOD_NEGERI;
		if (!testVals[stateName]) { 
			testVals[stateName] = true;
			values.push({name:stateName, id:stateCode});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "id", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	storeStateLot = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_state_lot").set('store', storeStateLot);
}
		
// DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_lot(results) {
	document.getElementById('imageLoadingLot').innerHTML = '';
	dijit.byId("select_district_lot").reset(); 
	
	var districtName, districtCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		districtName = feature.attributes.NAM;
		districtCode = feature.attributes.KOD_DAERAH;
		if (!testVals[districtName]) {
			testVals[districtName] = true;
			values.push({name:districtName, id:districtCode});			
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	storeDistrictLot = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_lot").set('store', storeDistrictLot);
}

// MUKIM Dropdown by STATE & DISTRICT
function populateList_mukim_byStateDistrict_lot(results) {
	document.getElementById('imageLoadingLot').innerHTML = '';
	dijit.byId("select_mukim_lot").reset(); 
	
	var mukimName, mukimCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukimName = feature.attributes.NAM;
		mukimCode = feature.attributes.KOD_MUKIM;
		if (!testVals[mukimName]) {
			testVals[mukimName] = true;
			values.push({name:mukimName, id:mukimCode});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	storeMukimLot = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_lot").set('store', storeMukimLot);
}

// Execute Search
function executeQueryTask_Lot() {
	var state = dojo.byId('select_state_lot').value;
	var district = dojo.byId('select_district_lot').value;
	var mukim = dojo.byId('select_mukim_lot').value;
	var seksyen_name = dojo.byId('seksyen_name').value;
	var lot_number = dojo.byId('lot_number').value;
	
	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(seksyen_name);
	console.log(lot_number);
	console.log("Negeri ID: " + selectStateIdLot + ", Daerah ID: " + selectDistrictIdLot + ", Mukim ID: " + selectMukimIdLot);

	var kes;
	
	if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number == "") kes = 1; // state
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number == "") kes = 2; // state, district
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && seksyen_name == "" && lot_number == "") kes = 3; // state, district, mukim
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && seksyen_name != "" && lot_number == "") kes = 4; // state, district, mukim, seksyen
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && seksyen_name != "" && lot_number != "") kes = 5; // state, district, mukim, seksyen, lot number
	else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number == "") kes = 6; // state, seksyen
	else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number != "") kes = 7; // state, lot number
	else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number != "") kes = 8; // state, seksyen, lot number
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number == "") kes = 9; // state, district, seksyen
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number != "") kes = 10; // state, district, seksyen, lot number
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number != "") kes = 11; // state, district, lot number
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && seksyen_name == "" && lot_number != "") kes = 12; // state, district, mukim, lot number
	else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number == "") kes = 13; // seksyen
	else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number != "") kes = 14; // lot number
	else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number != "") kes = 15; // seksyen, lot number
	else kes = 'default';
	
	switch (kes){
		case 1:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "' AND NO_LOT <> ''";
			break;
		case 2:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "'  AND KOD_DAERAH = '" + selectDistrictIdLot + "' AND NO_LOT <> ''";
			break;
		case 3:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "'  AND KOD_DAERAH = '" + selectDistrictIdLot + "' AND KOD_MUKIM = '" + selectMukimIdLot + "' AND NO_LOT <> ''";
			break;
		case 4:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "'  AND KOD_DAERAH = '" + selectDistrictIdLot + "' AND KOD_MUKIM = '" + selectMukimIdLot + "' AND NAMA_SEKSYEN LIKE '%" + seksyen_name + "%' AND NO_LOT <> ''";
			break;
		case 5:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "'  AND KOD_DAERAH = '" + selectDistrictIdLot + "' AND KOD_MUKIM = '" + selectMukimIdLot + "' AND NAMA_SEKSYEN LIKE '%" + seksyen_name + "%' AND NO_LOT LIKE '%" + lot_number + "%'";
			break;
		case 6:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "' AND NAMA_SEKSYEN LIKE '%" + seksyen_name + "%' AND NO_LOT <> ''";
			break;
		case 7:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "' AND NO_LOT LIKE '%" + lot_number + "%'";
			break;
		case 8:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "' AND NAMA_SEKSYEN LIKE '%" + seksyen_name + "%' AND NO_LOT LIKE '%" + lot_number + "%'";
			break;
		case 9:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "'  AND KOD_DAERAH = '" + selectDistrictIdLot + "' AND NAMA_SEKSYEN LIKE '%" + seksyen_name + "%' AND NO_LOT <> ''";
			break;
		case 10:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "'  AND KOD_DAERAH = '" + selectDistrictIdLot + "' AND NAMA_SEKSYEN LIKE '%" + seksyen_name + "%' AND NO_LOT LIKE '%" + lot_number + "%'";
			break;
		case 11:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "'  AND KOD_DAERAH = '" + selectDistrictIdLot + "' AND NO_LOT LIKE '%" + lot_number + "%'";
			break;
		case 12:
			query_Lot.where = "KOD_NEGERI = '" + selectStateIdLot  + "'  AND KOD_DAERAH = '" + selectDistrictIdLot + "' AND KOD_MUKIM = '" + selectMukimIdLot + "' AND NO_LOT LIKE '%" + lot_number + "%'";
			break;
		case 13:
			query_Lot.where = "NAMA_SEKSYEN LIKE '%" + seksyen_name + "%' AND NO_LOT <> ''";
			break;
		case 14:
			query_Lot.where = "NO_LOT LIKE '%" + lot_number + "%'";
			break;
		case 15:
			query_Lot.where = "NAMA_SEKSYEN LIKE '%" + seksyen_name + "%' AND NO_LOT LIKE '%" + lot_number + "%'";
			break;
		default:
			query_Lot.where = "NO_LOT <> ''";
		break;		
	}

	// Execute query
	search_pemohonan_layer.execute(query_Lot, showResults_Lot_Permohonan);
	search_qt_layer.execute(query_Lot, showResults_Lot_QT);
	search_ft_layer.execute(query_Lot, showResults_Lot_FT);
	search_rezab_layer.execute(query_Lot, showResults_Lot_Rizab);

	showResultpane_Lot();
} 

function showResultpane_Lot() {  
			dijit.byId('searchResultLot').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();
}

// Lot: Show Search Result - PERMOHONAN			
function showResults_Lot_Permohonan(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Lot = dijit.byId("gridLot");	
	var items_Lot = [];	// Create items array to be added to store's data
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Lot.set("noDataMessage", 'Sorry, there are no results.');
		grid_Lot.setStore(clearStore); 				
	}
	
	else {								    			    
		prevfeature = null;

		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicPermohonan = feature;  
			graphicPermohonan.setSymbol(symbolSearch);  
			map.graphics.add(graphicPermohonan);  
			items_Lot.push(feature.attributes);
			
			//Set the infoTemplate.
			var info_lot = new esri.InfoTemplate();
			info_lot.setTitle("Carian No. PT Berstatus QT");
			info_lot.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT}");
				
			graphicPermohonan.setInfoTemplate(info_lot); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gLot = evt.graphic;
          map.infoWindow.setContent(gLot.getContent());
          map.infoWindow.setTitle(gLot.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });	
									
		//Create data object to be used in store
		var data_Lot = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Lot
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Lot,
				clearOnClose: false
		});
												
		grid_Lot.set("store", store);
		
		// Clear previous search results
		if(grid_Lot.store.save) { grid_Lot.store.save();}
		grid_Lot.store.close();				
		grid_Lot._refresh(); // or grid.store.fetch();								
		grid_Lot.filter();

		grid_Lot.on("rowClick", onRowClickHandler_grid_Lot_Permohonan);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    		
	} 
}

// Lot: Show Search Result - QT			
function showResults_Lot_QT(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Lot_QT = dijit.byId("gridLotQT");	
	var items_Lot_QT = [];		

	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Lot_QT.set("noDataMessage", 'Sorry, there are no results.');
		grid_Lot_QT.setStore(clearStore); 					
	}
	
	else {								    			    
		prevfeature = null;
		console.log("QT");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;

		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicQT = feature;  
			graphicQT.setSymbol(symbolSearch);  
			map.graphics.add(graphicQT);  
			items_Lot_QT.push(feature.attributes);
		
			var info_lot_QT = new esri.InfoTemplate();
			info_lot_QT.setTitle("Carian No. PT Berstatus QT");
			info_lot_QT.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT}");

			graphicQT.setInfoTemplate(info_lot_QT); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gLotQT = evt.graphic;
          map.infoWindow.setContent(gLotQT.getContent());
          map.infoWindow.setTitle(gLotQT.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_Lot_QT = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Lot_QT
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Lot_QT,
				clearOnClose: false
		});
												
		grid_Lot_QT.set("store", store);
		
		// Clear previous search results
		if(grid_Lot_QT.store.save) { grid_Lot_QT.store.save();}
		grid_Lot_QT.store.close();				
		grid_Lot_QT._refresh(); // or grid.store.fetch();								
		grid_Lot_QT.filter();

		grid_Lot_QT.on("rowClick", onRowClickHandler_grid_Lot_QT);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    	
	} 
}

// Lot: Show Search Result - FT			
function showResults_Lot_FT(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Lot_FT = dijit.byId("gridLotFT");	
	var items_Lot_FT = [];		
	
	if (featureSet.features.length == 0) {
		grid_Lot_FT.set("noDataMessage", 'Sorry, there are no results.');
		grid_Lot_FT.setStore(clearStore); 					
	}
	
	else {								    			    
		prevfeature = null;
		console.log("FT");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;

		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicFT = feature;  
			graphicFT.setSymbol(symbolSearch);  
			map.graphics.add(graphicFT);  
			items_Lot_FT.push(feature.attributes);
				
			var info_lot_FT = new esri.InfoTemplate();
			info_lot_FT.setTitle("Carian No. PT Berstatus FT");
			info_lot_FT.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT}");

			graphicFT.setInfoTemplate(info_lot_FT); 					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gLotFT = evt.graphic;
          map.infoWindow.setContent(gLotFT.getContent());
          map.infoWindow.setTitle(gLotFT.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_Lot_FT = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Lot_FT
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Lot_FT,
				clearOnClose: false
		});
												
		grid_Lot_FT.set("store", store);
		
		// Clear previous search results
		if(grid_Lot_FT.store.save) { grid_Lot_FT.store.save();}
		grid_Lot_FT.store.close();				
		grid_Lot_FT._refresh(); // or grid.store.fetch();								
		grid_Lot_FT.filter();

		grid_Lot_FT.on("rowClick", onRowClickHandler_grid_Lot_FT);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    	
	} 
}

// Lot: Show Search Result - RIZAB		
function showResults_Lot_Rizab(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Lot_Rizab = dijit.byId("gridLotRizab");	
	var items_Lot_Rizab = [];		
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Lot_Rizab.set("noDataMessage", 'Sorry, there are no results.');
		grid_Lot_Rizab.setStore(clearStore); 					
	}
	
	else {								    			    
		prevfeature = null;
		console.log("RIZAB");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_Lot_Rizab.push(feature.attributes);
				
			var info_lot_Rizab = new esri.InfoTemplate();

			info_lot_Rizab.setTitle("Carian No. PT Berstatus QT");
			info_lot_Rizab.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT}");

			graphic.setInfoTemplate(info_lot_Rizab); 						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gLotRizab = evt.graphic;
          map.infoWindow.setContent(gLoRizab.getContent());
          map.infoWindow.setTitle(gLotRizab.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_Lot_Rizab = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Lot_Rizab
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Lot_Rizab,
				clearOnClose: false
		});
												
		grid_Lot_Rizab.set("store", store);
		
		// Clear previous search results
		if(grid_Lot_Rizab.store.save) { grid_Lot_Rizab.store.save();}
		grid_Lot_Rizab.store.close();				
		grid_Lot_Rizab._refresh(); // or grid.store.fetch();								
		grid_Lot_Rizab.filter();

		grid_Lot_Rizab.on("rowClick", onRowClickHandler_grid_Lot_Rizab);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    	
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Lot_Permohonan(evt){
	var grid = dijit.byId("gridLot");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Lot_QT(evt){
	var grid = dijit.byId("gridLotQT");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				
//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Lot_FT(evt){
	var grid = dijit.byId("gridLotFT");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Lot_Rizab(evt){
	var grid = dijit.byId("gridLotRizab");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

function clearSearch_Lot(){
	dojo.byId('select_service_lot').value = "Select service...";
	dojo.byId('select_state_lot').value = "Select state...";
	dojo.byId('select_district_lot').value = "Select district...";
	dojo.byId('select_mukim_lot').value = "Select sub district...";
	dojo.byId('seksyen_name').value = "";
	dojo.byId('lot_number').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridLot");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridLot"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultLot').hide();
	
}