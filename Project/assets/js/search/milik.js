
var prevfeature = null;
 
// Create result grid
var gridMilik_Structure =[[
	// declare your structure here
	{name: 'No. Fail', field: 'NO_JKPTG', width: '30%'},
	{name: 'Projek', field: 'GUNA_TANAH', width: '70%'},
	{name: 'Kementerian', field: 'KEMENTERIAN', width: '50%'},
	{name: 'Negeri', field: 'NAMA_NEGERI', width: '30%'},
	{name: 'Daerah', field: 'NAMA_DAERAH', width: '30%'},
	{name: 'Mukim', field: 'NAMA_MUKIM', width: '30%'},
	{name: 'Seksyen', field: 'NAMA_SEKSYEN', width: '30%'},
	// {name: 'No. Hakmilik Sementara', field: 'HMLK_SMTARA', width: '32%'},
	// {name: 'No. PT', field: 'NO_PT', width: '30%'},
	{name: 'No. Hakmilik Tetap', field: 'HMLK_TETAP', width: '30%'},
	{name: 'No. Lot', field: 'NO_LOT'}
	// {name: 'No. PA', field: 'NO_PA'}
]];

// REGION Dropdown 			
function populateList_state_milik(results) {
	document.getElementById('imageLoadingMilik').innerHTML = '';
	var stateName, stateCode;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		stateName = feature.attributes.NAM;
		stateCode = feature.attributes.KOD_NEGERI;
		if (!testVals[stateName]) { 
			testVals[stateName] = true;
			values.push({name:stateName, id:stateCode});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "id", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	storeStateMilik = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_state_milik").set('store', storeStateMilik);
}

		
// DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_milik(results) {
	document.getElementById('imageLoadingMilik').innerHTML = '';
	dijit.byId("select_district_milik").reset(); 
	
	var districtName, districtCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		districtName = feature.attributes.NAM;
		districtCode = feature.attributes.KOD_DAERAH;
		if (!testVals[districtName]) {
			testVals[districtName] = true;
			values.push({name:districtName, id:districtCode});			
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	storeDistrictMilik = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_milik").set('store', storeDistrictMilik);
}

// MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_milik(results) {
	document.getElementById('imageLoadingMilik').innerHTML = '';
	dijit.byId("select_mukim_milik").reset(); 
	
	var mukimName, mukimCode;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukimName = feature.attributes.NAM;
		mukimCode = feature.attributes.KOD_MUKIM;
		if (!testVals[mukimName]) {
			testVals[mukimName] = true;
			values.push({name:mukimName, id:mukimCode});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	storeMukimMilik = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_milik").set('store', storeMukimMilik);
}

// Execute Search
function executeQueryTask_Milik() { console.log("execute task search Hak Milik");
	// Check if all values have been selected
	var state = dojo.byId('select_state_milik').value;
	var district = dojo.byId('select_district_milik').value;
	var mukim = dojo.byId('select_mukim_milik').value;
	var tetap = dojo.byId('milik').value;
		
	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(tetap);
	console.log("Negeri ID: " + selectStateIdMilik + ", Daerah ID: " + selectDistrictIdMilik + ", Mukim ID: " + selectMukimIdMilik);

	var kes;
		
	if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tetap == "") kes = 1; // state
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && tetap == "") kes = 2; // state, district
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && tetap == "") kes = 3; // state, district, mukim
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && tetap != "") kes = 4; // state, district, mukim, tetap
	else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tetap != "") kes = 5; // state, tetap
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && tetap != "") kes = 6; // state, district, tetap
	else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tetap != "") kes = 7; // tetap
	else kes = 'default';
		
	console.log("kes = "+ kes);
		
	switch (kes){
			
		case 1:
			query_Milik.where = "KOD_NEGERI = '" + selectStateIdMilik  + "' AND HMLK_TETAP <> ''";
		break;
			
		case 2:
			query_Milik.where = "KOD_NEGERI = '" + selectStateIdMilik  + "'  AND KOD_DAERAH = '" + selectDistrictIdMilik + "' AND HMLK_TETAP <> ''";
		break;
			
		case 3:
			query_Milik.where = "KOD_NEGERI = '" + selectStateIdMilik  + "'  AND KOD_DAERAH = '" + selectDistrictIdMilik + "' AND KOD_MUKIM = '" + selectMukimIdMilik + "' AND HMLK_TETAP <> ''";
		break;

		case 4:
			query_Milik.where = "KOD_NEGERI = '" + selectStateIdMilik  + "'  AND KOD_DAERAH = '" + selectDistrictIdMilik + "' AND KOD_MUKIM = '" + selectMukimIdMilik + "' AND HMLK_TETAP LIKE '%" + tetap + "%'";
		break;

		case 5:
			query_Milik.where = "KOD_NEGERI = '" + selectStateIdMilik  + "' AND HMLK_TETAP LIKE '%" + tetap + "%'";
		break;

		case 6:
			query_Milik.where = "KOD_NEGERI = '" + selectStateIdMilik  + "'  AND KOD_DAERAH = '" + selectDistrictIdMilik + "' AND HMLK_TETAP LIKE '%" + tetap + "%'";
		break;

		case 7:
			query_Milik.where = "HMLK_TETAP LIKE '%" + tetap + "%'";
		break;
			
		default:
			query_Milik.where = "HMLK_TETAP <> ''";
		break;		
	}
		
	console.log(query_Milik.where);

	// Execute query
	search_pemohonan_layer.execute(query_Milik, showResults_Milik_Permohonan);
	search_qt_layer.execute(query_Milik, showResults_Milik_QT);
	search_ft_layer.execute(query_Milik, showResults_Milik_FT);
	search_rezab_layer.execute(query_Milik, showResults_Milik_Rizab);

	showResultpane_Milik();
} 

function showResultpane_Milik() {  
	dijit.byId('searchResultMilik').show();
	dijit.byId('searchFormNDCDB').hide();
	dijit.byId('searchFormLot').hide();
	dijit.byId('searchFormProject').hide();
	dijit.byId('searchFormSementara').hide();
	dijit.byId('searchFormMilik').hide();
	dijit.byId('searchFormFile').hide();
	dijit.byId('searchFormIndexmap').hide();
}

// Milik: Show Search Result			
function showResults_Milik_Permohonan(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Milik_Permohonan = dijit.byId("gridMilikPermohonan");
	var items_Milik_Permohonan = []; // all items to be stored in data store			
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Milik_Permohonan.set("noDataMessage", 'Sorry, there are no results.');
		grid_Milik_Permohonan.setStore(clearStore);				
	}
	
	else {								    			    
		prevfeature = null;
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicMilikPermohonan = feature;  
			graphicMilikPermohonan.setSymbol(symbolSearch);  
			map.graphics.add(graphicMilikPermohonan);  
			items_Milik_Permohonan.push(feature.attributes);  
				
			var info_milik_permohonan = new esri.InfoTemplate();
			info_milik_permohonan.setTitle("Carian Berdasarkan No. Hak Milik");
			info_milik_permohonan.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT} <br> No Hak Milik Tetap : ${HMLK_TETAP} <br> LOT : ${NO_LOT} <br> PA : ${NO_PA}");
 
			graphicMilikPermohonan.setInfoTemplate(info_milik_permohonan); 						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gMilik = evt.graphic;
          map.infoWindow.setContent(gMilik.getContent());
          map.infoWindow.setTitle(gMilik.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
									
		//Create data object to be used in store
		var data_Milik = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Milik_Permohonan
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Milik,
				clearOnClose: true
		});
												
		grid_Milik_Permohonan.set("store", store);
		
		// Clear previous search results
		if(grid_Milik_Permohonan.store.save) { grid_Milik_Permohonan.store.save();}
		grid_Milik_Permohonan.store.close();				
		grid_Milik_Permohonan._refresh(); // or grid.store.fetch();								
		grid_Milik_Permohonan.filter();
		
		grid_Milik_Permohonan.on("rowClick", onRowClickHandler_grid_Milik_Permohonan);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	} 
}

function showResults_Milik_QT(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Milik_QT = dijit.byId("gridMilikQT");
	var items_Milik_QT = []; // all items to be stored in data store			
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Milik_QT.set("noDataMessage", 'Sorry, there are no results.');
		grid_Milik_QT.setStore(clearStore);				
	}
	
	else {								    			    
		prevfeature = null;
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicMilikQT = feature;  
			graphicMilikQT.setSymbol(symbolSearch);  
			map.graphics.add(graphicMilikQT);  
			items_Milik_QT.push(feature.attributes);  
				
			var info_milik_qt = new esri.InfoTemplate();
			info_milik_qt.setTitle("Carian Berdasarkan No. Hak Milik");
			info_milik_qt.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT} <br> No Hak Milik Tetap : ${HMLK_TETAP} <br> LOT : ${NO_LOT} <br> PA : ${NO_PA}");
 
			graphicMilikQT.setInfoTemplate(info_milik_qt); 						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gMilik = evt.graphic;
          map.infoWindow.setContent(gMilik.getContent());
          map.infoWindow.setTitle(gMilik.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
									
		//Create data object to be used in store
		var data_Milik = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Milik_QT
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Milik,
				clearOnClose: true
		});
												
		grid_Milik_QT.set("store", store);
		
		// Clear previous search results
		if(grid_Milik_QT.store.save) { grid_Milik_QT.store.save();}
		grid_Milik_QT.store.close();				
		grid_Milik_QT._refresh(); // or grid.store.fetch();								
		grid_Milik_QT.filter();
		
		grid_Milik_QT.on("rowClick", onRowClickHandler_grid_Milik_QT);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	} 
}

function showResults_Milik_FT(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Milik_FT = dijit.byId("gridMilikFT");
	var items_Milik_FT = []; // all items to be stored in data store			
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Milik_FT.set("noDataMessage", 'Sorry, there are no results.');
		grid_Milik_FT.setStore(clearStore);				
	}
	
	else {								    			    
		prevfeature = null;
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicMilikFT = feature;  
			graphicMilikFT.setSymbol(symbolSearch);  
			map.graphics.add(graphicMilikFT);  
			items_Milik_FT.push(feature.attributes);  
				
			var info_milik_FT = new esri.InfoTemplate();
			info_milik_FT.setTitle("Carian Berdasarkan No. Hak Milik");
			info_milik_FT.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT} <br> No Hak Milik Tetap : ${HMLK_TETAP} <br> LOT : ${NO_LOT} <br> PA : ${NO_PA}");
 
			graphicMilikFT.setInfoTemplate(info_milik_FT); 						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gMilik = evt.graphic;
          map.infoWindow.setContent(gMilik.getContent());
          map.infoWindow.setTitle(gMilik.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
									
		//Create data object to be used in store
		var data_Milik = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Milik_FT
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Milik,
				clearOnClose: true
		});
												
		grid_Milik_FT.set("store", store);
		
		// Clear previous search results
		if(grid_Milik_FT.store.save) { grid_Milik_FT.store.save();}
		grid_Milik_FT.store.close();				
		grid_Milik_FT._refresh(); // or grid.store.fetch();								
		grid_Milik_FT.filter();
		
		grid_Milik_FT.on("rowClick", onRowClickHandler_grid_Milik_FT);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	} 
}

function showResults_Milik_Rizab(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Milik_Rizab = dijit.byId("gridMilikRizab");
	var items_Milik_Rizab = []; // all items to be stored in data store			
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Milik_Rizab.set("noDataMessage", 'Sorry, there are no results.');
		grid_Milik_Rizab.setStore(clearStore);				
	}
	
	else {								    			    
		prevfeature = null;
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphicMilikRizab = feature;  
			graphicMilikRizab.setSymbol(symbolSearch);  
			map.graphics.add(graphicMilikRizab);  
			items_Milik_Rizab.push(feature.attributes);  
				
			var info_milik_rizab = new esri.InfoTemplate();
			info_milik_rizab.setTitle("Carian Berdasarkan No. Hak Milik");
			info_milik_rizab.setContent("No. Fail JKPTG: ${NO_JKPTG} <br> Projek: ${GUNA_TANAH} <br> Tarikh Pohon: ${TRH_POHON} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${HMLK_SMTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNIT_LUAS} <br> NEGERI: ${NAMA_NEGERI} <br> DAERAH: ${NAMA_DAERAH} <br> MUKIM : ${NAMA_MUKIM} <br> SEKSYEN : ${NAMA_SEKSYEN} <br> NO PT : ${NO_PT} <br> No Hak Milik Tetap : ${HMLK_TETAP} <br> LOT : ${NO_LOT} <br> PA : ${NO_PA}");
 
			graphicMilikRizab.setInfoTemplate(info_milik_rizab); 						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gMilik = evt.graphic;
          map.infoWindow.setContent(gMilik.getContent());
          map.infoWindow.setTitle(gMilik.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
									
		//Create data object to be used in store
		var data_Milik = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Milik_Rizab
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Milik,
				clearOnClose: true
		});
												
		grid_Milik_Rizab.set("store", store);
		
		// Clear previous search results
		if(grid_Milik_Rizab.store.save) { grid_Milik_Rizab.store.save();}
		grid_Milik_Rizab.store.close();				
		grid_Milik_Rizab._refresh(); // or grid.store.fetch();								
		grid_Milik_Rizab.filter();
		
		grid_Milik_Rizab.on("rowClick", onRowClickHandler_grid_Milik_Rizab);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Milik_Permohonan(evt){
	var grid = dijit.byId("gridMilikPermohonan");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
		if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
			selectedId = currentGraphic;
			break;
		}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

function onRowClickHandler_grid_Milik_QT(evt){
	var grid = dijit.byId("gridMilikQT");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
		if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
			selectedId = currentGraphic;
			break;
		}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

function onRowClickHandler_grid_Milik_FT(evt){
	var grid = dijit.byId("gridMilikFT");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
		if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
			selectedId = currentGraphic;
			break;
		}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

function onRowClickHandler_grid_Milik_Rizab(evt){
	var grid = dijit.byId("gridMilikRizab");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
		if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
			selectedId = currentGraphic;
			break;
		}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}

function clearSearch_Milik(){
	dojo.byId('select_state_milik').value = "Select state...";
	dojo.byId('select_district_milik').value = "Select district...";
	dojo.byId('select_mukim_milik').value = "Select sub district...";
	dojo.byId('milik').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridMilik");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridMilik"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultMilik').hide();
	
}