/**
 * @author Khairul Abidin, October 2017
 * Search Indexmap
 */
 
 
var prevfeature = null;


// Create Index map result grid
var gridIndexmap_Structure =[[
	// declare your structure here
	{name: 'No Peta', field: 'NOPETA', width: '30%'}
]];




// Execute Search
function executeQueryTask_Indexmap() { 

	var state = dojo.byId('select_state_indexmap').value;
	var nopeta = dojo.byId('indexmap').value;
	
	
	if (state != 'Select state...' && nopeta != ''){
		

		query_Indexmap.where = "NOPETA LIKE '%" + nopeta + "%'";		
		console.log(query_Indexmap.where);

		if(state == 'Johor'){
			queryTask_Indexmap_johor.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Kedah'){
			queryTask_Indexmap_kedah.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Kelantan'){
			queryTask_Indexmap_kelantan.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Melaka'){
			queryTask_Indexmap_melaka.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Negeri Sembilan'){
			queryTask_Indexmap_n9.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Pahang'){
			queryTask_Indexmap_pahang.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Perak'){
			queryTask_Indexmap_perak.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Perlis'){
			queryTask_Indexmap_perlis.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Pulau Pinang'){
			queryTask_Indexmap_ppinang.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Selangor'){
			queryTask_Indexmap_selangor.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'Terengganu'){
			queryTask_Indexmap_terengganu.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'WP Kuala Lumpur'){
			queryTask_Indexmap_kl.execute(query_Indexmap, showResults_Indexmap);
		}
		if(state == 'WP Putrajaya'){
			queryTask_Indexmap_putrajaya.execute(query_Indexmap, showResults_Indexmap);
		}
	
		showResultpane_Indexmap();
	}
	
	
	if (state != 'Select state...' && nopeta == ''){
		alert('Please input map number!!!');	
	}
	if (state == 'Select state...' && nopeta != ''){
		alert('Please select state!!!');
	}
	
} 

function showResultpane_Indexmap() {  
			dijit.byId('searchResultIndexmap').show();
			dijit.byId('searchFormNDCDB').hide();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
			dijit.byId('searchFormIndexmap').hide();
}

// File: Show Search Result			
function showResults_Indexmap(featureSet) {

	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Indexmap = dijit.byId("gridIndexmap");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Indexmap.set("noDataMessage", 'Sorry, there are no results.');
		grid_Indexmap.setStore(clearStore); 	
		console.log("I'M HERE");				
	}
	
	else {								    			    
		prevfeature = null;
		console.log("I'M HERE TOO");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_Indexmap = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_Indexmap.push(feature.attributes);
		
			var info_indexmap = new esri.InfoTemplate();
			
			info_indexmap.setTitle("Carian index map");
			info_indexmap.setContent("${*}");		
			graphic.setInfoTemplate(info_indexmap); 
					
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onClick", function(evt) {
          var gIndexmap = evt.graphic;
          map.infoWindow.setContent(gIndexmap.getContent());
          map.infoWindow.setTitle(gIndexmap.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_Indexmap = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Indexmap
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Indexmap,
				clearOnClose: true
		});
												
		grid_Indexmap.set("store", store);
		
		// Clear previous search results
		if(grid_Indexmap.store.save) { grid_Indexmap.store.save();}
		grid_Indexmap.store.close();				
		grid_Indexmap._refresh(); // or grid.store.fetch();								
		grid_Indexmap.filter();
		
		grid_Indexmap.on("rowClick", onRowClickHandler_grid_Indexmap);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Indexmap(evt){
	var grid = dijit.byId("gridIndexmap");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	var selectedId;
	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_Indexmap(){
	dojo.byId('select_state_indexmap').value = "Select state...";
	dojo.byId('indexmap').value = "";
	
	//clearQueryTask("gridFile");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridIndexmap"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultIndexmap').hide();
	
}