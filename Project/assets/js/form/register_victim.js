/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;
 
// Create Victim Centre result grid
var gridVictim_Structure =[[
	// declare your structure here
	{name: 'Nama', field: 'name', width: '40%'},
	{name: 'Jenis', field: 'type'},
	{name: 'Kategori', field: 'category'},
	{name: 'Negeri', field: ''},
	{name: 'Daerah', field: 'district'},
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_victim_reg(results) {
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.STATE
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Pilih negeri...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_victim_reg").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		
// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_victim_reg(results) {
	dijit.byId("select_district_victim_reg").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.Daerah;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Pilih daerah...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_victim_reg").set('store', store);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_victim_reg(results) {
	dijit.byId("select_mukim_victim_reg").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukim = feature.attributes.Mukim;
		if (!testVals[mukim]) {
			testVals[mukim] = true;
			values.push({name:mukim, id:mukim});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Pilih mukim...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_victim_reg").set('store', store);
}

function populateList_evacuation_byStateDistrictMukim_victim_reg(results) { 
	dijit.byId("select_evacuation_victim_reg").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	console.log(features);
	dojo.forEach (features, function(feature) {
		nama_pusat = feature.attributes.nama;
		if (!testVals[nama_pusat]) {
			testVals[nama_pusat] = true;
			values.push({name:nama_pusat, id:nama_pusat});				
		}
		console.log(nama_pusat);
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Pilih pusat pemindahan...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_evacuation_victim_reg").set('store', store);
}

// Victim Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Victim Centre: Execute Search
function executeQueryTask_Victim() { console.log("execute task register");
	// Check if all values have been selected
	var state = dojo.byId('select_state_victim').value;
	var district = dojo.byId('select_district_victim').value;
	var victim_name = dojo.byId('victim_name').value;
	
	console.log(state);
	console.log(district);
	console.log(victim_name);
	
	var kes;
	
	if(state == "Pilih negeri..." && district == "Pilih daerah..." && victim_name == "") kes = 1; // none
	if(state != "Pilih negeri..." && district == "Pilih daerah..." && victim_name == "") kes = 2; // state
	if(state == "Pilih negeri..." && district == "Pilih daerah..." && victim_name != "") kes = 3; // victim name
	if(state != "Pilih negeri..." && district == "Pilih daerah..." && victim_name != "") kes = 4; // state & victim
	if(state != "Pilih negeri..." && district != "Pilih daerah..." && victim_name != "") kes = 5; // ALL
	else kes = 'default';
	
	console.log(kes);
	
	switch (kes){
		case 1:
			query_Victim.where = "NAME <> ''";
		break;
		
		case 2:
			query_Victim.where = "STATE = '" + state  + "'";
			console.log("STATE = '" + state  + "'");
		break;
		
		case 3:
			query_Victim.where = "NAME LIKE '%" + victim_name + "%'";
			console.log("NAME LIKE '%" + victim_name + "%'");
		break;
		
		case 4:
			query_Victim.where = "STATE = '" + state  + "' AND NAME LIKE '%" + victim_name + "%'";
			console.log("STATE = '" + state  + "' AND NAME LIKE '%" + victim_name + "%'");
		break;
		
		case 5:
			//query_Victim.where = "STATE = '" + state  + "' AND DISTRICT = '" + district + "' AND NAME LIKE '%" + victim_name + "%'";
			query_Victim.where = "DISTRICT = '" + district + "' AND NAME LIKE '%" + victim_name + "%'";
			console.log("STATE = '" + state  + "' AND DISTRICT = '" + district + "' AND NAME LIKE '%" + victim_name + "%'");
		break;
		
		default:
			query_Victim.where = "NAME <> ''";
		break;		
	}
	
	// Execute query
	queryTask_Victim.execute(query_Victim,showResults_Victim);
	showResultpane_Victim();
} 

function showResultpane_Victim() {  
			dijit.byId('searchResultVictim').show();
}

// Victim Centre: Show Search Result			
function showResults_Victim(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Victim = dijit.byId("gridVictim");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Victim.set("noDataMessage", 'Maaf, tiada hasil carian.');
		grid_Victim.setStore(clearStore); 					
	}
	
	else {								    			    
		prevfeature = null;
	
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_Victim = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_Victim.push(feature.attributes);  
		
			//Set the infoTemplate.
			var info_Victim = new esri.InfoTemplate();
			info_Victim.setTitle("Lokasi Mangsa Banjir");
			info_Victim.setContent(
					"<table id='info'>" +
				        "<tr><td><b>Nama</b></td><td><b>: </b>${name}</td></tr>" +
						"<tr><td><b>Jenis</b></td><td><b>: </b>${type}</td></tr>" +
						"<tr><td><b>Kategori</b></td><td><b>: </b>${category}</td></tr>" +
				    "</table>" 
			); 			         			      
			graphic.setInfoTemplate(info_Victim); 
			console.log(feature.attributes);						
		}); // close loop forEach
									
		//Create data object to be used in store
		var data_Victim = {
			identifier : "objectid", //This field needs to have unique values
			label : "objectid", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Victim
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Victim,
				clearOnClose: true
		});
												
		grid_Victim.set("store", store);
		
		// Clear previous search results
		if(grid_Victim.store.save) { grid_Victim.store.save();}
		grid_Victim.store.close();				
		grid_Victim._refresh(); // or grid.store.fetch();								
		grid_Victim.filter();
		
		grid_Victim.on("rowClick", onRowClickHandler_grid_Victim);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Victim(evt){
	console.log('prevfeature = '+ prevfeature);	
	var grid = dijit.byId("gridVictim");				
	var clickedId = grid.getItem(evt.rowIndex).objectid_12;
	console.log('clickedId = ' + clickedId);
	var selectedId;	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.objectid_12 == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;			
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearForm_RegVictim(){
	dojo.byId('select_state_victim_reg').value = "Pilih negeri...";
	dojo.byId('select_district_victim_reg').value = "Pilih daerah...";
	dojo.byId('select_mukim_victim_reg').value = "Pilih mukim...";
	dojo.byId('select_evacuation_victim_reg').value = "Pilih pusat pemindahan...";
	dojo.byId('regVictim_name').value = "";
	dojo.byId('regVictim_id').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridVictim");
	//dijit.byId('searchResultVictim').hide();
	
}