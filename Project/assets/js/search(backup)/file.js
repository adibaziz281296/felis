/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;
 
// Create Victim Centre result grid
var gridFile_Structure =[[
	// declare your structure here
	{name: 'KTPK', field: 'KTPK', width: '30%'},
	{name: 'STATE', field: 'NAMA_NEGERI'}
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_file(results) {

	alert("test");
	
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.NAMA_NEGERI
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_file").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		


// Victim Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Victim Centre: Execute Search
function executeQueryTask_File() { console.log("execute task search file name");
	// Check if all values have been selected
	var service = dojo.byId('select_service_file').value;
	if (service != 'Select service...') {
		var state = dojo.byId('select_state_file').value;
		var ktpk = dojo.byId('ktpk').value;
		
		console.log(state);
		console.log(ktpk);

		var kes;
		
		if(state != "Select state..." && ktpk == "") kes = 1; // state
		else if(state != "Select state..." && ktpk != "") kes = 2; // state, ktpk
		else if(state == "Select state..." && ktpk != "") kes = 3; // ktpk
		else kes = 'default';
		
		console.log("kes = "+ kes);
		
		switch (kes){
			
			case 1:
				query_File.where = "NEGERI = '" + state  + "' AND KTPK <> ''";
			break;
			
			case 2:
				query_File.where = "NEGERI = '" + state  + "' AND KTPK LIKE '%" + ktpk + "%'";
			break;

			case 3:
				query_File.where = "KTPK LIKE '%" + ktpk + "%'";
			break;
			
			default:
				query_File.where = "KTPK <> ''";
			break;		
		}
		
		console.log(query_File.where);

		// Execute query
		queryTask_File.execute(query_File, showResults_File);
		showResultpane_File();
	}
	else
	{
		alert('Please select service!!!');
	}
} 

function showResultpane_File() {  
			dijit.byId('searchResultFile').show();

			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
			dijit.byId('searchFormFile').hide();
}

// File: Show Search Result			
function showResults_File(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_File = dijit.byId("gridFile");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_File.set("noDataMessage", 'Sorry, there are no results.');
		grid_File.setStore(clearStore); 	
		console.log("I'M HERE");				
	}
	
	else {								    			    
		prevfeature = null;
		console.log("I'M HERE TOO");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_File = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_File.push(feature.attributes);  
		
			//Set the infoTemplate.
			// var bil_mangsa_list = feature.attributes.bil_mangsa_daftar;
			// var turun_senarai = '';
			// if (bil_mangsa_list > 0) turun_senarai = "<a href='assets/flood_report/victim_list.xls' target='blank'>Download list</a>";
			// else turun_senarai = "-Tiada-";
				
			var info_file = new esri.InfoTemplate();
			info_file.setTitle("ktpk");
			info_file.setContent("No. Fail Pemohonan: ${KPKT} <br> Projek: ${CATATAN} <br> Tarikh Permohonan: ${PERMOHONAN} <br> Tanah Asal : ${TANAH_ASAL} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${SEMENTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNITLUAS} <br> Status : ${HAKMILIK1} <br> NEGERI: ${NEGERI} <br> DAERAH: ${DAERAH} <br> MUKIM : ${MUKIM} <br> SEKSYEN : ${SEKSYEN} <br> LOT : ${LOT} <br> UPI : ${UPI} <br> PA : ${PA}");

			// info_file.setContent(
			// 						"<table id='info'>" +
			// 							"<tr><td><b>LOT</b></td><td><b>: </b>${LOT}</td></tr>" +
			// 							"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
			// 						"</table>" 
			//  ); 
			graphic.setInfoTemplate(info_file); 
			//console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gFile = evt.graphic;
          map.infoWindow.setContent(gFile.getContent());
          map.infoWindow.setTitle(gFile.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_File = {
			identifier : "OBJECTID_1", //This field needs to have unique values
			label : "OBJECTID_1", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_File
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_File,
				clearOnClose: true
		});
												
		grid_File.set("store", store);
		
		// Clear previous search results
		if(grid_File.store.save) { grid_File.store.save();}
		grid_File.store.close();				
		grid_File._refresh(); // or grid.store.fetch();								
		grid_File.filter();
		
		grid_File.on("rowClick", onRowClickHandler_grid_File);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_File(evt){
	var grid = dijit.byId("gridFile");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID_1;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID_1 == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_Project(){
	dojo.byId('select_service_file').value = "Select service...";
	dojo.byId('select_state_file').value = "Select state...";
	dojo.byId('ktpk').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridFile");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridFile"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultFile').hide();
	
}