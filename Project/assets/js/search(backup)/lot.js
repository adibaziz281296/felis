/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;
 
// Create Victim Centre result grid
var gridLot_Structure =[[
	// declare your structure here
	{name: 'LOT', field: 'LOT', width: '30%'},
	{name: 'STATE', field: 'NEGERI'},
	{name: 'CITY', field: 'DAERAH'},	
	{name: 'SUB DISTRICT', field: 'MUKIM'}
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_lot(results) {
	dijit.byId("select_district_lot").reset();
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.NEGERI
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_lot").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		
// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_lot(results) {
	dijit.byId("select_district_lot").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DAERAH;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_lot").set('store', store);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_lot(results) {
	dijit.byId("select_mukim_lot").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukim = feature.attributes.MUKIM;
		if (!testVals[mukim]) {
			testVals[mukim] = true;
			values.push({name:mukim, id:mukim});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_lot").set('store', store);
}

// Victim Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Victim Centre: Execute Search
function executeQueryTask_Lot() { console.log("execute task search LOT");
	// Check if all values have been selected
	var service = dojo.byId('select_service_lot').value;
	// alert(service);
	if (service != 'Select service...') {
		var state = dojo.byId('select_state_lot').value;
		var district = dojo.byId('select_district_lot').value;
		var mukim = dojo.byId('select_mukim_lot').value;
		var seksyen_name = dojo.byId('seksyen_name').value;
		var lot_number = dojo.byId('lot_number').value;
		
		console.log(state);
		console.log(district);
		console.log(mukim);
		console.log(seksyen_name);
		console.log(lot_number);

		var kes;
		
		if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number == "") kes = 1; // state
		else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number == "") kes = 2; // state, district
		else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && seksyen_name == "" && lot_number == "") kes = 3; // state, district, mukim
		else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && seksyen_name != "" && lot_number == "") kes = 4; // state, district, mukim, seksyen
		else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && seksyen_name != "" && lot_number != "") kes = 5; // state, district, mukim, seksyen, lot number
		else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number == "") kes = 6; // state, seksyen
		else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number != "") kes = 7; // state, lot number
		else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number != "") kes = 8; // state, seksyen, lot number
		else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number == "") kes = 9; // state, district, seksyen
		else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number != "") kes = 10; // state, district, seksyen, lot number
		else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number != "") kes = 11; // state, district, lot number
		else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && seksyen_name == "" && lot_number != "") kes = 12; // state, district, mukim, lot number
		else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number == "") kes = 13; // seksyen
		else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name == "" && lot_number != "") kes = 14; // lot number
		else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && seksyen_name != "" && lot_number != "") kes = 15; // seksyen, lot number
		else kes = 'default';
		
		console.log("kes = "+ kes);
		
		switch (kes){
			
			case 1:
				query_Lot.where = "NEGERI = '" + state  + "' AND LOT <> ''";
			break;
			
			case 2:
				query_Lot.where = "NEGERI = '" + state  + "'  AND daerah = '" + district + "' AND LOT <> ''";
			break;
			
			case 3:
				query_Lot.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND LOT <> ''";
			break;

			case 4:
				query_Lot.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND SEKSYEN LIKE '%" + seksyen_name + "%' AND LOT <> ''";
			break;

			case 5:
				query_Lot.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND SEKSYEN LIKE '%" + seksyen_name + "%' AND LOT LIKE '%" + lot_number + "%'";
			break;

			case 6:
				query_Lot.where = "NEGERI = '" + state  + "' AND SEKSYEN LIKE '%" + seksyen_name + "%' AND LOT <> ''";
			break;

			case 7:
				query_Lot.where = "NEGERI = '" + state  + "' AND LOT LIKE '%" + lot_number + "%'";
			break;

			case 8:
				query_Lot.where = "NEGERI = '" + state  + "' AND SEKSYEN LIKE '%" + seksyen_name + "%' AND LOT LIKE '%" + lot_number + "%'";
			break;

			case 9:
				query_Lot.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND SEKSYEN LIKE '%" + seksyen_name + "%' AND LOT <> ''";
			break;

			case 10:
				query_Lot.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND SEKSYEN LIKE '%" + seksyen_name + "%' AND LOT LIKE '%" + lot_number + "%'";
			break;

			case 11:
				query_Lot.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND LOT LIKE '%" + lot_number + "%'";
			break;

			case 12:
				query_Lot.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND LOT LIKE '%" + lot_number + "%'";
			break;
			
			case 13:
				query_Lot.where = "SEKSYEN LIKE '%" + seksyen_name + "%' AND LOT <> ''";
			break;
			
			case 14:
				query_Lot.where = "LOT LIKE '%" + lot_number + "%'";
			break;
			
			case 15:
				query_Lot.where = "SEKSYEN LIKE '%" + seksyen_name + "%' AND LOT LIKE '%" + lot_number + "%'";
			break;

			default:
				query_Lot.where = "LOT <> ''";
			break;		
		}
		
		console.log(query_Lot.where);

		// Execute query
		queryTask_Lot.execute(query_Lot, showResults_Lot);
		showResultpane_Lot();
	}
	else
	{
		alert('Please select service!!!');
	}
} 

function showResultpane_Lot() {  
			dijit.byId('searchResultLot').show();

			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
}

// Lot: Show Search Result			
function showResults_Lot(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Lot = dijit.byId("gridLot");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Lot.set("noDataMessage", 'Sorry, there are no results.');
		grid_Lot.setStore(clearStore); 	
		console.log("I'M HERE");				
	}
	
	else {								    			    
		prevfeature = null;
		console.log("I'M HERE TOO");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_Lot = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_Lot.push(feature.attributes);  
		
			//Set the infoTemplate.
			// var bil_mangsa_list = feature.attributes.bil_mangsa_daftar;
			// var turun_senarai = '';
			// if (bil_mangsa_list > 0) turun_senarai = "<a href='assets/flood_report/victim_list.xls' target='blank'>Download list</a>";
			// else turun_senarai = "-Tiada-";
				
			var info_lot = new esri.InfoTemplate();
			info_lot.setTitle("Lot");
			info_lot.setContent("No. Fail Pemohonan: ${KPKT} <br> Projek: ${CATATAN} <br> Tarikh Permohonan: ${PERMOHONAN} <br> Tanah Asal : ${TANAH_ASAL} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${SEMENTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNITLUAS} <br> Status : ${HAKMILIK1} <br> NEGERI: ${NEGERI} <br> DAERAH: ${DAERAH} <br> MUKIM : ${MUKIM} <br> SEKSYEN : ${SEKSYEN} <br> LOT : ${LOT} <br> UPI : ${UPI} <br> PA : ${PA}");

			// info_lot.setContent(
			// 						"<table id='info'>" +
			// 							"<tr><td><b>LOT</b></td><td><b>: </b>${LOT}</td></tr>" +
			// 							"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
			// 						"</table>" 
			//  ); 
			graphic.setInfoTemplate(info_lot); 
			//console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gLot = evt.graphic;
          map.infoWindow.setContent(gLot.getContent());
          map.infoWindow.setTitle(gLot.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_Lot = {
			identifier : "OBJECTID_1", //This field needs to have unique values
			label : "OBJECTID_1", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Lot
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Lot,
				clearOnClose: true
		});
												
		grid_Lot.set("store", store);
		
		// Clear previous search results
		if(grid_Lot.store.save) { grid_Lot.store.save();}
		grid_Lot.store.close();				
		grid_Lot._refresh(); // or grid.store.fetch();								
		grid_Lot.filter();
		
		grid_Lot.on("rowClick", onRowClickHandler_grid_Lot);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Lot(evt){
	var grid = dijit.byId("gridLot");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID_1;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID_1 == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_Lot(){
	dojo.byId('select_service_lot').value = "Select service...";
	dojo.byId('select_state_lot').value = "Select state...";
	dojo.byId('select_district_lot').value = "Select district...";
	dojo.byId('select_mukim_lot').value = "Select sub district...";
	dojo.byId('seksyen_name').value = "";
	dojo.byId('lot_number').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridLot");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridLot"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultLot').hide();
	
}