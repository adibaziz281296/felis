/**
 * @author Fadhilah, Nov 2015
 * Search Emergency Centre Props
 */
var prevfeature = null;
 
// Create Emergency Centre result grid
var gridEmergency_Structure =[[
	// declare your structure here
	{name: 'Name', field: 'Nama', width: '40%'},
	{name: 'Category', field: 'Kategori'},
	{name: 'City', field: 'Daerah'},
]];

// Emergency Centre: REGION Dropdown 			
function populateList_state_emergency(results) {
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.STATE
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_emergency").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		
// Emergency Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_emergency(results) {
	dijit.byId("select_district_emergency").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DISTRICT;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_emergency").set('store', store);
}

// Emergency Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Emergency Centre: Execute Search
function executeQueryTask_EmergencyCentre() { 
	// Check if all values have been selected
	var state = dojo.byId('select_state_emergency').value;
	var district = dojo.byId('select_district_emergency').value;
	var emergency_name = dojo.byId('emergency_name').value;
	
	console.log(state);
	console.log(district);
	console.log(emergency_name);
	
	var kes;
	
	if(state == "Select state..." && district == "Select district..." && emergency_name == "") kes = 1; // none
	else if(state != "Select state..." && district == "Select district..." && emergency_name == "") kes = 2; // state
	else if(state != "Select state..." && district != "Select district..." && emergency_name == "") kes = 3; // state, district
	else if(state != "Select state..." && district == "Select district..." && emergency_name != "") kes = 4; // state & emergency
	else if(state != "Select state..." && district != "Select district..." && emergency_name != "") kes = 5; // ALL
	else kes = 'default';
	
	console.log(kes);
	
	switch (kes){
		case 1:
			query_EmergencyCentre.where = "Nama <> ''";
		break;
		
		case 2:
			query_EmergencyCentre.where = "negeri = '" + state  + "'";
		break;
		
		case 3:
			query_EmergencyCentre.where = "negeri = '" + state  + "' AND daerah = '" + district + "'";
		break;
		
		case 4:
			query_EmergencyCentre.where = "negeri = '" + state  + "' AND Nama LIKE '%" + emergency_name + "%'";
		break;
				
		case 5:
			query_EmergencyCentre.where = "negeri = '" + state  + "' AND Daerah = '" + district + "' AND Nama LIKE '%" + emergency_name + "%'";
		break;
		
		default:
			query_EmergencyCentre.where = "nama <> ''";
		break;		
	}
	
	// Execute query
	console.log(query_EmergencyCentre.where);
	queryTask_EmergencyCentre.execute(query_EmergencyCentre,showResults_EmergencyCentre);
	showResultpane_EmergencyCentre();
} 

function showResultpane_EmergencyCentre() {  
			dijit.byId('searchResultEmergency').show();
			
			dijit.byId('searchFormEvacuation').hide(); 
			dijit.byId('searchFormEmergency').hide();
			dijit.byId('searchFormFacility').hide();
			dijit.byId('searchFormVictim').hide();
}

// Emergency Centre: Show Search Result			
function showResults_EmergencyCentre(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_EmergencyCentre = dijit.byId("gridEmergency");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_EmergencyCentre.set("noDataMessage", 'Sorry, there are no results.');
		grid_EmergencyCentre.setStore(clearStore); 					
	}
	
	else {								    			    
		prevfeature = null;
	
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_EmergencyCentre = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_EmergencyCentre.push(feature.attributes);  
		
			//Set the infoTemplate.
			var info_emergency = new esri.InfoTemplate();
			info_emergency.setTitle("Emergency Response Center");
			info_emergency.setContent(
									"<table id='info'>" +
										"<tr><td><b>NAME</b></td><td><b>: </b>${Nama}</td></tr>" +
										"<tr><td><b>CATEGORY</b></td><td><b>: </b>${Kategori}</td></tr>" +
										"<tr><td><b>CITY</b></td><td><b>: </b>${Daerah}</td></tr>" + 
										"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
										"<tr><td colspan='2' align='center'><img src='/assets/img/Kecemasan/${Gambar}' width='100%' height='50%'/></td></tr>" +
									"</table>" 
			 ); 
			 			/*
			var info_EmergencyCentre = new esri.InfoTemplate();
			info_EmergencyCentre.setTitle("Pusat Pemindahan Sementara");
			info_EmergencyCentre.setContent(
					"<table id='info'>" +
				        "<tr><td><b>Nama</b></td><td><b>: </b>${nama}</td></tr>" +
						"<tr><td><b>Kategori</b></td><td><b>: </b>${kategori}</td></tr>" +
				    "</table>" 
			); */
			
			graphic.setInfoTemplate(info_emergency); 
			//console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gEmergency = evt.graphic;
          map.infoWindow.setContent(gEmergency.getContent());
          map.infoWindow.setTitle(gEmergency.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
		
									
		//Create data object to be used in store
		var data_EmergencyCentre = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_EmergencyCentre
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_EmergencyCentre,
				clearOnClose: true
		});
												
		grid_EmergencyCentre.set("store", store);
		
		// Clear previous search results
		if(grid_EmergencyCentre.store.save) { grid_EmergencyCentre.store.save();}
		grid_EmergencyCentre.store.close();				
		grid_EmergencyCentre._refresh(); // or grid.store.fetch();								
		grid_EmergencyCentre.filter();
		
		grid_EmergencyCentre.on("rowClick", onRowClickHandler_grid_EmergencyCentre);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_EmergencyCentre(evt){
	console.log('prevfeature = '+ prevfeature);	
	var grid = dijit.byId("gridEmergency");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID;
	console.log('clickedId = ' + clickedId);
	var selectedId;	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;			
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_EmergencyCentre(){
	dojo.byId('select_state_emergency').value = "Select state...";
	dojo.byId('select_district_emergency').value = "Select district...";
	dojo.byId('emergency_name').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	clearQueryTask("gridEmergency");
	dijit.byId('searchResultEmergency').hide();
	
}