<?php
	include("../../assets/conn/sql_server.php");

	# RETRIEVE STATE LIST	-------------------------------------------------------------------------------
	$sql_state = "SELECT DISTINCT(state_code), STATE
					FROM sde.MUKIM
					ORDER BY STATE"; 
	$params_state  = array();
	$options_state  =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_state  = sqlsrv_query($conn, $sql_state, $params_state , $options_state);	
  
	if( $stmt_state  === false ) { print( print_r( sqlsrv_errors() ) ); }

	# RETRIEVE SEX LIST	-------------------------------------------------------------------------------
	$sql_sex = "SELECT *
					FROM JANTINA";
	$params_sex  = array();
	$options_sex  =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_sex  = sqlsrv_query($conn, $sql_sex, $params_sex, $options_sex);	
  
	if( $stmt_sex  === false ) { print( print_r( sqlsrv_errors() ) ); }
	
	# RETRIEVE BANGSA LIST	-------------------------------------------------------------------------------
	$sql_race = "SELECT *
					FROM BANGSA";
	$params_race  = array();
	$options_race  =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_race  = sqlsrv_query($conn, $sql_race, $params_race, $options_race);	
  
	if( $stmt_race  === false ) { print( print_r( sqlsrv_errors() ) ); }
	
	
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Daftar Mangsa Banjir</title>               
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
                        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                 
	<script language="javascript" src="funcs/val2.js"></script>	
	<script language="javascript" type="text/javascript" src="http://code.jquery.com/jquery-1.6.2.min.js"></script>
	<script>
		function getOptions() {
			make=document.getElementById("state").value; //replace with where you actually get the make from
			//alert(make);
			var xmlRequest = new XMLHttpRequest();
			xmlRequest.onreadystatechange=function(){
				if (xmlRequest.readyState==4 && xmlRequest.status==200) {
					document.getElementById("tableContainerDistrict").innerHTML = xmlRequest.responseText;
					//someSelector is the ID of your <select> tag.
				}
			}
			xmlRequest.open("GET","getOptions_district.php?state="+make,true);
			xmlRequest.send();
		}
		
		function getOptionsMukim() {
			make=document.getElementById("district").value; //replace with where you actually get the make from
			//alert(make);
			var xmlRequest = new XMLHttpRequest();
			xmlRequest.onreadystatechange=function(){
				if (xmlRequest.readyState==4 && xmlRequest.status==200) {
					document.getElementById("tableContainerMukim").innerHTML = xmlRequest.responseText;
					//someSelector is the ID of your <select> tag.
				}
			}
			xmlRequest.open("GET","getOptions_mukim.php?district="+make,true);
			xmlRequest.send();
		}
		
		function getOptionsEvacuation() { 
			make=document.getElementById("mukim").value; //replace with where you actually get the make from
			//alert(make);
			var xmlRequest = new XMLHttpRequest();
			xmlRequest.onreadystatechange=function(){
				if (xmlRequest.readyState==4 && xmlRequest.status==200) {
					document.getElementById("tableContainerEvacuation").innerHTML = xmlRequest.responseText;
					//someSelector is the ID of your <select> tag.
				}
			}
			xmlRequest.open("GET","getOptions_evacuation.php?mukim="+make,true);
			xmlRequest.send();
		}

		/* $(function() {
				/*$("#state").change(function() { 
				//$("#state").on('change', '.inline', function(e) {
					//$("#district").append("getOptions.php?state=" + $("#state").val());
					//$('select[name="model"]').html('<option value="' + make + '">' + make + '</option>')
					//$("#district").html('<option value="' + make + '">' + make + '</option>')
					 $.get('getOptions.php?state='+ $("#state").val(), function(result){
						$('#district').html(result);
					});
				});*/
				
				/*$("#state").change(function() { alert("masuk jquery");
					$("#second-choice").load("getOptions_district.php?state=" + $("#state").val());
				});
				
				$("#state").bind("change", function() { alert($("#state").val());
					$.ajax({
						type: "GET", 
						url: "getOptions_district.php",
						data: "state="+$("#state").val(),
						success: function(html) {
							$("#tableContainerDistrict").html(html);
							//$('select#district2').html(html); 
						}
					});
				});
				
				$("#district").bind("change", function() { alert($("#district").val());
					$.ajax({
						type: "GET", 
						url: "getOptions_mukim.php",
						//data: "state="+$("#state").val()+"&district="+$("#district").val(),
						data: "district="+$("#district").val(),
						success: function(html) {
							$("#tableContainerMukim").html(html);
							//$('select#district2').html(html); 
						}
					});
				});

		  }); */
	</script>
    </head>
    <body>           
            <!-- PAGE CONTENT -->
            <div class="page-content">                             
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" name="form" onSubmit="return val_add(this, 'add_val.php')" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Borang Kemasukan Mangsa</h3>   
                                </div>
                                <!--<div class="panel-body">
                                    <p>This is non libero bibendum, scelerisque arcu id, placerat nunc. Integer ullamcorper rutrum dui eget porta. Fusce enim dui, pulvinar a augue nec, dapibus hendrerit mauris. Praesent efficitur, elit non convallis faucibus, enim sapien suscipit mi, sit amet fringilla felis arcu id sem. Phasellus semper felis in odio convallis, et venenatis nisl posuere. Morbi non aliquet magna, a consectetur risus. Vivamus quis tellus eros. Nulla sagittis nisi sit amet orci consectetur laoreet. Vivamus volutpat erat ac vulputate laoreet. Phasellus eu ipsum massa.</p>
                                </div> -->
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Nama Mangsa</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="fullName"/>
                                            </div>                                            
                                            <span class="help-block">Seperti di dalam MyKad</span>
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">No. MyKad/MyKid</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="icNo"/>
                                            </div>                                            
                                            <span class="help-block">No MyKad/Kad Pengenalan tanpa tanda '-'</span>
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Umur</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="age"/>
                                            </div>                                            
                                            <span class="help-block">Umur Mangsa</span>
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Jantina</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <select class="form-control select" name="sex">
                                                <option value="0">Pilih jantina...</option>
											<?php																						  
											  while( $row_sex = sqlsrv_fetch_array( $stmt_sex, SQLSRV_FETCH_ASSOC) ) { 	
											?>
                                                <option value="<?php echo $row_sex['jantinaNama'] ?>"><?php echo $row_sex['jantinaNama'] ?></option>
											<?php } ?>
                                            </select>                                     
                                            <span class="help-block">Jantina Mangsa</span>
                                        </div>
                                    </div>
									
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Bangsa</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" name="race">
                                                <option value="0">Pilih bangsa...</option>
											<?php																						  
											  while( $row_race = sqlsrv_fetch_array( $stmt_race, SQLSRV_FETCH_ASSOC) ) { 	
											?>
                                                <option value="<?php echo $row_race['bangsaNama'] ?>"><?php echo $row_race['bangsaNama'] ?></option>
											<?php } ?>
                                            </select>
                                            <span class="help-block">Bangsa mangsa</span>
                                        </div>
                                    </div>

									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Status OKU</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                             <select class="form-control select" name="status">
                                                <option value="0">Pilih status...</option>
	                                            <option value="Normal">Normal</option>
												<option value="OKU">OKU</option>
                                            </select>                                           
                                            <span class="help-block">Mangsa Normal atau OKU</span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">No. Telefon</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="no_tel"/>
                                            </div>     
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Negeri</label>
                                        <div class="col-md-6 col-xs-12">			
										<select class="form-control select" name="state" id="state" onchange="getOptions();">
										  <option selected value="0">Pilih negeri...</option>
										  <?php																						  
											  while( $row_state = sqlsrv_fetch_array( $stmt_state, SQLSRV_FETCH_ASSOC) ) { 	
											?>
                                                <option value="<?php echo $row_state['state_code'] ?>"><?php echo $row_state['STATE'] ?></option>
											<?php } ?>
										</select>
	                                            <span class="help-block">Negeri tempat pendaftaran mangsa</span>
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Daerah</label>
                                        <div class="col-md-6 col-xs-12" id = 'tableContainerDistrict'>         		
										<!--<select id="second-choice">
										  <option>Please choose from above</option>
										</select> -->                                                                                  
											<select class="form-control select" name="district2" id="district2">
                                                <option value="0">Pilih daerah...</option>
                                            </select> 
											
											<div ></div>
                                            <span class="help-block">Daerah tempat pendaftaran mangsa</span>
                                        </div>
										
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Mukim</label>
                                        <div class="col-md-6 col-xs-12" id = 'tableContainerMukim'>                                                                                            
                                            <select class="form-control select" name="mukim2" id="mukim2">
                                                <option value="0">Pilih mukim...</option>
                                            </select>
                                            <span class="help-block">Mukim tempat pendaftaran mangsa</span>
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Pusat Penempatan Sementara</label>
                                        <div class="col-md-6 col-xs-12" id = 'tableContainerEvacuation'>                                                                                            
                                            <select class="form-control select" name="evacuation2" id="evacuation2">
                                                <option value="0">Pilih pusat...</option>
                                            </select>
                                            <span class="help-block">Pusat penempatan di mana mangsa ditempatkan</span>
                                        </div>
                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <!-- <button type="submit" class="btn btn-default">Bersihkan Borang</button> -->                                   
                                    <button type="submit" class="btn btn-info pull-right">Daftar Masuk</button>
                                </div>						
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->             
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>                
        <!-- END PLUGINS -->
        
        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <!-- END THIS PAGE PLUGINS -->       
        
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/settings.js"></script>
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->                   
    </body>
</html>






