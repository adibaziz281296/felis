<?php
session_start();
$state = $_SESSION['state'];
$state_code = $_SESSION['state_code'];
$category = $_SESSION['category'];

// $state = "JOHOR";
// $state_code = "01";
// $category = "Staff";
// echo $state_code;
include('../sql_server_db.php');

$sql = "SELECT * FROM location WHERE state = '$state_code'";

$result = sqlsrv_query( $conn, $sql);
if( $result === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

while($row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )){	
	$latitude = $row['latitude'];
	$longitude = $row['longitude'];
	$zoom = $row['zoom'];
}
$phpToJsVars = array(
  'value1' => $latitude,
  'value2' => $longitude,
  'value3' => $zoom,
  'value4' => $state_code,
  'value5' => $category
   );
?>
<!DOCTYPE html>
<html>
  <head>
	<title>MyeTaPP-GIS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	
	<!-- Bootstrap core CSS -->
	<link href="assets/css/gis/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Custom styles for this template -->
	<link href="assets/css/gis/simple-sidebar.css" rel="stylesheet">

	<!-- Reference the ArcGIS API for CSS -->
	<!--<link rel="stylesheet" href="http://js.arcgis.com/3.14/dijit/themes/claro/claro.css">-->
	<!-- <link rel="stylesheet" type="text/css" href="http://serverapi.arcgisonline.com/jsapi/arcgis/3.1/js/esri/dijit/css/Popup.css"> -->
    <link rel="stylesheet" href="https://js.arcgis.com/3.14/esri/css/esri.css">
	<link rel="stylesheet" type="text/css" href="//js.arcgis.com/3.7/js/dojo/dijit/themes/claro/claro.css">
	<link rel="stylesheet" type="text/css" href="toc/2.10/src/agsjs/css/agsjs.css" />

	<link rel="stylesheet" type="text/css" href="css/modal.css" />	
	
	<style>
		 @import url("https://js.arcgis.com/3.10/js/dojo/dijit/themes/claro/claro.css");
		 @import url("css/FloatingPane.css");
		 @import url("css/ResizeHandle.css");
		 @import url("css/EnhancedGrid.css");


		.upperRight {
        position: absolute;
        bottom: 20px;
        right: 20px;
        z-index: 40;
        width: 300px;
      }
      
    </style>

	
	<!-- Custom info popup style to overwrite the default info popup style -->
    <link rel="stylesheet" type="text/css" href="assets/css/gis/modernGrey.css">
	
	<!-- Custom widget style to overwrite the default widget style -->
	<link rel="stylesheet" type="text/css" href="assets/css/gis/widget.css">
	
	<!-- Growler css -->
	<link rel="stylesheet" type="text/css" href="widget/Growler.css">
	
	<!--for uploading flood report file -->
	<script language="javascript" src="report/funcs/val.js"></script>
	
 	<script>
		var dojoConfig = {
		isDebug: true,
		parseOnLoad: true,
		foo: "bar"
		};
	</script> 
	<!-- Load dojo and provide config via data attribute for TOC -->
	<!--script type="text/javascript">
		// Registers the correct location of the "demo" package so we can load Dojo
		var dojoConfig = { 
			paths: {
				//if you want to host on your own server, download and put in folders then use path like: 
				agsjs: location.pathname.replace(/\/[^/]+$/, '') + '/toc/2.10/src/agsjs' 
				
			}
		};      
	</script-->
	
	<script type="text/javascript">
		var dojoConfig = {
			packages: [
			{
			  name: "js",
			  location: location.pathname.replace(/[^\/]+$/, '') + "widget"
			},
			{
			  name: "agsjs",
			  location: location.pathname.replace(/\/[^/]+$/, "") + "/toc/2.10/src/agsjs"
			}
			]
		  };
	</script>

	<script type="text/javascript">
	var phpVars = { 
	<?php 
	  foreach ($phpToJsVars as $key => $value) {
	    echo '  ' . $key . ': ' . '"' . $value . '",' . "\n";  
	  }
	?>
	};
	</script>
	
	<script>
	
		// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
	
	
	</script>
	
	<!-- required: dojo.js -->
	<!--<script src="//ajax.googleapis.com/ajax/libs/dojo/1.6.2/dojo/dojo.js" data-dojo-config="parseOnLoad: true"></script> <!-- same as parser.parse(); -->
	
	<!-- for debugging: 
	<script type="text/javascript" src="//dojotoolkit.org/reference-guide/1.6/_static/js_bak/dojox/layout/FloatingPane.js"></script> -->

	<script src="https://ajax.googleapis.com/ajax/libs/dojo/1.6.2/dojo/dojo.xd.js"></script>
	<!-- Reference for StreetView -->
	<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp" type="text/javascript"></script>  -->
	<script src="https://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyDEfWbEK3OTIRtSJhxCW_AmerJQOu9asdw" type="text/javascript"></script>
	
 
	<!-- Reference the ArcGIS API for JavaScript -->
	<script src="https://js.arcgis.com/3.14/"></script>
  </head>
  <body>

	<!-- Small modal -->
        <div class="modal fade bs-example-modal-sm" id="test" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 150px;">
          <div class="modal-dialog modal-sm">
            <div class=" logout-message">
              <div class="modal-body">
              <iframe name="iframelot" src="" width="100%" height="100%" frameborder="0" allowtransparency="true"></iframe>
                <!-- <h3>
                  <i class="fa fa-sign-out text-green"></i> Ready to go?
                </h3>
                <p>Select "Logout" below if you are ready<br> to end your current session.</p>
                <input type="text" name="city" class="results" style="color: black;"> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>        
        <!-- /modals -->

  
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
		  <!-- <ul class="nav navbar-nav navbar-right">
            <li><a ref="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="login-btn"><i class="fa fa-user white"></i>&nbsp;&nbsp;Daftar Masuk</a></li>
          </ul> -->	  
          <ul class="nav navbar-nav">
		   	<li class="dropdown">
				<a  href="#" role="button"  id="basic_legend" style="font-size: 13px;"><i class="fa fa-bars icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Legend Map</a>
			</li>
			
			<li>
				<a  href="#" role="button"  id="basemap" style="font-size: 13px;"><i class="fa fa-map icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Basemap</a>
			</li>
			
            <li class="dropdown">
				<a id="searchDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 13px;"><i class="fa fa-search icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Search <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_lot" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search by No. Lot / No. PT</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_project" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search by Project Name</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_sementara" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search by No. Hak Milik Sementara</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_milik" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search by No. Hak Milik</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_file" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search by JKPTG File Name</a></li>				
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_ndcdb" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search by Lot NDCDB</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_pandcdb" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search by PA For NDCDB</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_indexmap" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search by No. Index Map</a></li>

					</ul>
			</li>

            <li class="dropdown">
				<a id="basicDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"  style="font-size: 13px;"><i class="fa fa-cog icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Basic Functions <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="basic_basemap"><i class="fa fa-map-o"></i>&nbsp;&nbsp;Basemap</a></li> -->
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="basic_locator" style="font-size: 12px;"><i class="fa fa-map-marker" style="font-size: 18px;"></i>&nbsp;&nbsp;Locator Coordinates</a></li>
						<li>
							<a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="street_view" style="font-size: 12px;"><i class="fa fa-street-view" style="font-size: 18px;"></i>&nbsp;&nbsp;Street View</a>
						</li>											
						<li>
							<a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="analysis_measure" style="font-size: 12px;"><i class="fa fa-object-group" style="font-size: 18px;"></i>&nbsp;&nbsp;Measurement</a>
						</li>
						<li>
							<a  href="#" role="button"  id="print" style="font-size: 13px;"><i class="fa fa-print" style="font-size: 19px;"></i>&nbsp;&nbsp;Print</a>
						<li>
							<a href="#" onclick="parent.window.focus();window.print();" id="snap"  data-toggle="collapse" data-target=".navbar-collapse.in" style="font-size: 12px;"><i class="fa fa-camera" style="font-size: 18px;"></i>&nbsp;&nbsp;Snap</a>
						</li>
				    </ul>			
			</li>
			
			<li>
				<a  href="#" role="button"  id="feedback" style="font-size: 13px;"><i class="fa fa-bullhorn icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Feedback</a>
			</li>

			<!--li>
				<a  href="#" role="button"  id="print" style="font-size: 13px;"><i class="fa fa-print icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Print</a>
			</li-->
		
			<!--li>
				<a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" style="font-size: 13px;"><i class="fa fa-print icon-blue" style="font-size: 19px; float: left;"></i><div id="printButton" style="float: right; margin-top: -8px;"></div></a>
			</li-->
			
			<li class="right">
				<a  href="#" role="button" onClick="window.location.reload()" style="font-size: 13px;" title="Refresh Page"><i class="fa fa-refresh" aria-hidden="true" style="font-size: 19px;"></i></a>
			</li>
			
			<li id="oninfo" class="right">
				<a  href="#" role="button" onClick="disabledInfo()" style="font-size: 13px;" title="Info Window"><i class="fa fa-info" aria-hidden="true" style="font-size: 19px;"></i>&nbsp;ON</a>
			</li>
			
			<li id="offinfo" class="right" style="display:none;">
				<a  href="#" role="button" onClick="enabledInfo()" style="font-size: 13px;" title="Info Window"><i class="fa fa-info" aria-hidden="true" style="font-size: 19px;"></i>&nbsp;OFF</a>
			</li>
			
			<!-- <li class="right">
				<label id="print_button" style="margin-top: -10px; margin-bottom: -10px;"></label>
			</li> -->
			<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="extra_street"><i class="fa fa-street-view"></i>&nbsp;&nbsp;StreetView</a></li> -->
          </ul>
        </div>
      </div>
    </nav> 
    <!--<div id="wrapper" class="toggled">-->
	<div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#"><img src="assets/img/arrow.png" width="32px" height="29px" alt="Tutup petunjuk peta"></a>
                </li>
				<li>
					<a href="#">Table of Content</a>
                     <div id="tocCharting"></div>
                     <div id="toc1MalaysiaMap"></div>
                     <div id="tocJKRRoad"></div>
                     <div id="tocIndexMap"></div>
                     <div id="tocLotNDCDB"></div>
                     <div id="tocSempadan"></div>
                </li>
				<br>
            </ul>
			
        </div>
        <!-- /#sidebar-wrapper -->
		
        <!-- Page Content -->
		<div id="mapWrapper">
			<div id="HomeButton"></div>
			<div id="LocateButton"></div>
			<div id="EraseButton" title="Erase graphic"></div>
			<!--div id="BasemapToggle"></div-->
			<span id="coordinate_info" style="color:orange; text-shadow:1px 1px 1px #000;"></span>
			<div id ="SearchAddress"><input id="searchBox" type="text" class="form-control"></div>
			<img id="loadingImg" src="loader/loader.gif" style="position:absolute; right:612px; top:356px; z-index:100;" />
			<div class="upperRight">
				<div id="growlerDijit">
				</div>
			</div>
			
			<!--div style="position:absolute; right:20px; top:10px; z-Index:999;">
				<div data-dojo-type="dijit/TitlePane" 
					 data-dojo-props="title:'Switch Basemap', open:true">
				  <div data-dojo-type="dijit/layout/ContentPane" style="width:380px; height:280px; overflow:auto;">
					<div id="basemapGallery"></div>
				  </div>
				</div>
			 </div-->
			
			
			
		</div>
		
<!--         <div id="page-content-wrapper">
            <!-- <div class="container-fluid-2"> -->
                <!-- <div class="row"> -->
                    <!-- <div class="col-lg-12">  
					<div id="map"></div>	
                        <!-- <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a> -->
                    <!-- </div> -->
                <!-- </div> -->
            <!-- </div> 
       </div>  -->
        <!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->	
	
	<!-- ======================================================================================================== -->
	<!-- 											  WIDGETS 													  -->
	<!-- ======================================================================================================== -->
	
	<!-- ================================================= [ SEARCH ] =========================================== -->
	<!-------------------------------------------- SEARCH: LOT ------------------------------------ -->
	<div id="searchFormLot"  title="Search by No. Lot / No. PT" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:350px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div id="imageLoadingLot"></div>
			<div class="widgetForm">					
				<form>
				  <!-- <div class="form-group">
					<label class="fieldlabel">Service *:</label>
					<select class="form-control" name="color"  id="select_service_lot" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select service...</option>
					<option value="Pemohonan">Permohonan</option>
					<option value="FT">FT</option>
					<option value="QT">QT</option>
					<option value="Rezab">Rezab</option>
					</select>
				  </div> -->
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_lot" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					<?php
					if (strcmp($_SESSION['category'], "Admin") == 0){
						if (strcmp($_SESSION['state_code'], "14") == 0 || strcmp($_SESSION['state_code'], "15") == 0){
						?>
						<option value="16">Wilayah Persekutuan Putrajaya</option>
						<option value="14">Wilayah Persekutuan Kuala Lumpur</option>
						<option value="15">Wilayah Persekutuan Labuan</option>
						<?php	
						}elseif (strcmp($_SESSION['state_code'], "16") == 0) {
						?>

						<?php
						}else{
						?>
						<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
						<?php
						}
					?>

					<?php
					}else{
					?>
					<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
					<?php
					}
					?>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">District *:</label>
					<select  class="form-control" id="select_district_lot" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District *:</label>
					<select  class="form-control" id="select_mukim_lot" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Seksyen:</label>
					<input  class="form-control" value="000" type="text" id="seksyen_name" dojoType="dijit/form/TextBox" data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Lot Number:</label>
					<input  class="form-control" type="text" id="lot_number" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_lot" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Lot();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_lot" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Lot();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultLot" title="Search Results for Lot" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable="false" duration="350" dockable="true" dockTo="dock_searchFunctions" style="position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;">
		<div data-dojo-type="dijit/layout/TabContainer" style="width:100%; height:100%;" tabStrip="true">
			<div data-dojo-type="dijit/layout/ContentPane" title="| PERMOHONAN " selected="true">
		    	<div id="gridLot"></div>
		  	</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| QT ">
				<div id="gridLotQT"></div>
			</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| FT ">
			    <div id="gridLotFT"></div>
			</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| RIZAB |">
			    <div id="gridLotRizab"></div>
			</div>
		</div>	
	</div>


	<!-------------------------------------------- SEARCH: PROJECT ------------------------------------ -->
	<div id="searchFormProject"  title="Search by Project Name" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:350px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div id="imageLoadingProject"></div>
			<div class="widgetForm">					
				<form>			  
				 <!-- <div class="form-group">
					<label class="fieldlabel">Service *:</label>
					<select class="form-control" name="color"  id="select_service_project" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select service...</option>
					<option value="FT">FT</option>
					<option value="Pemohonan">Permohonan</option>
					<option value="QT">QT</option>
					<option value="Rezab">Rezab</option>
					</select>
				  </div> -->
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_project" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					<?php
					if (strcmp($_SESSION['category'], "Admin") == 0){
						if (strcmp($_SESSION['state_code'], "14") == 0 || strcmp($_SESSION['state_code'], "15") == 0){
						?>
						<option value="16">Wilayah Persekutuan Putrajaya</option>
						<option value="14">Wilayah Persekutuan Kuala Lumpur</option>
						<option value="15">Wilayah Persekutuan Labuan</option>
						<?php	
						}elseif (strcmp($_SESSION['state_code'], "16") == 0) {
						?>

						<?php
						}else{
						?>
						<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
						<?php
						}
					?>

					<?php
					}else{
					?>
					<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
					<?php
					}
					?>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">District *:</label>
					<select  class="form-control" id="select_district_project" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District *:</label>
					<select  class="form-control" id="select_mukim_project" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Kegunaan Tanah:</label>
					<input  class="form-control" type="text" id="tanah" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_tanah" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Project();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button>
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_project" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Project();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<!-- <div id="searchResultProject"  title="Search Results for Project" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;duration=300;">
		<div id="gridProject"></div>
	</div> -->

	<div id="searchResultProject" title="Search Results for Project" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable="false" duration="350" dockable="true" dockTo="dock_searchFunctions" style="position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;">
		<div data-dojo-type="dijit/layout/TabContainer" style="width:100%; height:100%;" tabStrip="true">
			<div data-dojo-type="dijit/layout/ContentPane" title="| PERMOHONAN " selected="true">
		    	<div id="gridProjectPermohonan"></div>
		  	</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| QT ">
				<div id="gridProjectQT"></div>
			</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| FT ">
			    <div id="gridProjectFT"></div>
			</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| RIZAB |">
			    <div id="gridProjectRizab"></div>
			</div>
		</div>	
	</div> 

	<!-------------------------------------------- SEARCH: HAK MILIK SEMENTARA ------------------------------------ -->
	<div id="searchFormSementara"  title="Search by No. Hak Milik Sementara" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:350px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div id="imageLoadingSementara"></div>
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_sementara" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					<?php
					if (strcmp($_SESSION['category'], "Admin") == 0){
						if (strcmp($_SESSION['state_code'], "14") == 0 || strcmp($_SESSION['state_code'], "15") == 0){
						?>
						<option value="16">Wilayah Persekutuan Putrajaya</option>
						<option value="14">Wilayah Persekutuan Kuala Lumpur</option>
						<option value="15">Wilayah Persekutuan Labuan</option>
						<?php	
						}elseif (strcmp($_SESSION['state_code'], "16") == 0) {
						?>

						<?php
						}else{
						?>
						<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
						<?php
						}
					?>

					<?php
					}else{
					?>
					<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
					<?php
					}
					?>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">District *:</label>
					<select  class="form-control" id="select_district_sementara" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District *:</label>
					<select  class="form-control" id="select_mukim_sementara" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">No. Hak Milik Sementara:</label>
					<input  class="form-control" type="text" id="sementara" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_sementara" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Sementara();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_sementara" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Sementara();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<!-- <div id="searchResultSementara"  title="Search Results for Hak Milik Sementara" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;duration=300;">
		<div id="gridSementara"></div>
	</div> -->

	<div id="searchResultSementara" title="Search Results for No Hakmilik Sementara" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable="false" duration="350" dockable="true" dockTo="dock_searchFunctions" style="position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;">
		<div data-dojo-type="dijit/layout/TabContainer" style="width:100%; height:100%;" tabStrip="true">
			<div data-dojo-type="dijit/layout/ContentPane" title="| PERMOHONAN " selected="true">
		    	<div id="gridSementaraPermohonan"></div>
		  	</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| QT ">
				<div id="gridSementaraQT"></div>
			</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| FT ">
			    <div id="gridSementaraFT"></div>
			</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| RIZAB |">
			    <div id="gridSementaraRizab"></div>
			</div>
		</div>	
	</div> 

	<!-------------------------------------------- SEARCH: HAK MILIK ------------------------------------ -->
	<div id="searchFormMilik"  title="Search by No. Hak Milik" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:350px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div id="imageLoadingMilik"></div>
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_milik" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					<?php
					if (strcmp($_SESSION['category'], "Admin") == 0){
						if (strcmp($_SESSION['state_code'], "14") == 0 || strcmp($_SESSION['state_code'], "15") == 0){
						?>
						<option value="16">Wilayah Persekutuan Putrajaya</option>
						<option value="14">Wilayah Persekutuan Kuala Lumpur</option>
						<option value="15">Wilayah Persekutuan Labuan</option>
						<?php	
						}elseif (strcmp($_SESSION['state_code'], "16") == 0) {
						?>

						<?php
						}else{
						?>
						<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
						<?php
						}
					?>

					<?php
					}else{
					?>
					<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
					<?php
					}
					?>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">District *:</label>
					<select  class="form-control" id="select_district_milik" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District(Mukim) *:</label>
					<select  class="form-control" id="select_mukim_milik" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">No. Hak Milik Tetap:</label>
					<input  class="form-control" type="text" id="milik" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_milik" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Milik();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_milik" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Milik();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<!-- <div id="searchResultMilik"  title="Search Results for Hak Milik" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;duration=300;">
		<div id="gridMilik"></div>
	</div> -->

	<div id="searchResultMilik" title="Search Results for No Hakmilik Tetap" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable="false" duration="350" dockable="true" dockTo="dock_searchFunctions" style="position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;">
		<div data-dojo-type="dijit/layout/TabContainer" style="width:100%; height:100%;" tabStrip="true">
			<div data-dojo-type="dijit/layout/ContentPane" title="| PERMOHONAN " selected="true">
		    	<div id="gridMilikPermohonan"></div>
		  	</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| QT ">
				<div id="gridMilikQT"></div>
			</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| FT ">
			    <div id="gridMilikFT"></div>
			</div>
			<div data-dojo-type="dijit/layout/ContentPane" title="| RIZAB |">
			    <div id="gridMilikRizab"></div>
			</div>
		</div>	
	</div> 

	<!-------------------------------------------- SEARCH: FILE NAME ------------------------------------ -->
	<div id="searchFormFile"  title="Search by JKPTG File Name" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:350px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div id="imageLoadingFile"></div>
			<div class="widgetForm">					
				<form>			  
				 <div class="form-group">
					<label class="fieldlabel">Service *:</label>
					<select class="form-control" name="color"  id="select_service_file" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select service...</option>
					<option value="FT">FT</option>
					<option value="Pemohonan">Permohonan</option>
					<option value="QT">QT</option>
					<option value="Rezab">Rezab</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_file" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">File Name:</label>
					<input  class="form-control" type="text" id="ktpk" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_file" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_File();}'>Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_file" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_File();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultFile"  title="Search Results for File Name" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:950px;height:300px;visibility:hidden;duration=300;">
		<div id="gridFile"></div>
	</div>
	
	<!-------------------------------------------- SEARCH: LOT NDCDB ------------------------------------ -->
	<div id="searchFormNDCDB"  title="Search by No. Lot NDCDB" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:350px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div id="imageLoadingNDCDB"></div>
			<div class="widgetForm">					
				<form>
					<div class="form-group">
						<label class="fieldlabel">State *:</label>
						<select class="form-control" name="color"  id="select_state_ndcdb" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
						<option value="0A">Select state...</option>
						<?php
						if (strcmp($_SESSION['category'], "Admin") == 0){
							if (strcmp($_SESSION['state_code'], "14") == 0 || strcmp($_SESSION['state_code'], "15") == 0){
							?>
							<option value="16">Wilayah Persekutuan Putrajaya</option>
							<option value="14">Wilayah Persekutuan Kuala Lumpur</option>
							<option value="15">Wilayah Persekutuan Labuan</option>
							<?php	
							}elseif (strcmp($_SESSION['state_code'], "16") == 0) {
							?>

							<?php
							}else{
							?>
							<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
							<?php
							}
						?>

						<?php
						}else{
						?>
						<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
						<?php
						}
						?>
						</select>
					</div>
					<div class="form-group">
						<label class="fieldlabel">District *:</label>
						<select  class="form-control" id="select_district_ndcdb" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
						<option value="0A" selected>Select district...</option>
						</select>
					</div>
					<div class="form-group">
						<label class="fieldlabel">Sub District(Mukim) *:</label>
						<select  class="form-control" id="select_mukim_ndcdb" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}" >
						<option value="0A" selected>Select sub district...</option>
						</select>
					</div>		  
				  <div class="form-group">
					<label class="fieldlabel">No. Lot NDCDB *:</label>
					<input  class="form-control" type="text" id="ndcdb" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_ndcdb" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_NDCDB();}'>Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_ndcdb" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_NDCDB();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultNDCDB"  title="Search Results for Lot NDCDB" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;duration=300;">
		<div id="gridNDCDB"></div>
	</div>

	<!-------------------------------------------- SEARCH: PA NDCDB ------------------------------------ -->
	<div id="searchFormPANDCDB"  title="Search by PA For NDCDB" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:350px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div id="imageLoadingPANDCDB"></div>
			<div class="widgetForm">					
				<form>
					<div class="form-group">
						<label class="fieldlabel">State *:</label>
						<select class="form-control" name="color"  id="select_state_pandcdb" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
						<option value="0A">Select state...</option>
						<?php
						if (strcmp($_SESSION['category'], "Admin") == 0){
							if (strcmp($_SESSION['state_code'], "14") == 0 || strcmp($_SESSION['state_code'], "15") == 0){
							?>
							<option value="16">Wilayah Persekutuan Putrajaya</option>
							<option value="14">Wilayah Persekutuan Kuala Lumpur</option>
							<option value="15">Wilayah Persekutuan Labuan</option>
							<?php	
							}elseif (strcmp($_SESSION['state_code'], "16") == 0) {
							?>

							<?php
							}else{
							?>
							<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
							<?php
							}
						?>

						<?php
						}else{
						?>
						<option value="<?php echo $state_code; ?>"><?php echo $state; ?></option>
						<?php
						}
						?>
						</select>
					</div>
					<div class="form-group">
						<label class="fieldlabel">District *:</label>
						<select  class="form-control" id="select_district_pandcdb" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
						<option value="0A" selected>Select district...</option>
						</select>
					</div>
					<div class="form-group">
						<label class="fieldlabel">Sub District(Mukim) *:</label>
						<select  class="form-control" id="select_mukim_pandcdb" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}" >
						<option value="0A" selected>Select sub district...</option>
						</select>
					</div>		  
				  <div class="form-group">
					<label class="fieldlabel">PA *:</label>
					<input  class="form-control" type="text" id="pandcdb" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_pandcdb" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_PANDCDB();}'>Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_pandcdb" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_PANDCDB();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultPANDCDB"  title="Search Results for PA NDCDB" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:950px;height:350px;visibility:hidden;duration=300;">
		<div id="gridPANDCDB"></div>
	</div>
	
	<!-------------------------------------------- SEARCH: INDEX MAP ------------------------------------ -->
	<div id="searchFormIndexmap"  title="Search by No. Indexmap" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:350px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div id="imageLoadingIndexmap"></div>
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_indexmap" dojoType="dijit/form/ComboBox" >
					<option value="0A">Select state...</option>
					<option value="0">Johor</option>
					<option value="1">Kedah</option>
					<option value="2">Kelantan</option>
					<option value="3">Melaka</option>
					<option value="4">Negeri Sembilan</option>
					<option value="5">Pahang</option>
					<option value="6">Perak</option>
					<option value="7">Perlis</option>
					<option value="8">Pulau Pinang</option>
					<option value="9">Selangor</option>
					<option value="10">Terengganu</option>
					<option value="11">WP Kuala Lumpur</option>
					<option value="12">WP Putrajaya</option>
					</select>
				  </div>

				  <div class="form-group">
					<label class="fieldlabel">Indexmap No.:</label>
					<input  class="form-control" type="text" id="indexmap" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_indexmap" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Indexmap();}'>Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_indexmap" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Indexmap();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultIndexmap"  title="Search Results for Index Map" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:550px;height:350px;visibility:hidden;duration=300;">
		<div id="gridIndexmap"></div>
	</div>
                

	<!-------------------------------------------- STREET VIEW -------------------------------------->
	<div id="streetFormView" title="Street View" dojoType="dojox.layout.FloatingPane" resizable="true" closable: "true" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:270px;right:50px;width:400px;height:300px;visibility:hidden;">
        <table cellpadding="0" style="width: 100%; height: 100%; margin:0px; padding:0px" border="1">  
            <tr style="height:80%">
	            <td align="center">
	            	
	                <div id="streetview" runat="server" style="border-style: inset; background-color: White; width: 99%; height:99%; margin:0px; padding:0px">
	                </div>
	            </td>
	        </tr>
		</table>    
    </div> 
	
	<script> 
		function Toggle()
		{   
			dijit.byId('streetFormView').hide();
		} 
	</script> 


	
	<!---------------------------------------------- DOCK: SEARCH -------------------------------------- -->
	<div id="dock_searchFunctions" dojoType="dojox.layout.Dock" 
		style= "position:relative; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div>
	
	
	<!-- ============================================ [ BASIC FUNCTIONS ] ========================================= -->
	<!---------------------------------------------- BASEMAP GALLERY -------------------------------------- -->	
	<div id="basemapGalleryPane"  title="Basemap" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:140px;right:20px;width:270px;height:350px;visibility:hidden;">
		<div id="basemapGallery"></div>
	</div>
	
	<!---------------------------------------------- PRINT -------------------------------------- -->
	<div id="printPane"  title="Print" dojoType="dojox.layout.FloatingPane" resizable="true"  
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:140px;right:20px;width:270px;height:230px;visibility:hidden;">
		 <div class="widgetWrapper">
		 <div id="imageLoadingPrint"></div>
		<div class="widgetForm">					
				<form>			  				 
				  <div class="form-group">
					<label class="fieldlabel">Title:</label>
					<textarea maxlength="25" class="form-control" cols="33" rows="5" id="pdfTitle"></textarea>
				  </div>
				  
				  <h6><b>Notes:</b> <i>If print did'nt work please allow 'Pop Up' in web browser</i></h6>
				   <button type="button" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props="onClick:function(){myPDF();}">Set Map Title</button><br><br> 
  
				  <div class="form-group">
					<div id="print_button" style="float: left; margin-top: -8px;"></div>
				  </div>
				  
				  <!--div class="btn btn-success">
					<button  type="button" id="printbut" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ masukPrint();}'>Print</button>
				  </div-->
				  
				 
				</form>
			</div>
			</div>

	</div>
	
	<!---------------------------------------------- BOOKMARK -------------------------------------- -->	
	<!-- <div id="bookmarkPane"  title="Tanda Buku" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:150px;right:25px;width:230px;height:350px;visibility:hidden;">		
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
					<div id="bookmarkDiv"></div>
				</form>
			</div>		
		</div>
	</div> -->
		 
	<!---------------------------------------------- LOCATOR -------------------------------------- -->	
	<div id="locatorPane"  title="Locator Coordinates" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:270px;right:910px;width:270px;height:270px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">X Value(Longitude):</label>
					<input  class="form-control" type="text" id="longitude" dojoType="dijit/form/TextBox" placeholder="Coordinate X" />
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Y Value(Latitude):</label>
					<input  class="form-control" type="text" id="latitude" dojoType="dijit/form/TextBox"  placeholder="Coordinate Y" />
				  </div>				 	
				  <div class="btn btn-primary">
					<button type="button"  id="search_coordinate" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_coordinate" dojoType="dijit/form/Button" data-dojo-attach-point="button">Clear</button>
				  </div>				   
				</form>
			</div>
		</div>
	</div>

	
	
		<!---------------------------------------------- MEASURE -------------------------------------- -->	
	<div id="measurePane"  title="Measurement" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:270px;right:30px;width:270px;height:270px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
					<div id="measurementDiv"></div>	
					<br><br>
					<div class="btn btn-success">
						<button  type="button" id="clearMeasure" dojoType="dijit/form/Button" data-dojo-attach-point="button">Clean graphics</button>
					</div>
				</form>
			</div>		
		</div>
	</div>

	
	<!---------------------------------------------- DOCK: BASIC -------------------------------------- -->
	<div id="dock_basicFunctions" dojoType="dojox.layout.Dock" 
		style= "position:absolute; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div>
	
	
	<!-- ============================================== [ ANALYSIS ] =============================================== -->
	
	
	
	<!---------------------------------------------- DOCK: ANALYSIS -------------------------------------- -->
	<div id="dock_analysisFunctions" dojoType="dojox.layout.Dock" 
		style= "position:absolute; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div>	
	
		
	<!-- ============================================== [ REPORT ] =============================================== -->
	<!---------------------------------------------- UPLOAD REPORT FILE -------------------------------------- -->	
	<!--<iframe name="votar" style="display:none;"></iframe>
	<div id="uploadReportPane"  title="Muatnaik Laporan" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_reportFunctions" style= "position:absolute;top:130px;right:20px;width:270px;height:270px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">				
				<form id="form" name="form" onSubmit="return val_upload(this, 'flood_report/upload_val.php')" method="post" enctype="multipart/form-data"target="votar">	
				  <div class="form-group">
					<label class="fieldlabel">Tajuk Laporan:</label>
					<input  class="form-control" type="text" id="report_name" name="report_name" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"/>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Fail Laporan Untuk Dimuatnaik:</label>
					<input  class="form-control" type="file" id="fileSelect" name="fileSelect" dojoType="dijit/form/TextBox"/>
				  </div>			 	
				  <div class="btn btn-primary">
					<button type="submit"  id="uploadBtn" dojoType="dijit/form/Button" data-dojo-attach-point="button">Muatnaik Fail</button> 	
				  </div>				   
				</form>
			</div>
		</div>
	</div>	-->
	
	<!---------------------------------------------- DOCK: REPORT -------------------------------------- -->
	<!-- <div id="dock_reportFunctions" dojoType="dojox.layout.Dock" 
		style= "position:absolute; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div> -->
	
<!-- Trigger/Open The Modal -->


<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>
	
	

	<!-- ============================================== [ EXTRA ] =============================================== -->	
	<!---------------------------------------------- SOCIAL MEDIA -------------------------------------- -->
	<!---------------------------------------------- STREETVIEW -------------------------------------- -->
	<div id="street_panel"  title="StreetViewPanel" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_extraFunctions" style= "position:absolute;top:120px;left:220px;width:40%;height:550px;visibility:hidden;">		
		<table cellpadding="0" style="width: 100%; height: 100%; margin:0px; padding:0px">  
			<tr style="height:80%">
			<!--td align="center">
				<div id="streetview" runat="server" style="border-style: inset; background-color: White; width: 99%; height:99%; margin:0px; padding:0px">
				</div>
			</td-->
			</tr>
			<tr style="height:20%">
				<td align="center" valign="top">
					<div id="Desc" style="border-style: single; overflow: auto; width: 98%; height: 98%; padding:0px; margin:0px">
					</div>
				</td>
			</tr>
			<tr style="height:5%"></tr>
		</table>
	</div>	
	
	<!---------------------------------------------- DOCK: EXTRA-------------------------------------- -->
	<div id="dock_extraFunctions" dojoType="dojox.layout.Dock" 
		style= "position:absolute; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div>
	
	<!-- =========== Bootstrap core JavaScript =========== -->
    <!-- Placed at the end of the document so the pages load faster -->	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/css/gis/bootstrap.min.js"></script>
	
	<!-- JavaScript for map application -->
	<script src="mapOK2.js"></script>
	<!-- <script src="identifyTask.js"></script> -->

		<!-- Reference the Query/Search JavaScript functions -->
	<script type="text/javascript" src="assets/js/search/global.js"></script>
	<script type="text/javascript" src="assets/js/search/lot.js"></script>
	<script type="text/javascript" src="assets/js/search/project.js"></script>
	<script type="text/javascript" src="assets/js/search/sementara.js"></script>
	<script type="text/javascript" src="assets/js/search/milik.js"></script>
	<script type="text/javascript" src="assets/js/search/file.js"></script>
	<script type="text/javascript" src="assets/js/search/ndcdb.js"></script>
	<script type="text/javascript" src="assets/js/search/pandcdb.js"></script>
	<script type="text/javascript" src="assets/js/search/indexmap.js"></script>

	
	<script type="text/javascript" src="assets/js/form/register_victim.js"></script>
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
