<?php 
# ============================= GET CONNECTED TO DATABASE ============================= 
include("../../assets/conn/sql_server.php"); 

	// AUDIT TRAIL OPEN 
	// include("../../common/auditTrail.php"); //call audit trail function
#======================================================================================

//if(isset($_POST['submit']))
//{

	$fullName = $_POST["fullName"];
	$icNo = $_POST["icNo"];
	$userName = $_POST["userName"];
	$password = md5($_POST["userPassword"]);
	$levelID = $_POST["levelName"];
	
	# Get Timestamp
	date_default_timezone_set('Asia/Kuala_Lumpur');
	$now = date('Y-m-d H:i:s');
	
	
	# Check if username already exist or not
	$sql_usr = "SELECT * FROM PENGGUNA_SISTEM WHERE idSistem = ? AND kataLaluan= ?"; 
	$params_usr = array($userName, $password);
	$options_usr =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_usr = sqlsrv_query($conn, $sql_usr, $params_usr, $options_usr );	
	$row_usr = sqlsrv_fetch_array( $stmt_usr, SQLSRV_FETCH_ASSOC);
	$row_count_usr = sqlsrv_num_rows( $stmt_usr ); 
									
	if($row_count_usr > 0) 
	{ ?>
		<script language = "Javascript">
			alert("ID Pengguna ini telah berdaftar dan sudah digunakan.\nSila pilih ID Pengguna yang lain.")
			window.location = 'info.php?uname=<?php $userName ?>';
		</script>
	<?php 
	}
	else 
	{								
			$parQuery = array($fullName, $icNo, $userName, $password, $levelID, 'Admin', $now,'');
			$insQuery = "INSERT INTO PENGGUNA_SISTEM 
						 VALUES (?, ?, ?, ?, ?, ?, ?, ?)"; 					 
			$resQuery = sqlsrv_query($conn, $insQuery, $parQuery) or die ("Error when register user statement".$insQuery);
			
							
			if ($resQuery){ ?>
				<script language = "Javascript">
					alert("Pengguna <?php echo $fullName ?> telah berjaya didaftarkan.")
					window.location = 'info.php?uname=<?php echo $userName ?>';
				</script>
			<?php }
	}
//}
?>