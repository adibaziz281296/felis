<?php
	include("../../assets/conn/sql_server.php");
	
	# RETRIEVE USERS INFORMATION	-------------------------------------------------------------------------------
	$sql = "SELECT *
			FROM PENGGUNA_SISTEM u, AGENSI a
			WHERE u.agensiID = a.agensiID
			AND u.agensiID <> 'admin'
			ORDER BY penggunaNama"; 
	$params = array();
	$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt = sqlsrv_query($conn, $sql , $params, $options );	
  
	if( $stmt === false ) { print( print_r( sqlsrv_errors() ) ); }
	
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Senarai Pengguna Berdaftar Sistem</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                     
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">           
            <!-- PAGE CONTENT -->
            <div class="page-content">
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Senarai Pengguna Berdaftar Sistem</h3>                              
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>Nama Pegawai</th>
                                                <th>No.MyKad/Kad Pengenalan</th>
												<th>Agensi</th>
												<th>ID Pengguna</th>
												<th>Tarikh & Masa Daftar</th>
												<th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php																						  
											  while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) { 	
											  $masa = date_format($row['masaMuatNaik'], 'd/m/Y H:i:s');
										?>
                                            <tr>
                                                <td><?php echo $row['penggunaNama'] ?></td>
												<td><?php echo $row['icNo'] ?></td>												
												<td><?php echo $row['agensiNama'] ?></td>
												<td><?php echo $row['idSistem'] ?></td>
                                                <td><?php echo date_format($row['daftar_tarikh'], 'd/m/Y H:i:s'); ?></td>
												<td>
                                                   <a href="edit.php?uname=<?php echo $row['idSistem'] ?>"><button class="btn btn-info btn-rounded btn-sm"><span class="fa fa-pencil"></span>Kemaskini</button></a> 
												   <a href="delete.php?uname=<?php echo $row['idSistem'] ?>"><button class="btn btn-danger btn-rounded btn-sm"><span class="fa fa-trash-o"></span>Hapus</button></a> 
                                                </td>
                                            </tr>
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->                                
            </div>    
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->       
                          
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->                

        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/settings.js"></script>
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 
        
    </body>
</html>






