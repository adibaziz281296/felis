/*
Author: Ahmed
Date: August 2017
*/

//var ipaddress = '150.242.180.128';
var ipaddress = 'www.gis.myetapp.gov.my';


$("#basic_legend").click(function() {
     $("#wrapper").toggleClass("toggled");
  return false;
});

$(".sidebar-brand").click(function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
});

$("#menu-toggle").click(function(e) {
	$("#wrapper").toggleClass("toggled");
});


//===============================================================//
//							ARCGIS								 //
//===============================================================//

var map,symbol, geomTask;
var carryValue;
var gviewClient, Pano, Glocator;
var viewPoint;
var viewStatus = false;
var viewAddress = "";

      require([
				"dojo/i18n!esri/nls/jsapi",
				"esri/map",
				"esri/dijit/Print",
				"esri/tasks/PrintTemplate",
				"esri/request",
				"esri/config",
				"esri/geometry/Extent",
				"esri/dijit/HomeButton",
				"esri/layers/ArcGISDynamicMapServiceLayer",
				"esri/layers/FeatureLayer",
				"agsjs/dijit/TOC", // file TOC.js dlm folder agsjs/dijit/
				"esri/SpatialReference",
				"esri/layers/ImageParameters",
				
				
				"esri/geometry/webMercatorUtils", // display coordinate
				"esri/dijit/Search", // google search map
				"esri/dijit/OverviewMap",
				//"esri/dijit/BasemapGallery",
				"esri/dijit/BasemapToggle", // Basemap 2 choices
				"esri/dijit/Scalebar",
				// for Bookmark
				"esri/dijit/Bookmarks",
				"dojo/cookie",
				
				"esri/InfoTemplate",
				"esri/tasks/IdentifyTask",
				"esri/tasks/IdentifyParameters",
				"esri/dijit/Popup",	
				"esri/dijit/PopupTemplate",				
				"esri/symbols/SimpleFillSymbol",
				"esri/symbols/SimpleLineSymbol",
				
				"dojox/layout/FloatingPane",
				"dijit/layout/TabContainer",
				"dijit/form/ComboBox", 
				"dijit/form/Button",
				"dojox/layout/Dock",
				
				"esri/tasks/QueryTask",
				"esri/tasks/query",
				"dojo/data/ItemFileReadStore",
				"dojo/data/ItemFileWriteStore",
				"dojox/grid/EnhancedGrid",
				"dojox/grid/enhanced/plugins/Pagination",
				
				"esri/symbols/SimpleMarkerSymbol",	
				
				"esri/tasks/GeometryService", // for buffer
				"esri/toolbars/draw",
				"esri/graphic",
				"esri/tasks/BufferParameters",
				"dojo/dom",
				"dojo/on",
				"esri/geometry/normalizeUtils",
				"esri/SpatialReference",
				"dgrid/OnDemandGrid", 
				"dojo/store/Memory",
				"dijit/TooltipDialog",
				
				"esri/dijit/Measurement", // for measure
				"esri/urlUtils",
				// for get directions
				"esri/dijit/Directions", 
				"esri/geometry/Point", // for search coordinate			
				"esri/dijit/LocateButton",
				
				// For StreetView
				"esri/symbols/PictureMarkerSymbol", // for flying man streetview
				"esri/symbols/PictureFillSymbol", 
				"esri/symbols/CartographicLineSymbol",
				
				// For Route with Barrier
				"esri/tasks/RouteTask", 
				"esri/tasks/RouteParameters", 
				"esri/tasks/FeatureSet",
				//"esri/SpatialReference",
				"esri/units",
				//"dojo/_base/Color",	
				"dojo/_base/connect", 
				
				"dojo/parser", // scan page for widgets	
				"dojo/_base/array",
				"esri/Color",
				"dojo/query",
				"dojo/promise/all",
				"dojo/dom-class",
				"dojo/dom-construct",
				"dojo/domReady!"
      ], 
	  function (
				esriBundle, Map, Print, PrintTemplate, esriRequest, esriConfig, Extent, HomeButton, ArcGISDynamicMapServiceLayer, FeatureLayer, TOC, SpatialReference, ImageParameters,
				webMercatorUtils, Search, OverviewMap, //BasemapGallery,
				BasemapToggle, Scalebar, Bookmarks, cookie,
				InfoTemplate, IdentifyTask, IdentifyParameters, Popup, PopupTemplate,
				SimpleFillSymbol, SimpleLineSymbol,
				FloatingPane, TabContainer, ComboBox, Button, Dock,
				QueryTask, Query, ItemFileReadStore, ItemFileWriteStore, EnhancedGrid, pagination,
				SimpleMarkerSymbol,
				GeometryService, Draw, Graphic, BufferParameters, dom, on, normalizeUtils, SpatialReference, OnDemandGrid, Memory, TooltipDialog,
				Measurement, urlUtils, Directions, Point, LocateButton,
				PictureMarkerSymbol, PictureFillSymbol, CartographicLineSymbol, 
				RouteTask, RouteParameters, FeatureSet, Units, conn,
				parser, arrayUtils, Color, query, All, domClass, domConstruct
      ){

		parser.parse(); 

		// ---------------------- RENAME WIDGET'S DEFAULT NAME ---------------------
		esriBundle.widgets.Search.main.placeholder = "Search an address or location";
		esriBundle.widgets.homeButton.home.title = "Full Extent";
		//esriBundle.toolbars.draw.addPoint = "Add a new tree to the map";
		
		esri.bundle.toolbars.draw.addPoint = "Click on the map to draw a point";
		esri.bundle.toolbars.draw.addLine = "Click on the map to draw a line";
		esri.bundle.toolbars.draw.start = "Click to start drawing";
		esri.bundle.toolbars.draw.resume = "Click to continue to draw";
		esri.bundle.toolbars.draw.finish = "Click to continue drawing or double-click to finish";
		esri.bundle.toolbars.draw.complete = "Click to continue drawing or double-click to finish";

			
		// ---------------------- GLOBAL VARIABLES DECLARATION -------------------------------
		var identifyParams, identifyTask, tb;
		var center = [108.75,5.391]; // lon, lat
		
		var routeTask, routeParams, routes = [];
        var stopSymbol, barrierSymbol, routeSymbols;
        var mapOnClick_addStops_connect, mapOnClick_addBarriers_connect;
        var clickmap;
		var dynaLayer1, dynaLayer2, dynaLayer3, dynaLayer4;
		var app = {};
      	app.map = null; app.printer = null;
		// ---------------------- INFO POPUP PROPS -------------------------------
        var popup = new Popup({ fillSymbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
											new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
											new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25]))
					}, domConstruct.create("div"));
		
 	
		// popup theme
        //dojo.addClass(popup.domNode, "modernGrey");
		 domClass.add(popup.domNode, "dark");
		 
		var latitude = phpVars["value1"];
		var longitude = phpVars["value2"];
		var zoomin = phpVars["value3"];
		var spatialReference = new SpatialReference({wkid:4742});
          
        loading = dom.byId("loadingImg");  //loading image. id  

		 map = new Map("mapWrapper", { 
			center: [latitude, longitude],
			zoom: zoomin,
			basemap: "hybrid",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false*/
			infoWindow: popup,
			showAttribution: false
		});
				

		// ---------------------- DEFINE SERVICES -------------------------------	
		//esriConfig.defaults.io.proxyUrl = "/proxy/";
		esriConfig.defaults.io.proxyUrl = "proxy/PHP/proxy.php";
        esriConfig.defaults.io.alwaysUseProxy = false;
		
			// SETUP PROXY for Buffer, Measure
		//	esriConfig.defaults.io.proxyUrl = 'dist/proxy-1.1.0/PHP/proxy.php';
		//	esriConfig.defaults.io.alwaysUseProxy = false;
//			esriConfig.defaults.io.corsDetection = false; // to get rid of HTTPS (share services in applications that utilize HTTPS)
			esriConfig.defaults.io.corsEnabledServers.push("http://www.gis.myetapp.gov.my/myetappgis");
			esriConfig.defaults.io.corsEnabledServers.push("http://www.gis.myetapp.gov.my");
//			esriConfig.request.corsEnabledServers.push("www.gis.myetapp.gov.my");
//			esriConfig.request.corsEnabledServers.push("http://150.242.180.128");
	
		
		//Use the ImageParameters to set map service layer definitions and map service visible layers before adding to the client map.
          var imageParameters = new ImageParameters();

          //ImageParameters.layerDefinitions takes an array.  The index of the array corresponds to the layer id.
          //In the sample below an element is added in the array at 3, 4, and 5 indexes.
          //Those array elements correspond to the layer id within the remote ArcGISDynamicMapServiceLayer
          var layerDefs = [];
          layerDefs[0] = "STATUS_TERKINI='2'";
          layerDefs[1] = "STATUS_TERKINI='2'";
         // layerDefs[2] = "STATUS_TERKINI='2'";
		  layerDefs[3] = "STATUS_TERKINI='2'";
          imageParameters.layerDefinitions = layerDefs;
		  
		  //I want layers 5,4, and 3 to be visible
          imageParameters.layerIds = [0, 1, 3];
          imageParameters.layerOption = ImageParameters.LAYER_OPTION_SHOW;
          imageParameters.transparent = true;
		
		
			  dynaLayer1 = new ArcGISDynamicMapServiceLayer("http://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/sani/1MyMap/MapServer", {
                opacity: 0.8
              });
			  
			 /* dynaLayer2 = new ArcGISDynamicMapServiceLayer("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer", {
                opacity: 0.8
              }); */
			  
			    //construct ArcGISDynamicMapServiceLayer with imageParameters from above
				dynaLayer2 = new ArcGISDynamicMapServiceLayer("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer",{
				opacity: 0.8
			//	"imageParameters": imageParameters
				});
			  
			  dynaLayer3 = new ArcGISDynamicMapServiceLayer("http://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/jalan_jkr/Rangkaian_Jalan_JKR/MapServer", {
                opacity: 0.8
              });
			  
			  dynaLayer4 = new ArcGISDynamicMapServiceLayer("http://"+ipaddress+"/arcgis/rest/services/JKPTG/ndcdb/MapServer", {
                opacity: 0.8
              });
			             
			  map.on('layers-add-result', function(evt){
                // overwrite the default visibility of service.
                // TOC will honor the overwritten value.
				
			   // SET LAYER VISIBILITY ================================================
				 dynaLayer1.setVisibility(false); // to default unchecked
				 dynaLayer3.setVisibility(false); // to default unchecked
				 dynaLayer4.setVisibility(false); // to default unchecked
	
                   dynaLayer1.setVisibleLayers([]);
				   dynaLayer3.setVisibleLayers([]);
				   dynaLayer2.setVisibleLayers([]);
				   dynaLayer4.setVisibleLayers([]);
                //try {
                  toc = new TOC({
                    map: map,
                    layerInfos: [
					/*{
                      layer: featLayer1,
                      title: "FeatureLayer1"
                    },*/
					
					{
                      layer: dynaLayer2,
                      title: "Charting",
                      //collapsed: false, // whether this root layer should be collapsed initially, default false.
                      slider: true // whether to display a transparency slider.
                    },
					
					{
                      layer: dynaLayer1,
                      title: "1 Malaysia Map"
                      //collapsed: false, // whether this root layer should be collapsed initially, default false.
                      //slider: false // whether to display a transparency slider.
                    },
										
					{
                      layer: dynaLayer3,
                      title: "JKR Road"
                      //collapsed: false, // whether this root layer should be collapsed initially, default false.
                      //slider: false // whether to display a transparency slider.
                    },
					{
                      layer: dynaLayer4,
                      title: "Lot NDCDB"
                      //collapsed: false, // whether this root layer should be collapsed initially, default false.
                      //slider: false // whether to display a transparency slider.
                    }		
					]
                  }, 'tocDiv');
                  toc.startup();
         
			
                //} catch (e) {  alert(e); }
              });
              map.addLayers([dynaLayer4 ,dynaLayer2, dynaLayer1, dynaLayer3]);


		// For Measure
		esriConfig.defaults.geometryService = new GeometryService("http://"+ipaddress+"/arcgis/rest/services/Utilities/Geometry/GeometryServer");
		var geometryService = new GeometryService("http://"+ipaddress+"/arcgis/rest/services/Utilities/Geometry/GeometryServer"); 
		
		// Create empty store for each search result's grid
		var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});

		// Search (Query Task) service
		var search_pemohonan_layer = new QueryTask("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer/0");
		var search_qt_layer = new QueryTask("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer/1");
		var search_ft_layer = new QueryTask("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer/2");
		var search_rezab_layer = new QueryTask("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer/3");


		// ---------------------- INITIALIZE MAP --------------------------------	
		/*var extentMalaysia = new Extent({
			xmin: 98.947096,
			ymin: 0.358901,
			xmax: 119.689284,
			ymax: 8.414430,
			spatialReference: {
				wkid: 4326
			}
		});*/
						
		/*map = new Map("mapWrapper", { 
			extent: extentMalaysia,
			basemap: "hybrid",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false
			infoWindow: popup,
			showAttribution: false,
			zoom: 6
		});*/
		
		/*map = new Map("mapWrapper", { 
			center: [107.988844, 4.550888],
			zoom: 6,
			basemap: "osm",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false
			infoWindow: popup,
			showAttribution: false
		});*/
		
		//"streets" | "satellite" | "hybrid" | "topo" | "gray" | "dark-gray" | "oceans"| "national-geographic" | "terrain" | "osm" | "dark-gray-vector" | "gray-vector" | "streets-vector" | "streets-night-vector" | "streets-relief-vector" | "streets-navigation-vector" | "topo-vector"
		
		//if there are url params zoom to location   
		var coords, zoomLevel;  
		var urlObject = esri.urlToObject(document.location.href);  
	
		// Infowindows
		map.infoWindow.resize(550, 250);			

		// Resize map
		map.on('resize', function (evt) {
			var pnt = evt.target.extent.getCenter();
			setTimeout(function () { evt.target.centerAt(pnt);}, 500);
		});
		
		// ---------------------- Activate HOME button ----------------------
		var home = new HomeButton({ map: map}, "HomeButton");      	
		home.title = "This is the new title"; 
		home.startup();	
		
		map.on("load", function() {
          //after map loads, connect to listen to mouse move & drag events
          map.on("mouse-move", showCoordinates);
          map.on("mouse-drag", showCoordinates);
	
        });
          
        // ---------------------- Show Loading ----------------------
        on(map, "update-start", showLoading);
		on(map, "update-end", hideLoading);
		
		 function showLoading() {
			esri.show(loading);
			map.disableMapNavigation();
			map.hideZoomSlider();
		  }

		 function hideLoading(error) {
			esri.hide(loading);
			map.enableMapNavigation();
			map.showZoomSlider();
		  }  
		

		function connectInfo(){
			console.log("connect info");
			klikInfo = map.on("click", executeIdentifyTask);				
		}

		function disconnectInfo(){
			console.log("disconnect info");	
			dojo.disconnect(klikInfo);
			klikInfo.remove();
		}
		
		// ---------------------- ERASE button ----------------------
		on(dom.byId("EraseButton"), "click", function(){
			if(map){ 
				map.graphics.clear(); 
				connectInfo();
				// stop measure
				measurement.setTool("area",false); 
				measurement.setTool("distance",false);
				measurement.setTool("location",false);
				measurement.clearResult(); 		
				clickmap.remove();

			}
        }); 
		
		// ---------------------- LOCATE/Geolocation button ----------------------
		geoLocate = new LocateButton({
			map: map
		}, "LocateButton");
		geoLocate.startup();

		// ---------------------- Show Coordinates ----------------------
        function showCoordinates(evt) {
			//the map is in web mercator but display coordinates in geographic (lat, long)
			var mp = webMercatorUtils.webMercatorToGeographic(evt.mapPoint);
			//display mouse coordinates
			dom.byId("coordinate_info").innerHTML = "X: "+ mp.x.toFixed(6) + ", Y: " + mp.y.toFixed(6);
        }
		
		// ---------------------- SCALEBAR --------------------------------
		var scalebar = new Scalebar({
			map: map,
			attachTo: 'bottom-left',
			scalebarStyle: 'line',
			scalebarUnit: 'dual'
		});

		// ------------------------ SEARCH MAP LOCATION TASK ----------------------------
		var search = new Search({
			map: map,   
		}, "searchBox");				
	
		search.sources[0].countryCode = "MY";
		search.startup();
		
		// ------------------------------- OVERVIEW MAP-------- -----------------------
		var overviewMapDijit = new OverviewMap({
			map: map,
			attachTo: "bottom-right",
			/*color:"#fdf874",*/
			height : 200,
			width : 225,
			opacity: .40
        });
		
        overviewMapDijit.startup();
		
				
	/* =====================================================================================================
											WIDGETS: BASIC FUNCTIONS 													  
	======================================================================================================== */
				
		// ---------------------- BASEMAP TOGGLE ---------------------
		var toggle = new BasemapToggle({
			map: map,
			basemap: "osm"
		}, "BasemapToggle");
      
		toggle.startup();
		
     		
		// ---------------------- LOCATOR COORDINATE ---------------------
		// Open Locator Pane
 		locator_button = dojo.byId('basic_locator');
		dojo.connect(locator_button, "onclick", open_locatorPane);
		function open_locatorPane() { dijit.byId('locatorPane').show(); }  
		
		// Connection so that the dijit button can read the function inside require.
		dijit.byId("search_coordinate").on("click",  locate_Coordinate);
		dijit.byId("clear_coordinate").on("click",  clear_Coordinate);		
		
		function locate_Coordinate(){  
			console.log("got here 2")  
			var lat_value = document.getElementById("latitude").value  
			var long_value = document.getElementById("longitude").value 
			
			xmin =  98.947096;
			ymin =  0.358901;
			xmax = 119.689284;
			ymax = 8.414430;
			
			
			if(lat_value == "" || long_value == "")
				alert ("Please enter X and Y values (Malaysia's extent)");
			else {
				// check for values
				if ( (long_value >= xmin && long_value <= xmax ) && (lat_value >= ymin && lat_value <= ymax ) ) {
					var mp = new Point(long_value, lat_value);  
					var graphic = new esri.Graphic(mp, symbol_coordinates);  
					map.graphics.add(graphic);  
					map.centerAndZoom(mp, 12);
				}
				
				else {
					alert ("Please enter valid X and Y values");
				}
			}
		}	
		
		function clear_Coordinate() {			
			// Remove all graphics on the maps graphics layer
			map.graphics.clear();
						
			// Remove previous search results
			document.getElementById("latitude").value = "";
			document.getElementById("longitude").value = "";
			map.centerAndZoom(center, 5);	
		}
		
		// ---------------------- DRAW ---------------------
		// ---------------------- PRINT ---------------------
		
	/* =====================================================================================================
											WIDGETS: ANALYSIS													  
	======================================================================================================== */		
		
		// ---------------------- MEASURE ---------------------
		var measurement = new Measurement({
			map: map
		}, dom.byId("measurementDiv"));		
		measurement.startup();
				
		// Open Measure Pane
 		measure_button = dojo.byId('analysis_measure');
		dojo.connect(measure_button, "onclick", open_measurePane);
		function open_measurePane() { 
			dijit.byId('measurePane').show(); 
			disconnectInfo(); 
//			dijit.byId('streetFormView').hide();
			map.graphics.clear();
			clickmap.remove();
		} 
		
		//Setup button click handlers -stop measure
        on(dom.byId("clearMeasure"), "click", function(){
			if(map){ 
				map.graphics.clear(); 
				measurement.setTool("area",false); 
				measurement.setTool("distance",false);
				measurement.setTool("location",false);
				measurement.clearResult(); 
			}
        }); 
		         
        //Stop function measure when panel measure hide
		dojo.connect(dijit.byId('measurePane'), "hide", exitMeasure);
		
		function exitMeasure() {   
			map.graphics.clear(); 
            measurement.setTool("area",false); 
            measurement.setTool("distance",false);
            measurement.setTool("location",false);
            measurement.clearResult();
			connectInfo();

		}
		// ---------------------- CLOSE MEASURE -----------------------

		// ---------------------- STREET VIEW -------------------------
		
		// Open Search Form
 		street_view_button = dojo.byId('street_view');
		dojo.connect(street_view_button, "onclick", open_streetViewPane);
		function open_streetViewPane() {
				dijit.byId('streetFormView').show();
				clickmap = map.on("click", doStreetView);			
				dijit.byId('searchFormLot').hide();
				dijit.byId('searchFormProject').hide();
				disconnectInfo(); 
	//			dijit.byId('measurePane').hide();
	//			map.graphics.clear(); 
				measurement.setTool("area",false); 
				measurement.setTool("distance",false);
				measurement.setTool("location",false);
				measurement.clearResult(); 
		}
		
		dojo.connect(dijit.byId('streetFormView'), "hide", exitStreetview);
		
		function exitStreetview() { 
           // dijit.byId('streetFormView').hide();
			clickmap.remove();
            map.graphics.clear();
			connectInfo();
			
		}


		symbol = new PictureMarkerSymbol("assets/img/flying_man.png", 32, 34);
            //set up street view
            Glocator = new google.maps.Geocoder();
            gviewClient = new google.maps.DirectionsService();
			sv = new google.maps.StreetViewService();
            Pano = new google.maps.StreetViewPanorama(document.getElementById("streetview"));
            google.maps.event.addListener(Pano, "error", handleNoFlash);
			
			function resizeMap() {
            //resize the map when the browser resizes - view the 'Resizing and repositioning the map' section in  
            //the following help topic for more details http://help.esri.com/EN/webapi/javascript/arcgis/help/jshelp_start.htm#jshelp/inside_faq.htm 
            var resizeTimer;
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                map.resize();
                map.reposition();
            }, 500);
        }
        function doStreetView(evt) {
            map.graphics.clear();           
            viewPoint = esri.geometry.webMercatorToGeographic(evt.mapPoint);
            sv.getPanoramaByLocation(new google.maps.LatLng(viewPoint.y, viewPoint.x), 100, showStreetView);

        }
        function handleNoFlash(errorCode) {
            if (errorCode == FLASH_UNAVAILABLE) {
                alert("Error: Flash doesn't appear to be supported by your browser");
                return;
            }
        }
		
		function showStreetView(data, status) {
			if(status == google.maps.StreetViewStatus.OK) {   //street view is available 
				map.infoWindow.hide(); 
				// alert("clicked");
                viewStatus = true;
                viewAddress = data.location.description;
                google.maps.event.addListener(Pano, 'initialized', streetViewChanged); // add an event handling street view changes
				var markerPanoID = data.location.pano;
      			// Set the Pano to use the passed panoID
      			Pano.setPano(markerPanoID);
                Pano.setPov(data.location.latlng, { heading:34, pitch:10 }); //push the pano to a this location
				Pano.setVisible(true);
                //add graphic
                var graphic = new esri.Graphic(esri.geometry.geographicToWebMercator(viewPoint), symbol);
                map.graphics.add(graphic);
            }
            else{ //no street view for this location
				alert("No street view for this location");
                //if (Pano != null) Pano.hide(); //hide the street view
                viewStatus = false;
                viewAddress = "";
            }
            var locationpoint = new google.maps.LatLng(viewPoint.y, viewPoint.x);
            Glocator.geocode({'latLng': locationpoint}, function(results, status) {
			var addr_type = results[0].types[0];	// type of address inputted that was geocoded
			if ( status == google.maps.GeocoderStatus.OK ) 
				showStreetAddress(locationpoint, results[0].formatted_address, addr_type );
			else     
				alert("Geocode was not successful for the following reason: " + status);        
				}); //show street address info
        }
		
        function showStreetAddress(latlng, address, addr_type) {
			// alert("masuk doh");
            var descContent = "";
            descContent = "<table width='95%'><tr><td><b>View Point Address:</b></td><td>" + address + "</td></tr><tr><td><b>View Point Lat/Lon:</b></td><td>"
            descContent += latlng + "</td></tr><tr><td>";
            descContent += "<b>Address type:</b></td><td>" + addr_type + "<td><tr></table>";
            document.getElementById("Desc").innerHTML = descContent;
        }
        function getlocalAddress(Addr) {
            var index = Addr.indexOf(',');
            return Addr.substring(0, index);
        }
        function streetViewChanged(streetLocation) {
            viewStatus = true;
            viewAddress = streetLocation.description;
            var point = new esri.geometry.Point({"x": streetLocation.latlng.lng(), "y": streetLocation.latlng.lat(), " spatialReference": { " wkid": 4326} });
            //add to the map
            map.graphics.clear();
            var symbolPoint = esri.geometry.geographicToWebMercator(point);
            map.graphics.add(new esri.Graphic(symbolPoint, symbol));
            map.centerAt(symbolPoint);
            Glocator.getLocations(streetLocation.latlng, showStreetAddress);
        }
		
			// ---------------------- CLOSE STREET VIEW -------------------------
          		
		// ===============================================================================
		// 									QUERY TASK 
		// ===============================================================================	
		// ---------------------- STYLE for searched features ----------------------
		// Create symbol for search features
		/*symbolSearch = new SimpleMarkerSymbol();
		symbolSearch.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
		symbolSearch.setSize(10);
		symbolSearch.setColor(new dojo.Color([255,255,0,0.5]));*/
		
		var symbol_coordinates = new SimpleMarkerSymbol(  
			SimpleMarkerSymbol.STYLE_CIRCLE, 15,  
			new SimpleLineSymbol( "solid", new Color([0, 0, 255, 0.5]), 8),  
			new Color([0, 0, 255])  
		);  
		
		symbolSearch = new PictureMarkerSymbol("assets/img/map_pin.png", 16, 32);
		
		polygonsymbol = new SimpleFillSymbol( "solid", new SimpleLineSymbol("solid", new Color([232,104,80]), 2),
			new Color([232,104,80,0.25])
		);
		  
		// ---------------------- 1. SEARCH: LOT -------------------------
		dijit.byId("select_service_lot").on("change",  populateList_State_setbyService_lot);
		dijit.byId("select_state_lot").on("change",  populateList_District_setbyState_lot);
		dijit.byId("select_district_lot").on("change",  populateList_Mukim_setbyDistrict_lot);
		// The SEARCH FORM
		// Open Search Form
 		search_lot_button = dojo.byId('search_lot');
		dojo.connect(search_lot_button, "onclick", open_searchLotForm);
		function open_searchLotForm() { 
			dijit.byId('searchFormLot').show();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();

			dojo.byId('select_service_lot').value = "Select service...";
			dojo.byId('select_state_lot').value = "Select state...";
			dojo.byId('select_district_lot').value = "Select district...";
			dojo.byId('select_mukim_lot').value = "Select sub district...";
			dojo.byId('seksyen_name').value = "";
			dojo.byId('lot_number').value = "";

			var selectedService_lot = dojo.byId('select_service_lot').value;

			if (selectedService_lot == 'FT') {
			 	queryTask_Lot =  search_ft_layer;
				// alert('FT');
			}else{
				if (selectedService_lot == 'QT') {
					queryTask_Lot =  search_qt_layer;
					// alert('QT');
				}
				else{
					queryTask_Lot =  search_rezab_layer;
					// alert('Rezab');
				}
			} 		
		}  
		
		var selectedService_lot = dojo.byId('select_service_lot').value;
		// D. Initialize Query Task
		// var variable = 'before';

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_lot() { 
			var selectedService_lot = dojo.byId('select_service_lot').value;
			document.getElementById('imageLoadingLot').innerHTML = '<img src="loading.gif" />'; 
			// D. Initialize Query Task
			var state_list_lot = new Query();
			state_list_lot.where = "OBJECTID <> ''";
			state_list_lot.returnGeometry = false;
			state_list_lot.outFields = ["*"];
			console.log("sini ok");
			if (selectedService_lot == 'FT') {
				var state_ddown_lot = search_ft_layer;
				queryTask_Lot =  search_ft_layer;
				// variable = 'after FT';
				// alert('FT');
			}else{
				if (selectedService_lot == 'QT') {
					var state_ddown_lot = search_qt_layer;
					queryTask_Lot =  search_qt_layer;
					// variable = 'after QT';
					// alert('QT');
				}
				else{
					var state_ddown_lot =  search_rezab_layer;
					queryTask_Lot =  search_rezab_layer;
					// variable = 'after Rezab';
					// alert('Rezab');
				}
			} 
			//var state_ddown_lot = state_layer1;
			state_ddown_lot.execute(state_list_lot, populateList_state_lot);
		}
		// alert(variable);
		// B. Initial DISTRICT Dropdown query 			
		/* var district_list = new Query();
		district_list.where = "DISTRICT >''";
		district_list.returnGeometry = false;
		district_list.outFields = ["DISTRICT"];
		
		district_ddown = mukim_layer;
		district_ddown.execute(district_list, populateList_district); */

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_lot() { 
			var selectedState_lot = dojo.byId('select_state_lot').value;
			var selectedService_lot = dojo.byId('select_service_lot').value;
			document.getElementById('imageLoadingLot').innerHTML = '<img src="loading.gif" />'; 
			// D. Initialize Query Task
			if (selectedService_lot == 'FT') {
				var district_ddown_lot = search_ft_layer;
			}else{
				if (selectedService_lot == 'QT') {
					var district_ddown_lot = search_qt_layer;
				}
				else{
					var district_ddown_lot =  search_rezab_layer;
				}
			}
			
			var district_list_lot = new Query();
			district_list_lot.where = "NAMA_NEGERI  = '" + selectedState_lot + "'";
			//district_list_lot.where = "Negeri  = 'Kedah'";
			//district_list_lot.returnGeometry = false
			district_list_lot.outFields = ["NAMA_NEGERI", "NAMA_DAERAH"];
			
			//var district_ddown_lot = state_layer1;
			district_ddown_lot.execute(district_list_lot, populateList_district_byState_lot);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_lot() { 
			var selectedState_lot = dojo.byId('select_state_lot').value;
			var selectedDistrict_lot = dojo.byId('select_district_lot').value;
			var selectedService_lot = dojo.byId('select_service_lot').value;
			document.getElementById('imageLoadingLot').innerHTML = '<img src="loading.gif" />'; 
			// D. Initialize Query Task
			if (selectedService_lot == 'FT') {
				var mukim_ddown_lot = search_ft_layer;
			}else{
				if (selectedService_lot == 'QT') {
					var mukim_ddown_lot = search_qt_layer;
				}
				else{
					var mukim_ddown_lot =  search_rezab_layer;
				}
			}
			
			var mukim_list_lot = new Query();
			mukim_list_lot.where = "NAMA_NEGERI = '" + selectedState_lot + "' AND NAMA_DAERAH = '" + selectedDistrict_lot + "'";
			mukim_list_lot.returnGeometry = false;
			mukim_list_lot.outFields = ["NAMA_NEGERI", "NAMA_DAERAH", "NAMA_MUKIM"];
			console.log(mukim_list_lot.where);
			
			//var mukim_ddown_lot = state_layer1;
			mukim_ddown_lot.execute(mukim_list_lot, populateList_mukim_byStateDistrict_lot);
		}
		
		//var selectedService_lot = dojo.byId('select_service_lot').value;
		// D. Initialize Query Task
		// if (selectedService_lot == 'FT') {
		// 	queryTask_Lot =  search_ft_layer;
		// 	alert('FT');
		// }else{
		// 	if (selectedService_lot == 'QT') {
		// 		queryTask_Lot =  search_qt_layer;
		// 		alert('QT');
		// 	}
		// 	else{
		// 		queryTask_Lot =  search_rezab_layer;
		// 		alert('Rezab');
		// 	}
		// }
		// alert(queryTask_Lot);
		//queryTask_Lot =  search_lot_layer;     		
		query_Lot = new Query();
		query_Lot.returnGeometry = true;
		query_Lot.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Lot = new EnhancedGrid({
					structure: gridLot_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridLot');	
			
		gridResult_Lot.set("store",clearStore); 
		gridResult_Lot.startup();  

		

		// ---------------------- 2. SEARCH: Project -------------------------
		dijit.byId("select_service_project").on("change",  populateList_State_setbyService_project);
		dijit.byId("select_state_project").on("change",  populateList_District_setbyState_project);
		dijit.byId("select_district_project").on("change",  populateList_Mukim_setbyDistrict_project);
		// The SEARCH FORM
		// Open Search Form
 		search_project_button = dojo.byId('search_project');
		dojo.connect(search_project_button, "onclick", open_searchProjectForm);
		function open_searchProjectForm() { 
			dijit.byId('searchFormProject').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();

			dojo.byId('select_service_project').value = "Select service...";
			dojo.byId('select_state_project').value = "Select state...";
			dojo.byId('select_district_project').value = "Select district...";
			dojo.byId('select_mukim_project').value = "Select sub district...";
			dojo.byId('tanah').value = "";

			var selectedService_project = dojo.byId('select_service_project').value;

			if (selectedService_project == 'Pemohonan') {
				queryTask_Project =  search_pemohonan_layer;
				// alert('Pemohonan');
			}else{
				if (selectedService_project == 'FT') {
				 	queryTask_Project =  search_ft_layer;
					// alert('FT');
				}else{
					if (selectedService_project == 'QT') {
						queryTask_Project =  search_qt_layer;
						// alert('QT');
					}
					else{
						selectedService_project =  search_rezab_layer;
						// alert('Rezab');
					}
				} 	
			}
					
		}  
		var selectedService_project = dojo.byId('select_service_project').value;

		

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_project() { 
			var selectedService_project = dojo.byId('select_service_project').value;
			document.getElementById('imageLoadingProject').innerHTML = '<img src="loading.gif" />'; 

			var state_list_project = new Query();
			state_list_project.where = "NAMA_NEGERI <> ''";
			state_list_project.returnGeometry = false;
			state_list_project.outFields = ["NAMA_NEGERI"];
			console.log("sini ok");
			if (selectedService_project == 'Pemohonan') {
				var state_ddown_project = search_pemohonan_layer;
				queryTask_Project =  search_pemohonan_layer;

			}else{
				if (selectedService_project == 'FT') {
					var state_ddown_project = search_ft_layer;
					queryTask_Project =  search_ft_layer;
				}else{
					if (selectedService_project == 'QT') {
						var state_ddown_project = search_qt_layer;
						queryTask_Project =  search_qt_layer;
					}
					else{
						var state_ddown_project =  search_rezab_layer;
						queryTask_Project =  search_rezab_layer;
					}
				}
			}
			//var state_ddown_project = state_layer1;
			state_ddown_project.execute(state_list_project, populateList_state_project);

			// B. Initial DISTRICT Dropdown query 			
			/* var district_list = new Query();
			district_list.where = "DISTRICT >''";
			district_list.returnGeometry = false;
			district_list.outFields = ["DISTRICT"];
			
			district_ddown = mukim_layer;
			district_ddown.execute(district_list, populateList_district); */
		}

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_project() { 
			var selectedState_project = dojo.byId('select_state_project').value;
			var selectedService_project = dojo.byId('select_service_project').value;
			document.getElementById('imageLoadingProject').innerHTML = '<img src="loading.gif" />'; 

			if (selectedService_project == 'Pemohonan') {
				var district_ddown_project = search_pemohonan_layer;
			}else{
				if (selectedService_project == 'FT') {
					var district_ddown_project = search_ft_layer;
				}else{
					if (selectedService_project == 'QT') {
						var district_ddown_project = search_qt_layer;
					}
					else{
						var district_ddown_project =  search_rezab_layer;
					}
				}
			}
			

			var district_list_project = new Query();
			district_list_project.where = "NAMA_NEGERI  = '" + selectedState_project + "'";
			//district_list_project.where = "Negeri  = 'Kedah'";
			//district_list_project.returnGeometry = false
			district_list_project.outFields = ["NAMA_NEGERI", "NAMA_DAERAH"];
			
			//var district_ddown_project = state_layer1;
			district_ddown_project.execute(district_list_project, populateList_district_byState_project);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_project() { 
			var selectedState_project = dojo.byId('select_state_project').value;
			var selectedDistrict_project = dojo.byId('select_district_project').value;
			var selectedService_project = dojo.byId('select_service_project').value;
			document.getElementById('imageLoadingProject').innerHTML = '<img src="loading.gif" />'; 
			
			if (selectedService_project == 'Pemohonan') {
				var mukim_ddown_project = search_pemohonan_layer;
			}else{
				if (selectedService_project == 'FT') {
					var mukim_ddown_project = search_ft_layer;
				}else{
					if (selectedService_project == 'QT') {
						var mukim_ddown_project = search_qt_layer;
					}
					else{
						var mukim_ddown_project =  search_rezab_layer;
					}
				}
			}
				
			
			var mukim_list_project = new Query();
			mukim_list_project.where = "NAMA_NEGERI = '" + selectedState_project + "' AND NAMA_DAERAH = '" + selectedDistrict_project + "'";
			mukim_list_project.returnGeometry = false;
			mukim_list_project.outFields = ["NAMA_NEGERI", "NAMA_DAERAH", "NAMA_MUKIM"];
			console.log(mukim_list_project.where);
			
			//var mukim_ddown_project = state_layer1;
			mukim_ddown_project.execute(mukim_list_project, populateList_mukim_byStateDistrict_project);
		}
		
		// if (selectedService_project == 'Pemohonan') {
		// 	queryTask_Project =  search_pemohonan_layer;
		// }else{
		// 	if (selectedService_project == 'FT') {
		// 		queryTask_Project =  search_ft_layer;
		// 	}else{
		// 		if (selectedService_project == 'QT') {
		// 			queryTask_Project =  search_qt_layer;
		// 		}
		// 		else{
		// 			queryTask_Project =  search_rezab_layer;
		// 		}
		// 	}
		// }
		
		// D. Initialize Query Task
		//queryTask_Project =  search_project_layer;     		
		query_Project = new Query();
		query_Project.returnGeometry = true;
		query_Project.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Project = new EnhancedGrid({
					structure: gridProject_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridProject');	
			
		gridResult_Project.set("store",clearStore); 
		gridResult_Project.startup();  

		// ---------------------- 3. SEARCH: Hak Milik Sementara -------------------------
		dijit.byId("select_state_sementara").on("change",  populateList_District_setbyState_sementara);
		dijit.byId("select_district_sementara").on("change",  populateList_Mukim_setbyDistrict_sementara);
		// The SEARCH FORM
		// Open Search Form
 		search_sementara_button = dojo.byId('search_sementara');
		dojo.connect(search_sementara_button, "onclick", open_searchSementaraForm);
		function open_searchSementaraForm() { 
			dijit.byId('searchFormSementara').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormMilik').hide();

			dojo.byId('select_state_sementara').value = "Select state...";
			dojo.byId('select_district_sementara').value = "Select district...";
			dojo.byId('select_mukim_sementara').value = "Select sub district...";
			dojo.byId('sementara').value = "";
					
		}  

		// A. Initial STATE Dropdown query	

		var state_list_sementara = new Query();
		state_list_sementara.where = "NAMA_NEGERI <> ''";
		state_list_sementara.returnGeometry = false;
		state_list_sementara.outFields = ["NAMA_NEGERI"];
		console.log("sini ok");
		var state_ddown_sementara = search_qt_layer;
		state_ddown_sementara.execute(state_list_sementara, populateList_state_sementara);

		// B. Initial DISTRICT Dropdown query 			
		/* var district_list = new Query();
		district_list.where = "DISTRICT >''";
		district_list.returnGeometry = false;
		district_list.outFields = ["DISTRICT"];
			
		district_ddown = mukim_layer;
		district_ddown.execute(district_list, populateList_district); */

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_sementara() { 
			var selectedState_sementara = dojo.byId('select_state_sementara').value;
			document.getElementById('imageLoadingSementara').innerHTML = '<img src="loading.gif" />'; 

			var district_list_sementara = new Query();
			district_list_sementara.where = "NAMA_NEGERI  = '" + selectedState_sementara + "'";
			//district_list_sementara.where = "Negeri  = 'Kedah'";
			//district_list_project.returnGeometry = false
			district_list_sementara.outFields = ["NAMA_NEGERI", "NAMA_DAERAH"];
			
			var district_ddown_sementara = search_qt_layer;
			district_ddown_sementara.execute(district_list_sementara, populateList_district_byState_sementara);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_sementara() { 
			var selectedState_sementara = dojo.byId('select_state_sementara').value;
			var selectedDistrict_sementara = dojo.byId('select_district_sementara').value;
			document.getElementById('imageLoadingSementara').innerHTML = '<img src="loading.gif" />'; 
			
			var mukim_list_sementara = new Query();
			mukim_list_sementara.where = "NAMA_NEGERI = '" + selectedState_sementara + "' AND NAMA_DAERAH = '" + selectedDistrict_sementara + "'";
			mukim_list_sementara.returnGeometry = false;
			mukim_list_sementara.outFields = ["NAMA_NEGERI", "NAMA_DAERAH", "NAMA_MUKIM"];
			console.log(mukim_list_sementara.where);
			
			var mukim_ddown_sementara = search_qt_layer;
			mukim_ddown_sementara.execute(mukim_list_sementara, populateList_mukim_byStateDistrict_sementara);
		}
		
		// D. Initialize Query Task
		queryTask_Sementara =  search_qt_layer;     		
		query_Sementara = new Query();
		query_Sementara.returnGeometry = true;
		query_Sementara.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Sementara = new EnhancedGrid({
					structure: gridSementara_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridSementara');	
			
		gridResult_Sementara.set("store",clearStore); 
		gridResult_Sementara.startup(); 

// ---------------------- 4. SEARCH: Hak Milik -------------------------
		dijit.byId("select_state_milik").on("change",  populateList_District_setbyState_milik);
		dijit.byId("select_district_milik").on("change",  populateList_Mukim_setbyDistrict_milik);
		// The SEARCH FORM
		// Open Search Form
 		search_milik_button = dojo.byId('search_milik');
		dojo.connect(search_milik_button, "onclick", open_searchMilikForm);
		function open_searchMilikForm() { 
			dijit.byId('searchFormMilik').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();

			dojo.byId('select_state_milik').value = "Select state...";
			dojo.byId('select_district_milik').value = "Select district...";
			dojo.byId('select_mukim_milik').value = "Select sub district...";
			dojo.byId('milik').value = "";
					
		}  

		// A. Initial STATE Dropdown query	

		var state_list_milik = new Query();
		state_list_milik.where = "NAMA_NEGERI <> ''";
		state_list_milik.returnGeometry = false;
		state_list_milik.outFields = ["NAMA_NEGERI"];
		console.log("sini ok");
		var state_ddown_milik = search_ft_layer;
		state_ddown_milik.execute(state_list_milik, populateList_state_milik);

		// B. Initial DISTRICT Dropdown query 			
		/* var district_list = new Query();
		district_list.where = "DISTRICT >''";
		district_list.returnGeometry = false;
		district_list.outFields = ["DISTRICT"];
			
		district_ddown = mukim_layer;
		district_ddown.execute(district_list, populateList_district); */

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_milik() { 
			var selectedState_milik = dojo.byId('select_state_milik').value;
			document.getElementById('imageLoadingMilik').innerHTML = '<img src="loading.gif" />'; 

			var district_list_milik = new Query();
			district_list_milik.where = "NAMA_NEGERI  = '" + selectedState_milik + "'";
			//district_list_milik.where = "Negeri  = 'Kedah'";
			//district_list_milik.returnGeometry = false
			district_list_milik.outFields = ["NAMA_NEGERI", "NAMA_DAERAH"];
			
			var district_ddown_milik = search_ft_layer;
			district_ddown_milik.execute(district_list_milik, populateList_district_byState_milik);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_milik() { 
			var selectedState_milik = dojo.byId('select_state_milik').value;
			var selectedDistrict_milik = dojo.byId('select_district_milik').value;
			document.getElementById('imageLoadingMilik').innerHTML = '<img src="loading.gif" />'; 
			
			var mukim_list_milik = new Query();
			mukim_list_milik.where = "NAMA_NEGERI = '" + selectedState_milik + "' AND NAMA_DAERAH = '" + selectedDistrict_milik + "'";
			mukim_list_milik.returnGeometry = false;
			mukim_list_milik.outFields = ["NAMA_NEGERI", "NAMA_DAERAH", "NAMA_MUKIM"];
			console.log(mukim_list_milik.where);
			
			var mukim_ddown_milik = search_ft_layer;
			mukim_ddown_milik.execute(mukim_list_milik, populateList_mukim_byStateDistrict_milik);
		}
		
		// D. Initialize Query Task
		queryTask_Milik =  search_ft_layer;     		
		query_Milik = new Query();
		query_Milik.returnGeometry = true;
		query_Milik.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Milik = new EnhancedGrid({
					structure: gridMilik_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridMilik');	
			
		gridResult_Milik.set("store",clearStore); 
		gridResult_Milik.startup(); 


		// ---------------------- 5. SEARCH: File Name -------------------------
		dijit.byId("select_service_file").on("change",  populateList_State_setbyService_file);
		// The SEARCH FORM
		// Open Search Form
 		search_file_button = dojo.byId('search_file');
		dojo.connect(search_file_button, "onclick", open_searchFileForm);
		function open_searchFileForm() { 
			dijit.byId('searchFormFile').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();

			dojo.byId('select_service_file').value = "Select service...";
			dojo.byId('select_state_file').value = "Select state...";
			dojo.byId('ktpk').value = "";

			var selectedService_file = dojo.byId('select_service_file').value;

			if (selectedService_file == 'Pemohonan') {
				queryTask_file =  search_pemohonan_layer;
				// alert('Pemohonan');
			}else{
				if (selectedService_file == 'FT') {
				 	queryTask_file =  search_ft_layer;
					// alert('FT');
				}else{
					if (selectedService_file == 'QT') {
						queryTask_file =  search_qt_layer;
						// alert('QT');
					}
					else{
						selectedService_file =  search_rezab_layer;
						// alert('Rezab');
					}
				} 	
			}
					
		}  
		var selectedService_file = dojo.byId('select_service_file').value;

		

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_file() { 
			var selectedService_file = dojo.byId('select_service_file').value;
			document.getElementById('imageLoadingFile').innerHTML = '<img src="loading.gif" />'; 

			var state_list_file = new Query();
			state_list_file.where = "NAMA_NEGERI <> ''";
			state_list_file.returnGeometry = false;
			state_list_file.outFields = ["NAMA_NEGERI"];
			console.log("sini ok");
			if (selectedService_file == 'Pemohonan') {
				var state_ddown_file = search_pemohonan_layer;
				queryTask_File =  search_pemohonan_layer;

			}else{
				if (selectedService_file == 'FT') {
					var state_ddown_file = search_ft_layer;
					queryTask_File =  search_ft_layer;
				}else{
					if (selectedService_file == 'QT') {
						var state_ddown_file = search_qt_layer;
						queryTask_File =  search_qt_layer;
					}
					else{
						var state_ddown_file =  search_rezab_layer;
						queryTask_File =  search_rezab_layer;
					}
				}
			}
			//var state_ddown_file = state_layer1;
			state_ddown_file.execute(state_list_file, populateList_state_file);

			// B. Initial DISTRICT Dropdown query 			
			/* var district_list = new Query();
			district_list.where = "DISTRICT >''";
			district_list.returnGeometry = false;
			district_list.outFields = ["DISTRICT"];
			
			district_ddown = mukim_layer;
			district_ddown.execute(district_list, populateList_district); */
		}

		
		
		// D. Initialize Query Task
		//queryTask_File =  search_project_layer;     		
		query_File = new Query();
		query_File.returnGeometry = true;
		query_File.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_File = new EnhancedGrid({
					structure: gridFile_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridFile');	
			
		gridResult_File.set("store",clearStore); 
		gridResult_File.startup();


        // ---------------------- Print map -------------------------
		       
			app.printUrl = "http://150.242.180.128/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";
			//https://www.gis.myetapp.gov.my:6443/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task
			esriConfig.defaults.io.proxyUrl = "/proxy/";

	          var mapOnlyIndex, templates;

	          var templateNames = ["A3 Landscape", "A3 Portrait", "A4 Landscape", "A4 Portrait", "Letter ANSI A Landscape", "Letter ANSI A Portrait", "Tabloid ANSI B Landscape", "Tabloid ANSI B Portrait", "MAP_ONLY"];
	          // remove the MAP_ONLY template then add it to the end of the list of templates 
	          mapOnlyIndex = arrayUtils.indexOf(templateNames, "MAP_ONLY");
	          if ( mapOnlyIndex > -1 ) {
	            var mapOnly = templateNames.splice(mapOnlyIndex, mapOnlyIndex + 1)[0];
	            templateNames.push(mapOnly);
	          }
	          // create a print template for each choice
	          templates = arrayUtils.map(templateNames, function(ch) {
	            var plate = new PrintTemplate();
	            plate.layout = plate.label = ch;
	            plate.format = "PDF";
	            plate.layoutOptions = { 
	              "authorText": "Made by:  JKPTG",
	              "copyrightText": "<copyright info here>",
	              "legendLayers": [], 
	              "titleText": "Myetapp GIS", 
	              "scalebarUnit": "Miles" 
	            };
	            return plate;
	          });

	          // create the print dijit
	          app.printer = new Print({
	            "map": map,
	            "templates": templates,
	            url: app.printUrl
	          }, dom.byId("printButton"));
	          app.printer.startup();
	        

	  		
}); // CLOSE FUNCTIONS

	