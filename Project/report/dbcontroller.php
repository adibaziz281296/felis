<?php
class DBController {
	private $host = "192.168.0.145";
	private $user = "sa";
	private $password = "p@ssw0rd";
	private $database = "ifms";
	private $serverName = "FADHILAH-NB\MSSQLSERVER, 1433"; 
	
	function __construct() {
		$conn = $this->connectDB();
		if(!empty($conn)) {
			$this->selectDB($conn);
		}
	}
	
	function connectDB() {
		$conn = sqlsrv_connect($this->host,$this->user,$this->password);
		return $conn;
	}
	
	function selectDB($conn) {
		mssql_select_db($this->database,$conn);
	}
	
	function runQuery($query) {
		$result = sqlsrv_query($query);
		while($row=sqlsrv_fetch_array($result)) {
			$resultset[] = $row;
		}		
		if(!empty($resultset))
			return $resultset;
	}
	
	function numRows($query) {
		$result  = sqlsrv_query($query);
		$rowcount = sqlsrv_num_rows($result);
		return $rowcount;	
	}
}
?>
