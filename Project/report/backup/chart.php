<?php
	include("../../assets/conn/sql_server.php");


/* $connectionInfo = array( "Database"=>$db, "UID"=>$uid, "PWD"=>$pwd);
$conn = sqlsrv_connect( $serverName, $connectionInfo); */
	/* # RETRIEVE STATE LIST	-------------------------------------------------------------------------------
	$sql_state = "SELECT DISTINCT(STATE)
					FROM sde.MUKIM
					ORDER BY STATE"; 
	$params_state  = array();
	$options_state  =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_state  = sqlsrv_query($conn, $sql_state, $params_state , $options_state);	
  
	if( $stmt_state  === false ) { print( print_r( sqlsrv_errors() ) ); }
	
	# RETRIEVE DISTRICT LIST	-------------------------------------------------------------------------------
	$sql_district = "SELECT DISTINCT(DISTRICT)
					FROM sde.MUKIM
					ORDER BY DISTRICT"; 
	$params_district  = array();
	$options_district   =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_district   = sqlsrv_query($conn, $sql_district, $params_district, $options_district);	
  
	if( $stmt_district === false ) { print( print_r( sqlsrv_errors() ) ); } */
	
	# RETRIEVE EVACUATION LIST	-------------------------------------------------------------------------------
	$sql_eva = "SELECT *
			FROM sde.PENEMPATANBANJIR
			ORDER BY nama"; 
	$params_eva = array();
	$options_eva =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_eva = sqlsrv_query($conn, $sql_eva, $params_eva, $options_eva);	
  
	if( $stmt_eva === false ) { print( print_r( sqlsrv_errors() ) ); }
	
//$tsql = sqlsrv_query("SELECT DISTINCT(STATE)FROM sde.MUKIM ORDER BY STATE");
//$stmt = sqlsrv_query( $conn, $tsql); 
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Jana Laporan Bergraf</title>               
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
                        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                 
	<script language="javascript" src="funcs/val.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
	
<script language=JavaScript>
$(document).ready(function(){
           $("#state").on('change',function(){
                 var STATE=$("#state").val();
				 //alert(STATE);
                 $.ajax({
                    type:"POST",
                    url:"getcity.php",
                    data:"STATE="+ STATE,
                    success:function(data){
                          $("#city").html(data);
                    }
                 });
           });
		   
			
			$(".location,.individual,.showlast").hide();
		 
       });
	   
	   
function Dropdown(value)
{
		if(value=="1"){
			
			$(".location,.showlast").show();
			$(".individual").hide();
			
		
		} else if(value=="2"){
			
			
			$(".location").hide();
			$(".individual,.showlast").show();
			

		} 
}

</script>	
    </head>
    <body>           
            <!-- PAGE CONTENT -->
            <div class="page-content">                             
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!--<form class="form-horizontal" name="form" onSubmit="return val_add(this, 'add_val.php')" method="post"> -->
							<form class="form-horizontal" name="form" method="post"  action="display_chart.php" target="main" >
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Jana Laporan Bergraf</h3>   
                                </div>
                                <div class="panel-body"> 

								<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Jana Laporan Untuk</label>
                                        <div class="col-md-6 col-xs-12">                                                                                         
                                            <select class="form-control select" name="key" id="reportType" onChange="Dropdown(this.value);" autocomplete="off" required>
											<!--<select class="form-control select" name="reportType" id="reportType">-->
                                                <option value="0">Pilih maklumat...</option>
                                                <option value="1">Mangsa Banjir mengikut Lokasi</option>
												<option value="2">Mangsa Banjir mengikut Individu</option>
                                            </select>
                                        </div>
                                </div>						

								
								<!-- Location Based !-->
								
								<div class="location">
								
								<div class="form-group showlast">
										<label class="col-md-3 col-xs-12 control-label">Rujukan Kategori</label>
										<div class="col-md-6 col-xs-12">          
										<select  class="form-control select" name="saring1" id="saring1">
												<option value="">Pilih maklumat...</option>
												<option value="negeri">Negeri</option>
												<option value="daerah">Daerah</option>
												<option value="jantina">Jantina</option>
											</select>                                                                               
											
										</div>
								</div>
                                 
								<div class="form-group">
										<label class="col-md-3 col-xs-12 control-label">Saring Negeri</label>
										<div class="col-md-6 col-xs-12">                                                                                         
											<select class="form-control select" name="negeri" id="state"  autocomplete="off">
												<option value="">Pilih negeri...</option>
											<?php	 
											$sql = "SELECT DISTINCT(STATE),state_code FROM sde.MUKIM ORDER BY STATE";
											$result=sqlsrv_query($conn,$sql);
											while($STATE = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){ 	
											?>
												<option value="<?php echo $STATE['STATE'] ?>"><?php echo $STATE['STATE'] ?></option>
											<?php } ?>
											</select>
										</div>
								</div>
								
									
								<div class="form-group">
										<label class="col-md-3 col-xs-12 control-label">Saring Daerah</label>
										<div class="col-md-6 col-xs-12" id="city">          
										<select  class="form-control select" name="daerah"  autocomplete="off">
												<option value="0">Pilih daerah...</option>
											</select>                                                                               
											
										</div>
										
								</div>
								
								
								
								
								</div>
								
								
								
								<!-- Individual Based !-->
								
								<div class="individual">
								
								<div class="form-group showlast">
										<label class="col-md-3 col-xs-12 control-label">Rujukan Kategori</label>
										<div class="col-md-6 col-xs-12">          
										<select  class="form-control select" name="saring2" id="saring2">
												<option value="">Pilih maklumat...</option>
												<option value="umur">Umur</option>
												<option value="bangsa">Bangsa</option>
												<option value="jantina">Jantina</option>
											</select>                                                                               
											
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-md-3 col-xs-12 control-label">Saring Umur</label>
										<div class="col-md-6 col-xs-12">                                                                                         
											<select class="form-control select" name="umur" id="umur"  autocomplete="off">
												<option value="">Pilih umur...</option>
											<?php	 
											$sql = "SELECT DISTINCT umur FROM MANGSABANJIR ORDER BY Umur ASC";
											$result=sqlsrv_query($conn,$sql);
											while($ROW = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){ 	
											?>
												<option value="<?php echo $ROW['umur'] ?>"><?php echo $ROW['umur'] ?></option>
											<?php } ?>
											</select>
										</div>
								</div>
									
								<div class="form-group">
										<label class="col-md-3 col-xs-12 control-label">Saring Bangsa</label>
										<div class="col-md-6 col-xs-12">          
										<select  class="form-control select" name="bangsa" id="bangsa" autocomplete="off">
										<option value="">Pilih bangsa...</option>
												<?php	 
											$sql = "SELECT DISTINCT CONVERT(NVARCHAR(MAX),bangsa) 'bangsa' FROM MANGSABANJIR";
											$result=sqlsrv_query($conn,$sql);
											while($ROW = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){ 	
											?>
												<option value="<?php echo $ROW['bangsa'] ?>"><?php echo $ROW['bangsa'] ?></option>
											<?php } ?>
											</select>                                                                               
											
										</div>
								</div>
								
								
								
								
								
								</div>
								
								<div class="form-group showlast">
										<label class="col-md-3 col-xs-12 control-label">Saring Jantina</label>
										<div class="col-md-6 col-xs-12">          
										<select  class="form-control select" name="jantina" id="jantina">
												<option value="">Pilih jantina...</option>
												<option value="Lelaki">Lelaki</option>
												<option value="Perempuan">Perempuan</option>
											</select>                                                                               
											
										</div>
								</div>
								
								
								
								
												 
                                    
									
                                <div class="panel-footer">                            
                                    <button type="submit" class="btn btn-info pull-right">Jana Laporan</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->             
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>                
        <!-- END PLUGINS -->
        
        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <!-- END THIS PAGE PLUGINS -->       
        
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/settings.js"></script>
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->                   
    </body>
</html>






