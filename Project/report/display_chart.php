


<!DOCTYPE html>
<html>
<head>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/excanvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.categories.min.js"></script>
<script src="http://ifms.uum.edu.my/ifms_r3/js/jquery.flot.axislabels.js"></script>

        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
<meta charset=utf-8 />
</head>
<body style="margin:50px 20px;">

<?php


$sumber = $_POST['sumber'];
$saring = $_POST['saring'];

if((!$saring)||(!$sumber))
{
		echo "<center>Graf tidak dapat dijana. Sila pilih Sumber Data dan Rujukan Kategori (Paksi-X) terlebih dahulu.</center>";
		exit;
}

$url = "../../api/request.php?view=graph&source={$_POST['sumber']}&negeri={$_POST['negeri']}&daerah={$_POST['daerah']}&jantina={$_POST['jantina']}&saring={$saring}";


?>

	<center><strong>GRAF JUMLAH MANGSA BANJIR BERDASARKAN <?php echo strtoupper($saring); ?></strong></center>
  <div id="bar-example" style="width:100%; height:400px;"></div>


  <script language="javascript" type="text/javascript">  
$(document).ready(function(){
    $.getJSON("<?php echo $url; ?>", function(json) {
       //succes - data loaded, now use plot:
           var plotarea = $("#bar-example");
           var dataBar=json.dataBar;
           console.log(json);
            $.plot(plotarea, [
                {
                    data: dataBar,
                    bars: {show: true}
                }], {
			series: {
				bars: {
					show: true,
					barWidth: 0.5,
					align: "center"
				}
			},
			xaxis: {
				mode: "categories",
				axisLabel: '<?php echo strtoupper($saring); ?>',
				axisLabelUseCanvas: true
			},
			yaxis: {
				axisLabel: 'JUMLAH MANGSA',
				axisLabelUseCanvas: true
			}
		});
    });
});

</script>     

</body>
</html>

