<?php

include("../../assets/conn/sql_server.php");

$STATE_id = ($_REQUEST["STATE"] <> "") ? trim($_REQUEST["STATE"]) : "";

if ($STATE <> "") {
    $sql = "SELECT DISTINCT(DISTRICT)
					FROM sde.MUKIM WHERE STATE = :STATE ORDER BY DISTRICT";
    try {
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":STATE", trim($STATE_id));
        $stmt->execute();
        $results = $stmt->fetchAll();
    } catch (Exception $ex) {
        echo($ex->getMessage());
    }
    if (count($results) > 0) {
        ?>
        <label>State: 
            <!--<select name="state" onchange="showCity(this);">-->
			<select name="STATE">
                <option value="">Please Select</option>
                <?php foreach ($results as $rs) { ?>
                    <option value="<?php echo $rs["STATE"]; ?>"><?php echo $rs["STATE"]; ?></option>
                <?php  } ?>
            </select>
        </label>
        <?php
    }
}
?>