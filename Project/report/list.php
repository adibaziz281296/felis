<?php
	include("../../assets/conn/sql_server.php");
	session_start();
	$user_login = $_SESSION['login_user'];
	
	# GET AGENSI ID FOR CURRENT USER LEVEL
	$sql_lvl = "SELECT *
				FROM PENGGUNA_SISTEM
				WHERE penggunaID = $user_login";  
	$params_lvl = array();
	$options_lvl =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_lvl = sqlsrv_query($conn, $sql_lvl, $params_lvl, $options_lvl);	
	$row_lvl = sqlsrv_fetch_array( $stmt_lvl, SQLSRV_FETCH_ASSOC);
	$user_login_agency = $row_lvl['agensiID'];

	
	# RETRIEVE FLOOD REPORT INFORMATION	BASED ON USER LEVEL -------------------------------------------------------------------------------
	$sql = "SELECT *
			FROM LAPORAN_BANJIR l, PENGGUNA_SISTEM p, AGENSI a
			WHERE l.penggunaID = p.penggunaID
			AND p.agensiID = a.agensiID
			AND p.agensiID = '$user_login_agency'
			ORDER BY masaMuatNaik"; 
	$params = array();
	$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt = sqlsrv_query($conn, $sql , $params, $options );	
  
	if( $stmt === false ) { print( print_r( sqlsrv_errors() ) ); }
	
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Senarai Laporan Banjir</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                     
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">           
            <!-- PAGE CONTENT -->
            <div class="page-content">
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Senarai Laporan Banjir</h3>                              
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>Tajuk Laporan</th>
                                                <th>Masa Muatnaik</th>
												<th>Laporan Dari Agensi</th>
												<th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php																						  
											  while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) { 	
											  $masa = date_format($row['masaMuatNaik'], 'd/m/Y H:i:s');
											  $nama_fail = $row['namaFail'];
										?>
                                            <tr>
                                                <td><?php echo $row['laporanNama'] ?></td>	
                                                <td><?php echo $masa ?></td>
												<td><?php echo $row['agensiNama'] ?></td>
												<td>
												   <a href="info.php?id=<?php echo $row['laporanID'] ?>"><button class="btn btn-info btn-rounded btn-sm"><span class="fa fa-info"></span>Info</button></a> 
                                                   <a href="uploads/<?php echo $nama_fail ?>"><button class="btn btn-info btn-rounded btn-sm"><span class="fa fa-download"></span>Muatturun</button></a> 
												   <a href="delete.php?id=<?php echo $row['laporanID'] ?>&nama_fail=<?php echo $nama_fail ?>"><button class="btn btn-info btn-rounded btn-sm"><span class="fa fa-trash-o"></span>Hapus</button></a> 
                                                </td>
                                            </tr>
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->                                
            </div>    
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->       
                          
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->                

        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/settings.js"></script>
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 
        
    </body>
</html>






