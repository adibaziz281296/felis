/*
Author: Ahmed
Date: Oct 2015
*/

var ipaddress = 'geo.fortiddns.com';
//"dojo.parser" -- Required if you have dojo widgets (aka. dijits)in your HTML markup 

$("#basic_legend").click(function() {
        $("#wrapper").toggleClass("toggled");
  return false;
});

$(".sidebar-brand").click(function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
});

$("#menu-toggle").click(function(e) {
	$("#wrapper").toggleClass("toggled");
});


//===============================================================//
//							ARCGIS								 //
//===============================================================//

var map,symbol, geomTask, loading;
var gviewClient, Pano, Glocator;
var viewPoint;	
var viewStatus = false;
var viewAddress = "";

      require([
				"dojo/i18n!esri/nls/jsapi",
				"esri/map",
				"esri/dijit/Print",
				"esri/tasks/PrintTemplate",
				"esri/request",
				"esri/config",
				"esri/geometry/Extent",
				"esri/dijit/HomeButton",
				"esri/layers/ArcGISDynamicMapServiceLayer",
				"esri/layers/FeatureLayer",
				"agsjs/dijit/TOC", // file TOC.js dlm folder agsjs/dijit/
				
				
				"esri/geometry/webMercatorUtils", // display coordinate
				"esri/dijit/Search", // google search map
				"esri/dijit/OverviewMap",
				//"esri/dijit/BasemapGallery",
				"esri/dijit/BasemapToggle", // Basemap 2 choices
				"esri/dijit/Scalebar",
				// for Bookmark
				"esri/dijit/Bookmarks",
				"dojo/cookie",
				
				"esri/InfoTemplate",
				"esri/tasks/IdentifyTask",
				"esri/tasks/IdentifyParameters",
				"esri/dijit/Popup",	
				"esri/dijit/PopupTemplate",				
				"esri/symbols/SimpleFillSymbol",
				"esri/symbols/SimpleLineSymbol",
				
				"dojox/layout/FloatingPane",
				"dijit/layout/TabContainer",
				"dijit/form/ComboBox", 
				"dijit/form/Button",
				"dojox/layout/Dock",
				
				"esri/tasks/QueryTask",
				"esri/tasks/query",
				"dojo/data/ItemFileReadStore",
				"dojo/data/ItemFileWriteStore",
				"dojox/grid/EnhancedGrid",
				"dojox/grid/enhanced/plugins/Pagination",
				
				"esri/symbols/SimpleMarkerSymbol",	
				
				"esri/tasks/GeometryService", // for buffer
				"esri/toolbars/draw",
				"esri/graphic",
				"esri/tasks/BufferParameters",
				"dojo/dom",
				"dojo/on",
				"esri/geometry/normalizeUtils",
				"esri/SpatialReference",
				"dgrid/OnDemandGrid", 
				"dojo/store/Memory",
				"dijit/TooltipDialog",
				
				"esri/dijit/Measurement", // for measure
				"esri/urlUtils",
				// for get directions
				"esri/dijit/Directions", 
				"esri/geometry/Point", // for search coordinate			
				"esri/dijit/LocateButton",
				
				// For StreetView
				"esri/symbols/PictureMarkerSymbol", // for flying man streetview
				"esri/symbols/PictureFillSymbol", 
				"esri/symbols/CartographicLineSymbol",
				
				// For Route with Barrier
				"esri/tasks/RouteTask", 
				"esri/tasks/RouteParameters", 
				"esri/tasks/FeatureSet",
				//"esri/SpatialReference",
				"esri/units",
				//"dojo/_base/Color",	
				"dojo/_base/connect", 
				
				"dojo/parser", // scan page for widgets	
				"dojo/_base/array",
				"esri/Color",
				"dojo/query",
				"dojo/promise/all",
				"dojo/dom-class",
				"dojo/dom-construct",
				"dojo/domReady!"
      ], 
	  function (
				esriBundle, Map, Print, PrintTemplate, esriRequest, esriConfig, Extent, HomeButton, ArcGISDynamicMapServiceLayer, FeatureLayer, TOC,
				webMercatorUtils, Search, OverviewMap, //BasemapGallery,
				BasemapToggle, Scalebar, Bookmarks, cookie,
				InfoTemplate, IdentifyTask, IdentifyParameters, Popup, PopupTemplate,
				SimpleFillSymbol, SimpleLineSymbol,
				FloatingPane, TabContainer, ComboBox, Button, Dock,
				QueryTask, Query, ItemFileReadStore, ItemFileWriteStore, EnhancedGrid, pagination,
				SimpleMarkerSymbol,
				GeometryService, Draw, Graphic, BufferParameters, dom, on, normalizeUtils, SpatialReference, OnDemandGrid, Memory, TooltipDialog,
				Measurement, urlUtils, Directions, Point, LocateButton,
				PictureMarkerSymbol, PictureFillSymbol, CartographicLineSymbol, 
				RouteTask, RouteParameters, FeatureSet, Units, conn,
				parser, arrayUtils, Color, query, All, domClass, domConstruct
      ){

		parser.parse(); 

		// ---------------------- RENAME WIDGET'S DEFAULT NAME ---------------------
		esriBundle.widgets.Search.main.placeholder = "Search an address or location";
		esriBundle.widgets.homeButton.home.title = "Full Extent";
		//esriBundle.toolbars.draw.addPoint = "Add a new tree to the map";
		
		esri.bundle.toolbars.draw.addPoint = "Click on the map to draw a point";
		esri.bundle.toolbars.draw.addLine = "Click on the map to draw a point 2";
		esri.bundle.toolbars.draw.start = "Click to start drawing";
		esri.bundle.toolbars.draw.resume = "Click to continue to draw";
		esri.bundle.toolbars.draw.finish = "Click to continue drawing or double-click to finish";
		esri.bundle.toolbars.draw.complete = "Click to continue drawing or double-click to finish";

			
		// ---------------------- GLOBAL VARIABLES DECLARATION -------------------------------
		var identifyParams, identifyTask, tb;
		var center = [108.75,5.391]; // lon, lat
		
		var routeTask, routeParams, routes = [];
        var stopSymbol, barrierSymbol, routeSymbols;
        var mapOnClick_addStops_connect, mapOnClick_addBarriers_connect;
		var clickmap;
		var dynaLayer1, dynaLayer2;
		var app = {};
      	app.map = null; app.printer = null;
		// ---------------------- INFO POPUP PROPS -------------------------------
        var popup = new Popup({ fillSymbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
											new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
											new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25]))
					}, domConstruct.create("div"));
		
 	
		// popup theme
        //dojo.addClass(popup.domNode, "modernGrey");
		 domClass.add(popup.domNode, "dark");
		 
		var latitude = phpVars["value1"];
		var longitude = phpVars["value2"];
		var zoomin = phpVars["value3"];
		
		loading = dom.byId("loadingImg");  //loading image. id

		 map = new Map("mapWrapper", { 
			center: [latitude, longitude],
			zoom: zoomin,
			basemap: "osm",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false*/
			infoWindow: popup,
			showAttribution: false
		});
				

		// ---------------------- DEFINE SERVICES -------------------------------	
		esriConfig.defaults.io.proxyUrl = "/proxy/";
        esriConfig.defaults.io.alwaysUseProxy = false;
		
		//all requests to route.arcgis.com will proxy to the proxyUrl defined in this object.
        urlUtils.addProxyRule({ urlPrefix: "route.arcgis.com", proxyUrl: "/DotNet/proxy.ashx" });
        urlUtils.addProxyRule({ urlPrefix: "traffic.arcgis.com", proxyUrl: "/DotNet/proxy.ashx" }); //http://<yourServer>/<pathToProxy>/proxy.ashx
		
	
/////////////////////////////////////////////////////////

			  dynaLayer1 = new ArcGISDynamicMapServiceLayer("http://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/sani/1MyMap/MapServer", {
                opacity: 0.8
              });
			  
			  dynaLayer2 = new ArcGISDynamicMapServiceLayer("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer", {
                opacity: 0.8
              });
			  
			  dynaLayer3 = new ArcGISDynamicMapServiceLayer("http://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/jalan_jkr/Rangkaian_Jalan_JKR/MapServer", {
                opacity: 0.8
              });
			  
             
			  map.on('layers-add-result', function(evt){
                // overwrite the default visibility of service.
                // TOC will honor the overwritten value.
                dynaLayer1.setVisibleLayers([]);
				dynaLayer2.setVisibleLayers([]);
				dynaLayer3.setVisibleLayers([]);
                //try {
                  toc = new TOC({
                    map: map,
                    layerInfos: [
					/*{
                      layer: featLayer1,
                      title: "FeatureLayer1"
                    },*/ 
					
					{
                      layer: dynaLayer2,
                      title: "Charting"
                      //collapsed: false, // whether this root layer should be collapsed initially, default false.
                      //slider: false // whether to display a transparency slider.
                    },
					
					{
                      layer: dynaLayer1,
                      title: "1 Malaysia Map"
                      //collapsed: false, // whether this root layer should be collapsed initially, default false.
                      //slider: false // whether to display a transparency slider.
                    },
										
					{
                      layer: dynaLayer3,
                      title: "JKR Road"
                      //collapsed: false, // whether this root layer should be collapsed initially, default false.
                      //slider: false // whether to display a transparency slider.
                    }					
					]
                  }, 'tocDiv');
                  toc.startup();
         
			
                //} catch (e) {  alert(e); }
              });
              map.addLayers([dynaLayer2, dynaLayer1, dynaLayer3]);



/////////////////////////////////////////////////////////		
		
		// For Buffer & Measure
		esriConfig.defaults.geometryService = new GeometryService("http://"+ipaddress+"/arcgis/rest/services/Utilities/Geometry/GeometryServer");
		var geometryService = new GeometryService("http://"+ipaddress+"/arcgis/rest/services/Utilities/Geometry/GeometryServer"); 
		
		// Create empty store for each search result's grid
		var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});

		// Search (Query Task) service
		var search_pemohonan_layer = new QueryTask("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer/0");
		var search_qt_layer = new QueryTask("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer/1");
		var search_ft_layer = new QueryTask("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer/2");
		var search_rezab_layer = new QueryTask("http://"+ipaddress+"/arcgis/rest/services/JKPTG/charting/MapServer/3");


		// ---------------------- INITIALIZE MAP --------------------------------	
		/*var extentMalaysia = new Extent({
			xmin: 98.947096,
			ymin: 0.358901,
			xmax: 119.689284,
			ymax: 8.414430,
			spatialReference: {
				wkid: 4326
			}
		});*/
						
		/*map = new Map("mapWrapper", { 
			extent: extentMalaysia,
			basemap: "hybrid",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false
			infoWindow: popup,
			showAttribution: false,
			zoom: 6
		});*/
		
		/*map = new Map("mapWrapper", { 
			center: [107.988844, 4.550888],
			zoom: 6,
			basemap: "osm",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false
			infoWindow: popup,
			showAttribution: false
		});*/
		
		//"streets" | "satellite" | "hybrid" | "topo" | "gray" | "dark-gray" | "oceans"| "national-geographic" | "terrain" | "osm" | "dark-gray-vector" | "gray-vector" | "streets-vector" | "streets-night-vector" | "streets-relief-vector" | "streets-navigation-vector" | "topo-vector"
		
		//if there are url params zoom to location   
		var coords, zoomLevel;  
		var urlObject = esri.urlToObject(document.location.href);  
	
		// Infowindows
		map.infoWindow.resize(550, 250);			

		// Resize map
		map.on('resize', function (evt) {
			var pnt = evt.target.extent.getCenter();
			setTimeout(function () { evt.target.centerAt(pnt);}, 500);
		});
		
		// ---------------------- Activate HOME button ----------------------
		var home = new HomeButton({ map: map}, "HomeButton");      	
		home.title = "This is the new title"; 
		home.startup();	
		
		map.on("load", function() {
          //after map loads, connect to listen to mouse move & drag events
          map.on("mouse-move", showCoordinates);
          map.on("mouse-drag", showCoordinates);
		  
		  //dijit.byId('measurePane').hide();		  
		  //map.on("click", runIdentifies);
        });
		
		on(map, "update-start", showLoading);
		on(map, "update-end", hideLoading);
		
		 function showLoading() {
			esri.show(loading);
			map.disableMapNavigation();
			map.hideZoomSlider();
		  }

		 function hideLoading(error) {
			esri.hide(loading);
			map.enableMapNavigation();
			map.showZoomSlider();
		  }
		
		
		
		// InfoWindow's Handler ---------------------------------------------------
		/*var mapInfo = map.on("click", connectInfo);	
		function connectInfo(evt){
			  //alert("connect");
			  console.log("connect info");
			  if(!evt) mapInfo = map.on("click", runIdentifies);
			  else runIdentifies(evt);
		} 
			
		function disconnectInfo(){
			  //alert("disconnect");
			  console.log("disconnect info");
			  mapInfo.remove();
		}*/
		//var klikInfo = map.on("click", executeIdentifyTask);
		function connectInfo(){
			console.log("connect info");
			klikInfo = map.on("click", executeIdentifyTask);				
		}

		function disconnectInfo(){
			console.log("disconnect info");	
			dojo.disconnect(klikInfo);
			klikInfo.remove();
		}

		
		// ---------------------- ERASE button ----------------------
		on(dom.byId("EraseButton"), "click", function(){
			if(map){ 
				map.graphics.clear(); 
				connectInfo();
				
				// stop buffer
				if(tb) tb.deactivate(); 
				
				// stop measure
				measurement.setTool("area",false); 
				measurement.setTool("distance",false);
				measurement.setTool("location",false);
				measurement.clearResult(); 		
				clickmap.remove();

			}
        }); 
		
		// ---------------------- LOCATE/Geolocation button ----------------------
		geoLocate = new LocateButton({
			map: map
		}, "LocateButton");
		geoLocate.startup();

		// ---------------------- Show Coordinates ----------------------
        function showCoordinates(evt) {
			//the map is in web mercator but display coordinates in geographic (lat, long)
			var mp = webMercatorUtils.webMercatorToGeographic(evt.mapPoint);
			//display mouse coordinates
			dom.byId("coordinate_info").innerHTML = "X: "+ mp.x.toFixed(6) + ", Y: " + mp.y.toFixed(6);
        }
		
		// ---------------------- SCALEBAR --------------------------------
		var scalebar = new Scalebar({
			map: map,
			attachTo: 'bottom-left',
			scalebarStyle: 'line',
			scalebarUnit: 'dual'
		});

		// ------------------------ SEARCH MAP LOCATION TASK ----------------------------
		var search = new Search({
			map: map,   
		}, "searchBox");				
	
		search.sources[0].countryCode = "MY";
		/* searchExtent= {
						"xmin": -13052769,
						"ymin": 3951172,
						"xmax": -13019630,
						"ymax": 3978490,
						"spatialReference": {
							"wkid": 3395
						}
					} */
		search.startup();
		
		// ------------------------------- OVERVIEW MAP-------- -----------------------
		var overviewMapDijit = new OverviewMap({
			map: map,
			attachTo: "bottom-right",
			/*color:"#fdf874",*/
			height : 200,
			width : 225,
			opacity: .40
        });
		
        overviewMapDijit.startup();
		
		// ---------------------- IDENTIFY TASK --------------------------------------	
        function runIdentifies(evt) {  
			identifyResults = [];  
			idPoint = evt.mapPoint;  
			var layers = dojo.map(map.layerIds, function (layerId) {  
				//console.log("Layer identify: "+layerId);
				return map.getLayer(layerId);  
			});  
			
			layers = dojo.filter(layers, function (layer) {  
				if (layer.visibleLayers[0] !== -1) {  
					return layer.getImageUrl && layer.visible 
					//console.log("Layer visible: "+ layer.visible);
				}  
			}); //Only dynamic layers have the getImageUrl function. Filter so you only query visible dynamic layers  
			var tasks = dojo.map(layers, function (layer) {  
				return new IdentifyTask(layer.url);  
			}); //map each visible dynamic layer to a new identify task, using the layer url  
			var defTasks = dojo.map(tasks, function (task) {  
				return new dojo.Deferred();  
			}); //map each identify task to a new dojo.Deferred 
			
			var params = createIdentifyParams(layers, evt);  

			var promises = [];  

			for (i = 0; i < tasks.length; i++) {  
				promises.push(tasks[i].execute(params[i])); //Execute each task  
			}  

			var allPromises = new All(promises);  
			allPromises.then(function (r) { showIdentifyResults(r, tasks); });  
        }		

		function showIdentifyResults(r, tasks) {  
			var results = [];  
			var taskUrls = [];  
			r = dojo.filter(r, function (result) {  
				return r[0];  
			});  
			for (i = 0; i < r.length; i++) {  
				results = results.concat(r[i]);  
				for (j = 0; j < r[i].length; j++) {  
					taskUrls = taskUrls.concat(tasks[i].url);  
				}  
			}  
			results = dojo.map(results, function (result, index) {  
				var feature = result.feature;  
				var layerName = result.layerName;  
				var serviceUrl = taskUrls[index];  
				feature.attributes.layerName = result.layerName;  

				/*var template = new InfoTemplate("", "Service Url: " + serviceUrl + "<br/><br/>Layer name: " + result.layerName + "<br/><br/> Object Id: ${OBJECTID}");  
				feature.setInfoTemplate(template); */
				
				switch(layerName){	
					case 'Pusat Pemindahan Sementara':
							var info_evacuation = new esri.InfoTemplate();
							info_evacuation.setTitle("Pusat Pemindahan Sementara");
							info_evacuation.setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${nama}</td></tr>" +
														"<tr><td><b>KAPASITI</b></td><td><b>: </b>${kapasiti}</td></tr>" +
														"<tr><td><b>JENIS</b></td><td><b>: </b>${jenis}</td></tr>" +
														"<tr><td><b>MUKIM</b></td><td><b>: </b>${mukim}</td></tr>" +
														"<tr><td><b>DAERAH</b></td><td><b>: </b>${daerah}</td></tr>" +
														"<tr><td><b>BIL. MANGSA BERDAFTAR</b></td><td><b>: </b>${bil_mangsa_daftar}</td></tr>" +
														"<tr><td><b>SENARAI MANGSA BANJIR</b></td><td><b>: </b><a href='../assets/flood_report/victim_list.xls' target='blank'>HYPERLINK</a></td></tr>" +
														"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
														"<tr><td colspan='2' align='center'><img src='../assets/img/Point Of Interest/${photos}' width='100%' height='50%'</td></tr>" +
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_evacuation);   			
					break;	
					
					//case 'Pusat Tindakan Kecemasan':
					case 'Semua Pusat':
					case 'Hospital':
					case 'Balai Polis':
					case 'Balai Bomba & Penyelamat':
							var info_emergency = new esri.InfoTemplate();
							info_emergency.setTitle("Pusat Tindakan Kecemasan");
							info_emergency.setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${Nama}</td></tr>" +
														"<tr><td><b>KATEGORI</b></td><td><b>: </b>${Kategori}</td></tr>" +
														"<tr><td><b>DAERAH</b></td><td><b>: </b>${Daerah}</td></tr>" + 
														"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
														"<tr><td colspan='2' align='center'><img src='../assets/img/Kecemasan/${Gambar}' width='100%' height='50%'/></td></tr>" +
													"</table>" 
							 ); 
							 feature.setInfoTemplate(info_emergency);   			
					break;	
					
					//case 'Stesen Hujan':
					case 'Semua Stesen':
							var info_rainStation = new esri.InfoTemplate();
							info_rainStation.setTitle("Stesen Hujan");
							info_rainStation.setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${Nama}</td></tr>" +
														"<tr><td><b>ID Stesen</b></td><td><b>: </b>${ID_Stesen}</td></tr>" +
														"<tr><td><b>Status</b></td><td><b>: </b>${Status}</td></tr>" +
														"<tr><td>&nbsp;</td></tr>" +
														"<tr><td><b><a href='http://infobanjir.water.gov.my/trend.cfm?${ID_Stesen_JPS}' target='blank'>Bacaan Taburan Hujan</a></b></td></tr>" +
														"<tr><td><b><a href='http://infobanjir.water.gov.my/wl_chart_scatter.cfm?${ID_Stesen_JPS}' target='blank'>Graf River Level</a></b></td></tr>" +
														"<tr><td><b><a href='http://infobanjir.water.gov.my/xsection/${ID_XSection}.htm' target='blank'>River Cross Section</a></b></td></tr>" +
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_rainStation);   			
					break;
					
					case 'Taburan Hujan JPS':
							var info_rainLevel = new esri.InfoTemplate();
							info_rainLevel.setTitle("Taburan Hujan JPS");
							info_rainLevel.setContent(
													"<table id='info'>" +
														"<tr><td><b>Nama Stesen Hujan</b></td><td><b>: </b>${Nama}</td></tr>" +
														"<tr><td><b>ID Stesen Hujan</b></td><td><b>: </b>${ID_Stesen}</td></tr>" +
														"<tr><td><b>Taburan Hujan Semasa</b></td><td><b>: </b>${current_rainfall_lvl} mm</td></tr>" +
														"<tr><td><b>Taburan Hujan Ramalan</b></td><td><b>: </b>${forecasted_rainfall_lvl} mm</td></tr>" +
														"<tr><td>&nbsp;</td></tr>" +
														"<tr><td><b><a href='http://infobanjir.water.gov.my/trend.cfm?${ID_Stesen_JPS}' target='blank'>Bacaan Taburan Hujan</a></b></td></tr>" +
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_rainLevel);   			
					break;

					case 'Paras Air Sungai':
							var info_waterLevel = new esri.InfoTemplate();
							info_waterLevel.setTitle("Paras Air Sungai");
							info_waterLevel.setContent(
													"<table id='info'>" +
														"<tr><td><b>Nama Stesen Hujan</b></td><td><b>: </b>${Nama}</td></tr>" +
														"<tr><td><b>ID Stesen Hujan</b></td><td><b>: </b>${ID_Stesen}</td></tr>" +
														"<tr><td><b>Paras Air Sungai Semasa</b></td><td><b>: </b>${current_water_lvl} m</td></tr>" +							
														"<tr><td><b>Paras Air Sungai Ramalan</b></td><td><b>: </b>${forecasted_water_lvl} m</td></tr>" +
														"<tr><td><b>Peringkat Paras</b></td><td><b>: </b>${Water_Level_Type}</td></tr>" +
														"<tr><td>&nbsp;</td></tr>" +														
														"<tr><td><b><a href='http://infobanjir.water.gov.my/wl_chart_scatter.cfm?${ID_Stesen_JPS}' target='blank'>Graf Paras Sungai</a></b></td></tr>" +
														"<tr><td><b><a href='http://infobanjir.water.gov.my/xsection/${ID_XSection}.htm' target='blank'>River Cross Section</a></b></td></tr>" +
														 +												
													"</table>" 
													/* "<table id='info'>" +
														"<tr><td>&nbsp;</td></tr>" +
														"<tr><td><b>Graf Paras Air Sungai:</b></td></tr>" +
														"<tr><td><div style='width:700px; overflow:hidden; height:500px'><iframe src='http://infobanjir.water.gov.my/wl_chart_scatter.cfm?${ID_Stesen_JPS}' width='100%' height='100%' frameborder='0' scroll=no></iframe></div></td></tr>" +		
													"</table>"  */
													
							 ); 
							 
							 feature.setInfoTemplate(info_waterLevel);   			
					break;	
					
					case 'Laporan Awam':
							var info_crowd = new esri.InfoTemplate();
							info_crowd.setTitle("Laporan Awam");
							info_crowd.setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${NAME}</td></tr>" +
														"<tr><td><b>Tarikh</b></td><td><b>: </b>${TARIKH}</td></tr>" +
														"<tr><td><b>Masa</b></td><td><b>: </b>${MASA}</td></tr>" +
														"<tr><td><b>Peranti</b></td><td><b>: </b>${MOBILE_INF}</td></tr>" +
														"<tr><td>&nbsp;</td></tr>" +
														"<tr><td><b>Paras Air Banjir</b></td><td><b>: </b>${PARAS_AIR}</td></tr>" +
														
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_crowd);   			
					break;		
					
					case 'Point of Interest':
							var info_poi = new esri.InfoTemplate();
							info_poi.setTitle("Point of Interest");
							info_poi.setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${NAME}</td></tr>" +
														"<tr><td><b>JENIS</b></td><td><b>: </b>${TYPE}</td></tr>" +
														"<tr><td><b>KATEGORI</b></td><td><b>: </b>${CATEGORY}</td></tr>" +
														"<tr><td><b>DAERAH</b></td><td><b>: </b>${DISTRICT}</td></tr>" +
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_poi);   			
					break;		

					case 'Sungai':
							var info_sungai = new esri.InfoTemplate();
							info_sungai .setTitle("Jalan");
							info_sungai .setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${Nama}</td></tr>" +
														"<tr><td><b>DAERAH</b></td><td><b>: </b>${Daerah}</td></tr>" +
														"<tr><td><b>JENIS</b></td><td><b>: </b>${Jenis}</td></tr>" +
														"<tr><td><b>PANJANG</b></td><td><b>: </b>${Shape_Length}</td></tr>" +
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_sungai);   			
					break;
					
					case 'Jalanraya':
							var info_jalan = new esri.InfoTemplate();
							info_jalan .setTitle("Jalan");
							info_jalan .setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${Nama}</td></tr>" +
														"<tr><td><b>JENIS</b></td><td><b>: </b>${Jenis}</td></tr>" +
														"<tr><td><b>PANJANG</b></td><td><b>: </b>${Shape_Length}</td></tr>" +
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_jalan );   			
					break;	
					
					case 'Sempadan Mukim':
							var info_mukim = new esri.InfoTemplate();
							info_mukim.setTitle("Sempadan Mukim");
							info_mukim.setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${Mukim}</td></tr>" +
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_mukim);   			
					break;
					
					case 'Sempadan Daerah':
							// 37. DISTRICT POPUP INFO: Specify fields & Content
							var info_district = new esri.InfoTemplate();
							info_district.setTitle("Sempadan Daerah");
							info_district.setContent(
													"<table id='info'>" +
														"<tr><td><b>NAME</b></td><td><b>: </b>${NAME}</td></tr>" +
													"</table>" 
							 ); 
							 
							 feature.setInfoTemplate(info_district);   			
					break;	

					case 'Sempadan Negeri':
							// 38. STATE POPUP INFO: Specify fields & Content
							var info_state = new esri.InfoTemplate();
							info_state.setTitle("Sempadan Negeri");
							info_state.setContent(
													"<table id='info'>" +
														"<tr><td><b>NEGERI</b></td><td><b>: </b>${STATE}</td></tr>" +
														"<tr><td><b>PERIMETER</b></td><td><b>: </b>${PERIMETER}</td></tr>" +
														"<tr><td><b>LUAS (HEKTAR)</b></td><td><b>: </b>${HECTARES}</td></tr>" +	
														"<tr><td><b>X Coor</b></td><td><b>: </b>${Xcoor}</td></tr>" +
														"<tr><td><b>Y Coor</b></td><td><b>: </b>${Ycoor}</td></tr>" +
													"</table>"  
							 ); 
							 
							 feature.setInfoTemplate(info_state);   			
					break; 	
					
					default:
							var template = new esri.InfoTemplate("${*}");
							template.setTitle("Maklumat "+ layerName);
							feature.setInfoTemplate(template);
					break;
					
				} //close switch
				
				

				var resultGeometry = feature.geometry;  
				var resultType = resultGeometry.type;  
				return feature;  
			});  

			if (results.length === 0) {  
				map.infoWindow.clearFeatures();  
			} else {  
				map.infoWindow.setFeatures(results);  
			}  
			map.infoWindow.show(idPoint);  
			return results;  
		}  		
		
		function createIdentifyParams(layers, evt) {  
			var identifyParamsList = [];  
			identifyParamsList.length = 0;  
			dojo.forEach(layers, function (layer) {  
				var idParams = new esri.tasks.IdentifyParameters();  
				idParams.width = map.width;  
				idParams.height = map.height;  
				idParams.geometry = evt.mapPoint;  
				//idParams.layerIds = [0, 2];
				idParams.mapExtent = map.extent;  
				idParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_ALL;  
				var visLayers = layer.visibleLayers;  
				if (visLayers !== -1) {  
					var subLayers = [];  
					for (var i = 0; i < layer.layerInfos.length; i++) {  
						if (layer.layerInfos[i].subLayerIds == null)  
							subLayers.push(layer.layerInfos[i].id);  
					}  
					idParams.layerIds = subLayers;  
				} else {  
					idParams.layerIds = [];
					disconnectInfo();
				}  
				idParams.tolerance = 3;  
				idParams.returnGeometry = true;  
				identifyParamsList.push(idParams);  
			});  
			return identifyParamsList;  
		}			
	/* =====================================================================================================
											WIDGETS: BASIC FUNCTIONS 													  
	======================================================================================================== */
		
		// ---------------------- TOC/LEGEND (attached wif toc.js) ---------------------
/*		map.on('layers-add-result', tocRun);
		
		
		function tocRun(evt){
			// If coordinates are from landing page ---------------------------------------------
			//if(urlObject.query && urlObject.query.coords && urlObject.query.zoomLevel){  
			if(urlObject.query && urlObject.query.coords){ 
				var coords = urlObject.query.coords.split(',');  

				var lon = parseFloat(coords[0]);  
				var lat = parseFloat(coords[1]);  


				var zoomLevel = parseInt(urlObject.query.zoomLevel); 
				//var zoomLevel = 5; 			
				var point_assetLocation = esri.geometry.geographicToWebMercator(new esri.geometry.Point(lon,lat));  
				
				//xyPointSymbol = new PictureMarkerSymbol('images/map_pin.png', 32, 32);
						
				mp = new Point(lon, lat);  
				//console.log(mp);
				//graphic_assetLocation = new Graphic(mp, xyPointSymbol);  
				//map.graphics.add(graphic_assetLocation);  
						
				map.centerAndZoom(point_assetLocation, zoomLevel); 
				
				//Set the infoTemplate.
				//map.infoWindow.resize(250,100);
				 
				//var info_assetLocation =  new InfoTemplate("Lokasi Aset", "X: "+lon+"<br>Y: "+lat+"<br>");	         			      
				//graphic_assetLocation.setInfoTemplate(info_assetLocation); 		
			}
			
			// overwrite the default visibility of service.
			// TOC will honor the overwritten value.
			
			
			console.log('sini toc');
			//IFMSdynaLayer.setVisibleLayers([10]);
		   	toc1 = new TOC({
				map: map,
				layerInfos: [{
					layer: ChartingLayer,
					title: "", 
					collapsed: false
				}]
			}, 'tocDiv1');  

			toc1.startup();
			console.log("start toc 1");
			
			toc1.on('load', function(){
				if (console) 
					console.log('TOC 1 loaded');				
			});
			  
			toc1.on('toc-node-checked', function(evt){
				if (console) {
					console.log("TOCNodeChecked1, rootLayer:"
					+(evt.rootLayer?evt.rootLayer.id:'NULL')
					+", serviceLayer:"+(evt.serviceLayer?evt.serviceLayer.id:'NULL')
					+ " Checked:"+evt.checked);
					if (evt.checked && evt.rootLayer && evt.serviceLayer){
					 //evt.rootLayer.setVisibleLayers([evt.serviceLayer.id])
					}
				}
			});
			

		} //close TOCRun function */
		
		// ---------------------- BASEMAP GALLERY ---------------------
		// Open Basemap Pane
 		/*basemap_button = dojo.byId('basic_basemap');
		dojo.connect(basemap_button, "onclick", open_basemapGallerryPane);
		function open_basemapGallerryPane() { dijit.byId('basemapGalleryPane').show(); }  
		
		var basemapGallery = new BasemapGallery({
			showArcGISBasemaps: true,
			map: map
		}, "basemapGalleryDiv");
		 
		basemapGallery.startup();*/
		
		// ---------------------- BASEMAP TOGGLE ---------------------
		var toggle = new BasemapToggle({
			map: map,
			basemap: "satellite"
		}, "BasemapToggle");
      
		toggle.startup();
		
     		
		// ---------------------- LOCATOR COORDINATE ---------------------
		// Open Locator Pane
 		locator_button = dojo.byId('basic_locator');
		dojo.connect(locator_button, "onclick", open_locatorPane);
		function open_locatorPane() { dijit.byId('locatorPane').show(); }  
		
		// Connection so that the dijit button can read the function inside require.
		dijit.byId("search_coordinate").on("click",  locate_Coordinate);
		dijit.byId("clear_coordinate").on("click",  clear_Coordinate);		
		
		function locate_Coordinate(){  
			console.log("got here 2")  
			var lat_value = document.getElementById("latitude").value  
			var long_value = document.getElementById("longitude").value 
			
			xmin =  98.947096;
			ymin =  0.358901;
			xmax = 119.689284;
			ymax = 8.414430;
			
			
			if(lat_value == "" || long_value == "")
				alert ("Please enter X and Y values (Malaysia's extent)");
			else {
				// check for values
				if ( (long_value >= xmin && long_value <= xmax ) && (lat_value >= ymin && lat_value <= ymax ) ) {
					var mp = new Point(long_value, lat_value);  
					var graphic = new esri.Graphic(mp, symbol_coordinates);  
					map.graphics.add(graphic);  
					map.centerAndZoom(mp, 12);
				}
				
				else {
					alert ("Please enter valid X and Y values");
				}
			}
		}	
		
		function clear_Coordinate() {			
			// Remove all graphics on the maps graphics layer
			map.graphics.clear();
						
			// Remove previous search results
			document.getElementById("latitude").value = "";
			document.getElementById("longitude").value = "";
			map.centerAndZoom(center, 5);	
		}
		
		// ---------------------- DRAW ---------------------
		// ---------------------- PRINT ---------------------
		
	/* =====================================================================================================
											WIDGETS: ANALYSIS													  
	======================================================================================================== */		
		// ---------------------- BUFFER ---------------------
		// Open Buffen Pane
 		buffer_button = dojo.byId('analysis_buffer');
		dojo.connect(buffer_button, "onclick", open_bufferPane);
		function open_bufferPane() { //dijit.byId('bufferPane').show();
	} 
		
				var mapInfo = map.on("click", changeHandler);
		
		function changeHandler(evt){
			  if(!evt) mapInfo = map.on("click", executeIdentifyTask);
			  else //executeIdentifyTask(evt);
			  console.log("ok bersih");
			} 
			
				function changeHandlerRemove(){
			 
			  mapInfo.remove();
			   console.log("ok stop");
			}
		
		 // Create a dgrid for buffer 
		var sortAttr = [{attribute: "OBJECTID",  descending: true }];  
		gridBuffer = new OnDemandGrid({  
			store: Memory({  
				idProperty: "OBJECTID"  
			}),  
			columns:{
				OBJECTID: "OBJECTID",
				nama : "",
				//Lebuhraya: "Highway", 
				//Pemilikan: "Ownership", 
		 //       Negeri: "Negeri", 
		 //       Daerah: "Daerah", 
		 //       Mukim: "Mukim", 
			},  
			sort: sortAttr  
		}, "gridBuffer");
		
		gridBuffer.styleColumn("OBJECTID", "display: none;");
		
		var featureLayerBuffer = new FeatureLayer("http://"+ipaddress+"/arcgis/rest/services/IFMS/FloodInfov7/FeatureServer/0", {  
            mode: FeatureLayer.MODE_SELECTION,  
            outFields: ['*']  
          }); 
		  
        map.infoWindow.resize(245,125);  
        markerSymbol = new PictureMarkerSymbol("/assets/img/map_pin.png",16,32);
		
		dialog = new TooltipDialog({ style: "position: absolute; width: 300px; font: normal normal normal 10pt Helvetica;z-index:100" });  
        dialog.startup(); 
                              
        var highlightSymbol = new SimpleFillSymbol(
          SimpleFillSymbol.STYLE_SOLID,
          new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_SOLID,
            new Color([255,0,0]), 3
          ),
          new Color([125,125,125,0.35])
        ); 
		
		//listen for when the onMouseOver event fires on the countiesGraphicsLayer  
        //when fired, create a new graphic with the geometry from the event.graphic and add it to the maps graphics layer  
        featureLayerBuffer.on("mouse-over", function(evt){ 
            
          var t = "<table id='info'>" +
                            "<tr><td><b>Name</b></td><td><b>: </b>${nama}</td></tr>" +
    				        
    					"</table>"; 

            
          var contentBuffer = esriLang.substitute(evt.graphic.attributes,t); 
            
          var highlightGraphic = new Graphic(evt.graphic.geometry,highlightSymbol);  
          map.graphics.add(highlightGraphic);  
              
          dialog.setContent(contentBuffer);  
  
          domStyle.set(dialog.domNode, "opacity", 0.85);  
          dijitPopup.open({  
            popup: dialog,   
            x: evt.pageX,  
            y: evt.pageY  
          });  
        });

		function closeDialog() { 
                map.graphics.clear();  
                dijitPopup.close(dialog);  
        }   
		
		 var symbolPointBuffer = new SimpleFillSymbol().setColor(new Color([0, 0, 255, 0.5]));
        

        featureLayerBuffer.setSelectionSymbol(symbolPointBuffer);  
            
        // change cursor to indicate features are click-able            
        featureLayerBuffer.on("mouse-over", function () {  
            map.setMapCursor("pointer");  
        });  
        featureLayerBuffer.on("mouse-out", function () {  
            map.setMapCursor("default");  
        });  
		
		//featureLayer.on("click", selectGrid);
            
        // Add the Feature Layer To Map    
        map.addLayer(featureLayerBuffer);          

                
		//when the map is clicked create a buffer around the click point of the specified distance.
		buffer_click = map.on("click", function(evt){   
			console.log(dojo.byId("distance").value);
			console.log("buffer-click-map");
			map.graphics.clear();
			
		 
			//define input buffer parameters  
			var params = new BufferParameters();  
			params.geometries  = [ evt.mapPoint ];  
			//params.distances = [dojo.byId("distance").value];
			var distance = dojo.byId("distance").value;
			var correction = '000';
			var real_distance = distance.concat(correction);
			params.distances = [ real_distance ];			
			params.unit = GeometryService[dojo.byId("unit").value];  
			

			  //params.outSpatialReference = map.spatialReference;

			geometryService.buffer(params);
			
			if(map){ changeHandlerRemove(); }				
		});
            
		geometryService.on("buffer-complete", function(result){  
			console.log("buffer-complete");
			//map.graphics.clear(); 
			
			tb = new Draw(map);
			tb.on("draw-end", addGraphic); 
			tb.activate('point');
		    changeHandlerRemove();
			
			// draw the buffer geometry on the map as a map graphic  
			var symbolBuffer = new SimpleFillSymbol(  
				SimpleFillSymbol.STYLE_NULL,  
				new SimpleLineSymbol(  
					SimpleLineSymbol.STYLE_SHORTDASHDOTDOT,  
					new Color([105,105,105]),  
					2  
				),new Color([255,255,0,0.25])  
			);  
			
			var bufferGeometry = result.geometries[0]  
			var graphic = new Graphic(bufferGeometry, symbolBuffer);  
			map.graphics.add(graphic); 
 
			var query = new Query();  
			query.geometry = bufferGeometry;  
			featureLayerBuffer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function(results){ 
				//var totalPopulation = sumPopulation(results);  
				//   var r = "";  
				//   r = "<b>The total Census PLUS land lot is <i>" + results.length + "</i>.</b>";  
				//  dom.byId('messages').innerHTML = r;    
			}); 
			
			// Query for the records with the given object IDs and populate the grid  
			featureLayerBuffer.queryFeatures(query, function (featureSet) {  
				updateGrid(featureSet);  
			});  	
		}); 
                
            
                
		function updateGrid(featureSet) {
			var data = arrayUtils.map(featureSet.features, function (entry, i) {  
				return {  
					OBJECTID: entry.attributes.OBJECTID,  
					nama: entry.attributes.nama, 
					//Lebuhraya: entry.attributes.Lebuhraya,
					//Pemilikan: entry.attributes.Pemilikan 
					// Negeri: entry.attributes.Negeri,
					// Daerah: entry.attributes.Daerah,
					// Mukim: entry.attributes.Mukim			 
				};  
			});  
			gridBuffer.store.setData(data);  
			gridBuffer.refresh(); 
		} 
    	  
        gridBuffer.on(".dgrid-row:click", function (evt) {
			var object = gridBuffer.row(evt.target).data; 
			var zoomObject = object.OBJECTID;
			featureLayerBuffer.clearSelection();

			var query = new Query();
			query.objectIds = [zoomObject];
			featureLayerBuffer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function(features) {
				//zoom to the selected feature
				var stateExtent = features[0].geometry.getExtent().expand(5.0);
				map.setExtent(stateExtent);
			});
		});
                
        btnBufferClear = dojo.byId('clearBuffer');
        dojo.connect(btnBufferClear, "onclick", function(evt){
			map.graphics.clear();
			gridBuffer.store.setData("");  
			gridBuffer.refresh(); 
			dojo.byId("distance").value="";
			console.log('buffer clear');
			
			changeHandler();
			//click_infolot = map.on("click", infoconnect);
			//var c = "<b>Click on the map to select PLUS land lot</b>";  
			   // dom.byId('messages').innerHTML = c;
		});
			
           
		function addGraphic(evt) {
			//deactivate the toolbar and clear existing graphics 
			console.log("addGraphic");
			map.graphics.clear();
			tb.deactivate(); 
			//map.enableMapNavigation();
			var buffer_center = new Graphic(evt.geometry, markerSymbol);  
			map.graphics.add(buffer_center);
			changeHandlerRemove();
			
		}        

        //--------------------- NEW BUFFER WIDGET END-----------------------//
		

		//Setup button click handlers -stop buffer
        /*on(dom.byId("clearBuffer"), "click", function(){
			if(map){ 
				map.graphics.clear(); 
				tb.deactivate(); 
				connectInfo();
			}
        }); 
			
        //click handler for the draw tool buttons
        query("#point").on("click", activateToolbar);
		query("#line").on("click", activateToolbar);
		query("#polyline").on("click", activateToolbar);
		query("#polygon").on("click", activateToolbar);
		
		function activateToolbar(evt) {
			if(tb){
				console.log("activate tb");
				tb.activate(evt.target.id);
				disconnectInfo(); 
			}	
		}
		
		map.on("load", initToolbar);
		
		function initToolbar(evtObj) { console.log("init toolbar");
			tb = new Draw(evtObj.map, { showDrawtips: true });
			tb.on("draw-end", doBuffer);

		}
		
		function doBuffer(evtObj) { console.log("do buffer");
			tb.deactivate();
			var geometry = evtObj.geometry, symbol;
			console.log(geometry.type);
			switch (geometry.type) {
			   case "point":
				 symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255,0,0]), 1), new Color([0,255,0,0.25]));
				 break;
			   case "polyline":
				 symbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255,0,0]), 1);
				 break;
			   case "polygon":
				 symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NONE, new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT, new Color([255,0,0]), 2), new Color([255,255,0,0.25]));
				 break;
			}

			  var graphic = new Graphic(geometry, symbol);
			  map.graphics.add(graphic);

			  //setup the buffer parameters
			  var params = new BufferParameters();
			  var distance = dom.byId("distance").value;
			  var correction = '000';
			  var real_distance = distance.concat(correction);
			  params.distances = [ real_distance ];
			  params.outSpatialReference = map.spatialReference;
			  params.unit = GeometryService[dom.byId("unit").value];
			  //normalize the geometry 

			  normalizeUtils.normalizeCentralMeridian([geometry]).then(function(normalizedGeometries){
				var normalizedGeometry = normalizedGeometries[0];
				if (normalizedGeometry.type === "polygon") {
				  //if geometry is a polygon then simplify polygon.  This will make the user drawn polygon topologically correct.
				  esriConfig.defaults.geometryService.simplify([normalizedGeometry], function(geometries) {
					params.geometries = geometries;
					esriConfig.defaults.geometryService.buffer(params, showBuffer);
				  });
				} else {
				  params.geometries = [normalizedGeometry];
				  esriConfig.defaults.geometryService.buffer(params, showBuffer);
				}

			  });
        }
		
		function showBuffer(bufferedGeometries) {
          var symbol = new SimpleFillSymbol(
            SimpleFillSymbol.STYLE_SOLID,
            new SimpleLineSymbol(
              SimpleLineSymbol.STYLE_SOLID,
              new Color([255,0,0,0.65]), 2
            ),
            new Color([255,0,0,0.35])
          );

          arrayUtils.forEach(bufferedGeometries, function(geometry) {
            var graphic = new Graphic(geometry, symbol);
            map.graphics.add(graphic);
          });

        }*/
		
		// ---------------------- BOOKMARK---------------------

		
		// Open Measure Pane
 	// 	bookmark_button = dojo.byId('bookmark');
		// dojo.connect(bookmark_button, "onclick", open_bookmarkPane);
		// function open_bookmarkPane() 
		// { 

		// 	dijit.byId('streetviewPane').hide();
		// } 
			
		// ---------------------- MEASURE ---------------------
		var measurement = new Measurement({
			map: map
		}, dom.byId("measurementDiv"));
		

		
		// Open Measure Pane
 		measure_button = dojo.byId('analysis_measure');
		dojo.connect(measure_button, "onclick", open_measurePane);
		function open_measurePane() { 
			dijit.byId('measurePane').show(); 
			disconnectInfo(); 
			dijit.byId('streetFormView').hide();
			map.graphics.clear();
			clickmap.remove();
            connectInfo();
		} 
		
		//Setup button click handlers -stop measure
        on(dom.byId("clearMeasure"), "click", function(){
			if(map){ 
				map.graphics.clear(); 
				measurement.setTool("area",false); 
				measurement.setTool("distance",false);
				measurement.setTool("location",false);
				measurement.clearResult(); 
				connectInfo(); 
			}
        }); 
		
		measurement.startup();
				
		//minimizeWindow()	
		/*on(dijit.byId('measurePane'), "minimize", function(){		
		//on(dijit.byId('measurePane'), "hide", function(){
			alert("close");
			/*if(map){ 
				map.graphics.clear(); 
				measurement.setTool("area",false); 
				measurement.setTool("distance",false);
				measurement.setTool("location",false);
				measurement.clearResult();
				connectInfo(); 
			}
		}); */

		//var sfs = new SimpleFillSymbol("solid", new SimpleLineSymbol("solid", new Color([195, 176, 23]), 2), null);
		
		//dojo.keys.copyKey maps to CTRL on windows and Cmd on Mac., but has wrong code for Chrome on Mac
		// 'snapping' = enable the '+' symbol
        /* var snapManager = map.enableSnapping({
			snapKey: has("mac") ? keys.META : keys.CTRL
        });
		
        var layerInfos = [{
			layer: parcelsLayer
        }];
        snapManager.setLayerInfos(layerInfos); */



		// ---------------------- EXPORT DATA ---------------------
		// ---------------------- GET DIRECTIONS ---------------------
		// Open Directions Pane
 	// 	directions_button = dojo.byId('analysis_direction');
		// dojo.connect(directions_button, "onclick", open_directionsPane);
		// function open_directionsPane() { dijit.byId('directionsPane').show(); } 
		
		// //travelModesServiceUrl option points to logistics service hosted from ArcGIS Online
  //       //by indicating this URL, a login prompt should not display
  //       var directions = new Directions({
		// 	map: map,
		// 	travelModesServiceUrl: "http://utility.arcgis.com/usrsvcs/servers/cdc3efd03ddd4721b99adce219629489/rest/services/World/Utilities/GPServer"
		// 	},"directionsDiv");
			
		// directions.startup();
		
// ---------------------- ROUTE AND BARRIER ---------------------
		// Open Route Pane
 		/*var route_button = dojo.byId('analysis_route');
		dojo.connect(route_button, "onclick", open_routePane);
		function open_routePane() { dijit.byId('routePane').show(); disconnectInfo();} 
		
		// Create route task
		// routeTask = new RouteTask("http://tasks.arcgisonline.com/ArcGIS/rest/services/NetworkAnalysis/ESRI_Route_NA/NAServer/Route");
		//routeTask = new RouteTask("http://sampleserver3.arcgisonline.com/ArcGIS/rest/services/Network/USA/NAServer/Route");
		routeTask = new RouteTask("http://utility.arcgis.com/usrsvcs/servers/b67b724b883b417e8818eda3efd5d260/rest/services/World/Route/NAServer/Route_World?token=Adto-ZTQVogina8UZ-v0HCMgyPB-3M-druhPXPHJkoCxMuIZhhjjDGiyiAJm1c0KGbVyUkl3IJD7YUE7gsFQQdXTw2JijiQ0iB_If98JHZ3-mf4xqm4eOGQQS3JiOMBzA4YItIAyd7du9tky62pI3B0BG9dwWRG_P_tKiFd3hH8cWHaUz6KuXj_ZnJc0g2CI");
        //routeTask = new RouteTask("https://route.arcgis.com/arcgis/rest/services/World/Route/NAServer/Route_World");
        routeParams = new RouteParameters();
        routeParams.stops = new FeatureSet();
        routeParams.barriers = new FeatureSet();
        routeParams.outSpatialReference = {"wkid":102100};
        
        routeTask.on("solve-complete", showRoute);
        routeTask.on("error", errorHandler);
                
        stopSymbol = new SimpleMarkerSymbol().setStyle(SimpleMarkerSymbol.STYLE_CROSS).setSize(15);
        stopSymbol.outline.setWidth(3);

        barrierSymbol = new SimpleMarkerSymbol().setStyle(SimpleMarkerSymbol.STYLE_X).setSize(10);
        barrierSymbol.outline.setWidth(3).setColor(new Color([255,0,0]));

        routeSymbols = {
          "Route 1": new SimpleLineSymbol().setColor(new Color([0,0,255,0.5])).setWidth(5),
          "Route 2": new SimpleLineSymbol().setColor(new Color([0,255,0,0.5])).setWidth(5),
          "Route 3": new SimpleLineSymbol().setColor(new Color([255,0,255,0.5])).setWidth(5)
        };
                        
        //button click event listeners can't be added directly in HTML when the code is wrapped in an AMD callback
        on(dom.byId("addStopsBtn"), "click", addStops);
        on(dom.byId("clearStopsBtn"), "click", clearStops);
        on(dom.byId("addBarriersBtn"), "click", addBarriers);
        on(dom.byId("clearBarriersBtn"), "click", clearBarriers);
        on(dom.byId("solveRoutesBtn"), "click", solveRoute);
        on(dom.byId("clearRoutesBtn"), "click", clearRoutes);        
      
        //Begins listening for click events to add stops
        function addStops() {
          //removeEventHandlers();
          mapOnClick_addStops_connect = map.on("click", addStop);
        }

        //Clears all stops
        function clearStops() {
          removeEventHandlers();
          for (var i=routeParams.stops.features.length-1; i>=0; i--) {
            map.graphics.remove(routeParams.stops.features.splice(i, 1)[0]);
          }
        }

        //Adds a stop. The stop is associated with the route currently displayed in the dropdown
        function addStop(evt) {
          routeParams.stops.features.push(
            map.graphics.add(
              new esri.Graphic(
                evt.mapPoint,
                stopSymbol,
                { RouteName:dom.byId("routeName").value }
              )
            )
          );
        }

        //Begins listening for click events to add barriers
        function addBarriers() {
          removeEventHandlers();
          mapOnClick_addBarriers_connect = on(map, "click", addBarrier);
        }

        //Clears all barriers
        function clearBarriers() {
          removeEventHandlers();
          for (var i=routeParams.barriers.features.length-1; i>=0; i--) {
            map.graphics.remove(routeParams.barriers.features.splice(i, 1)[0]);
          }
        }

        //Adds a barrier
        function addBarrier(evt) {
          routeParams.barriers.features.push(
            map.graphics.add(
              new esri.Graphic(
                evt.mapPoint,
                barrierSymbol
              )
            )
          );
        }

        //Stops listening for click events to add barriers and stops (if they've already been wired)
        function removeEventHandlers() {        
          if (mapOnClick_addStops_connect) {
            mapOnClick_addStops_connect.remove();
          }
          if (mapOnClick_addBarriers_connect) {
            mapOnClick_addBarriers_connect.remove();
          }
        }

        //Solves the routes. Any errors will trigger the errorHandler function.
        function solveRoute() {
          removeEventHandlers();
          routeTask.solve(routeParams);
        }

        //Clears all routes
        function clearRoutes() {
          for (var i=routes.length-1; i>=0; i--) {
            map.graphics.remove(routes.splice(i, 1)[0]);
          }
          routes = [];
        }

        //Draws the resulting routes on the map
        function showRoute(evt) {
          clearRoutes();

          arrayUtils.forEach(evt.result.routeResults, function(routeResult, i) {
            routes.push(
              map.graphics.add(
                routeResult.route.setSymbol(routeSymbols[routeResult.routeName])
              )
            );
          });

          var msgs = ["Server messages:"];
          arrayUtils.forEach(evt.result.messages, function(message) {
            msgs.push(message.type + " : " + message.description);
          });
          if (msgs.length > 1) {
            alert(msgs.join("\n - "));
          }
        }

        //Reports any errors that occurred during the solve
        function errorHandler(err) {
          alert("An error occured\n" + err.message + "\n" + err.details.join("\n"));
        }*/
		

	/* =====================================================================================================
											WIDGETS: EXTRA													  
	======================================================================================================== */	
		// ---------------------- SOCIAL MEDIA ---------------------
		// ---------------------- STREETVIEW ---------------------	
		/*streetview = dojo.byId('extra_street');
		dojo.connect(streetview, "onclick", streetmark);
			
		function streetmark() {         
                //console.log("open bookmark");
				dijit.byId('street_panel').show();	
				dojo.connect(map, "onClick", doStreetView); 
		} 
		
		symbol = new PictureMarkerSymbol("../assets/img/flying_man.png", 32, 34);
            //set up street view
            Glocator = new google.maps.Geocoder();
            gviewClient = new google.maps.DirectionsService();
			sv = new google.maps.StreetViewService();
            Pano = new google.maps.StreetViewPanorama(document.getElementById("streetview"));
            google.maps.event.addListener(Pano, "error", handleNoFlash);
			
			function resizeMap() {
            //resize the map when the browser resizes - view the 'Resizing and repositioning the map' section in  
            //the following help topic for more details http://help.esri.com/EN/webapi/javascript/arcgis/help/jshelp_start.htm#jshelp/inside_faq.htm 
            var resizeTimer;
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                map.resize();
                map.reposition();
            }, 500);
        }
        function doStreetView(evt) {
            map.graphics.clear();
            viewPoint = esri.geometry.webMercatorToGeographic(evt.mapPoint);
            sv.getPanoramaByLocation(new google.maps.LatLng(viewPoint.y, viewPoint.x), 100, showStreetView);

        }
        function handleNoFlash(errorCode) {
            if (errorCode == FLASH_UNAVAILABLE) {
                alert("Error: Flash doesn't appear to be supported by your browser");
                return;
            }
        }
		
		function showStreetView(data, status) {
			if(status == google.maps.StreetViewStatus.OK) {   //street view is available 
				map.infoWindow.hide(); 
				//alert("clicked");
                viewStatus = true;
                viewAddress = data.location.description;
                google.maps.event.addListener(Pano, 'initialized', streetViewChanged); // add an event handling street view changes
				var markerPanoID = data.location.pano;
      			// Set the Pano to use the passed panoID
      			Pano.setPano(markerPanoID);
                Pano.setPov(data.location.latlng, { heading:34, pitch:10 }); //push the pano to a this location
				Pano.setVisible(true);
                //add graphic
                var graphic = new esri.Graphic(esri.geometry.geographicToWebMercator(viewPoint), symbol);
                map.graphics.add(graphic);
            }
            else{ //no street view for this location
				//alert("No street view for this location");
                //if (Pano != null) Pano.hide(); //hide the street view
                viewStatus = false;
                viewAddress = "";
            }
            var locationpoint = new google.maps.LatLng(viewPoint.y, viewPoint.x);
            Glocator.geocode({'latLng': locationpoint}, function(results, status) {
			var addr_type = results[0].types[0];	// type of address inputted that was geocoded
			if ( status == google.maps.GeocoderStatus.OK ) 
				showStreetAddress(locationpoint, results[0].formatted_address, addr_type );
			else     
				alert("Geocode was not successful for the following reason: " + status);        
				}); //show street address info
        }
		
        function showStreetAddress(latlng, address, addr_type) {
			//alert("masuk doh");
            var descContent = "";
            descContent = "<table width='95%'><tr><td><b>View Point Address:</b></td><td>" + address + "</td></tr><tr><td><b>View Point Lat/Lon:</b></td><td>"
            descContent += latlng + "</td></tr><tr><td>";
            descContent += "<b>Address type:</b></td><td>" + addr_type + "<td><tr></table>";
            document.getElementById("Desc").innerHTML = descContent;
        }
        function getlocalAddress(Addr) {
            var index = Addr.indexOf(',');
            return Addr.substring(0, index);
        }
        function streetViewChanged(streetLocation) {
            viewStatus = true;
            viewAddress = streetLocation.description;
            var point = new esri.geometry.Point({"x": streetLocation.latlng.lng(), "y": streetLocation.latlng.lat(), " spatialReference": { " wkid": 4326} });
            //add to the map
            map.graphics.clear();
            var symbolPoint = esri.geometry.geographicToWebMercator(point);
            map.graphics.add(new esri.Graphic(symbolPoint, symbol));
            map.centerAt(symbolPoint);
            Glocator.getLocations(streetLocation.latlng, showStreetAddress);
        }*/

		// Sampai sini saje
		/*var markerSymbol = new SimpleMarkerSymbol();
        markerSymbol.setPath("M16,4.938c-7.732,0-14,4.701-14,10.5c0,1.981,0.741,3.833,2.016,5.414L2,25.272l5.613-1.44c2.339,1.316,5.237,2.106,8.387,2.106c7.732,0,14-4.701,14-10.5S23.732,4.938,16,4.938zM16.868,21.375h-1.969v-1.889h1.969V21.375zM16.772,18.094h-1.777l-0.176-8.083h2.113L16.772,18.094z");
        markerSymbol.setColor(new Color("#00FFFF"));
		
        var lineSymbol = new CartographicLineSymbol(
          CartographicLineSymbol.STYLE_SOLID,
          new Color([255,0,0]), 3, 
          CartographicLineSymbol.CAP_ROUND,
          CartographicLineSymbol.JOIN_MITER, 5
        );

        // fill symbol used for extent, polygon and freehand polygon, use a picture fill symbol
        // the images folder contains additional fill images, other options: sand.png, swamp.png or stiple.png
        var fillSymbol = new PictureFillSymbol(
          "images/mangrove.png",
          new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_SOLID,
            new Color('#000'), 
            1
          ), 
          42, 
          42
        );
		
		function initToolbar() {
          tb = new Draw(map);
          tb.on("draw-end", addGraphic);

          // event delegation so a click handler is not
          // needed for each individual button
          on(dom.byId("info"), "click", function(evt) {
            if ( evt.target.id === "info" ) {
              return;
            }
            var tool = evt.target.id.toLowerCase();
            map.disableMapNavigation();
            tb.activate(tool);
          });
        }
		
		 function addGraphic(evt) {
          //deactivate the toolbar and clear existing graphics 
          tb.deactivate(); 
          map.enableMapNavigation();

          // figure out which symbol to use
          var symbol;
          if ( evt.geometry.type === "point" || evt.geometry.type === "multipoint") {
            symbol = markerSymbol;
          } else if ( evt.geometry.type === "line" || evt.geometry.type === "polyline") {
            symbol = lineSymbol;
          }
          else {
            symbol = fillSymbol;
          }

          map.graphics.add(new Graphic(evt.geometry, symbol));
        }*/
			
		// ===============================================================================
		// 									QUERY TASK 
		// ===============================================================================	
		// ---------------------- STYLE for searched features ----------------------
		// Create symbol for search features
		/*symbolSearch = new SimpleMarkerSymbol();
		symbolSearch.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
		symbolSearch.setSize(10);
		symbolSearch.setColor(new dojo.Color([255,255,0,0.5]));*/
		
		var symbol_coordinates = new SimpleMarkerSymbol(  
			SimpleMarkerSymbol.STYLE_CIRCLE, 15,  
			new SimpleLineSymbol( "solid", new Color([0, 0, 255, 0.5]), 8),  
			new Color([0, 0, 255])  
		);  
		
		symbolSearch = new PictureMarkerSymbol("assets/img/map_pin.png", 16, 32);
		
		polygonsymbol = new SimpleFillSymbol( "solid", new SimpleLineSymbol("solid", new Color([232,104,80]), 2),
			new Color([232,104,80,0.25])
		);
		  
		// ---------------------- 1. SEARCH: LOT -------------------------
		dijit.byId("select_service_lot").on("change",  populateList_State_setbyService_lot);
		dijit.byId("select_state_lot").on("change",  populateList_District_setbyState_lot);
		dijit.byId("select_district_lot").on("change",  populateList_Mukim_setbyDistrict_lot);
		// The SEARCH FORM
		// Open Search Form
 		search_lot_button = dojo.byId('search_lot');
		dojo.connect(search_lot_button, "onclick", open_searchLotForm);
		function open_searchLotForm() { 
			dijit.byId('searchFormLot').show();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();

			dojo.byId('select_service_lot').value = "Select service...";
			dojo.byId('select_state_lot').value = "Select state...";
			dojo.byId('select_district_lot').value = "Select district...";
			dojo.byId('select_mukim_lot').value = "Select sub district...";
			dojo.byId('seksyen_name').value = "";
			dojo.byId('lot_number').value = "";

			var selectedService_lot = dojo.byId('select_service_lot').value;

			if (selectedService_lot == 'FT') {
			 	queryTask_Lot =  search_ft_layer;
				// alert('FT');
			}else{
				if (selectedService_lot == 'QT') {
					queryTask_Lot =  search_qt_layer;
					// alert('QT');
				}
				else{
					queryTask_Lot =  search_rezab_layer;
					// alert('Rezab');
				}
			} 		
		}  
		
		var selectedService_lot = dojo.byId('select_service_lot').value;
		// D. Initialize Query Task
		// var variable = 'before';

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_lot() { 
			var selectedService_lot = dojo.byId('select_service_lot').value;
			// D. Initialize Query Task
			var state_list_lot = new Query();
			state_list_lot.where = "NEGERI <> ''";
			state_list_lot.returnGeometry = false;
			state_list_lot.outFields = ["NEGERI"];
			console.log("sini ok");
			if (selectedService_lot == 'FT') {
				var state_ddown_lot = search_ft_layer;
				queryTask_Lot =  search_ft_layer;
				// variable = 'after FT';
				// alert('FT');
			}else{
				if (selectedService_lot == 'QT') {
					var state_ddown_lot = search_qt_layer;
					queryTask_Lot =  search_qt_layer;
					// variable = 'after QT';
					// alert('QT');
				}
				else{
					var state_ddown_lot =  search_rezab_layer;
					queryTask_Lot =  search_rezab_layer;
					// variable = 'after Rezab';
					// alert('Rezab');
				}
			} 
			//var state_ddown_lot = state_layer1;
			state_ddown_lot.execute(state_list_lot, populateList_state_lot);
		}
		// alert(variable);
		// B. Initial DISTRICT Dropdown query 			
		/* var district_list = new Query();
		district_list.where = "DISTRICT >''";
		district_list.returnGeometry = false;
		district_list.outFields = ["DISTRICT"];
		
		district_ddown = mukim_layer;
		district_ddown.execute(district_list, populateList_district); */

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_lot() { 
			var selectedState_lot = dojo.byId('select_state_lot').value;
			var selectedService_lot = dojo.byId('select_service_lot').value;
			// D. Initialize Query Task
			if (selectedService_lot == 'FT') {
				var district_ddown_lot = search_ft_layer;
			}else{
				if (selectedService_lot == 'QT') {
					var district_ddown_lot = search_qt_layer;
				}
				else{
					var district_ddown_lot =  search_rezab_layer;
				}
			}
			
			var district_list_lot = new Query();
			district_list_lot.where = "NEGERI  = '" + selectedState_lot + "'";
			//district_list_lot.where = "Negeri  = 'Kedah'";
			//district_list_lot.returnGeometry = false
			district_list_lot.outFields = ["NEGERI", "DAERAH"];
			
			//var district_ddown_lot = state_layer1;
			district_ddown_lot.execute(district_list_lot, populateList_district_byState_lot);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_lot() { 
			var selectedState_lot = dojo.byId('select_state_lot').value;
			var selectedDistrict_lot = dojo.byId('select_district_lot').value;
			var selectedService_lot = dojo.byId('select_service_lot').value;
			// D. Initialize Query Task
			if (selectedService_lot == 'FT') {
				var mukim_ddown_lot = search_ft_layer;
			}else{
				if (selectedService_lot == 'QT') {
					var mukim_ddown_lot = search_qt_layer;
				}
				else{
					var mukim_ddown_lot =  search_rezab_layer;
				}
			}
			
			var mukim_list_lot = new Query();
			mukim_list_lot.where = "NEGERI = '" + selectedState_lot + "' AND DAERAH = '" + selectedDistrict_lot + "'";
			mukim_list_lot.returnGeometry = false;
			mukim_list_lot.outFields = ["NEGERI", "DAERAH", "MUKIM"];
			console.log(mukim_list_lot.where);
			
			//var mukim_ddown_lot = state_layer1;
			mukim_ddown_lot.execute(mukim_list_lot, populateList_mukim_byStateDistrict_lot);
		}
		
		//var selectedService_lot = dojo.byId('select_service_lot').value;
		// D. Initialize Query Task
		// if (selectedService_lot == 'FT') {
		// 	queryTask_Lot =  search_ft_layer;
		// 	alert('FT');
		// }else{
		// 	if (selectedService_lot == 'QT') {
		// 		queryTask_Lot =  search_qt_layer;
		// 		alert('QT');
		// 	}
		// 	else{
		// 		queryTask_Lot =  search_rezab_layer;
		// 		alert('Rezab');
		// 	}
		// }
		// alert(queryTask_Lot);
		//queryTask_Lot =  search_lot_layer;     		
		query_Lot = new Query();
		query_Lot.returnGeometry = true;
		query_Lot.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Lot = new EnhancedGrid({
					structure: gridLot_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridLot');	
			
		gridResult_Lot.set("store",clearStore); 
		gridResult_Lot.startup();  

		

		// ---------------------- 2. SEARCH: Project -------------------------
		dijit.byId("select_service_project").on("change",  populateList_State_setbyService_project);
		dijit.byId("select_state_project").on("change",  populateList_District_setbyState_project);
		dijit.byId("select_district_project").on("change",  populateList_Mukim_setbyDistrict_project);
		// The SEARCH FORM
		// Open Search Form
 		search_project_button = dojo.byId('search_project');
		dojo.connect(search_project_button, "onclick", open_searchProjectForm);
		function open_searchProjectForm() { 
			dijit.byId('searchFormProject').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();

			dojo.byId('select_service_project').value = "Select service...";
			dojo.byId('select_state_project').value = "Select state...";
			dojo.byId('select_district_project').value = "Select district...";
			dojo.byId('select_mukim_project').value = "Select sub district...";
			dojo.byId('tanah').value = "";

			var selectedService_project = dojo.byId('select_service_project').value;

			if (selectedService_project == 'Pemohonan') {
				queryTask_Project =  search_pemohonan_layer;
				// alert('Pemohonan');
			}else{
				if (selectedService_project == 'FT') {
				 	queryTask_Project =  search_ft_layer;
					// alert('FT');
				}else{
					if (selectedService_project == 'QT') {
						queryTask_Project =  search_qt_layer;
						// alert('QT');
					}
					else{
						selectedService_project =  search_rezab_layer;
						// alert('Rezab');
					}
				} 	
			}
					
		}  
		var selectedService_project = dojo.byId('select_service_project').value;

		

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_project() { 
			var selectedService_project = dojo.byId('select_service_project').value;

			var state_list_project = new Query();
			state_list_project.where = "NEGERI <> ''";
			state_list_project.returnGeometry = false;
			state_list_project.outFields = ["NEGERI"];
			console.log("sini ok");
			if (selectedService_project == 'Pemohonan') {
				var state_ddown_project = search_pemohonan_layer;
				queryTask_Project =  search_pemohonan_layer;

			}else{
				if (selectedService_project == 'FT') {
					var state_ddown_project = search_ft_layer;
					queryTask_Project =  search_ft_layer;
				}else{
					if (selectedService_project == 'QT') {
						var state_ddown_project = search_qt_layer;
						queryTask_Project =  search_qt_layer;
					}
					else{
						var state_ddown_project =  search_rezab_layer;
						queryTask_Project =  search_rezab_layer;
					}
				}
			}
			//var state_ddown_project = state_layer1;
			state_ddown_project.execute(state_list_project, populateList_state_project);

			// B. Initial DISTRICT Dropdown query 			
			/* var district_list = new Query();
			district_list.where = "DISTRICT >''";
			district_list.returnGeometry = false;
			district_list.outFields = ["DISTRICT"];
			
			district_ddown = mukim_layer;
			district_ddown.execute(district_list, populateList_district); */
		}

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_project() { 
			var selectedState_project = dojo.byId('select_state_project').value;
			var selectedService_project = dojo.byId('select_service_project').value;

			if (selectedService_project == 'Pemohonan') {
				var district_ddown_project = search_pemohonan_layer;
			}else{
				if (selectedService_project == 'FT') {
					var district_ddown_project = search_ft_layer;
				}else{
					if (selectedService_project == 'QT') {
						var district_ddown_project = search_qt_layer;
					}
					else{
						var district_ddown_project =  search_rezab_layer;
					}
				}
			}
			

			var district_list_project = new Query();
			district_list_project.where = "NEGERI  = '" + selectedState_project + "'";
			//district_list_project.where = "Negeri  = 'Kedah'";
			//district_list_project.returnGeometry = false
			district_list_project.outFields = ["NEGERI", "DAERAH"];
			
			//var district_ddown_project = state_layer1;
			district_ddown_project.execute(district_list_project, populateList_district_byState_project);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_project() { 
			var selectedState_project = dojo.byId('select_state_project').value;
			var selectedDistrict_project = dojo.byId('select_district_project').value;
			var selectedService_project = dojo.byId('select_service_project').value;
			
			if (selectedService_project == 'Pemohonan') {
				var mukim_ddown_project = search_pemohonan_layer;
			}else{
				if (selectedService_project == 'FT') {
					var mukim_ddown_project = search_ft_layer;
				}else{
					if (selectedService_project == 'QT') {
						var mukim_ddown_project = search_qt_layer;
					}
					else{
						var mukim_ddown_project =  search_rezab_layer;
					}
				}
			}
				
			
			var mukim_list_project = new Query();
			mukim_list_project.where = "NEGERI = '" + selectedState_project + "' AND DAERAH = '" + selectedDistrict_project + "'";
			mukim_list_project.returnGeometry = false;
			mukim_list_project.outFields = ["NEGERI", "DAERAH", "MUKIM"];
			console.log(mukim_list_project.where);
			
			//var mukim_ddown_project = state_layer1;
			mukim_ddown_project.execute(mukim_list_project, populateList_mukim_byStateDistrict_project);
		}
		
		// if (selectedService_project == 'Pemohonan') {
		// 	queryTask_Project =  search_pemohonan_layer;
		// }else{
		// 	if (selectedService_project == 'FT') {
		// 		queryTask_Project =  search_ft_layer;
		// 	}else{
		// 		if (selectedService_project == 'QT') {
		// 			queryTask_Project =  search_qt_layer;
		// 		}
		// 		else{
		// 			queryTask_Project =  search_rezab_layer;
		// 		}
		// 	}
		// }
		
		// D. Initialize Query Task
		//queryTask_Project =  search_project_layer;     		
		query_Project = new Query();
		query_Project.returnGeometry = true;
		query_Project.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Project = new EnhancedGrid({
					structure: gridProject_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridProject');	
			
		gridResult_Project.set("store",clearStore); 
		gridResult_Project.startup();  

		// ---------------------- 3. SEARCH: Hak Milik Sementara -------------------------
		dijit.byId("select_state_sementara").on("change",  populateList_District_setbyState_sementara);
		dijit.byId("select_district_sementara").on("change",  populateList_Mukim_setbyDistrict_sementara);
		// The SEARCH FORM
		// Open Search Form
 		search_sementara_button = dojo.byId('search_sementara');
		dojo.connect(search_sementara_button, "onclick", open_searchSementaraForm);
		function open_searchSementaraForm() { 
			dijit.byId('searchFormSementara').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormMilik').hide();

			dojo.byId('select_state_sementara').value = "Select state...";
			dojo.byId('select_district_sementara').value = "Select district...";
			dojo.byId('select_mukim_sementara').value = "Select sub district...";
			dojo.byId('sementara').value = "";
					
		}  

		// A. Initial STATE Dropdown query	

		var state_list_sementara = new Query();
		state_list_sementara.where = "NEGERI <> ''";
		state_list_sementara.returnGeometry = false;
		state_list_sementara.outFields = ["NEGERI"];
		console.log("sini ok");
		var state_ddown_sementara = search_qt_layer;
		state_ddown_sementara.execute(state_list_sementara, populateList_state_sementara);

		// B. Initial DISTRICT Dropdown query 			
		/* var district_list = new Query();
		district_list.where = "DISTRICT >''";
		district_list.returnGeometry = false;
		district_list.outFields = ["DISTRICT"];
			
		district_ddown = mukim_layer;
		district_ddown.execute(district_list, populateList_district); */

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_sementara() { 
			var selectedState_sementara = dojo.byId('select_state_sementara').value;

			var district_list_sementara = new Query();
			district_list_sementara.where = "NEGERI  = '" + selectedState_sementara + "'";
			//district_list_sementara.where = "Negeri  = 'Kedah'";
			//district_list_project.returnGeometry = false
			district_list_sementara.outFields = ["NEGERI", "DAERAH"];
			
			var district_ddown_sementara = search_qt_layer;
			district_ddown_sementara.execute(district_list_sementara, populateList_district_byState_sementara);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_sementara() { 
			var selectedState_sementara = dojo.byId('select_state_sementara').value;
			var selectedDistrict_sementara = dojo.byId('select_district_sementara').value;
			
			var mukim_list_sementara = new Query();
			mukim_list_sementara.where = "NEGERI = '" + selectedState_sementara + "' AND DAERAH = '" + selectedDistrict_sementara + "'";
			mukim_list_sementara.returnGeometry = false;
			mukim_list_sementara.outFields = ["NEGERI", "DAERAH", "MUKIM"];
			console.log(mukim_list_sementara.where);
			
			var mukim_ddown_sementara = search_qt_layer;
			mukim_ddown_sementara.execute(mukim_list_sementara, populateList_mukim_byStateDistrict_sementara);
		}
		
		// D. Initialize Query Task
		queryTask_Sementara =  search_qt_layer;     		
		query_Sementara = new Query();
		query_Sementara.returnGeometry = true;
		query_Sementara.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Sementara = new EnhancedGrid({
					structure: gridSementara_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridSementara');	
			
		gridResult_Sementara.set("store",clearStore); 
		gridResult_Sementara.startup(); 

// ---------------------- 4. SEARCH: Hak Milik -------------------------
		dijit.byId("select_state_milik").on("change",  populateList_District_setbyState_milik);
		dijit.byId("select_district_milik").on("change",  populateList_Mukim_setbyDistrict_milik);
		// The SEARCH FORM
		// Open Search Form
 		search_milik_button = dojo.byId('search_milik');
		dojo.connect(search_milik_button, "onclick", open_searchMilikForm);
		function open_searchMilikForm() { 
			dijit.byId('searchFormMilik').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();

			dojo.byId('select_state_milik').value = "Select state...";
			dojo.byId('select_district_milik').value = "Select district...";
			dojo.byId('select_mukim_milik').value = "Select sub district...";
			dojo.byId('milik').value = "";
					
		}  

		// A. Initial STATE Dropdown query	

		var state_list_milik = new Query();
		state_list_milik.where = "NEGERI <> ''";
		state_list_milik.returnGeometry = false;
		state_list_milik.outFields = ["NEGERI"];
		console.log("sini ok");
		var state_ddown_milik = search_ft_layer;
		state_ddown_milik.execute(state_list_milik, populateList_state_milik);

		// B. Initial DISTRICT Dropdown query 			
		/* var district_list = new Query();
		district_list.where = "DISTRICT >''";
		district_list.returnGeometry = false;
		district_list.outFields = ["DISTRICT"];
			
		district_ddown = mukim_layer;
		district_ddown.execute(district_list, populateList_district); */

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_milik() { 
			var selectedState_milik = dojo.byId('select_state_milik').value;

			var district_list_milik = new Query();
			district_list_milik.where = "NEGERI  = '" + selectedState_milik + "'";
			//district_list_milik.where = "Negeri  = 'Kedah'";
			//district_list_milik.returnGeometry = false
			district_list_milik.outFields = ["NEGERI", "DAERAH"];
			
			var district_ddown_milik = search_ft_layer;
			district_ddown_milik.execute(district_list_milik, populateList_district_byState_milik);
		}

		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_milik() { 
			var selectedState_milik = dojo.byId('select_state_milik').value;
			var selectedDistrict_milik = dojo.byId('select_district_milik').value;
			
			var mukim_list_milik = new Query();
			mukim_list_milik.where = "NEGERI = '" + selectedState_milik + "' AND DAERAH = '" + selectedDistrict_milik + "'";
			mukim_list_milik.returnGeometry = false;
			mukim_list_milik.outFields = ["NEGERI", "DAERAH", "MUKIM"];
			console.log(mukim_list_milik.where);
			
			var mukim_ddown_milik = search_ft_layer;
			mukim_ddown_milik.execute(mukim_list_milik, populateList_mukim_byStateDistrict_milik);
		}
		
		// D. Initialize Query Task
		queryTask_Milik =  search_ft_layer;     		
		query_Milik = new Query();
		query_Milik.returnGeometry = true;
		query_Milik.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_Milik = new EnhancedGrid({
					structure: gridMilik_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridMilik');	
			
		gridResult_Milik.set("store",clearStore); 
		gridResult_Milik.startup(); 


		// ---------------------- 5. SEARCH: File Name -------------------------
		dijit.byId("select_service_file").on("change",  populateList_State_setbyService_file);
		// The SEARCH FORM
		// Open Search Form
 		search_file_button = dojo.byId('search_file');
		dojo.connect(search_file_button, "onclick", open_searchFileForm);
		function open_searchFileForm() { 
			dijit.byId('searchFormFile').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();

			dojo.byId('select_service_file').value = "Select service...";
			dojo.byId('select_state_file').value = "Select state...";
			dojo.byId('ktpk').value = "";

			var selectedService_file = dojo.byId('select_service_file').value;

			if (selectedService_file == 'Pemohonan') {
				queryTask_file =  search_pemohonan_layer;
				// alert('Pemohonan');
			}else{
				if (selectedService_file == 'FT') {
				 	queryTask_file =  search_ft_layer;
					// alert('FT');
				}else{
					if (selectedService_file == 'QT') {
						queryTask_file =  search_qt_layer;
						// alert('QT');
					}
					else{
						selectedService_file =  search_rezab_layer;
						// alert('Rezab');
					}
				} 	
			}
					
		}  
		var selectedService_file = dojo.byId('select_service_file').value;

		

		// A. Initial STATE Dropdown query	
		function populateList_State_setbyService_file() { 
			var selectedService_file = dojo.byId('select_service_file').value;

			var state_list_file = new Query();
			state_list_file.where = "NEGERI <> ''";
			state_list_file.returnGeometry = false;
			state_list_file.outFields = ["NEGERI"];
			console.log("sini ok");
			if (selectedService_file == 'Pemohonan') {
				var state_ddown_file = search_pemohonan_layer;
				queryTask_File =  search_pemohonan_layer;

			}else{
				if (selectedService_file == 'FT') {
					var state_ddown_file = search_ft_layer;
					queryTask_File =  search_ft_layer;
				}else{
					if (selectedService_file == 'QT') {
						var state_ddown_file = search_qt_layer;
						queryTask_File =  search_qt_layer;
					}
					else{
						var state_ddown_file =  search_rezab_layer;
						queryTask_File =  search_rezab_layer;
					}
				}
			}
			//var state_ddown_file = state_layer1;
			state_ddown_file.execute(state_list_file, populateList_state_file);

			// B. Initial DISTRICT Dropdown query 			
			/* var district_list = new Query();
			district_list.where = "DISTRICT >''";
			district_list.returnGeometry = false;
			district_list.outFields = ["DISTRICT"];
			
			district_ddown = mukim_layer;
			district_ddown.execute(district_list, populateList_district); */
		}

		
		
		// D. Initialize Query Task
		//queryTask_File =  search_project_layer;     		
		query_File = new Query();
		query_File.returnGeometry = true;
		query_File.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// The SEARCH RESULT
		// Create KM Point empty grid
		  var gridResult_File = new EnhancedGrid({
					structure: gridFile_Structure, /* inside evacuationCentre.js */
					selectionMode: 'single',
					rowSelector: '20px',
					loadingMessage:'Loading results, please wait a moment.......',
					noDataMessage: "There are no items to display.",
					plugins: {
					  pagination: {
						  pageSizes: ["10", "25", "50", "All"],
						  description: true,
						  sizeSwitch: true,
						  pageStepper: true,
						  gotoButton: true,
						  /*page step to be displayed*/
						  maxPageStep: 4,
						  /*position of the pagination bar*/
						  position: "bottom"
					  }
					}
		}, 'gridFile');	
			
		gridResult_File.set("store",clearStore); 
		gridResult_File.startup();


		// ---------------------- STREET VIEW -------------------------
		
		// Open Search Form
 		street_view_button = dojo.byId('street_view');
		dojo.connect(street_view_button, "onclick", open_streetViewForm);
		function open_streetViewForm() {

			alert("mamammama");
			dijit.byId('streetFormView').show();
			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('measurePane').hide();
			map.graphics.clear(); 
			measurement.setTool("area",false); 
			measurement.setTool("distance",false);
			measurement.setTool("location",false);
			measurement.clearResult(); 
			connectInfo(); 
			// alert(map);
			// dojo.connect(map, "onClick", doStreetView);	
			clickmap = map.on("click", doStreetView);
	

			// map.on("Click", doStreetView);
		}
		
				
		dojo.connect(dojo.byId('streetFormView'), "hide", function(){
		
			dijit.byId('streetFormView').hide();
			
		});

		btnStreetViewClose = dojo.byId('btnStreetView_exit');
        dojo.connect(btnStreetViewClose, "onclick", function(evt){
        	// alert('closed');
            clickmap.remove();
            map.graphics.clear();
            connectInfo();
          //  click_infolot = map.on("click", infoconnect);
        });

		symbol = new PictureMarkerSymbol("assets/img/flying_man.png", 32, 34);
            //set up street view
            Glocator = new google.maps.Geocoder();
            gviewClient = new google.maps.DirectionsService();
			sv = new google.maps.StreetViewService();
            Pano = new google.maps.StreetViewPanorama(document.getElementById("streetview"));
            google.maps.event.addListener(Pano, "error", handleNoFlash);
			
			function resizeMap() {
            //resize the map when the browser resizes - view the 'Resizing and repositioning the map' section in  
            //the following help topic for more details http://help.esri.com/EN/webapi/javascript/arcgis/help/jshelp_start.htm#jshelp/inside_faq.htm 
            var resizeTimer;
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                map.resize();
                map.reposition();
            }, 500);
        }
        function doStreetView(evt) {
        	 alert('click');
            map.graphics.clear();
            
            viewPoint = esri.geometry.webMercatorToGeographic(evt.mapPoint);
            sv.getPanoramaByLocation(new google.maps.LatLng(viewPoint.y, viewPoint.x), 100, showStreetView);

        }
        function handleNoFlash(errorCode) {
            if (errorCode == FLASH_UNAVAILABLE) {
                alert("Error: Flash doesn't appear to be supported by your browser");
                return;
            }
        }
		
		function showStreetView(data, status) {
			if(status == google.maps.StreetViewStatus.OK) {   //street view is available 
				map.infoWindow.hide(); 
				// alert("clicked");
                viewStatus = true;
                viewAddress = data.location.description;
                google.maps.event.addListener(Pano, 'initialized', streetViewChanged); // add an event handling street view changes
				var markerPanoID = data.location.pano;
      			// Set the Pano to use the passed panoID
      			Pano.setPano(markerPanoID);
                Pano.setPov(data.location.latlng, { heading:34, pitch:10 }); //push the pano to a this location
				Pano.setVisible(true);
                //add graphic
                var graphic = new esri.Graphic(esri.geometry.geographicToWebMercator(viewPoint), symbol);
                map.graphics.add(graphic);
            }
            else{ //no street view for this location
				alert("No street view for this location");
                //if (Pano != null) Pano.hide(); //hide the street view
                viewStatus = false;
                viewAddress = "";
            }
            var locationpoint = new google.maps.LatLng(viewPoint.y, viewPoint.x);
            Glocator.geocode({'latLng': locationpoint}, function(results, status) {
			var addr_type = results[0].types[0];	// type of address inputted that was geocoded
			if ( status == google.maps.GeocoderStatus.OK ) 
				showStreetAddress(locationpoint, results[0].formatted_address, addr_type );
			else     
				alert("Geocode was not successful for the following reason: " + status);        
				}); //show street address info
        }
		
        function showStreetAddress(latlng, address, addr_type) {
			// alert("masuk doh");
            var descContent = "";
            descContent = "<table width='95%'><tr><td><b>View Point Address:</b></td><td>" + address + "</td></tr><tr><td><b>View Point Lat/Lon:</b></td><td>"
            descContent += latlng + "</td></tr><tr><td>";
            descContent += "<b>Address type:</b></td><td>" + addr_type + "<td><tr></table>";
            document.getElementById("Desc").innerHTML = descContent;
        }
        function getlocalAddress(Addr) {
            var index = Addr.indexOf(',');
            return Addr.substring(0, index);
        }
        function streetViewChanged(streetLocation) {
            viewStatus = true;
            viewAddress = streetLocation.description;
            var point = new esri.geometry.Point({"x": streetLocation.latlng.lng(), "y": streetLocation.latlng.lat(), " spatialReference": { " wkid": 4326} });
            //add to the map
            map.graphics.clear();
            var symbolPoint = esri.geometry.geographicToWebMercator(point);
            map.graphics.add(new esri.Graphic(symbolPoint, symbol));
            map.centerAt(symbolPoint);
            Glocator.getLocations(streetLocation.latlng, showStreetAddress);
        }
		

        // ---------------------- Print map -------------------------
		       
            console.log("open bookmark");
			app.printUrl = "http://"+ipaddress+"/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";
			esriConfig.defaults.io.proxyUrl = "/proxy/";

	          console.log("open bookmark1");
	          var mapOnlyIndex, templates;

	          var templateNames = ["A3 Landscape", "A3 Portrait", "A4 Landscape", "A4 Portrait", "Letter ANSI A Landscape", "Letter ANSI A Portrait", "Tabloid ANSI B Landscape", "Tabloid ANSI B Portrait", "MAP_ONLY"];
	          // remove the MAP_ONLY template then add it to the end of the list of templates 
	          mapOnlyIndex = arrayUtils.indexOf(templateNames, "MAP_ONLY");
	          if ( mapOnlyIndex > -1 ) {
	            var mapOnly = templateNames.splice(mapOnlyIndex, mapOnlyIndex + 1)[0];
	            templateNames.push(mapOnly);
	          }
	          // create a print template for each choice
	          templates = arrayUtils.map(templateNames, function(ch) {
	            var plate = new PrintTemplate();
	            plate.layout = plate.label = ch;
	            plate.format = "PDF";
	            plate.layoutOptions = { 
	              "authorText": "Made by:  Esri's JS API Team",
	              "copyrightText": "<copyright info here>",
	              "legendLayers": [], 
	              "titleText": "Pool Permits", 
	              "scalebarUnit": "Miles" 
	            };
	            return plate;
	          });

	          // create the print dijit
	          app.printer = new Print({
	            "map": map,
	            "templates": templates,
	            url: app.printUrl
	          }, dom.byId("printButton"));
	          app.printer.startup();
	        

	  

		// // ---------------------- 2. SEARCH: EVACUATION CENTRE -------------------------
		// dijit.byId("select_state_evacuation").on("change",  populateList_District_setbyState_evacuation);
		
		// // The SEARCH FORM
		// // Open Search Form
 	// 	search_evacuation_button = dojo.byId('search_evacuationCentre');
		// dojo.connect(search_evacuation_button, "onclick", open_searchEvacutaionForm);
		// function open_searchEvacutaionForm() { 
		// 	dijit.byId('searchFormLot').hide();
		// 	dijit.byId('searchFormEvacuation').show(); 
		// 	dijit.byId('searchFormEmergency').hide();
		// 	dijit.byId('searchFormFacility').hide();
		// 	dijit.byId('searchFormVictim').hide();		
		// }  
		
		// // A. Initial STATE Dropdown query	
		// var state_list_evacuation = new Query();
		// state_list_evacuation.where = "STATE <> ''";
		// state_list_evacuation.returnGeometry = false;
		// state_list_evacuation.outFields = ["STATE"];
		// console.log("sini ok");
		// var state_ddown_evacuation = state_layer;
		// state_ddown_evacuation.execute(state_list_evacuation, populateList_state_evacuation);

		// // B. Initial DISTRICT Dropdown query 			
		// /* var district_list = new Query();
		// district_list.where = "DISTRICT >''";
		// district_list.returnGeometry = false;
		// district_list.outFields = ["DISTRICT"];
		
		// district_ddown = mukim_layer;
		// district_ddown.execute(district_list, populateList_district); */

		// // C. DISTRICT Dropdown by STATE dropdown
		// function populateList_District_setbyState_evacuation() { 
		// 	var selectedState_evacuation = dojo.byId('select_state_evacuation').value;
			
		// 	var district_list_evacuation = new Query();
		// 	district_list_evacuation.where = "STATE  = '" + selectedState_evacuation + "'";
		// 	//district_list_evacuation.where = "Negeri  = 'Kedah'";
		// 	//district_list_evacuation.returnGeometry = false
		// 	district_list_evacuation.outFields = ["STATE", "DISTRICT"];
			
		// 	var district_ddown_evacuation = mukim_layer;
		// 	district_ddown_evacuation.execute(district_list_evacuation, populateList_district_byState_evacuation);
		// }
		
		// // C. Initialize Query Task
		// queryTask_EvacuationCentre =  search_evacuation_layer;     		
		// query_EvacuationCentre = new Query();
		// query_EvacuationCentre.returnGeometry = true;
		// query_EvacuationCentre.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// // The SEARCH RESULT
		// // Create KM Point empty grid
		//   var gridResult_Evacuation = new EnhancedGrid({
		// 			structure: gridEvacuation_Structure, /* inside evacuationCentre.js */
		// 			selectionMode: 'single',
		// 			rowSelector: '20px',
		// 			loadingMessage:'Loading results, please wait a moment.......',
		// 			noDataMessage: "There are no items to display.",
		// 			plugins: {
		// 			  pagination: {
		// 				  pageSizes: ["10", "25", "50", "All"],
		// 				  description: true,
		// 				  sizeSwitch: true,
		// 				  pageStepper: true,
		// 				  gotoButton: true,
		// 				  /*page step to be displayed*/
		// 				  maxPageStep: 4,
		// 				  /*position of the pagination bar*/
		// 				  position: "bottom"
		// 			  }
		// 			}
		// }, 'gridEvacuation');	
			
		// gridResult_Evacuation.set("store",clearStore); 
		// gridResult_Evacuation.startup();  

		// // ---------------------- 3. SEARCH: EMERGENCY CENTRE -------------------------
		// dijit.byId("select_state_emergency").on("change",  populateList_District_setbyState_emergency);
		
		// // The SEARCH FORM
		// // Open Search Form
 	// 	search_emergency_button = dojo.byId('search_emergencyCentre');
		// dojo.connect(search_emergency_button, "onclick", open_searchEmergencyForm);
		// function open_searchEmergencyForm() { 
		// 	dijit.byId('searchFormLot').hide();
		// 	dijit.byId('searchFormEvacuation').hide(); 
		// 	dijit.byId('searchFormEmergency').show();
		// 	dijit.byId('searchFormFacility').hide();
		// 	dijit.byId('searchFormVictim').hide();
		// } 
		
		// // A. Initial STATE Dropdown query	
		// var state_list_emergency = new Query();
		// state_list_emergency.where = "state <> ''";
		// state_list_emergency.returnGeometry = false;
		// state_list_emergency.outFields = ["state"];
		
		// var state_ddown_emergency = state_layer;
		// state_ddown_emergency.execute(state_list_emergency, populateList_state_emergency);

		// // B. DISTRICT Dropdown by STATE dropdown
		// function populateList_District_setbyState_emergency() { 
		// 	var selectedState_emergency = dojo.byId('select_state_emergency').value;
		// 	console.log("Emergency State = " + selectedState_emergency);;
			
		// 	var district_list_emergency = new Query();
		// 	district_list_emergency.where = "STATE = '" + selectedState_emergency + "'";
		// 	district_list_emergency.returnGeometry = false;
		// 	district_list_emergency.outFields = ["STATE", "DISTRICT"];
			
		// 	var district_ddown_emergency = mukim_layer;
		// 	district_ddown_emergency.execute(district_list_emergency, populateList_district_byState_emergency);
		// }
		
		// // C. Initialize Query Task
		// queryTask_EmergencyCentre =  search_emergency_layer;     		
		// query_EmergencyCentre = new Query();
		// query_EmergencyCentre.returnGeometry = true;
		// query_EmergencyCentre.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// // The SEARCH RESULT
		// // Create KM Point empty grid
		//   var gridResult_Emergency = new EnhancedGrid({
		// 			structure: gridEmergency_Structure, /* inside emergencyCentre.js */
		// 			selectionMode: 'single',
		// 			rowSelector: '20px',
		// 			loadingMessage:'Loading results, please wait a moment.......',
		// 			noDataMessage: "There are no items to display.",
		// 			plugins: {
		// 			  pagination: {
		// 				  pageSizes: ["10", "25", "50", "All"],
		// 				  description: true,
		// 				  sizeSwitch: true,
		// 				  pageStepper: true,
		// 				  gotoButton: true,
		// 				  /*page step to be displayed*/
		// 				  maxPageStep: 4,
		// 				  /*position of the pagination bar*/
		// 				  position: "bottom"
		// 			  }
		// 			}
		// }, 'gridEmergency');	
			
		// gridResult_Emergency.set("store",clearStore); 
		// gridResult_Emergency.startup();    

		// // ---------------------- 4. SEARCH: Affected Facility -------------------------
		// dijit.byId("select_state_facility").on("change",  populateList_District_setbyState_facility);
		
		// // The SEARCH FORM
		// // Open Search Form
 	// 	search_facility_button = dojo.byId('search_affectedFacility');
		// dojo.connect(search_facility_button, "onclick", open_searchFacilityForm);
		// function open_searchFacilityForm() { 
		// 	dijit.byId('searchFormLot').hide();
		// 	dijit.byId('searchFormEvacuation').hide(); 
		// 	dijit.byId('searchFormEmergency').hide();
		// 	dijit.byId('searchFormFacility').show();
		// 	dijit.byId('searchFormVictim').hide();
		// } 
		
		// // A. Initial STATE Dropdown query	
		// var state_list_facility = new Query();
		// state_list_facility.where = "STATE <> ''";
		// state_list_facility.returnGeometry = false;
		// state_list_facility.outFields = ["STATE"];
		
		// var state_ddown_facility = state_layer;
		// state_ddown_facility.execute(state_list_facility, populateList_state_facility);

		// // B. DISTRICT Dropdown by STATE dropdown
		// function populateList_District_setbyState_facility() { 
		// 	var selectedState_facility = dojo.byId('select_state_facility').value;
		// 	console.log("Facility State = " + selectedState_facility);;
			
		// 	var district_list_facility = new Query();
		// 	district_list_facility.where = "STATE = '" + selectedState_facility + "'";
		// 	district_list_facility.returnGeometry = false;
		// 	district_list_facility.outFields = ["STATE", "DISTRICT"];
			
		// 	var district_ddown_facility = mukim_layer;
		// 	district_ddown_facility.execute(district_list_facility, populateList_district_byState_facility);
		// }
		
		// // C. Initialize Query Task
		// queryTask_AffectedFacility =  search_facility_layer;     		
		// query_AffectedFacility = new Query();
		// query_AffectedFacility.returnGeometry = true;
		// query_AffectedFacility.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
		
		// // The SEARCH RESULT
		// // Create KM Point empty grid
		//   var gridResult_Facility = new EnhancedGrid({
		// 			structure: gridFacility_Structure, /* inside affectedFacility.js */
		// 			selectionMode: 'single',
		// 			rowSelector: '20px',
		// 			loadingMessage:'Loading results, please wait a moment.......',
		// 			noDataMessage: "There are no items to display.",
		// 			plugins: {
		// 			  pagination: {
		// 				  pageSizes: ["10", "25", "50", "All"],
		// 				  description: true,
		// 				  sizeSwitch: true,
		// 				  pageStepper: true,
		// 				  gotoButton: true,
		// 				  /*page step to be displayed */
		// 				  maxPageStep: 4,
		// 				  /*position of the pagination bar */
		// 				  position: "bottom"
		// 			  }
		// 			}
		// }, 'gridFacility');	
			
		// gridResult_Facility.set("store",clearStore); 
		// gridResult_Facility.startup();  
		
		// // ---------------------- 5. SEARCH: Victim -------------------------	
		// dijit.byId("select_state_victim").on("change",  populateList_District_setbyState_victim);
		// dijit.byId("select_district_victim").on("change",  populateList_Mukim_setbyDistrict_victim);
		// var grid_FloodVictims = dijit.byId("gridFloodVictims");	
		
		// // The SEARCH FORM
		// // Open Search Form
 	// 	search_victim_button = dojo.byId('search_floodVictim');
		// dojo.connect(search_victim_button, "onclick", open_searchVictimForm);
		// function open_searchVictimForm() { 
		// 	dijit.byId('searchFormLot').hide();
		// 	dijit.byId('searchFormEvacuation').hide(); 
		// 	dijit.byId('searchFormEmergency').hide();
		// 	dijit.byId('searchFormFacility').hide();
		// 	dijit.byId('searchFormVictim').show();
			
			
		// }

		// // When submit Search button
		// dijit.byId("search_victim").on("click", searchDd);
		
		// var stateStore = new dojo.data.ItemFileReadStore({ url: "../Project/victim.php" });
		// var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
		
		// function clearOldList(size, request){ //alert("masuk sini");
  //          var list = dojo.byId("list3");
  //          if(list){
  //            while(list.firstChild){
  //              list.removeChild(list.firstChild);
  //            }
  //          }
		// }
		   
		// function searchDd(){
		// 	//alert("yeay masuk");
		// 	//grid_FloodVictims.setStore(clearStore); 
		// 	var grid_FloodVictims = dijit.byId("gridFloodVictims");	
			
		// 	//Create data store and bind to grid.
		// 	var store = new dojo.data.ItemFileWriteStore({
		// 			//data: data_FloodVictims,
		// 			//clearOnClose: true
		// 	});
					
		// 	grid_FloodVictims.set("clearStore", store);
			
		// 	var queryObj = {};

		// 	 // Build up the query from the input boxes.
		// 	 /*var name = nameBox.getValue();
		// 	 if( name && dojo.trim(name) !== "" ){
		// 	   queryObj["name"] = name;
		// 	 }
		// 	 var aisle = aisleBox.getValue();
		// 	 if( aisle && dojo.trim(aisle) !== "" ){
		// 	   queryObj["aisle"] = aisle;
		// 	 }*/

		// 	var state = dojo.byId('select_state_victim').value;
		// 	if( state && dojo.trim(state) !== "Select State..." ){
		// 	   queryObj["negeri"] = state;
		// 	 }
			 
		// 	var district = dojo.byId('select_district_victim').value;
		// 	if( district && dojo.trim(district) !== "Select City..." ){
		// 	   queryObj["daerah"] = district;
		// 	 }
			 
		// 	var mukim = dojo.byId('select_mukim_victim').value;
		// 	if( mukim && dojo.trim(mukim) !== "Select sub district..." ){
		// 	   queryObj["mukim"] = mukim;
		// 	 }
			
		// 	//var victim_name = dojo.byId('victim_name').value;
	
		// 	 var qNode = dojo.byId("query");
		// 	 if(qNode){
		// 	   qNode.innerHTML = dojo.toJson(queryObj);
		// 	 }
			 
		// 	 // Build up query options, if any.
		// 	 var queryOptionsObj = {};
		// 	 queryOptionsObj["ignoreCase"] = true;
		 
			
		// 	//alert("sini ok");
			
		// 	// Callback for processing a returned list of items.
		// 	 function gotItems(data_items, request){
		// 	   var list = dojo.byId("list3");
		// 	   if(list){
		// 		 var i;
		// 		 for(i = 0; i < data_items.length; i++){
		// 		   var item = data_items[i];
		// 		   list.appendChild(document.createTextNode(stateStore.getValue(item, "mangsaNama")));
		// 		   list.appendChild(document.createElement("br"));
		// 		 }
		// 	   }
			   
		// 		dijit.byId('searchResultFloodVictims').show();
		// 		dijit.byId('searchFormLot').hide();
		// 		dijit.byId('searchFormEvacuation').hide(); 
		// 		dijit.byId('searchFormEmergency').hide();
		// 		dijit.byId('searchFormFacility').hide();
		// 		dijit.byId('searchFormVictim').hide();
				
				
				
			   
				
					
		// 		// Remove all graphics on the maps graphics layer
		// 		//map.graphics.clear();
		// 		//console.log("result length = " + featureSet.features.length);
		// 		// Remove previous search results
		// 		if (data_items.length == 0) {
		// 			grid_FloodVictims.set("noDataMessage", 'Sorry, no results.');
		// 			grid_FloodVictims.setStore(clearStore); 					
		// 		}
				
		// 		else {								
		// 			//alert("masuk grid");
		// 			// Create items array to be added to store's data
					
		// 			//var items_Victim = []; // all items to be stored in data store
		// 			var items_Victim = []; // all items to be stored in data store
		// 			items_Victim = data_items;
				
			   
		// 			//Create data object to be used in store
		// 			var data_FloodVictims = {
		// 				identifier : "mangsaID", //This field needs to have unique values
		// 				label : "mangsaID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
		// 				items : items_Victim
		// 			}
					 
		// 			//Create data store and bind to grid.
		// 			var store = new dojo.data.ItemFileWriteStore({
		// 					data: data_FloodVictims,
		// 					clearOnClose: true
		// 			});
															
		// 			grid_FloodVictims.set("store", store);
					
		// 			// Clear previous search results
		// 			if(grid_FloodVictims.store.save) { grid_FloodVictims.store.save();}
		// 			grid_FloodVictims.store.close();				
		// 			grid_FloodVictims._refresh(); // or grid.store.fetch();								
		// 			grid_FloodVictims.filter();
		// 		}
	   
		// 	 }
			 
		// 	 // Callback for if the lookup fails.
		// 	 function fetchFailed(error, request){
		// 		//alert("lookup failed.");
		// 		//alert(error);
		// 	 }

		// 	 // Fetch the data.
		// 	 //victimStore.fetch({query: queryObj, queryOptions: queryOptionsObj, onComplete: gotItems, onError: fetchFailed});
			 
		// 	 stateStore.fetch({query: queryObj, queryOptions: queryOptionsObj, onBegin: clearOldList, onComplete: gotItems, onError: fetchFailed});
		
		// }	 

		// // A. Initial STATE Dropdown query	
		// var state_list_victim = new Query();
		// state_list_victim.where = "state <> ''";
		// state_list_victim.returnGeometry = false;
		// state_list_victim.outFields = ["state"];
		
		// var state_ddown_victim = state_layer;
		// state_ddown_victim.execute(state_list_victim, populateList_state_victim);

		// // B. DISTRICT Dropdown by STATE dropdown
		// function populateList_District_setbyState_victim() { 
		// 	var selectedState_victim = dojo.byId('select_state_victim').value;
		// 	console.log("Victim State = " + selectedState_victim);
			
		// 	var district_list_victim = new Query();
		// 	district_list_victim.where = "STATE = '" + selectedState_victim + "'";
		// 	district_list_victim.returnGeometry = false;
		// 	district_list_victim.outFields = ["STATE", "DISTRICT"];
			
		// 	var district_ddown_victim = mukim_layer;
		// 	district_ddown_victim.execute(district_list_victim, populateList_district_byState_victim);
		// }
		
		// // C. MUKIM Dropdown by DISTRICT dropdown
		// function populateList_Mukim_setbyDistrict_victim() { 
		// 	var selectedState_victim = dojo.byId('select_state_victim').value;
		// 	var selectedDistrict_victim = dojo.byId('select_district_victim').value;
			
		// 	var mukim_list_victim = new Query();
		// 	mukim_list_victim.where = "STATE = '" + selectedState_victim + "' AND DISTRICT = '" + selectedDistrict_victim + "'";
		// 	mukim_list_victim.returnGeometry = false;
		// 	mukim_list_victim.outFields = ["STATE", "DISTRICT", "NAME"];
		// 	console.log(mukim_list_victim.where);
			
		// 	var mukim_ddown_victim = mukim_layer;
		// 	mukim_ddown_victim.execute(mukim_list_victim, populateList_mukim_byStateDistrict_victim);
		// }
		
		// // D. Initialize Query Task
		// queryTask_FloodVictims =  search_victim_layer;     		
		// query_FloodVictims = new Query();
		// query_FloodVictims.returnGeometry = true;
		// query_FloodVictims.outFields = ["*"]; // kena include identifier (OBJECTID) dlm list ni	 
	
		// // The SEARCH RESULT
		// // Create Victim empty grid
		// var gridResult_FloodVictims = new EnhancedGrid({
		// 			structure: gridFloodVictims_Structure, /* inside victim.js */
		// 			selectionMode: 'single',
		// 			rowSelector: '20px',
		// 			loadingMessage:'Loading results, please wait a moment.......',
		// 			noDataMessage: "There are no items to display.",
		// 			plugins: {
		// 			  pagination: {
		// 				  pageSizes: ["10", "25", "50", "All"],
		// 				  description: true,
		// 				  sizeSwitch: true,
		// 				  pageStepper: true,
		// 				  gotoButton: true,
		// 				  /*page step to be displayed */
		// 				  maxPageStep: 4,
		// 				  /*position of the pagination bar */
		// 				  position: "bottom"
		// 			  }
		// 			}
		// }, 'gridFloodVictims');	
			
		// gridResult_FloodVictims.set("store",clearStore); 
		// gridResult_FloodVictims.startup(); 	
		
			
		// ---------------------- REGISTER VICTIM -------------------------
		/*dijit.byId("select_state_victim_reg").on("change",  populateList_District_setbyState_victim_reg);
		dijit.byId("select_district_victim_reg").on("change",  populateList_Mukim_setbyDistrict_victim_reg);
		dijit.byId("select_mukim_victim_reg").on("change",  populateList_Evacuation_setbyMukim_victim_reg);
		
		register_victim_button = dojo.byId('reg_flood_victim');
		dojo.connect(register_victim_button, "onclick", open_registerVictimForm);
		function open_registerVictimForm() { dijit.byId('registerFormVictim').show(); } 
		
		// A. Initial STATE Dropdown query	
		var state_list_victim_reg = new Query();
		state_list_victim_reg.where = "state <> ''";
		state_list_victim_reg.returnGeometry = false;
		state_list_victim_reg.outFields = ["state"];
		
		var state_ddown_victim_reg = state_layer;
		state_ddown_victim_reg.execute(state_list_victim_reg, populateList_state_victim_reg);

		// B. DISTRICT Dropdown by STATE dropdown
		function populateList_District_setbyState_victim_reg() { 
			var selectedState_victim_reg = dojo.byId('select_state_victim_reg').value;
			console.log("Victim State = " + selectedState_victim_reg);
			
			var district_list_victim_reg = new Query();
			district_list_victim_reg.where = "Negeri = '" + selectedState_victim_reg + "'";
			district_list_victim_reg.returnGeometry = false;
			district_list_victim_reg.outFields = ["Negeri", "Daerah"];
			
			var district_ddown_victim_reg = mukim_layer;
			district_ddown_victim_reg.execute(district_list_victim_reg, populateList_district_byState_victim_reg);
		}
		
		// C. MUKIM Dropdown by DISTRICT dropdown
		function populateList_Mukim_setbyDistrict_victim_reg() { 
			var selectedState_victim_reg = dojo.byId('select_state_victim_reg').value;
			var selectedDistrict_victim_reg = dojo.byId('select_district_victim_reg').value;
			
			var mukim_list_victim_reg = new Query();
			mukim_list_victim_reg.where = "Negeri = '" + selectedState_victim_reg + "' AND Daerah = '" + selectedDistrict_victim_reg + "'";
			mukim_list_victim_reg.returnGeometry = false;
			mukim_list_victim_reg.outFields = ["Negeri", "Daerah", "Mukim"];
			console.log(mukim_list_victim_reg.where);
			
			var mukim_ddown_victim_reg = mukim_layer;
			mukim_ddown_victim_reg.execute(mukim_list_victim_reg, populateList_mukim_byStateDistrict_victim_reg);
		}
		
		// D. EVACUATION Dropdown by MUKIM dropdown
		function populateList_Evacuation_setbyMukim_victim_reg() { 
			var selectedState_victim_reg = dojo.byId('select_state_victim_reg').value;
			var selectedDistrict_victim_reg = dojo.byId('select_district_victim_reg').value;
			var selectedMukim_victim_reg = dojo.byId('select_mukim_victim_reg').value;
			
			var evacuation_list_victim_reg = new Query();
			evacuation_list_victim_reg.where = "negeri = '" + selectedState_victim_reg + "' AND daerah = '" + selectedDistrict_victim_reg + "' AND mukim = '" + selectedMukim_victim_reg + "'";
			evacuation_list_victim_reg.returnGeometry = false;
			evacuation_list_victim_reg.outFields = ["negeri", "daerah", "mukim", "nama"];
			console.log(evacuation_list_victim_reg.where);
			
			var evacuation_ddown_victim_reg = search_evacuation_layer;
			evacuation_ddown_victim_reg.execute(evacuation_list_victim_reg, populateList_evacuation_byStateDistrictMukim_victim_reg);
		} */
	
	
	
		// Open Upload File Pane
 		/*uploadReport_button = dojo.byId('report_upload');
		dojo.connect(uploadReport_button, "onclick", open_uploadReportPane);
		function open_uploadReportPane() { 
			dijit.byId('uploadReportPane').show();
			dojo.byId('report_name').value = "";
			dojo.byId('fileSelect').value = "";
		
		} */
}); // CLOSE FUNCTIONS

	/* function showFeature (feature) {
        map.graphics.clear();
        feature.setSymbol(symbol);
        map.graphics.add(feature);
    } */