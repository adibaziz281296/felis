<?php
//session_start();
$state = $_SESSION['state'];
// echo $state;
include('../sql_server_db.php');

$sql = "SELECT * FROM location WHERE state = '$state'";

$result = sqlsrv_query( $conn, $sql);
if( $result === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

while($row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )){	
	$latitude = $row['latitude'];
	$longitude = $row['longitude'];
	$zoom = $row['zoom'];
}
$phpToJsVars = array(
  'value1' => $latitude,
  'value2' => $longitude,
  'value3' => $zoom
   );
?>
<!DOCTYPE html>
<html>
  <head>
	<title>Integrated Flood Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	
	<!-- Bootstrap core CSS -->
	<link href="assets/css/gis/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Custom styles for this template -->
	<link href="assets/css/gis/simple-sidebar.css" rel="stylesheet">

	<!-- Reference the ArcGIS API for CSS -->
	<!--<link rel="stylesheet" href="http://js.arcgis.com/3.14/dijit/themes/claro/claro.css">-->
	<!-- <link rel="stylesheet" type="text/css" href="http://serverapi.arcgisonline.com/jsapi/arcgis/3.1/js/esri/dijit/css/Popup.css"> -->
    <link rel="stylesheet" href="http://js.arcgis.com/3.14/esri/css/esri.css">
	<link rel="stylesheet" type="text/css" href="//js.arcgis.com/3.7/js/dojo/dijit/themes/claro/claro.css">
	<link rel="stylesheet" type="text/css" href="toc/2.10/src/agsjs/css/agsjs.css" />
	
	<link rel="stylesheet" type="text/css" href="css/modal.css" />
	
	
	<style>
		 @import url("http://js.arcgis.com/3.10/js/dojo/dijit/themes/claro/claro.css");
		 @import url("http://archive.dojotoolkit.org/nightly/dojotoolkit/dojox/layout/resources/FloatingPane.css"); 
		 @import url("http://archive.dojotoolkit.org/nightly/dojotoolkit/dojox/layout/resources/ResizeHandle.css");	
		 @import url("http://ajax.googleapis.com/ajax/libs/dojo/1.6/dojox/grid/enhanced/resources/claro/EnhancedGrid.css");
      
    </style>

	
	<!-- Custom info popup style to overwrite the default info popup style -->
    <link rel="stylesheet" type="text/css" href="assets/css/gis/modernGrey.css">
	
	<!-- Custom widget style to overwrite the default widget style -->
	<link rel="stylesheet" type="text/css" href="assets/css/gis/widget.css">
	
	<!--for uploading flood report file -->
	<script language="javascript" src="report/funcs/val.js"></script>
	
 	<script>
		var dojoConfig = {
		isDebug: true,
		parseOnLoad: true,
		foo: "bar"
		};
	</script> 
	<!-- Load dojo and provide config via data attribute for TOC -->
	<script type="text/javascript">
		// Registers the correct location of the "demo" package so we can load Dojo
		var dojoConfig = { 
			paths: {
				//if you want to host on your own server, download and put in folders then use path like: 
				agsjs: location.pathname.replace(/\/[^/]+$/, '') + '/toc/2.10/src/agsjs' 
				
			}
		};      
	</script>

	<script type="text/javascript">
	var phpVars = { 
	<?php 
	  foreach ($phpToJsVars as $key => $value) {
	    echo '  ' . $key . ': ' . '"' . $value . '",' . "\n";  
	  }
	?>
	};
	</script>
	
	<!--script>
	
		// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
	
	
	</script-->
	
  <!-- Modal CSS & JS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	
	
	<!-- required: dojo.js -->
	<!--<script src="//ajax.googleapis.com/ajax/libs/dojo/1.6.2/dojo/dojo.js" data-dojo-config="parseOnLoad: true"></script> <!-- same as parser.parse(); -->
	
	<!-- for debugging: 
	<script type="text/javascript" src="//dojotoolkit.org/reference-guide/1.6/_static/js_bak/dojox/layout/FloatingPane.js"></script> -->

	<script src="//ajax.googleapis.com/ajax/libs/dojo/1.6.2/dojo/dojo.xd.js"></script>
	<!-- Reference for StreetView -->
	<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp" type="text/javascript"></script>  -->
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyDEfWbEK3OTIRtSJhxCW_AmerJQOu9asdw" type="text/javascript"></script>
	
 
	<!-- Reference the ArcGIS API for JavaScript -->
	<script src="http://js.arcgis.com/3.14/"></script>
  </head>
  <body>

  
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
		  <!-- <ul class="nav navbar-nav navbar-right">
            <li><a ref="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="login-btn"><i class="fa fa-user white"></i>&nbsp;&nbsp;Daftar Masuk</a></li>
          </ul> -->	  
          <ul class="nav navbar-nav">
		   <li class="dropdown">
				<a  href="#" role="button"  id="basic_legend" style="font-size: 13px;"><i class="fa fa-bars icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Legend Map</a>
			</li>
            <li class="dropdown">
				<a id="searchDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 13px;"><i class="fa fa-search icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Search <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_lot" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search Lot</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_project" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search Project</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_sementara" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search Hak Milik Sementara</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_milik" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search Hak Milik</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_file" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Search File Name</a></li>
						<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_evacuationCentre" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Evacuation Center</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_emergencyCentre" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Emergency Response Center</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_affectedFacility" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Facility</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="search_floodVictim" style="font-size: 12px;"><i class="fa fa-search" style="font-size: 18px;"></i>&nbsp;&nbsp;Flood Victims</a></li> -->
					</ul>
			</li>
            <!-- <li class="dropdown">
				<a id="linkDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-link icon-blue"></i>&nbsp;&nbsp;Pautan <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="http://infobanjir.water.gov.my/" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in" id="link_jps"><i class="fa fa-globe"></i>&nbsp;&nbsp;JPS</a></li>
						<li><a href="http://portalbencana.ndcc.gov.my/portal" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in" id="link_mkn"><i class="fa fa-globe"></i>&nbsp;&nbsp;MKN</a></li>
						<li><a href="http://bencanaalam.jkr.gov.my/v2/" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in" id="link_jkr"><i class="fa fa-globe"></i>&nbsp;&nbsp;JKR</a></li>
						<li><a href="http://publicinfobanjir.water.gov.my/" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in" id="link_info"><i class="fa fa-globe"></i>&nbsp;&nbsp;Public InfoBanjir</a></li>
						<li><a href="http://infobanjir.water.gov.my/real_time.cfm" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in" id="link_online"><i class="fa fa-globe"></i>&nbsp;&nbsp;Online Hydrological Data</a></li>
						<li><a href="http://ebanjir.kelantan.gov.my/" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in" id="link_kelantan"><i class="fa fa-globe"></i>&nbsp;&nbsp;eBanjir Kelantan</a></li>
					</ul>	
			</li> -->
            <li class="dropdown">
				<a id="basicDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"  style="font-size: 13px;"><i class="fa fa-cog icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Basic Functions <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="basic_basemap"><i class="fa fa-map-o"></i>&nbsp;&nbsp;Basemap</a></li> -->
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="basic_locator" style="font-size: 12px;"><i class="fa fa-map-marker" style="font-size: 18px;"></i>&nbsp;&nbsp;Locator Coordinates</a></li>
						<li>
							<a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="street_view" style="font-size: 12px;"><i class="fa fa-street-view" style="font-size: 18px;"></i>&nbsp;&nbsp;Street View</a>
						</li>						
						<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="analysis_buffer" style="font-size: 12px;"><i class="fa fa-dot-circle-o" style="font-size: 18px;"></i>&nbsp;&nbsp;Buffer Zone</a></li>	
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="basic_print"><i class="fa fa-print"></i>&nbsp;&nbsp;Cetak</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="analysis_direction" style="font-size: 12px;"><i class="fa fa-road" style="font-size: 18px;"></i>&nbsp;&nbsp;Navigation</a></li> -->
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="analysis_measure" style="font-size: 12px;"><i class="fa fa-object-group" style="font-size: 18px;"></i>&nbsp;&nbsp;Measurement</a></li>
						<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="analysis_route"><i class="fa fa-road"></i>&nbsp;&nbsp;Navigasi dan Halangan</a></li> -->
						<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="reg_flood_victim"><i class="fa fa-upload"></i>&nbsp;&nbsp;Kemasukan Data Mangsa Banjir</a></li> -->
				    </ul>			
			</li>
			<!-- <li class="dropdown">
				<a id="victimDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users icon-blue"></i>&nbsp;&nbsp;Data Mangsa Banjir <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="reg_flood_victim"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Kemasukan Data</a></li>
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="list_flood_victim"><i class="fa fa-list-ol"></i>&nbsp;&nbsp;Senarai Data</a></li>
					</ul>			
			</li> -->
           <!-- <li class="dropdown">
				<a id="basicDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs icon-blue"></i>&nbsp;&nbsp;Analisis Banjir <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="flood_forecast"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Flood Forecasting</a></li>
						<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="economic_forecast"><i class="fa fa-line-chart"></i>&nbsp;&nbsp;Economic Forecasting</a></li> -->	
						<!--<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="extra_social"><i class="fa fa-twitter"></i>&nbsp;&nbsp;Flood Position (Twitter)</a></li>-->
						<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="extra_street"><i class="fa fa-street-view"></i>&nbsp;&nbsp;StreetView</a></li>
					</ul>			
			</li> -->
            <!-- <li class="dropdown">
				<a id="reportDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-file-text  icon-blue"></i>&nbsp;&nbsp;Laporan <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<!--<li><a href="javascript:child_open()" data-toggle="collapse" data-target=".navbar-collapse.in" id="report_upload"><i class="fa fa-upload"></i>&nbsp;&nbsp;Muatnaik Laporan </a></li>
						<li><a data-toggle="collapse" data-target=".navbar-collapse.in" id="report_upload"><i class="fa fa-upload"></i>&nbsp;&nbsp;Muatnaik Laporan </a></li>
						<li><a href="../assets/flood_report/bangsa.pdf" target="blank" argettdata-toggle="collapse" data-target=".navbar-collapse.in" id="report_list"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;1. Laporan Mengikut Bangsa & Jantina</a></li>
						<li><a href="../assets/flood_report/umur.pdf" target="blank"  data-toggle="collapse" data-target=".navbar-collapse.in" id="report_list"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;2. Laporan Mengikut Umur</a></li>
						<li><a href="../assets/flood_report/daerah.pdf" target="blank"  data-toggle="collapse" data-target=".navbar-collapse.in" id="report_list"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;3. Laporan Mengikut Daerah</a></li>
					</ul>		
			</li> -->
			<!-- <li class="dropdown right" style="margin-right:2em;"">
				<a  href="#" role="button"  id="user_lvl" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px;"><i class="fa fa-cog fa-spin icon-blue" style="font-size: 22px;"></i>&nbsp;&nbsp;<?php //echo $_SESSION['uname']; ?><b class="caret"></b></a>
				
				<ul class="dropdown-menu">
					<li><a data-toggle="collapse" data-target=".navbar-collapse.in" id="report_upload"><i class="fa fa-briefcase"></i>&nbsp;&nbsp;Kemaskini Akaun </a></li>
					<li><a href="../../Login/" data-toggle="collapse" data-target=".navbar-collapse.in" id="report_upload" style="font-size: 14px;"><i class="fa fa-sign-out fa-fw" style="font-size: 20px;"></i>&nbsp;&nbsp;Logout</a></li>
				</ul>
			</li> id="print_button"-->
			<li>
				<a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" style="font-size: 13px;"><i class="fa fa-print icon-blue" style="font-size: 19px; float: left;"></i><div id="printButton" style="float: right; margin-top: -8px;"></div></a>
			</li>
			
			
			<!-- <li class="right">
				<label id="print_button" style="margin-top: -10px; margin-bottom: -10px;"></label>
			</li> -->
			<!-- <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="extra_street"><i class="fa fa-street-view"></i>&nbsp;&nbsp;StreetView</a></li> -->
          </ul>
        </div>
      </div>
    </nav> 
    <!--<div id="wrapper" class="toggled">-->
	<div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#"><img src="assets/img/arrow.png" width="32px" height="29px" alt="Tutup petunjuk peta"></a>
                </li>
				<li>
					<a href="#">Table of Content</a>
                     <div id="tocDiv"></div>
                </li>
				<br>
            </ul>
			
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
		<div id="mapWrapper">
			<div id="HomeButton"></div>
			<div id="LocateButton"></div>
			<div id="EraseButton" title="Erase graphic"></div>
			<div id="BasemapToggle"></div>
			<span id="coordinate_info" style="color: black;"></span>
			<div id ="SearchAddress"><input id="searchBox" type="text" class="form-control"></div>
			<img id="loadingImg" src="loader/loader.gif" style="position:absolute; right:612px; top:356px; z-index:100;" />
		</div>
		
<!--         <div id="page-content-wrapper">
            <!-- <div class="container-fluid-2"> -->
                <!-- <div class="row"> -->
                    <!-- <div class="col-lg-12">  
					<div id="map"></div>	
                        <!-- <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a> -->
                    <!-- </div> -->
                <!-- </div> -->
            <!-- </div> 
       </div>  -->
        <!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->	
	
	<!-- ======================================================================================================== -->
	<!-- 											  WIDGETS 													  -->
	<!-- ======================================================================================================== -->
	
	<!-- ================================================= [ SEARCH ] =========================================== -->
	<!-------------------------------------------- SEARCH: LOT ------------------------------------ -->
	<div id="searchFormLot"  title="Search Lot" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:270px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
				  <div class="form-group">
					<label class="fieldlabel">Service *:</label>
					<select class="form-control" name="color"  id="select_service_lot" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select service...</option>
					<option value="FT">FT</option>
					<option value="QT">QT</option>
					<option value="Rezab">Rezab</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_lot" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">District *:</label>
					<select  class="form-control" id="select_district_lot" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District *:</label>
					<select  class="form-control" id="select_mukim_lot" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Seksyen:</label>
					<input  class="form-control" value="000" type="text" id="seksyen_name" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Lot Number:</label>
					<input  class="form-control" type="text" id="lot_number" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_lot" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Lot();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_lot" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Lot();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultLot"  title="Search Results for Lot" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridLot"></div>
	</div> 


	<!-------------------------------------------- SEARCH: PROJECT ------------------------------------ -->
	<div id="searchFormProject"  title="Search Project" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:270px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				 <div class="form-group">
					<label class="fieldlabel">Service *:</label>
					<select class="form-control" name="color"  id="select_service_project" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select service...</option>
					<option value="FT">FT</option>
					<option value="Pemohonan">Pemohonan</option>
					<option value="QT">QT</option>
					<option value="Rezab">Rezab</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_project" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">District *:</label>
					<select  class="form-control" id="select_district_project" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District *:</label>
					<select  class="form-control" id="select_mukim_project" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Kegunaan Tanah:</label>
					<input  class="form-control" type="text" id="tanah" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_tanah" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Project();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_project" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Project();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultProject"  title="Search Results for Project" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridProject"></div>
	</div> 

	<!-------------------------------------------- SEARCH: HAK MILIK SEMENTARA ------------------------------------ -->
	<div id="searchFormSementara"  title="Search Hak Milik Sementara" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:270px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_sementara" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">District *:</label>
					<select  class="form-control" id="select_district_sementara" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District *:</label>
					<select  class="form-control" id="select_mukim_sementara" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sementara:</label>
					<input  class="form-control" type="text" id="sementara" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_sementara" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Sementara();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_sementara" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Sementara();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultSementara"  title="Search Results for Hak Milik Sementara" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridSementara"></div>
	</div>

	<!-------------------------------------------- SEARCH: HAK MILIK ------------------------------------ -->
	<div id="searchFormMilik"  title="Search Hak Milik" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:270px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_milik" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">District *:</label>
					<select  class="form-control" id="select_district_milik" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District *:</label>
					<select  class="form-control" id="select_mukim_milik" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Tetap:</label>
					<input  class="form-control" type="text" id="milik" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_milik" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_Milik();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_milik" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_Milik();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultMilik"  title="Search Results for Hak Milik" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridMilik"></div>
	</div>

	<!-------------------------------------------- SEARCH: FILE NAME ------------------------------------ -->
	<div id="searchFormFile"  title="Search File Name" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:270px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				 <div class="form-group">
					<label class="fieldlabel">Service *:</label>
					<select class="form-control" name="color"  id="select_service_file" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select service...</option>
					<option value="FT">FT</option>
					<option value="Pemohonan">Pemohonan</option>
					<option value="QT">QT</option>
					<option value="Rezab">Rezab</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_file" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">File Name:</label>
					<input  class="form-control" type="text" id="ktpk" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_file" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_File();}'>Find</button> 	
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div> -->
				  <div class="btn btn-success">
					<button  type="button" id="clear_file" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_File();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-->
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultFile"  title="Search Results for File Name" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridFile"></div>
	</div>
                

	<!-------------------------------------------- STREET VIEW -------------------------------------->
	<div id="streetFormView" title="Street View" dojoType="dojox.layout.FloatingPane" resizable="true" closable: "true" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:270px;right:50px;width:400px;height:300px;visibility:hidden;">
		<div id="btnStreetView_exit" onclick="Toggle();return false;";  class="streetview_exit"><img src="assets/img/pangkah.png" width="29" height="28" align="right"></div> 
        <table cellpadding="0" style="width: 100%; height: 100%; margin:0px; padding:0px" border="1">  
            <tr style="height:80%">
	            <td align="center">
	            	
	                <div id="streetview" runat="server" style="border-style: inset; background-color: White; width: 99%; height:99%; margin:0px; padding:0px">
	                </div>
	            </td>
	        </tr>
		</table>    
    </div> 
	
	<script> 
		function Toggle()
		{   
			dijit.byId('streetFormView').hide();
		} 
	</script> 


	<!-------------------------------------------- SEARCH: EVACUATION CENTRE ------------------------------------ 
	<div id="searchFormEvacuation"  title="Search Temporary Evacuation Center" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;right:520px;width:280px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_evacuation" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">City:</label>
					<select  class="form-control" id="select_district_evacuation" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Center Name:</label>
					<input  class="form-control" type="text" id="evacuation_name" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:false"  placeholder="Keywords" />
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Capacity:</label>
					<input  class="form-control" type="text" id="evacuation_capacity" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true"  placeholder="Center's capacity" />
				  </div>
				  <div class="btn btn-primary">
					<button type="button"  id="search_evacuation" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_EvacuationCentre();}'>Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_evacuation" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_EvacuationCentre();}'>Clear</button>
				  </div>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultEvacuation"  title="Search results Evacuation Center" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridEvacuation"></div>
	</div> 
	
	<!-------------------------------------------- SEARCH: EMERGENCY CENTRE ------------------------------------ 
	<div id="searchFormEmergency"  title="Search Emergency Response Center" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:270px;height:350px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_emergency" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">City:</label>
					<select  class="form-control" id="select_district_emergency" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Center Name:</label>
					<input  class="form-control" type="text" id="emergency_name" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true"  placeholder="Keywords" />
				  </div>	
				  <div class="btn btn-primary">
					<button type="button"  id="search_emergency" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_EmergencyCentre();}'>Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_emergency" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_EmergencyCentre();}'>Clear</button>
				  </div>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultEmergency"  title="Search results Emergency Response Center" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridEmergency"></div>
	</div> 
	
	<!-------------------------------------------- SEARCH: AFFECTED FACILITY ------------------------------------ 
	<div id="searchFormFacility"  title="Search Facility" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:270px;height:340px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_facility" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">City:</label>
					<select  class="form-control" id="select_district_facility" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select City...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Center Name:</label>
					<input  class="form-control" type="text" id="facility_name" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
				  </div>	
				  <div class="btn btn-primary">
					<button type="button"  id="search_facility" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_AffectedFacility();}'>Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_facility" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_AffectedFacility();}'>Clear</button>
				  </div>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultFacility"  title="Search results Facility" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridFacility"></div>
	</div> 
	
	<!-------------------------------------------- SEARCH: VICTIM ------------------------------------ 
	<div id="searchFormVictim"  title="Search Flood Victims" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:130px;right:520px;width:270px;height:410px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				 <div class="form-group">
					<label class="fieldlabel">State *:</label>
					<select class="form-control" name="color"  id="select_state_victim" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Select state...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">City *:</label>
					<select  class="form-control" id="select_district_victim" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select City...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Sub District *:</label>
					<select  class="form-control" id="select_mukim_victim" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Select sub district...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Victim's Name:</label>
					<input  class="form-control" type="text" id="victim_name" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Keywords" />
					<span id="query"></span>
				  </div>
				  <!-- <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ executeQueryTask_FloodVictims();}'>Cari</button> 	
				  </div> 
				  <div class="btn btn-primary">
					<button type="button"  id="search_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_victim" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearSearch_FloodVictims();}'>Clear</button>
				  </div>
				  
				  <!--<div data-dojo-type="dojo/data/ItemFileReadStore" data-dojo-props="url:'../victim.php', urlPreventCache:true, clearOnClose:true" data-dojo-id="victimStore"></div>-
				  <span id="list3"></span>
				   
				</form>
			</div>
		</div>
	</div>  
	<div id="searchResultFloodVictims"  title="Search Results for Flood Victims" dojoType="dojox/layout/FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_searchFunctions" style= "position:absolute;top:120px;left:220px;width:495px;height:350px;visibility:hidden;duration=300;">
		<div id="gridFloodVictims"></div>
	</div> 
	
	<!---------------------------------------------- DOCK: SEARCH -------------------------------------- -->
	<div id="dock_searchFunctions" dojoType="dojox.layout.Dock" 
		style= "position:relative; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div>
	
	
	<!-- ============================================ [ BASIC FUNCTIONS ] ========================================= -->
	<!---------------------------------------------- BASEMAP GALLERY -------------------------------------- -->	
	<div id="basemapGalleryPane"  title="Basemap" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:140px;right:20px;width:270px;height:350px;visibility:hidden;">
		<div id="basemapGalleryDiv"></div>
	</div>
	
	<!---------------------------------------------- BOOKMARK -------------------------------------- -->	
	<!-- <div id="bookmarkPane"  title="Tanda Buku" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:150px;right:25px;width:230px;height:350px;visibility:hidden;">		
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
					<div id="bookmarkDiv"></div>
				</form>
			</div>		
		</div>
	</div> -->
		 
	<!---------------------------------------------- LOCATOR -------------------------------------- -->	
	<div id="locatorPane"  title="Locator Coordinates" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:270px;right:910px;width:270px;height:270px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>			  
				  <div class="form-group">
					<label class="fieldlabel">X Value(Longitude):</label>
					<input  class="form-control" type="text" id="longitude" dojoType="dijit/form/TextBox" placeholder="Coordinate X" />
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Y Value(Latitude):</label>
					<input  class="form-control" type="text" id="latitude" dojoType="dijit/form/TextBox"  placeholder="Coordinate Y" />
				  </div>				 	
				  <div class="btn btn-primary">
					<button type="button"  id="search_coordinate" dojoType="dijit/form/Button" data-dojo-attach-point="button">Find</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_coordinate" dojoType="dijit/form/Button" data-dojo-attach-point="button">Clear</button>
				  </div>				   
				</form>
			</div>
		</div>
	</div>

	<!---------------------------------------------- BUFFER -------------------------------------- -->
	<div id="bufferPane"  title="Buffer Zone" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_analysisFunctions" style= "position:absolute;top:140px;right:620px;width:275px;height:400px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
					<div class="form-group">
						<label class="fieldlabel">Temporary search around the buffer zone.</label>
						<label class="fieldlabel">Please activate the layer in the Temporary Placement Centre 'Guide Map'</label>
					</div>
				  
					<!--<div class="form-group">
						<label class="fieldlabel">Lapisan:</label>
						<select class="form-control" id="bufferLayer"  dojoType="dijit/form/ComboBox">
							<option value="poi">Kemudahan Awam</option>
							<option value="pusat_pindah">Pusat Penempatan Sementara</option>
						</select>	
					</div>-->
					  
					<div class="form-group"> 
						<label class="fieldlabel">Distance:</label>
						<input class="form-control" type="text" id="distance" dojoType="dijit/form/TextBox" size="5" value=""	placeholder="Buffer distance" /> 
					</div>
					
					<div class="form-group">
						<label class="fieldlabel">Unit:</label>
						<select class="form-control" id="unit"  dojoType="dijit/form/ComboBox">
							<option value="UNIT_STATUTE_MILE">Miles</option>
							<option value="UNIT_FOOT">Feet</option>
							<option value="UNIT_KILOMETER">Kilometers</option>
							<option value="UNIT_METER">Meters</option>
							<option value="UNIT_NAUTICAL_MILE">Nautical Miles</option>
							<option value="UNIT_US_NAUTICAL_MILE">US Nautical Miles</option>
							<option value="UNIT_DEGREE">Degrees</option>
						</select>	
					  </div>
					<!-- <div class="form-group">
						<label class="fieldlabel">Sila pilih kaedah untuk menanda:</label>
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-default" id="point">Point
								<img src="../assets/img/point.png" width="16" height="16"/>
							</button>
							<button type="button" class="btn btn-default" id="line">Line
								<img src="../assets/img/line.png" width="16" height="16"/>
							</button>
						</div>
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-default" id="polyline">Polyline
								<img src="../assets/img/polyline.png" width="16" height="16"/>
							</button>
							<button type="button" class="btn btn-default" id="polygon">Polygon
								<img src="../assets/img/polygon4.png" width="16" height="16"/>
							</button>							
						</div>
					</div> -->
					<div class="btn btn-success">
						<button  type="button" id="clearBuffer" dojoType="dijit/form/Button" data-dojo-attach-point="button">Clean graphics</button>
					</div>
					<br><br>
					<div class="form-group">
						<label class="fieldlabel">Search results:</label>
						<div id="gridBuffer"></div>	
					</div>
				</form>
			</div>		
		</div>
	</div>
	
	
	
		<!---------------------------------------------- MEASURE -------------------------------------- -->	
	<div id="measurePane"  title="Measurement" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:270px;right:30px;width:270px;height:270px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
					<div id="measurementDiv"></div>	
					<br><br>
					<div class="btn btn-success">
						<button  type="button" id="clearMeasure" dojoType="dijit/form/Button" data-dojo-attach-point="button">Clean graphics</button>
					</div>
				</form>
			</div>		
		</div>
	</div>	
	
	<!---------------------------------------------- GET DIRECTIONS -------------------------------------- -->	
	<!-- <div id="directionsPane"  title="Directions" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:140px;right:310px;width:300px;;height:400px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
					<div id="directionsDiv"></div>	
				</form>
			</div>		
		</div>
	</div>	 -->
	
	
	<!---------------------------------------------- ROUTE WITH BARRIER -------------------------------------- -->	
	<div id="routePane"  title="Arah Jalan dan Halangan" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:180px;right:20px;width:300px;;height:400px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
					<div id="routeDiv">
						<div class="form-group">
						<label class="fieldlabel">Pilih Jalan: </label>
						  <select id="routeName" class="dijit dijitReset dijitInline dijitLeft form-control dijitTextBox dijitComboBox dijitValidationTextBox">
							  <option value="Route 1">Jalan 1</option>
							  <option value="Route 2">Jalan 2</option>
							  <option value="Route 3">Jalan 3</option>
						  </select>
						</div>
							<div class="btn btn-default"  id="addStopsBtn">Tambah Hentian</div>
							<div class="btn btn-default"  id="clearStopsBtn">Bersihkan Hentian</div>
							<div class="btn btn-default"  id="addBarriersBtn">Tambah Halangan</div>
							<div class="btn btn-default"  id="clearBarriersBtn">Bersihkan Halangan</div>
							
							<p><div class="btn btn-primary"  id="solveRoutesBtn">Cari Jalan</div>
							<div class="btn btn-success"  id="clearRoutesBtn">Bersihkan Lakaran</div>
							</p>
						<br /><br />
						 <ol>
						  <li>Pilih Jalan 1, 2 atau 3.</li>
						  <li>Klik butang 'Tambah Hentian', kemudian klik atas peta untuk menanda hentian tersebut di atas jalanraya.</li>
						  <li>Klik butang 'Tambah Halangan', kemudian klik atas peta untuk menambah halangan jalan.</li>
						  <li>Klik 'Cari Jalan' untuk menghasilkan lakaran penyelesaian perjalanan.</li>
						  <li>Mesej akan popup jika terdapat masalah lain pada hentian yang ditanda seperti jalan mati.</li>
						</ul>
					</div>	
				</form>
			</div>		
		</div>
	</div>	
	
	<!-------------------------------------------- KEMASUKKAN DATA BANJIR ------------------------------------ -->
	<!-- <div id="registerFormVictim"  title="Daftar Mangsa Banjir" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_basicFunctions" style= "position:absolute;top:130px;right:30px;width:270px;height:400px;visibility:hidden;">
		 <div class="widgetWrapper">
			<div class="widgetForm">					
				<form>
				 <div class="form-group">
				  <label class="fieldlabel">Negeri *:</label>
					<select class="form-control" name="color"  id="select_state_victim_reg" dojoType="dijit/form/ComboBox"  fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A">Pilih negeri...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Daerah *:</label>
					<select  class="form-control" id="select_district_victim_reg" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Pilih daerah...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Mukim *:</label>
					<select  class="form-control" id="select_mukim_victim_reg" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Pilih mukim...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Pusat Pemindahan Sementara *:</label>
					<select  class="form-control" id="select_evacuation_victim_reg" dojoType="dijit/form/ComboBox" fetchProperties="{sort:[{attribute:'id', ascending:true}]}">
					<option value="0A" selected>Pilih pusat pemindahan...</option>
					</select>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Nama Mangsa *:</label>
					<input  class="form-control" type="text" id="regVictim_name" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Kata kunci" />
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">No. MyKad/MyKid:</label>
					<input  class="form-control" type="text" id="regVictim_id" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"  placeholder="Kata kunci" />
				  </div>			  
				  <div class="btn btn-primary">
					<button type="button"  id="save_regVictim" dojoType="dijit/form/Button" data-dojo-attach-point="button">Simpan</button> 	
				  </div>
				  <div class="btn btn-success">
					<button  type="button" id="clear_regVictim" dojoType="dijit/form/Button" data-dojo-attach-point="button" data-dojo-props='onClick:function(){ clearForm_RegVictim();}'>Bersihkan</button>
				  </div>

				
				</form>
			</div>
		</div>
	</div> -->
	
	<!---------------------------------------------- DOCK: BASIC -------------------------------------- -->
	<div id="dock_basicFunctions" dojoType="dojox.layout.Dock" 
		style= "position:absolute; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div>
	
	
	<!-- ============================================== [ ANALYSIS ] =============================================== -->
	
	
	
	<!---------------------------------------------- DOCK: ANALYSIS -------------------------------------- -->
	<div id="dock_analysisFunctions" dojoType="dojox.layout.Dock" 
		style= "position:absolute; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div>	
	
		
	<!-- ============================================== [ REPORT ] =============================================== -->
	<!---------------------------------------------- UPLOAD REPORT FILE -------------------------------------- -->	
	<!--<iframe name="votar" style="display:none;"></iframe>
	<div id="uploadReportPane"  title="Muatnaik Laporan" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_reportFunctions" style= "position:absolute;top:130px;right:20px;width:270px;height:270px;visibility:hidden;">
		<div class="widgetWrapper">
			<div class="widgetForm">				
				<form id="form" name="form" onSubmit="return val_upload(this, 'flood_report/upload_val.php')" method="post" enctype="multipart/form-data"target="votar">	
				  <div class="form-group">
					<label class="fieldlabel">Tajuk Laporan:</label>
					<input  class="form-control" type="text" id="report_name" name="report_name" dojoType="dijit/form/TextBox"	data-dojo-props="trim:true, propercase:true"/>
				  </div>
				  <div class="form-group">
					<label class="fieldlabel">Fail Laporan Untuk Dimuatnaik:</label>
					<input  class="form-control" type="file" id="fileSelect" name="fileSelect" dojoType="dijit/form/TextBox"/>
				  </div>			 	
				  <div class="btn btn-primary">
					<button type="submit"  id="uploadBtn" dojoType="dijit/form/Button" data-dojo-attach-point="button">Muatnaik Fail</button> 	
				  </div>				   
				</form>
			</div>
		</div>
	</div>	-->
	
	<!---------------------------------------------- DOCK: REPORT -------------------------------------- -->
	<!-- <div id="dock_reportFunctions" dojoType="dojox.layout.Dock" 
		style= "position:absolute; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div> -->
	
<!-- Trigger/Open The Modal -->




 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Maklumat Lengkap MyeTaPP</h4>
        </div>
        <div class="modal-body">
          <iframe name="iframelot" src="" width="100%" height="100%" frameborder="0" allowtransparency="true"></iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	
	

	<!-- ============================================== [ EXTRA ] =============================================== -->	
	<!---------------------------------------------- SOCIAL MEDIA -------------------------------------- -->
	<!---------------------------------------------- STREETVIEW -------------------------------------- -->
	<div id="street_panel"  title="StreetViewPanel" dojoType="dojox.layout.FloatingPane" resizable="true" 
			closable: "false" duration="350" dockable="true" dockTo="dock_extraFunctions" style= "position:absolute;top:120px;left:220px;width:40%;height:550px;visibility:hidden;">		
		<table cellpadding="0" style="width: 100%; height: 100%; margin:0px; padding:0px">  
			<tr style="height:80%">
			<td align="center">
				<div id="streetview" runat="server" style="border-style: inset; background-color: White; width: 99%; height:99%; margin:0px; padding:0px">
				</div>
			</td>
			</tr>
			<tr style="height:20%">
				<td align="center" valign="top">
					<div id="Desc" style="border-style: single; overflow: auto; width: 98%; height: 98%; padding:0px; margin:0px">
					</div>
				</td>
			</tr>
			<tr style="height:5%"></tr>
		</table>
	</div>	
	
	<!---------------------------------------------- DOCK: EXTRA-------------------------------------- -->
	<div id="dock_extraFunctions" dojoType="dojox.layout.Dock" 
		style= "position:absolute; bottom:0; right:0; height:0px; width:0px; display:none; z-index:0;">
	</div>
	
	<!-- =========== Bootstrap core JavaScript =========== -->
    <!-- Placed at the end of the document so the pages load faster -->	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/css/gis/bootstrap.min.js"></script>
	
	<!-- JavaScript for map application -->
	<script src="mapOK2.js"></script>
	<!-- <script src="identifyTask.js"></script> -->

		<!-- Reference the Query/Search JavaScript functions -->
	<script type="text/javascript" src="assets/js/search/global.js"></script>
	<script type="text/javascript" src="assets/js/search/lot.js"></script>
	<script type="text/javascript" src="assets/js/search/project.js"></script>
	<script type="text/javascript" src="assets/js/search/sementara.js"></script>
	<script type="text/javascript" src="assets/js/search/milik.js"></script>
	<script type="text/javascript" src="assets/js/search/file.js"></script>
	<!-- <script type="text/javascript" src="assets/js/search/evacuationCentre.js"></script>	
	<script type="text/javascript" src="assets/js/search/emergencyCentre.js"></script>
	<script type="text/javascript" src="assets/js/search/affectedFacility.js"></script>
	<script type="text/javascript" src="assets/js/search/victim.js"></script> -->
	<script type="text/javascript" src="assets/js/form/register_victim.js"></script>
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
