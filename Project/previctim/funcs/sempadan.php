<? /******************************************************************************************* 
Coded by: Dd! (fadhilah@onedynamic.com.my)
It is highly suggested NOT to edit any part of these codes unless you know what you're doing! 
***********************************************************************************************/ ?>
<?
include_once('../../assets/conn/sql_server.php');

class Sempadan {
	var $arrDaerah = array();
	var $arrMukim = array();
	var $dbConn = NULL;

	function Sempadan() 
	{
			$this->dbConnect = new DBConns84();
	}
	
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# ==================================== DISPLAY DROPDOWN DAERAH BY NEGERI ================================================
	function genOptDaerah() 
	{
		//echo "alert(\"masuk genOptMukimVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(Negeri), daerah
									FROM sde.MUKIM
									ORDER BY Negeri";
		$this->dbConnect->query84();
  
		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
	   	for ( $i = 0; $i < $rows; $i++ ) { 
				$rkd_dae =  pg_fetch_array($result);	
				$kod_dae = $rkd_dae['kod_daerah'];
	
			  echo "if ( d == '".$kod_dae."') {\n"; 
			  //echo "else {\n";
					//echo "alert(\"masuk if daerah\");";
					$this->dbConnect->query = "SELECT DISTINCT(kod_mukim), mukim 
												  FROM data_aset.\"Luput_Notis_5A\" 
												  WHERE kod_daerah = '".$kod_dae."' 
												  ORDER BY mukim";
					$this->dbConnect->query84();
	
					$result_mkm = $this->dbConnect->result;
					$rows_mkm = $this->dbConnect->rows;
					
					echo "select_mukim.options[select_mukim.options.length] = new Option('Pilih mukim' , '0');\n";
					for ( $z=0; $z<$rows_mkm; $z++ ) {
						$rkd_mkm = pg_fetch_object($result_mkm);
	
						array_push($this->arrMukim, $rkd_mkm->mukim);
						echo "select_mukim.options[select_mukim.options.length] = new Option('".$rkd_mkm->mukim."', '".$rkd_mkm->kod_mukim."');\n";
					} 
			  echo "}\n\n  "; 
			}
	} 
	
# ==================================== 5A: DISPLAY DROPDOWN MUKIM BY DAERAH ================================================
	function genOptMukim5A() 
	{
		//echo "alert(\"masuk genOptMukimVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									  FROM data_aset.\"Luput_Notis_5A\"
									  ORDER BY daerah";
		$this->dbConnect->query84();
  
		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
	   	for ( $i = 0; $i < $rows; $i++ ) { 
				$rkd_dae =  pg_fetch_array($result);	
				$kod_dae = $rkd_dae['kod_daerah'];
	
			  echo "if ( d == '".$kod_dae."') {\n"; 
			  //echo "else {\n";
					//echo "alert(\"masuk if daerah\");";
					$this->dbConnect->query = "SELECT DISTINCT(kod_mukim), mukim 
												  FROM data_aset.\"Luput_Notis_5A\" 
												  WHERE kod_daerah = '".$kod_dae."' 
												  ORDER BY mukim";
					$this->dbConnect->query84();
	
					$result_mkm = $this->dbConnect->result;
					$rows_mkm = $this->dbConnect->rows;
					
					echo "select_mukim.options[select_mukim.options.length] = new Option('Pilih mukim' , '0');\n";
					for ( $z=0; $z<$rows_mkm; $z++ ) {
						$rkd_mkm = pg_fetch_object($result_mkm);
	
						array_push($this->arrMukim, $rkd_mkm->mukim);
						echo "select_mukim.options[select_mukim.options.length] = new Option('".$rkd_mkm->mukim."', '".$rkd_mkm->kod_mukim."');\n";
					} 
			  echo "}\n\n  "; 
			}
	} 
	
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# ==================================== PAJAK: DISPLAY DROPDOWN DAERAH BY JENIS DATA ================================================
	function genOptDaerahPajak() 
	{
		//echo "alert(\"masuk genOptDaerahVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									FROM data_aset.\"Luput_Tempoh_Pajakan\"
									ORDER BY daerah";
		$this->dbConnect->query84();

		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
		echo "select_daerah.options[select_daerah.options.length] = new Option('Pilih daerah' , '0');\n";
		for ( $z=0; $z<$rows; $z++ ) {
			$rkd_dae = pg_fetch_object($result);

			array_push($this->arrDaerah, $rkd_dae->kod_daerah);
			echo "select_daerah.options[select_daerah.options.length] = new Option('".$rkd_dae->daerah."', '".$rkd_dae->kod_daerah."');\n";
		} 
	} 
	
# ==================================== PAJAK: DISPLAY DROPDOWN MUKIM BY DAERAH ================================================
	function genOptMukimPajak() 
	{
		//echo "alert(\"masuk genOptMukimVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									  FROM data_aset.\"Luput_Tempoh_Pajakan\"
									  ORDER BY daerah";
		$this->dbConnect->query84();
  
		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
	   	for ( $i = 0; $i < $rows; $i++ ) { 
				$rkd_dae =  pg_fetch_array($result);	
				$kod_dae = $rkd_dae['kod_daerah'];
	
			  echo "if ( d == '".$kod_dae."') {\n"; 
			  //echo "else {\n";
					//echo "alert(\"masuk if daerah\");";
					$this->dbConnect->query = "SELECT DISTINCT(kod_mukim), mukim 
												  FROM data_aset.\"Luput_Tempoh_Pajakan\" 
												  WHERE kod_daerah = '".$kod_dae."' 
												  ORDER BY mukim";
					$this->dbConnect->query84();
	
					$result_mkm = $this->dbConnect->result;
					$rows_mkm = $this->dbConnect->rows;
					
					echo "select_mukim.options[select_mukim.options.length] = new Option('Pilih mukim' , '0');\n";
					for ( $z=0; $z<$rows_mkm; $z++ ) {
						$rkd_mkm = pg_fetch_object($result_mkm);
	
						array_push($this->arrMukim, $rkd_mkm->mukim);
						echo "select_mukim.options[select_mukim.options.length] = new Option('".$rkd_mkm->mukim."', '".$rkd_mkm->kod_mukim."');\n";
					} 
			  echo "}\n\n  "; 
			}
	} 

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# ==================================== 8A: DISPLAY DROPDOWN DAERAH BY JENIS DATA ================================================
	function genOptDaerah8A() 
	{
		//echo "alert(\"masuk genOptDaerahVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									FROM data_aset.\"Notis_RampasanTanah_8A\"
									ORDER BY daerah";
		$this->dbConnect->query84();

		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
		echo "select_daerah.options[select_daerah.options.length] = new Option('Pilih daerah' , '0');\n";
		for ( $z=0; $z<$rows; $z++ ) {
			$rkd_dae = pg_fetch_object($result);

			array_push($this->arrDaerah, $rkd_dae->kod_daerah);
			echo "select_daerah.options[select_daerah.options.length] = new Option('".$rkd_dae->daerah."', '".$rkd_dae->kod_daerah."');\n";
		} 
	} 
	
# ==================================== 8A: DISPLAY DROPDOWN MUKIM BY DAERAH ================================================
	function genOptMukim8A() 
	{
		//echo "alert(\"masuk genOptMukimVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									  FROM data_aset.\"Notis_RampasanTanah_8A\"
									  ORDER BY daerah";
		$this->dbConnect->query84();
  
		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
	   	for ( $i = 0; $i < $rows; $i++ ) { 
				$rkd_dae =  pg_fetch_array($result);	
				$kod_dae = $rkd_dae['kod_daerah'];
	
			  echo "if ( d == '".$kod_dae."') {\n"; 
			  //echo "else {\n";
					//echo "alert(\"masuk if daerah\");";
					$this->dbConnect->query = "SELECT DISTINCT(kod_mukim), mukim 
												  FROM data_aset.\"Notis_RampasanTanah_8A\" 
												  WHERE kod_daerah = '".$kod_dae."' 
												  ORDER BY mukim";
					$this->dbConnect->query84();
	
					$result_mkm = $this->dbConnect->result;
					$rows_mkm = $this->dbConnect->rows;
					
					echo "select_mukim.options[select_mukim.options.length] = new Option('Pilih mukim' , '0');\n";
					for ( $z=0; $z<$rows_mkm; $z++ ) {
						$rkd_mkm = pg_fetch_object($result_mkm);
	
						array_push($this->arrMukim, $rkd_mkm->mukim);
						echo "select_mukim.options[select_mukim.options.length] = new Option('".$rkd_mkm->mukim."', '".$rkd_mkm->kod_mukim."');\n";
					} 
			  echo "}\n\n  "; 
			}
	} 


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# ==================================== LPS: DISPLAY DROPDOWN DAERAH BY JENIS DATA ================================================
	function genOptDaerahLPS() 
	{
		//echo "alert(\"masuk genOptDaerahVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									FROM data_aset.\"Tanah_LPS\"
									ORDER BY daerah";
		$this->dbConnect->query84();

		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
		echo "select_daerah.options[select_daerah.options.length] = new Option('Pilih daerah' , '0');\n";
		for ( $z=0; $z<$rows; $z++ ) {
			$rkd_dae = pg_fetch_object($result);

			array_push($this->arrDaerah, $rkd_dae->kod_daerah);
			echo "select_daerah.options[select_daerah.options.length] = new Option('".$rkd_dae->daerah."', '".$rkd_dae->kod_daerah."');\n";
		} 
	} 
	
# ==================================== LPS: DISPLAY DROPDOWN MUKIM BY DAERAH ================================================
	function genOptMukimLPS() 
	{
		//echo "alert(\"masuk genOptMukimVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									  FROM data_aset.\"Tanah_LPS\"
									  ORDER BY daerah";
		$this->dbConnect->query84();
  
		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
	   	for ( $i = 0; $i < $rows; $i++ ) { 
				$rkd_dae =  pg_fetch_array($result);	
				$kod_dae = $rkd_dae['kod_daerah'];
	
			  echo "if ( d == '".$kod_dae."') {\n"; 
			  //echo "else {\n";
					//echo "alert(\"masuk if daerah\");";
					$this->dbConnect->query = "SELECT DISTINCT(kod_mukim), mukim 
												  FROM data_aset.\"Tanah_LPS\" 
												  WHERE kod_daerah = '".$kod_dae."' 
												  ORDER BY mukim";
					$this->dbConnect->query84();
	
					$result_mkm = $this->dbConnect->result;
					$rows_mkm = $this->dbConnect->rows;
					
					echo "select_mukim.options[select_mukim.options.length] = new Option('Pilih mukim' , '0');\n";
					for ( $z=0; $z<$rows_mkm; $z++ ) {
						$rkd_mkm = pg_fetch_object($result_mkm);
	
						array_push($this->arrMukim, $rkd_mkm->mukim);
						echo "select_mukim.options[select_mukim.options.length] = new Option('".$rkd_mkm->mukim."', '".$rkd_mkm->kod_mukim."');\n";
					} 
			  echo "}\n\n  "; 
			}
	} 


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# ==================================== RIZAB: DISPLAY DROPDOWN DAERAH BY JENIS DATA ================================================
	function genOptDaerahRizab() 
	{
		//echo "alert(\"masuk genOptDaerahVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									FROM data_aset.\"Tanah_Rizab\"
									ORDER BY daerah";
		$this->dbConnect->query84();

		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
		echo "select_daerah.options[select_daerah.options.length] = new Option('Pilih daerah' , '0');\n";
		for ( $z=0; $z<$rows; $z++ ) {
			$rkd_dae = pg_fetch_object($result);

			array_push($this->arrDaerah, $rkd_dae->kod_daerah);
			echo "select_daerah.options[select_daerah.options.length] = new Option('".$rkd_dae->daerah."', '".$rkd_dae->kod_daerah."');\n";
		} 
	} 
	
# ==================================== RIZAB: DISPLAY DROPDOWN MUKIM BY DAERAH ================================================
	function genOptMukimRizab() 
	{
		//echo "alert(\"masuk genOptMukimVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									  FROM data_aset.\"Tanah_Rizab\"
									  ORDER BY daerah";
		$this->dbConnect->query84();
  
		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
	   	for ( $i = 0; $i < $rows; $i++ ) { 
				$rkd_dae =  pg_fetch_array($result);	
				$kod_dae = $rkd_dae['kod_daerah'];
	
			  echo "if ( d == '".$kod_dae."') {\n"; 
			  //echo "else {\n";
					//echo "alert(\"masuk if daerah\");";
					$this->dbConnect->query = "SELECT DISTINCT(kod_mukim), mukim 
												  FROM data_aset.\"Tanah_Rizab\" 
												  WHERE kod_daerah = '".$kod_dae."' 
												  ORDER BY mukim";
					$this->dbConnect->query84();
	
					$result_mkm = $this->dbConnect->result;
					$rows_mkm = $this->dbConnect->rows;
					
					echo "select_mukim.options[select_mukim.options.length] = new Option('Pilih mukim' , '0');\n";
					for ( $z=0; $z<$rows_mkm; $z++ ) {
						$rkd_mkm = pg_fetch_object($result_mkm);
	
						array_push($this->arrMukim, $rkd_mkm->mukim);
						echo "select_mukim.options[select_mukim.options.length] = new Option('".$rkd_mkm->mukim."', '".$rkd_mkm->kod_mukim."');\n";
					} 
			  echo "}\n\n  "; 
			}
	} 



# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ==================================== VIRGIN: DISPLAY DROPDOWN DAERAH BY JENIS DATA ================================================
	function genOptDaerahVirgin() 
	{
		//echo "alert(\"masuk genOptDaerahVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									FROM data_aset.\"Virgin_Land\"
									ORDER BY daerah";
		$this->dbConnect->query84();

		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
		echo "select_daerah.options[select_daerah.options.length] = new Option('Pilih daerah' , '0');\n";
		for ( $z=0; $z<$rows; $z++ ) {
			$rkd_dae = pg_fetch_object($result);

			array_push($this->arrDaerah, $rkd_dae->kod_daerah);
			echo "select_daerah.options[select_daerah.options.length] = new Option('".$rkd_dae->daerah."', '".$rkd_dae->kod_daerah."');\n";
		} 
	} 
	
# ====================================VIRGIN: DISPLAY DROPDOWN MUKIM BY DAERAH ================================================
	function genOptMukimVirgin() 
	{
		//echo "alert(\"masuk genOptMukimVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(kod_daerah), daerah
									  FROM data_aset.\"Virgin_Land\"
									  ORDER BY daerah";
		$this->dbConnect->query84();
  
		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
	   	for ( $i = 0; $i < $rows; $i++ ) { 
				$rkd_dae =  pg_fetch_array($result);	
				$kod_dae = $rkd_dae['kod_daerah'];
	
			  echo "if ( d == '".$kod_dae."') {\n"; 
			  //echo "else {\n";
					//echo "alert(\"masuk if daerah\");";
					$this->dbConnect->query = "SELECT DISTINCT(kod_mukim), mukim 
												  FROM data_aset.\"Virgin_Land\" 
												  WHERE kod_daerah = '".$kod_dae."' 
												  ORDER BY mukim";
					$this->dbConnect->query84();
	
					$result_mkm = $this->dbConnect->result;
					$rows_mkm = $this->dbConnect->rows;
					
					echo "select_mukim.options[select_mukim.options.length] = new Option('Pilih mukim' , '0');\n";
					for ( $z=0; $z<$rows_mkm; $z++ ) {
						$rkd_mkm = pg_fetch_object($result_mkm);
	
						array_push($this->arrMukim, $rkd_mkm->mukim);
						echo "select_mukim.options[select_mukim.options.length] = new Option('".$rkd_mkm->mukim."', '".$rkd_mkm->kod_mukim."');\n";
					} 
			  echo "}\n\n  "; 
			}
	} 
	
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ==================================== SPBT: DISPLAY DROPDOWN DAERAH BY JENIS DATA ================================================
	function genOptDaerahSPTB() 
	{
		//echo "alert(\"masuk genOptDaerahVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(s.daerah) AS kod_daerah, d.daerah
										  FROM data_aset.\"Lot_SPTB\" s, peta_asas.\"Daerah\" d
										  WHERE s.daerah = d.kod_daerah
										  ORDER BY d.daerah";
		$this->dbConnect->query84();

		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
		echo "select_daerah.options[select_daerah.options.length] = new Option('Pilih daerah' , '0');\n";
		for ( $z=0; $z<$rows; $z++ ) {
			$rkd_dae = pg_fetch_object($result);

			array_push($this->arrDaerah, $rkd_dae->kod_daerah);
			echo "select_daerah.options[select_daerah.options.length] = new Option('".$rkd_dae->daerah."', '".$rkd_dae->kod_daerah."');\n";
		} 
	} 
	
# ====================================SPBT: DISPLAY DROPDOWN MUKIM BY DAERAH ================================================
	function genOptMukimSPTB() 
	{
		//echo "alert(\"masuk genOptMukimVirgin\");";
		
		
		$this->dbConnect->query = "SELECT DISTINCT(s.daerah) AS kod_daerah, d.daerah
										  FROM data_aset.\"Lot_SPTB\" s, peta_asas.\"Daerah\" d
										  WHERE s.daerah = d.kod_daerah
										  ORDER BY d.daerah";
		$this->dbConnect->query84();
  
		$result = $this->dbConnect->result;
		$rows = $this->dbConnect->rows; 
					  
	   	for ( $i = 0; $i < $rows; $i++ ) { 
				$rkd_dae =  pg_fetch_array($result);	
				$kod_dae = $rkd_dae['kod_daerah'];
	
			  echo "if ( d == '".$kod_dae."') {\n"; 
			  //echo "else {\n";
					//echo "alert(\"masuk if daerah\");";
					$this->dbConnect->query = "SELECT DISTINCT(mukim) 
												  FROM data_aset.\"Lot_SPTB\" 
												  WHERE daerah = '".$kod_dae."' 
												  ORDER BY mukim";
					$this->dbConnect->query84();
	
					$result_mkm = $this->dbConnect->result;
					$rows_mkm = $this->dbConnect->rows;
					
					echo "select_mukim.options[select_mukim.options.length] = new Option('Pilih mukim' , '0');\n";
					for ( $z=0; $z<$rows_mkm; $z++ ) {
						$rkd_mkm = pg_fetch_object($result_mkm);
	
						array_push($this->arrMukim, $rkd_mkm->mukim);
						echo "select_mukim.options[select_mukim.options.length] = new Option('".$rkd_mkm->mukim."', '".$rkd_mkm->mukim."');\n";
					} 
			  echo "}\n\n  "; 
			}
	} 


} //CLOSE CLASS

?>

