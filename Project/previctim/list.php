<?php
	include("../../assets/conn/sql_server.php");
	
	# RETRIEVE VICTIMS LIST	(IMPOSSIBLE TO DO JOINING BECAUSE DB IS NOT NORMAL -----------------------------------------
	$sql = "SELECT MANGSABANJIR.*,MANGSAMASUK.lokasi_masuk, MANGSAMASUK.tarikh_masuk 
	FROM MANGSABANJIR
	LEFT JOIN MANGSAMASUK ON (MANGSABANJIR.noIC LIKE MANGSAMASUK.noIC)
	ORDER BY CAST(MANGSABANJIR.mangsaNama AS NVARCHAR(100))"; 
	$params = array();
	$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt = sqlsrv_query($conn, $sql , $params, $options );	
	
	//$row_vic = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)
  
	if( $stmt === false ) { print( print_r( sqlsrv_errors() ) ); }
	
	# RETRIEVE STATE, DISTRICT, MUKIM NAME
	/*$sql_mkm = "SELECT *
			FROM sde.MUKIM 
			WHERE NAME = $row_vic['mukim']
			AND DISTRICT = $row_vic['daerah']
			AND STATE = $row_vic['negeri']
			ORDER BY CAST(mangsaNama AS NVARCHAR(100))"; 
	$params_mkm = array();
	$options_mkm =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_mkm = sqlsrv_query($conn, $sql_mkm , $params_mkm, $options_mkm );	*/
	
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Senarai Pra Daftar</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                     
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">           
            <!-- PAGE CONTENT -->
            <div class="page-content">
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Senarai Pra Daftar</h3>                              
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>Nama Mangsa</th>
												<th>No. IC</th>
                                                <th>Jantina</th>
												<th>Umur</th>
												<th>Bangsa</th>
												<th>Status OKU</th>
												<th>Negeri</th>
												<th>Daerah</th>
												<th>Mukim</th>
												<th>No. Tel</th>
												<th>Lokasi Masuk</th>
												<th>Masa & Tarikh Daftar</th>
												<th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php																						  
											  while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) { 	
											?>
                                            <tr>
                                                <td><?php echo $row['mangsaNama'] ?></td>
												<td><?php echo $row['noIC'] ?></td>												
												<td><?php echo $row['jantina'] ?></td>
												<td><?php echo $row['umur'] ?></td>
                                                <td><?php echo $row['bangsa'] ?></td>
												<td><?php echo $row['status'] ?></td>
												<td><?php echo $row['negeri'] ?></td>
												<td><?php echo $row['daerah'] ?></td>
												<td><?php echo $row['mukim'] ?></td>
												<td><?php echo $row['no_tel'] ?></td>
												<td><?php echo $row['lokasi_masuk'] ?></td>
												<td><?php echo date_format($row['tarikh_masuk'], 'd/m/Y H:i:s'); ?></td>
												<td>
                                                   <a href="edit.php?icNo=<?php echo $row['noIC'] ?>"><button class="btn btn-info btn-rounded btn-sm"><span class="fa fa-pencil"></span>Kemaskini</button></a> 
												   <a href="delete.php?icNo=<?php echo $row['noIC'] ?>"><button class="btn btn-danger btn-rounded btn-sm"><span class="fa fa-trash-o"></span>Hapus</button></a> 
                                                </td>
                                            </tr>
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->                                
            </div>    
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->       
                          
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->                

        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/settings.js"></script>
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 
        
    </body>
</html>






