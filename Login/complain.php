<?php
session_start();
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>FeLIS</title>
  <!-- <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script> -->

<!-- <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'> -->
<!-- <link rel="stylesheet" href="https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/reset.css"> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"> -->
<link rel="stylesheet" href="css/family.css">
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/reset.min.css">
<link rel="stylesheet" href="css/style.css">

</head>

<body>
  <body>
	<header role="banner">
		<!--div id="cd-logo" style="margin-top: -10px; margin-left: 15px; width: 150px;"><a href="index.php"><img src="img/JKPTG_01.png" alt="Logo"></a></div-->
		<nav class="main-nav">
			<!--<ul>-->
				<!-- inser more links here -->
			<!--	<li><a class="cd-signin" href="#0">Sign in</a></li>
				<li><a class="cd-signup" href="#0">Sign up</a></li>
			</ul>-->
		</nav>
	</header>

	<div class="cd-user-modal" id="test"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
			<ul class="cd-switcher">
				<!-- <li><a href="#signin">Sign in</a></li> -->
				<li><a href="#complain" class="selected">Borang Aduan Masalah</a></li>
				<li><a href="#"></a></li>
			</ul>

			<div id="cd-complain"> <!-- sign up form -->
				<form class="cd-form" method="POST" action="email_complain.php">
					
					<p class="fieldset">
						<label class="image-replace cd-username1" for="complain-name">Nama Pengadu</label>
						<input name="txtname" class="full-width has-padding has-border" id="complain-name" type="text" autocomplete="off" placeholder="Nama Pengadu*" REQUIRED>
					</p>
					
					<p class="fieldset">
						<label class="image-replace cd-email1" for="complain-email">Emel</label>
						<input name="txtemail" class="full-width has-padding has-border" id="complain-email" type="email" autocomplete="off" placeholder="Emel*" REQUIRED>
					</p>

					<P class="fieldset">
						<!-- <label for="complain-jawatan">Jawatan*</label> -->
						<select name="txtjawatan" class="full-width has-padding has-border" id="jawatan">
							<option value="">Pilih Jawatan</option>
							<?php
								include('../sql_server_db.php');
								$sql = "SELECT ID_JAWATAN, NAMA_JAWATAN FROM tblrujjawatan";
								$result = sqlsrv_query($conn,$sql) or die("Couldn't execut query");
								while ($data=sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
									
									echo "<option value=";
									echo $data['ID_JAWATAN'];
									echo ">";
									echo $data['NAMA_JAWATAN']; 
									echo "</option>";
								}
							?>
						</select>
					</P>

					<p class="fieldset">
						<!-- <label for="complain-state">Negeri*</label> -->
						<select name="txtstate" class="full-width has-padding has-border" id="state">
							<option value="">Pilih Negeri</option>
							<?php
								include('../sql_server_db.php');
								$sql = "SELECT KOD_NEGERI, NAMA_NEGERI FROM tblrujnegeri_1";
								$result = sqlsrv_query($conn,$sql) or die("Couldn't execut query");
								while ($data=sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
									
									echo "<option value=";
									echo $data['KOD_NEGERI'];
									echo ">";
									echo $data['NAMA_NEGERI']; 
									echo "</option>";
								}
                            ?>
						</select>
					</p>

					<P class="fieldset">
						<input name="txtbahagian" class="full-width has-padding has-border" id="complain-bahagian" type="text" autocomplete="off" placeholder="Bahagian/Unit*" REQUIRED>
					</P>

					<p class="fieldset">
						<label for="complain-type" style="padding-right: 40px;">Jenis Aduan*</label>
						<input type="radio" name="type" value="eCharting"> eCharting &emsp;&emsp;
						<input type="radio" name="type" value="FeLIS"> FeLIS &emsp;&emsp;
						<input type="radio" name="type" value="Tapp n Go"> TaPP n Go
					</p>

					<P class="fieldset">
						<input name="txttitle" class="full-width has-padding has-border" id="complain-title" type="text" autocomplete="off" placeholder="Tajuk Aduan*" REQUIRED>
					</P>

					<P class="fieldset">
						<!-- <label for="complain-detail">Keterangan Aduan*</label><br><br> -->
						<textarea class="full-width has-padding has-border" id="txtdetail" rows="6" cols="63" name="txtdetail" placeholder="Keterangan Aduan*" REQUIRED></textarea>
					</P>

					<p class="fieldset">
						<input name="complain" class="full-width has-padding" type="submit" value="Hantar">
					</p>
				</form>

				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-complain -->
			<a href="#0" class="cd-close-form">Tutup</a>
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->
</body>
  <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->

  <script src="js/modernizr.js"></script>
  <script src="js/jquery.min.js"></script>
  <script src="js/index_complain.js"></script>

</body>
</html>
