jQuery(document).ready(function($){
	var $form_modal = $('.cd-user-modal'),
		$form_complain = $form_modal.find('#cd-complain'),
		$main_nav = $('.main-nav');

		$main_nav.children('ul').removeClass('is-visible');
		//show modal layer
		$form_modal.addClass('is-visible');	
		//show the selected form
		// complain_selected();
	//close modal
	// $('.cd-user-modal').on('click', function(event){
	// 	if( $(event.target).is($form_modal) || $(event.target).is('.cd-close-form') ) {
	// 		$form_modal.removeClass('is-visible');
	// 		window.location.href = "https://www.gis.myetapp.gov.my/myetappgis/Login/index.html";
	// 	}	
	// });

	function complain_selected(){
		$form_complain.addClass('is-selected');
	}



	//IE9 placeholder fallback
	//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
	// if(!Modernizr.input.placeholder){
	// 	$('[placeholder]').focus(function() {
	// 		var input = $(this);
	// 		if (input.val() == input.attr('placeholder')) {
	// 			input.val('');
	// 	  	}
	// 	}).blur(function() {
	// 	 	var input = $(this);
	// 	  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
	// 			input.val(input.attr('placeholder'));
	// 	  	}
	// 	}).blur();
	// 	$('[placeholder]').parents('form').submit(function() {
	// 	  	$(this).find('[placeholder]').each(function() {
	// 			var input = $(this);
	// 			if (input.val() == input.attr('placeholder')) {
	// 		 		input.val('');
	// 			}
	// 	  	})
	// 	});
	// }

});

//credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
    	// If this function exists...
    	if (this.setSelectionRange) {
      		// ... then use it (Doesn't work in IE)
      		// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
      		var len = $(this).val().length * 2;
      		this.setSelectionRange(len, len);
    	} else {
    		// ... otherwise replace the contents with itself
    		// (Doesn't work in Google Chrome)
      		$(this).val($(this).val());
    	}
	});
};