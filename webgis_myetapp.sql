-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2017 at 10:35 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webgis_myetapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

CREATE TABLE `audit` (
  `audit_id` int(11) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `activity` varchar(50) NOT NULL,
  `access_date_time` datetime NOT NULL,
  `authority` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit`
--

INSERT INTO `audit` (`audit_id`, `ip_address`, `username`, `activity`, `access_date_time`, `authority`) VALUES
(1, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-20 10:43:19', '0'),
(2, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-20 16:15:01', '0'),
(3, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-20 16:20:05', 'Pahang'),
(4, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-20 16:32:34', 'Pahang'),
(7, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-20 16:38:13', 'Pahang'),
(8, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2016-12-20 16:38:23', 'Pahang'),
(9, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-22 14:36:07', 'Pahang'),
(10, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2016-12-22 15:21:52', 'Pahang'),
(11, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-22 15:29:09', 'Pahang'),
(12, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2016-12-22 15:32:31', 'Pahang'),
(13, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-22 15:33:02', 'Pahang'),
(14, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2016-12-22 15:33:59', 'Pahang'),
(15, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-22 16:55:52', 'Pahang'),
(16, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-23 10:58:16', 'Pahang'),
(17, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2016-12-23 11:00:07', 'Pahang'),
(18, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-23 11:01:43', 'Pahang'),
(19, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2016-12-23 11:03:49', 'Pahang'),
(20, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-27 11:10:50', 'Pahang'),
(21, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2016-12-27 11:22:52', 'Pahang'),
(22, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2016-12-27 11:23:04', 'Pahang'),
(23, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2016-12-27 12:39:58', 'Pahang'),
(24, '::1', 'sunrise', 'Logout from WebGIS Myetapp', '2016-12-28 11:37:55', ''),
(25, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-01-03 10:36:40', 'Pahang'),
(26, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-01-03 10:36:56', 'Pahang'),
(27, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-02-28 10:53:54', 'Pahang'),
(28, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-02-28 10:55:43', 'Pahang'),
(29, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-02-28 11:03:27', 'Pahang'),
(30, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-02-28 11:03:52', 'Pahang'),
(31, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-02-28 15:42:44', 'Pahang'),
(32, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-02-28 15:43:12', 'Pahang'),
(33, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-02-28 17:02:32', 'Johor'),
(34, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-02-28 17:07:35', 'Johor'),
(35, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-01 11:18:25', 'Johor'),
(36, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 10:42:38', 'Johor'),
(37, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-02 10:44:08', 'Johor'),
(38, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 10:45:09', 'Johor'),
(39, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-02 10:45:37', 'Johor'),
(40, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 10:46:09', 'Johor'),
(41, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 11:17:02', 'Johor'),
(42, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 13:59:25', 'Johor'),
(43, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-02 14:03:48', 'Johor'),
(44, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 14:04:13', 'Johor'),
(45, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-02 14:09:12', 'Kedah'),
(46, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 14:09:34', 'Kedah'),
(47, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 14:14:32', 'Kedah'),
(48, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-02 14:36:30', 'Kedah'),
(49, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 14:36:52', 'Kedah'),
(50, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-02 15:16:38', 'Kedah'),
(51, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 15:21:06', 'Malacca'),
(52, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 16:04:23', 'Malacca'),
(53, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-02 16:39:20', 'Malacca'),
(54, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-02 17:14:45', 'Malacca'),
(55, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-03 09:12:43', 'Malacca'),
(56, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-03 15:11:30', 'Kedah'),
(57, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-03 15:15:54', 'Pahang'),
(58, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-03 15:16:17', 'Pahang'),
(59, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-03 15:21:08', 'Perak'),
(60, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-03 15:21:29', 'Perak'),
(61, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-03 15:33:00', 'Negeri Sembilan'),
(62, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-03 15:33:23', 'Negeri Sembilan'),
(63, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-03 15:37:16', 'Perlis'),
(64, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-03 15:37:42', 'Perlis'),
(65, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-03 15:49:55', 'Sabah'),
(66, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-03 15:50:15', 'Sabah'),
(67, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-03 16:43:13', 'Malacca'),
(68, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-03 16:43:42', 'Malacca'),
(69, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-03-03 17:04:21', 'Malacca'),
(70, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-03-08 10:39:32', 'Malacca'),
(71, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-04 12:14:13', 'Malacca'),
(72, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-04 12:15:19', 'Malacca'),
(73, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-06 16:01:53', 'Johor'),
(74, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-06 16:06:31', 'Johor'),
(75, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-06 16:40:14', 'Johor'),
(76, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-06 16:54:54', 'Johor'),
(77, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 12:36:06', 'Johor'),
(78, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-11 13:54:23', 'Johor'),
(79, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 13:54:36', 'Johor'),
(80, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-11 13:55:11', 'Johor'),
(81, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 13:56:34', 'Johor'),
(82, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-11 13:58:38', 'Johor'),
(83, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 13:58:50', 'Johor'),
(84, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-11 14:00:44', 'Johor'),
(85, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 14:00:57', 'Johor'),
(86, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 14:02:28', 'Johor'),
(87, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-11 14:03:28', 'Johor'),
(88, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 14:03:37', 'Johor'),
(89, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-11 14:06:42', 'Johor'),
(90, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 14:06:50', 'Johor'),
(91, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-11 14:07:56', 'Johor'),
(92, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-11 16:04:32', 'Johor'),
(93, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-11 17:25:53', 'Johor'),
(94, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-12 09:54:59', 'Johor'),
(95, '::1', 'Maryam', 'Login into WebGIS Myetapp', '2017-04-12 10:13:56', 'Johor'),
(96, '::1', 'Maryam', 'Logout from WebGIS Myetapp', '2017-04-12 15:24:27', 'Johor');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `state` varchar(30) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `zoom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`state`, `latitude`, `longitude`, `zoom`) VALUES
('Johor', '103.750291', '1.543435', 12),
('Kedah', '100.426867', '6.132636', 13),
('Kelantan', '102.028225', '5.277563', 9),
('Malacca', '102.235027', '2.229121', 13),
('Negeri Sembilan', '101.985175', '2.709879', 13),
('Pahang', '103.309757', '3.816595', 13),
('Penang', '100.400036', '5.365909', 9),
('Perak', '101.107603', '4.593495', 13),
('Perlis', '100.210531', '6.436170', 14),
('Sabah', '116.106362', '5.949478', 13),
('Sarawak', '', '', 9),
('Selangor', '101.49724', '3.272281', 9),
('Terengganu', '102.950159', '4.929182', 9),
('WP Kuala Lumpur', '101.700144', '3.138595', 12),
('WP Labuan', '115.214282', '5.313285', 9),
('WP Putrajaya', '101.692647', '2.932084', 9);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `name` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(30) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `address` varchar(300) NOT NULL,
  `category` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `register_date` datetime NOT NULL,
  `Last_time_seen` datetime NOT NULL,
  `profile_pic` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`name`, `username`, `email`, `password`, `phone_no`, `state`, `address`, `category`, `status`, `register_date`, `Last_time_seen`, `profile_pic`) VALUES
('', 'Maryam', 'maryamrufaida.dzulkefli@gmail.com', '123', '', 'Johor', '', 'Staff', 'Active', '2016-12-07 15:16:09', '2017-04-12 10:27:33', 'Maryam.jpg'),
('', 'Raafat2017', 'biomedical.engineer.usa@gmail.com', '12345678', '', '', '', 'Staff', 'Not Active', '2016-12-09 21:32:37', '2016-12-16 16:37:34', ''),
('Ahmed', 'sunrise', 'sunrise_alsameraai@yahoo.com', 'asd', '0173890490', 'WP Kuala Lumpur', 'Block B-07-02, Jalan Ukay Bistaria', 'Admin', 'Active', '0000-00-00 00:00:00', '2017-05-02 17:02:02', 'sunrise.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `no_login_per_day`
--

CREATE TABLE `no_login_per_day` (
  `day` varchar(10) NOT NULL,
  `no` int(11) NOT NULL,
  `month` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_login_per_day`
--

INSERT INTO `no_login_per_day` (`day`, `no`, `month`) VALUES
('01', 0, '05'),
('02', 0, '05'),
('03', 0, '05'),
('04', 0, '05'),
('05', 0, '05'),
('06', 0, '05'),
('07', 0, '05'),
('08', 0, '05'),
('09', 0, '05'),
('10', 0, '05'),
('11', 0, '05'),
('12', 0, '05'),
('13', 0, '05'),
('14', 0, '05'),
('15', 0, '05'),
('16', 0, '05'),
('17', 0, '05'),
('18', 0, '05'),
('19', 0, '05'),
('20', 0, '05'),
('21', 0, '05'),
('22', 0, '05'),
('23', 0, '05'),
('24', 0, '05'),
('25', 0, '05'),
('26', 0, '05'),
('27', 0, '05'),
('28', 0, '05'),
('29', 0, '05'),
('30', 0, '05'),
('31', 0, '05');

-- --------------------------------------------------------

--
-- Table structure for table `no_login_per_month`
--

CREATE TABLE `no_login_per_month` (
  `month` varchar(10) NOT NULL,
  `no` int(11) NOT NULL,
  `year` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_login_per_month`
--

INSERT INTO `no_login_per_month` (`month`, `no`, `year`) VALUES
('01', 1, 2017),
('02', 4, 2017),
('03', 22, 2017),
('04', 14, 2017),
('05', 0, 2017),
('06', 0, 2017),
('07', 0, 2017),
('08', 0, 2017),
('09', 0, 2017),
('10', 0, 2017),
('11', 0, 2017),
('12', 0, 2017);

-- --------------------------------------------------------

--
-- Table structure for table `no_login_per_state`
--

CREATE TABLE `no_login_per_state` (
  `id` int(11) NOT NULL,
  `state` varchar(50) NOT NULL,
  `month` varchar(12) NOT NULL,
  `year` varchar(12) NOT NULL,
  `no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_login_per_state`
--

INSERT INTO `no_login_per_state` (`id`, `state`, `month`, `year`, `no`) VALUES
(2, 'Johor', '3', '2017', 5),
(3, 'Kedah', '3', '2017', 4),
(4, 'Malacca', '3', '2017', 6),
(5, 'Pahang', '3', '2017', 1),
(6, 'Perak', '3', '2017', 1),
(7, 'Negeri Sembilan', '3', '2017', 1),
(8, 'Perlis', '3', '2017', 1),
(9, 'Sabah', '3', '2017', 1),
(10, 'Malacca', '4', '2017', 1),
(11, 'Johor', '4', '2017', 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audit`
--
ALTER TABLE `audit`
  ADD PRIMARY KEY (`audit_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`state`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `no_login_per_day`
--
ALTER TABLE `no_login_per_day`
  ADD PRIMARY KEY (`day`);

--
-- Indexes for table `no_login_per_month`
--
ALTER TABLE `no_login_per_month`
  ADD PRIMARY KEY (`month`);

--
-- Indexes for table `no_login_per_state`
--
ALTER TABLE `no_login_per_state`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audit`
--
ALTER TABLE `audit`
  MODIFY `audit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `no_login_per_state`
--
ALTER TABLE `no_login_per_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
