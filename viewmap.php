<?php

include('sql_server_db.php');

//$project_id_view = $_GET['upi'];
//$noFile = $_GET['nofail'];
//$kod = $_GET['kod'];

$project_id_view = '0101080000008254';
$noFile = 'P.T.TM 5/60 BAB 373';
$kod = '05';

//$project_id_view = '0108020000002153';

if($kod == '02' || $kod == '03')
{	
	$sqlView = "SELECT * FROM jkptg.dbo.PERMOHONAN  WHERE NO_JKPTG = '".$noFile."'"; 
	$paramsView = array();
	$optionsView =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmtView = sqlsrv_query($conn, $sqlView , $paramsView, $optionsView );

	# Get value from the $sql - table name & column/info to display
	$rowView = sqlsrv_fetch_array( $stmtView, SQLSRV_FETCH_ASSOC);
	$row_count = sqlsrv_num_rows( $stmtView );

    if($row_count.length != 0)
    {			
  		$lat = $rowView['LONG'];
  		$long = $rowView['LAT'];
  		$zoom = 15;
   }

  else
  {
	  echo "<script> alert('Tiada maklumat GIS'); </script>";
		$lat = 102.676;
		$long = 4.022;
		$zoom = 7;
  }
}

if($kod == '04')
{	
	$sqlView = "SELECT * FROM jkptg.dbo.QT  WHERE UPI = '".$project_id_view."' AND NO_JKPTG = '".$noFile."'"; 
	$paramsView = array();
	$optionsView =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmtView = sqlsrv_query($conn, $sqlView , $paramsView, $optionsView );

	# Get value from the $sql - table name & column/info to display
	$rowView = sqlsrv_fetch_array( $stmtView, SQLSRV_FETCH_ASSOC);
	$row_count = sqlsrv_num_rows( $stmtView );

    if($row_count.length != 0)
    {			
  		$lat = $rowView['LONG'];
  		$long = $rowView['LAT'];
  		$zoom = 15;
   }

  else
  {
	  echo "<script> alert('Tiada maklumat GIS'); </script>";
		$lat = 102.676;
		$long = 4.022;
		$zoom = 7;
  }
}

if($kod == '05')
{	
	$sqlView = "SELECT * FROM jkptg.dbo.FT  WHERE UPI = '".$project_id_view."' AND NO_JKPTG = '".$noFile."'"; 
	$paramsView = array();
	$optionsView =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmtView = sqlsrv_query($conn, $sqlView , $paramsView, $optionsView );

	# Get value from the $sql - table name & column/info to display
	$rowView = sqlsrv_fetch_array( $stmtView, SQLSRV_FETCH_ASSOC);
	$row_count = sqlsrv_num_rows( $stmtView );

    if($row_count.length != 0)
    {			
  		$lat = $rowView['LONG'];
  		$long = $rowView['LAT'];
  		$zoom = 15;
   }

  else
  {
	  echo "<script> alert('Tiada maklumat GIS'); </script>";
		$lat = 102.676;
		$long = 4.022;
		$zoom = 7;
  }
}

if($kod == '06')
{	
	$sqlView = "SELECT * FROM jkptg.dbo.RIZAB  WHERE UPI = '".$project_id_view."' AND NO_JKPTG = '".$noFile."'"; 
	$paramsView = array();
	$optionsView =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmtView = sqlsrv_query($conn, $sqlView , $paramsView, $optionsView );

	# Get value from the $sql - table name & column/info to display
	$rowView = sqlsrv_fetch_array( $stmtView, SQLSRV_FETCH_ASSOC);
	$row_count = sqlsrv_num_rows( $stmtView );

    if($row_count.length != 0)
    {			
  		$lat = $rowView['LONG'];
  		$long = $rowView['LAT'];
  		$zoom = 15;
   }

  else
  {
	  echo "<script> alert('Tiada maklumat GIS'); </script>";
		$lat = 102.676;
		$long = 4.022;
		$zoom = 7;
  }
}





?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
        <title>Display table</title>
        
        <link rel="stylesheet" href="https://js.arcgis.com/4.1/esri/css/main.css">
        <style>
            html, body, #viewDiv 
            {
                padding: 0;
                margin: 0;
                height: 100%;
                width: 100%;
            }
        </style>
        <link rel="stylesheet" href="https://js.arcgis.com/4.1/esri/css/main.css">
        <script src="https://js.arcgis.com/4.1/"></script>
        
        <script>
        require([
            "esri/config",
            "esri/Map",
            "esri/views/SceneView",
			"esri/layers/FeatureLayer",
            "esri/widgets/Locate",
            "esri/widgets/Home",
            "dojo/domReady!"
        ], function(esriConfig, Map, SceneView, FeatureLayer, Locate, Home) {

            esriConfig.request.corsEnabledServers.push("a.tile.openstreetmap.org",
                "b.tile.openstreetmap.org", "c.tile.openstreetmap.org");

            var map = new Map({
                basemap: "osm",
                ground: "world-elevation"
            });
 

            var view = new SceneView({
                map: map,
                container: "viewDiv",
                center: [<?php echo $lat; ?>, <?php echo $long; ?>],
                zoom: <?php echo $zoom; ?>
            });
			
		      /*************************************************************
       * The PopupTemplate content is the text that appears inside the
       * popup. {fieldName} can be used to reference the value of an
       * attribute of the selected feature. HTML elements can be used
       * to provide structure and styles within the content. The
       * fieldInfos property is an array of objects (each object representing
       * a field) that is use to format number fields and customize field
       * aliases in the popup and legend.
       **************************************************************/

      // autocasts as new PopupTemplate()
      var template = {
        title: "No Fail JKPTG: {NO_JKPTG}",
        content: "<ul><li>Kegunaan: {GUNATANAH}</li>" +
          "<ul><li>Negeri: {NAMA_NEGERI}</li>" +
		  "<ul><li>Daeraah: {NAMA_DAERAH}</li>" +
		  "<ul><li>Mukim: {NAMA_MUKIM}</li>" +
          "<li>No. Lot: {NO_LOT}</li>" +
          "<li>Kementerian: {KEMENTERIAN} </li></ul>",
        fieldInfos: [{
          fieldName: "OBJECT_ID",
          format: {
            digitSeparator: true,
            places: 0
          }
        }]
      };
	
			
		 /********************
         * Add feature layer
         ********************/


	  // Reference the popupTemplate instance in the
      // popupTemplate property of FeatureLayer
	  if(<?php echo $kod; ?> == '02' || <?php echo $kod; ?> == '03'){ // permohonan 
		  var url = "http://www.gis.myetapp.gov.my/arcgis/rest/services/JKPTG/charting/MapServer/0";
	  }else if(<?php echo $kod; ?> == '04'){ // QT
		  var url = "http://www.gis.myetapp.gov.my/arcgis/rest/services/JKPTG/charting/MapServer/1";
	  }else if(<?php echo $kod; ?> == '05'){ // FT
		  var url = "http://www.gis.myetapp.gov.my/arcgis/rest/services/JKPTG/charting/MapServer/2";
	  }else{
		  alert("Tiada maklumat GIS bagi kod = " + <?php echo $kod; ?> );
	  }
      var featureLayer = new FeatureLayer({
        url: url,
        outFields: ["*"],
        popupTemplate: template
      });

        map.add(featureLayer);

            var homeBtn = new Home({
                view: view
            });
            homeBtn.startup();

            view.ui.add(homeBtn, {
                position: "top-right",
                index: 0
            });
        });
        </script>
    </head>
    <body>
        <div id="viewDiv">
    </body>
</html>
