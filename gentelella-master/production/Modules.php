<?php
    $action = null;
	$category = $_SESSION['category'];

	
    if (empty($_GET['action']))
	{
			$action = "main";
	}
    else
	{
   		$action=$_GET['action'];
	}
	if (isset($action) and $action=="main") 
	{
		if($category == "Super Admin" || $category == "Admin"){
			require("Dashboard.php");
		}	
		else{
			require("WebGIS.php");
		}
		
    }
	if (isset($action) and $action=="WebGIS") 
	{
		require("WebGIS.php");		
    }
    if (isset($action) and $action=="Notification") 
	{
		require("Notification.php");		
    }
    if (isset($action) and $action=="AdminList") 
	{
		require("AdminList.php");		
    }
    if (isset($action) and $action=="AdminDetail") 
	{
		require("AdminDetail.php");		
    }
    if (isset($action) and $action=="StaffList") 
	{
		require("StaffList.php");		
    }
    if (isset($action) and $action=="StaffDetail") 
	{
		require("StaffDetail.php");		
    }
    if (isset($action) and $action=="RegisterStaff") 
	{
		require("RegisterStaff.php");		
    }
    if (isset($action) and $action=="UserProfile") 
	{
		require("UserProfile.php");		
    }
    if (isset($action) and $action=="WebGIS-temp") 
	{
		require("WebGIS-temp.php");		
    }
    if (isset($action) and $action=="Analysis") 
	{
		require("Analysis.php");		
    }
?>

