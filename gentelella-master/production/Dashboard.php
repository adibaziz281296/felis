    <!-- page content -->
    	<div class="right_col" role="main">
    	  <div class="">
    		  <div class="row top_tiles" >
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats3" style="color: white;">
                <div class="icon"><i class="fa fa-users"></i></div>
                <div class="count"><?php echo $count; ?></div>
                <h3>Online Users</h3>
    	         <!-- <p>Lorem ipsum psdea itgum rixt.</p> -->
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats2" style="color: white;">
                <div class="icon"><i class="fa fa-calendar"></i></div>
                <div class="count"><?php echo $cur_month_log_no; ?></div>
                <h3><?php echo $month; ?> Sign in</h3>
                  <!-- <p>For the current month.</p> -->
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats1" style="color: white;">
                <div class="icon"><i class="fa fa-address-book"></i></div>
                <div class="count"><?php echo $today_visit; ?></div>
                <h3>Today's Visits</h3>
                <!-- <p>Lorem ipsum psdea itgum rixt.</p> -->
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats4" style="color: white;">
                <div class="icon"><i class="fa fa-pencil-square-o"></i></div>
                <div class="count"><?php echo $total_sign_up; ?></div>
                <h3>Total Sign up</h3>
                <!-- <p>Lorem ipsum psdea itgum rixt.</p> -->
              </div>
            </div>
          </div>

          <div class="clearfix"></div>
          <!-- Line graph -->
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Visitor by months for year <?php echo $year; ?></h2>
                  <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id="lineChart" style="width:100%; height:265px;"></div>
                </div>
                <!-- <div class="x_content">
                  <canvas id="lineChart"></canvas>
                </div> -->
              </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Visitor by days for month <?php echo $month; ?></h2>
                  <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id="mybarChart" style="width:100%; height:265px;"></div>
                </div>
                <!-- <div class="x_content">
                  <canvas id="mybarChart"></canvas>
                </div> -->
              </div>
            </div>

          <!-- Pie graph -->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <?php
                    $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                    if (!isset($month_selected)) {
                      $view_month = $month;
                    }else{
                      $view_month = $months[$month_selected-1];
                    }
                  ?>
                  <h2>Visitor access by state JKPTG</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li style="padding-right: 50px; margin-top: 3px;">
                      <div>
                        <form method="POST" action="index.php?action=main">
                          <label for="months" style="margin-left: 1em;">Month</label>
                          <select name="txtmonth" class="has-padding">
                          <?php
                            $month_no = 0;
                            foreach($months as $item){
                            $month_no++;
                            ?>
                            <option value="<?php echo $month_no; ?>"><?php echo $item; ?></option>
                            <?php
                            }
                          ?>
                          </select>
                          <input name="months" type="submit" value="Submit">
                        </form>
                      </div>
                    </li>
                    <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li> -->
                  </ul>
                  <div class="clearfix"></div>
                </div>
                
                <div class="x_content">

                  <div id="echart_pie" style="height:400px;"></div>

                </div>
              </div>
            </div>
          
          <?php
          if (strcmp($_SESSION['category'], "Super Admin") == 0 || strcmp($_SESSION['category'], "Admin") == 0) {
          ?>
          <!-- Plus Table Design -->

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Audit Trail</h2>
                  <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                    <thead>
                      <tr>
                                    <!-- <th><input type="checkbox" id="check-all" class="flat"></th> -->
                        <th>No.</th>
                        <th>IP Address</th>
                        <th>User Name</th>
                        <th>Activity</th>
                        <th>Date/Time</th>
                        <th>Authority</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      // include('../../connect_db.php');
                      // include('../../sql_server_db.php');
                      $stmt = "SELECT ip_address, username, activity, CONVERT(nvarchar(50),[access_date_time],120) as [access_date_time], authority FROM audit";
                      $result = sqlsrv_query( $conn, $stmt );
                      if( $result === false) {
                        die( print_r( sqlsrv_errors(), true) );
                        echo "<script> console.log('PHP: wrong!!!');</script>";
                      }
                      $s=0;
                      while($row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )){
                        $s++;
                        $usr_ip = $row['ip_address'];
                        $username = $row['username'];
                        $activity = $row['activity'];
                        $access_date_time = $row['access_date_time'];   
                        $authority = $row['authority'];
                      ?>
                      <tr>
                        <!-- <td><input type="checkbox" class="flat" name="table_records"></td> -->
                        <td align="center"><?php echo $s;?></td>
                        <td align="center"><?php echo $usr_ip;?></td>
                        <td align="center"><?php echo $username;?></td>
                        <td align="center"><?php echo $activity;?></td>
                        <td align="center"><?php echo $access_date_time;?></td>
                        <td align="center"><?php echo $authority;?></td>
                      </tr>
                      <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <?php
            }
            ?>
          </div>
        </div>
    	</div>
    <!-- /page content -->
	