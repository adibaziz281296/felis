<?php
// Initialize the session.
// If you are using session_name("something"), don't forget it now!
session_start();
include('../../sql_server_db.php');
$username = $_SESSION['username'];
$state = $_SESSION['state'];
# Get user IP address
$usr_ip = $_SERVER["REMOTE_ADDR"];

$stmts="INSERT INTO audit(ip_address,username,activity,access_date_time,authority) VALUES('$usr_ip','$username','Logout from WebGIS Myetapp',GETDATE(),'$state')";
$sql=sqlsrv_query( $conn, $stmts);
if( $sql === false) {
	die( print_r( sqlsrv_errors(), true) );
	echo "<script> console.log('PHP: wrong!!!');</script>";
}

// Unset all of the session variables.
$_SESSION = array();

// If it's desired to kill the session, also delete the session cookie.
// Note: This will destroy the session, and not just the session data!
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}
// Finally, destroy the session.
session_destroy();
// header("location:http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/index.php");
header("location:https://www.gis.myetapp.gov.my/myetappgis/Login/index.html");
?>
