<!-- page content -->
	<div class="right_col" role="main">
		<div class="">

			<div style="padding-top:25px;"></div> <!-- This line is ONLY for maaking space from the top! -->
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-12">
						<div class="well well-sm">
							<div class="row">
								<div class="col-sm-6 col-md-2">
									<img src="images/<?php echo $profile_pic; ?>" class="img-rounded img-responsive" draggable="false" />
									<!-- <form action="index.php?action=UserProfile" class="dropzone">
									</form> -->
								</div>
								<div class="col-sm-6 col-md-4">
									<h4><?php echo $name;?></h4>
									<?php
						            	if(strcmp($category, "Guest")!=0)
						            	{
						                ?>
									<p>
										<!-- <i class="glyphicon glyphicon-map-marker glyphicon"></i> <?php //echo $address; ?><br/> -->
										<i class="glyphicon glyphicon-envelope"></i> <?php echo $email; ?><br/>
										<!-- <i class="glyphicon glyphicon-globe"></i> <a href="http://google.com/">www.google.com</a> <br/> -->
										<i class="glyphicon glyphicon-phone"></i> <?php echo $phone; ?><br/>
										<?php
						            	}
						                ?>
										<i class="glyphicon glyphicon-user"></i> <?php echo $category; ?>
									</p>
								</div>
								<!-- <div class="col-sm-6 col-md-6">
									<span class="label label-info pull-right">Level: 1</span>
									<h4>Biography</h4>
									<p>Here goes the biography...</p>
								</div> -->
								<div class="col-sm-6 col-md-6">
						            <div class="btn-group pull-right">
						            	<?php
						            	if(strcmp($category, "Guest")!=0)
						            	{
						                ?>
						                <a class="btn  btn-green dropdown-toggle " data-toggle="dropdown" href="#" >
						                     
						                    <span class="fa fa-cog icon-white"></span> Action <span class="caret"></span>
						                </a>
						                <ul class="dropdown-menu">
						                    <li><form action="index.php?action=UserProfile" method="POST">
						                    <button type="submit" class="btn btn-block" name="Modify"><i class="fa fa-wrench left"></i> Modify</button>
											</form></li>
											<li><form action="index.php?action=UserProfile" method="POST">
						                    <button type="submit" class="btn btn-block" name="ChangePassword"><i class="fa fa-key left"></i> Password</button>
											</form></li>
						                    <!-- <li><a href="#"><span class="icon-trash"></span> Delete</a></li> -->
						                </ul>
						                <?php
						            	}
						                ?>
						            </div>
						        </div>
							</div>
						</div>
					</div>
				</div>
				<?php
				if (isset($_REQUEST['Modify']))
				{ 
				?>
				<div class="row" id="main">
			        <div class="col-md-4 well" id="leftPanel">
			            <div class="row">
			                <div class="col-md-12">
			                	<div>
			        				<div class="main">
										<h3>Profile Picture</h3>
										<hr>
										<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
										<div id="image_preview"><img id="previewing" src="images/<?php echo $profile_pic; ?>" /></div>
										<hr id="line">
										<div id="selectImage">
										<label>Select Your Picture</label><br/>
										<input type="file" name="file" id="file" required /></br>
										<input type="submit" value="Upload" class="btn  btn-green" />
										</div>
										</form>
									</div>
									<h4 id='loading' >loading..</h4>
									<div id="message"></div>
			        			</div>
			        		</div>
			            </div>
			        </div>
			        <div class="col-md-8 well" id="rightPanel">
			            <div class="row">
						    <div class="col-md-12">
						    	<form action="ModifyProfile.php" method="POST" id="form1">
									<h2>Edit your profile.</h2>
									<hr class="colorgraph">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
						                        <label>Name</label>
						                        <input type="text" name="name" id="name" class="form-control input-lg" tabindex="1" value="<?php echo $name; ?>" REQUIRED>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Phone Number</label>
												<input type="text" name="phone_no" id="phone_no" class="form-control input-lg" value="<?php echo $phone; ?>" tabindex="2" REQUIRED>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Email Address</label>
										<input type="email" name="email" id="email" class="form-control input-lg" value="<?php echo $email; ?>" tabindex="4" DISABLED>
									</div>
									<!-- <div class="form-group">
										<label>Unit / Bahagian</label>
										<textarea type="text" name="address" id="address" class="form-control input-lg" tabindex="4" REQUIRED><?php //echo $address; ?></textarea>
									</div> -->
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Category</label>
												<input type="text" name="category" id="category" class="form-control input-lg" placeholder="<?php echo $_SESSION['category']; ?>" tabindex="5" DISABLED>
											</div>
										</div>
										<!--div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>JKPTG State</label>
												<1?php echo $state; ?>
												<!--select name="txtstate" class="form-control input-lg" tabindex="6" DISABLED>
													<option value="<1?php echo $state; ?>"><1?php echo $state; ?></option>
													<1?php
						                            $state1 = array("Johor", "Kedah", "Kelantan", "Melaka", "Negeri Sembilan", "Pahang", "Pulau Pinang", "Perak", "Perlis", "Sabah", "Sarawak", "Selangor", "Terengganu", "Kuala Lumpur", "Labuan", "Putrajaya");
						                            foreach($state1 as $item){
						                            	if (strcmp($item, $state)==0) {
						            						
						                            	}
						                            	else
						                            	{
							                            ?>
								                            <option value="<1?php echo $item; ?>"><1?php echo $item; ?></option>
								                        <1?php
						                            	}
						                        	}
						                            ?>
												</select-->
											<!--/div>
										</div-->
									</div>
									<!-- <div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5">
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
											<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password"> 
											</div>
										</div>
									</div> -->
									<hr class="colorgraph">
									<div class="row">
										<div class="col-xs-12 col-md-6"></div>
										<div class="col-xs-12 col-md-6"><a onclick="div_show()" class="btn  btn-green pull-right">Save</a><!-- <a href="#" class="btn  btn-green pull-right">Save</a> --></div>
									</div>
								</form>
							</div>
						</div>
						</div>
			        </div>
			        <?php
				    }else{
				    	if (isset($_REQUEST['ChangePassword']))
						{
							?>
							<div class="row" id="main">
			        			<div class="col-md-8 well" id="rightPanel">
			        				<div class="row">
									    <div class="col-md-12">
									    	<form action="ChangePassword.php" method="POST" id="form2">
												<h2>Change Password</h2>
												<hr class="colorgraph">
												<div class="form-group">
													<label>New Password</label>
													<input type="password" name="new_password" id="new_password" class="form-control input-lg" tabindex="1" REQUIRED>
												</div>
												<div class="form-group">
													<label>Confirm New Password</label>
													<input type="password" name="cnew_password" id="cnew_password" class="form-control input-lg" tabindex="2" REQUIRED>
												</div>
												<hr class="colorgraph">
												<div class="row">
													<div class="col-xs-12 col-md-6"></div>
													<div class="col-xs-12 col-md-6"><a onclick="div_show1()" class="btn  btn-green pull-right">Change</a><!-- <a href="#" class="btn  btn-green pull-right">Save</a> --></div>
												</div>
											</form>
										</div>
									</div>
			        			</div>
			        		</div>
							<?php
						}
				    }
				    ?>
			     </div>
			</div>

		</div>
	</div>
<!-- /page content -->

	<div id="abc" class="modal fade" >
		
		<div class="modal-dialog modal-sm" style="margin-top: 150px;">
		
		<div class="logout-message">
			<form action="#" id="form" method="post" name="form">
				<i class="fa fa-lock fa-4x text-green" style="padding-top: 20px;"></i> 
				<h2>Insert Password</h2>
				<hr>
				<input id="password" name="password" placeholder="password" type="password" style="color: black;"><br><br>
				<button type="button" class="btn btn-green" onclick ="div_hide()">Close</button>
				<a href="javascript:%20check_empty()" class="btn btn-green">Submit</a>
			</form>
		</div>
		</div>
		
	</div>

	<div id="abcd" class="modal fade" >
		
		<div class="modal-dialog modal-sm" style="margin-top: 150px;">
		
		<div class="logout-message">
			<form action="#" id="form0" method="post" name="form0">
				<i class="fa fa-lock fa-4x text-green" style="padding-top: 20px;"></i> 
				<h2>Insert Old Password</h2>
				<hr>
				<input id="password1" name="password1" placeholder="password" type="password" style="color: black;"><br><br>
				<button type="button" class="btn btn-green" onclick ="div_hide1()">Close</button>
				<a href="javascript:%20check_empty1()" class="btn btn-green">Submit</a>
			</form>
		</div>
		</div>
		
	</div>