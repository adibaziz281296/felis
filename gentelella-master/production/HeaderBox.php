<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>       

	   <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <!--div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div-->
			  <div class="nav toggle"><img style="margin-top: -15px; margin-left: 10px; float: left; width: 175px;" src="images/logo-jkptg-ev.png" alt="Logo"></div>
              <div class="nav toggle" style="margin-left: 120px;">
                <h3 style="margin-top: 0px; width: 700px; float: center; font-family: Copperplate Gothic Light;">Federal Land Information System (FeLIS)</h3>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/<?php echo $profile_pic; ?>" alt=""><img src="images/state/<?php echo $state; ?>.png" alt=""><b><?php echo $username; ?></b> JKPTG <?php echo $state; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="index.php?action=UserProfile"><i class="fa fa-address-card pull-right"></i> Profile</a></li>
                    <!--li>
                      <a href="javascript:;">
                        <i class="fa fa-cog pull-right"></i> Settings</a>
                    </li>
                    <li><a href="javascript:;"><i class="fa fa-info-circle pull-right"></i> Help</a></li-->
                    <li><a data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </li>
                  </ul>
                </li>
                <?php
                if (strcmp($_SESSION['category'], "Super Admin")==0){
                  // include('../../connect_db.php');
                  // include('../../sql_server_db.php');
                  $sta = "Not Active";
                  $stmt20 = "SELECT username, CONVERT(nvarchar(50),[register_date],120) as [register_date] FROM login WHERE status='$sta'";
                  $result = sqlsrv_query( $conn, $stmt20 );
                  if( $result === false) {
                    die( print_r( sqlsrv_errors(), true) );
                    echo "<script> console.log('PHP: wrong!!!');</script>";
                  }
                  
                  $res20 = sqlsrv_query($conn, $stmt20, array(), array( "Scrollable" => 'static' ));
                  if( $res20 === false) {
                    die( print_r( sqlsrv_errors(), true) );
                    echo "<script> console.log('PHP: wrong!!!');</script>";
                  }
                  $rows = sqlsrv_num_rows($res20);
                ?>
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false" id="read-notifications">
                    <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green" id="alerts"></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu" >
                    <ul style="overflow-y:scroll; height:125px;">
                  <?php
                    function time_elapsed_string($datetime, $full = false) {
                      date_default_timezone_set('Asia/Kuala_Lumpur');
                      $now = new DateTime;
                      $ago = new DateTime($datetime);
                      $diff = $now->diff($ago);

                      $diff->w = floor($diff->d / 7);
                      $diff->d -= $diff->w * 7;

                      $string = array(
                          'y' => 'year',
                          'm' => 'month',
                          'w' => 'week',
                          'd' => 'day',
                          'h' => 'hour',
                          'i' => 'minute',
                          's' => 'second',
                      );
                      foreach ($string as $k => &$v) {
                          if ($diff->$k) {
                              $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                          } else {
                              unset($string[$k]);
                          }
                      }

                      if (!$full) $string = array_slice($string, 0, 1);
                      return $string ? implode(', ', $string) . ' ago' : 'just now';
                    }
                  
                    while($row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )){
                      $user = $row['username'];
                      $register_date = $row['register_date'];
                      $timeago = time_elapsed_string($register_date);

                      
                    ?>
                    <li>
                      <a href="index.php?action=Notification">
                        <span class="image"><img src="images/user.png" alt="Profile Image" /></span>
                        <span>
                          <span><?php echo $user; ?></span>
                          <span class="time"><?php echo $timeago; ?></span>
                        </span>
                        <span class="message">
                          Has requested to join into the system...
                        </span>
                      </a>
                    </li>
                    <?php
                    }
                    ?>
                    </ul>
                    <li>
                      <div class="text-center">
                        <a href="index.php?action=Notification">
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
                <?php
                }
                ?>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- Small modal -->
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 150px;">
          <div class="modal-dialog modal-sm">
            <div class=" logout-message">
              <div class="modal-header" align="center">
                <img src="images/<?php echo $profile_pic; ?>" class="img-circle" width="100" height="100"/>
              </div>
              <div class="modal-body">
                <h3>
                  <i class="fa fa-sign-out text-green"></i> Ready to go?
                </h3>
                <p>Select "Logout" below if you are ready<br> to end your current session.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Close</button>
                <a href="logout.php" class="btn btn-green">
                  <strong>Logout</strong>
                </a>
              </div>
            </div>
          </div>
        </div>        
        <!-- /modals -->

        <!-- Small modal -->
        <div class="modal bs-example-modal-sm1" tabindex="-1" role="dialog" aria-hidden="true">
          <!-- <div class="modal-dialog modal-sm"> -->
            <!-- <div class=" logout-message">
              <div class="modal-header" align="center">
                <img src="images/img.jpg" class="img-circle" width="100" height="100"/>
              </div>
              <div class="modal-body">
                <h3>
                  <i class="fa fa-sign-out text-green"></i> Ready to go?
                </h3>
                <p>Select "Logout" below if you are ready<br> to end your current session.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Close</button>
                <a href="logout.php" class="btn btn-green">
                  <strong>Logout</strong>
                </a>
              </div>
            </div> -->
          <!-- </div> -->
        </div>        
        <!-- /modals -->