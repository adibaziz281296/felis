  <?php
    // include('../../sql_server_db.php');

    if (strcmp($_SESSION['category'], "Super Admin")==0 || strcmp($_SESSION['category'], "Admin")==0 || strcmp($_SESSION['category'], "Guest")==0) {
      if (isset($_REQUEST['special']))
      {
        $kementerian_selected = $_POST['kementerian'];
        $negeri_selected = $_POST['txtstate'];
        $date_selected = $_POST['date'];
        $type_selected = $_POST['type'];

        if ($kementerian_selected == '-1' && $negeri_selected == '-1' && $date_selected == '' && $type_selected == '-1') {
          $kes = 1;
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2";
          $sql_ft = "SELECT * FROM FT";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2";
        }elseif ($kementerian_selected != '-1' && $negeri_selected == '-1' && $date_selected == '' && $type_selected == '-1') {
          $kes = 2;
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
          $sql_ft = "SELECT * FROM FT WHERE KEMENTERIAN = '$kementerian_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
        }elseif ($kementerian_selected == '-1' && $negeri_selected != '-1' && $date_selected == '' && $type_selected == '-1') {
          $kes = 3;
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
          $sql_ft = "SELECT * FROM FT WHERE NAMA_NEGERI = '$negeri_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
        }elseif ($kementerian_selected != '-1' && $negeri_selected != '-1' && $date_selected == '' && $type_selected == '-1') {
          $kes = 4;
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_ft = "SELECT * FROM FT WHERE KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
        }elseif ($kementerian_selected == '-1' && $negeri_selected == '-1' && $date_selected != '' && $type_selected != '-1') {
          $kes = 5;
          if ($type_selected == 'DAY') {
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND TRH_POHON = '$date_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND TRH_DAF_HMLK = '$date_selected'";
          $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK = '$date_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND TRH_WARTA = '$date_selected'";
          }elseif ($type_selected == 'WEEK') {
            $time  = strtotime($date_selected);
            $start = date('Y-m-d', strtotime('Last Monday', $time));
            $end = date('Y-m-d', strtotime('Next Sunday', $time));
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE TRH_POHON >= '$start' AND TRH_POHON < '$end' AND STATUS_TERKINI = 2";
            $sql_qt = "SELECT * FROM QT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND STATUS_TERKINI = 2";
            $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE TRH_WARTA >= '$start' AND TRH_WARTA < '$end' AND STATUS_TERKINI = 2";
          }elseif ($type_selected == 'MONTH') {
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(m, TRH_POHON) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_POHON) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected'))";
            $sql_rizab = "SELECT * FROM RIZAB WHERE DATEPART(m, TRH_WARTA) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2";
          }elseif ($type_selected == 'YEAR') {
            $sql_permohonan = "  SELECT * FROM PERMOHONAN WHERE DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected'))";
            $sql_rezab = "SELECT * FROM RIZAB WHERE DATEPART(YEAR, TRH_WARTA) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2";
          }
        }elseif ($kementerian_selected != '-1' && $negeri_selected == '-1' && $date_selected != '' && $type_selected != '-1') {
          $kes = 6;
          if ($type_selected == 'DAY') {
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND TRH_POHON = '$date_selected' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND TRH_DAF_HMLK = '$date_selected' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK = '$date_selected' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND TRH_WARTA = '$date_selected' AND KEMENTERIAN = '$kementerian_selected'";
          }elseif ($type_selected == 'WEEK') {
            $time  = strtotime($date_selected);
            $start = date('Y-m-d', strtotime('Last Monday', $time));
            $end = date('Y-m-d', strtotime('Next Sunday', $time));
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE TRH_POHON >= '$start' AND TRH_POHON < '$end' AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
            $sql_qt = "SELECT * FROM QT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
            $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE TRH_WARTA >= '$start' AND TRH_WARTA < '$end' AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
          }elseif ($type_selected == 'MONTH') {
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(m, TRH_POHON) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_POHON) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND KEMENTERIAN = '$kementerian_selected'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE DATEPART(m, TRH_WARTA) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
          }
		  // Carian year
		  elseif ($type_selected == 'YEAR') {
            $sql_permohonan = "  SELECT * FROM PERMOHONAN WHERE DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND KEMENTERIAN = '$kementerian_selected'";
            $sql_rezab = "SELECT * FROM RIZAB WHERE DATEPART(YEAR, TRH_WARTA) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected'";
          }
        }elseif ($kementerian_selected == '-1' && $negeri_selected != '-1' && $date_selected != '' && $type_selected != '-1') {
          $kes = 7;
          if ($type_selected == 'DAY') {
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND TRH_POHON = '$date_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND TRH_DAF_HMLK = '$date_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK = '$date_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND TRH_WARTA = '$date_selected' AND NAMA_NEGERI = '$negeri_selected'";
          }elseif ($type_selected == 'WEEK') {
            $time  = strtotime($date_selected);
            $start = date('Y-m-d', strtotime('Last Monday', $time));
            $end = date('Y-m-d', strtotime('Next Sunday', $time));
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE TRH_POHON >= '$start' AND TRH_POHON < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
            $sql_qt = "SELECT * FROM QT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
            $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE TRH_WARTA >= '$start' AND TRH_WARTA < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
          }elseif ($type_selected == 'MONTH') {
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(m, TRH_POHON) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_POHON) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND NAMA_NEGERI = '$negeri_selected'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE DATEPART(m, TRH_WARTA) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
          }
		  // Carian Year
		  elseif ($type_selected == 'YEAR') {
            $sql_permohonan = "  SELECT * FROM PERMOHONAN WHERE DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND NAMA_NEGERI = '$negeri_selected'";
            $sql_rezab = "SELECT * FROM RIZAB WHERE DATEPART(YEAR, TRH_WARTA) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$negeri_selected'";
          }
        }elseif ($kementerian_selected != '-1' && $negeri_selected != '-1' && $date_selected != '' && $type_selected != '-1') {
          $kes = 8;
          if ($type_selected == 'DAY') {
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND TRH_POHON = '$date_selected' AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND TRH_DAF_HMLK = '$date_selected' AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK = '$date_selected' AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND TRH_WARTA = '$date_selected' AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          }elseif ($type_selected == 'WEEK') {
            $time  = strtotime($date_selected);
            $start = date('Y-m-d', strtotime('Last Monday', $time));
            $end = date('Y-m-d', strtotime('Next Sunday', $time));
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE TRH_POHON >= '$start' AND TRH_POHON < '$end' AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_qt = "SELECT * FROM QT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE TRH_WARTA >= '$start' AND TRH_WARTA < '$end' AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          }elseif ($type_selected == 'MONTH') {
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(m, TRH_POHON) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_POHON) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE DATEPART(m, TRH_WARTA) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          }
		  // Carian Year
		  elseif ($type_selected == 'YEAR') {
            $sql_permohonan = "  SELECT * FROM PERMOHONAN WHERE DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
            $sql_rezab = "SELECT * FROM RIZAB WHERE DATEPART(YEAR, TRH_WARTA) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND KEMENTERIAN = '$kementerian_selected' AND NAMA_NEGERI = '$negeri_selected'";
          }
        }
        // if ($_POST['type'] == 'DAY') {
        //   $date = $_POST['date'];
        //   $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE TRH_POHON = '$date' AND STATUS_TERKINI = 2";
        //   $sql_qt = "SELECT * FROM QT WHERE TRH_DAF_HMLK = '$date' AND STATUS_TERKINI = 2";
        //   $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK = '$date'";
        //   $sql_rizab = "SELECT * FROM RIZAB WHERE TRH_WARTA = '$date' AND STATUS_TERKINI = 2";
        // }else if ($_POST['type'] == 'WEEK') {
        //   $date = $_POST['date'];
        //   $time  = strtotime($date);
        //   $start = date('Y-m-d', strtotime('Last Monday', $time));
        //   $end = date('Y-m-d', strtotime('Next Sunday', $time));

        //   $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE TRH_POHON >= '$start' AND TRH_POHON < '$end' AND STATUS_TERKINI = 2";
        //   $sql_qt = "SELECT * FROM QT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND STATUS_TERKINI = 2";
        //   $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end'";
        //   $sql_rizab = "SELECT * FROM RIZAB WHERE TRH_WARTA >= '$start' AND TRH_WARTA < '$end' AND STATUS_TERKINI = 2";
        // }else if ($_POST['type'] == 'MONTH') {
        //   $date = $_POST['date'];
          
        //   $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(m, TRH_POHON) = DATEPART(m, DATEADD(m, 0, '$date')) AND DATEPART(yyyy, TRH_POHON) = DATEPART(yyyy, DATEADD(m, 0, '$date')) AND STATUS_TERKINI = 2";
        //   $sql_qt = "SELECT * FROM QT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date')) AND STATUS_TERKINI = 2";
        //   $sql_ft = "SELECT * FROM FT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date'))";
        //   $sql_rizab = "SELECT * FROM RIZAB WHERE DATEPART(m, TRH_WARTA) = DATEPART(m, DATEADD(m, 0, '$date')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(m, 0, '$date')) AND STATUS_TERKINI = 2";
        // }else{
        //   $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2";
        //   $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2";
        //   $sql_ft = "SELECT * FROM FT";
        //   $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2";
        // }  
      }else{
        $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2";
        $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2";
        $sql_ft = "SELECT * FROM FT";
        $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2";
      }
    }else{
      $login_state = $_SESSION['state'];
      $login_state = strtoupper($login_state);
      //echo $login_category;
      if (isset($_REQUEST['special']))
      {
        $kementerian_selected = $_POST['kementerian'];
        $date_selected = $_POST['date'];
        $type_selected = $_POST['type'];

        if ($kementerian_selected == '-1' && $date_selected == '' && $type_selected == '-1') {
          $kes = 1;
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
          $sql_ft = "SELECT * FROM FT WHERE NAMA_NEGERI = '$login_state'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
        }elseif ($kementerian_selected != '-1' && $date_selected == '' && $type_selected == '-1') {
          $kes = 2;
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_ft = "SELECT * FROM FT WHERE NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
        }elseif ($kementerian_selected == '-1' && $date_selected != '' && $type_selected != '-1') {
          $kes = 5;
          if ($type_selected == 'DAY') {
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND TRH_POHON = '$date_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND TRH_DAF_HMLK = '$date_selected'";
          $sql_ft = "SELECT * FROM FT WHERE NAMA_NEGERI = '$login_state' AND TRH_DAF_HMLK = '$date_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND TRH_WARTA = '$date_selected'";
          }elseif ($type_selected == 'WEEK') {
            $time  = strtotime($date_selected);
            $start = date('Y-m-d', strtotime('Last Monday', $time));
            $end = date('Y-m-d', strtotime('Next Sunday', $time));
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE TRH_POHON >= '$start' AND TRH_POHON < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
            $sql_qt = "SELECT * FROM QT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
            $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND NAMA_NEGERI = '$login_state'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE TRH_WARTA >= '$start' AND TRH_WARTA < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
          }elseif ($type_selected == 'MONTH') {
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(m, TRH_POHON) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_POHON) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND NAMA_NEGERI = '$login_state'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE DATEPART(m, TRH_WARTA) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
          }
		  // Carian Year
		  elseif ($type_selected == 'YEAR') {
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND NAMA_NEGERI = '$login_state'";
            $sql_rezab = "SELECT * FROM RIZAB WHERE DATEPART(YEAR, TRH_WARTA) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state'";
          }
        }elseif ($kementerian_selected != '-1' && $date_selected != '' && $type_selected != '-1') {
          $kes = 6;
          if ($type_selected == 'DAY') {
          $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND TRH_POHON = '$date_selected' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_qt = "SELECT * FROM QT WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND TRH_DAF_HMLK = '$date_selected' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK = '$date_selected' AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
          $sql_rizab = "SELECT * FROM RIZAB WHERE STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND TRH_WARTA = '$date_selected' AND KEMENTERIAN = '$kementerian_selected'";
          }elseif ($type_selected == 'WEEK') {
            $time  = strtotime($date_selected);
            $start = date('Y-m-d', strtotime('Last Monday', $time));
            $end = date('Y-m-d', strtotime('Next Sunday', $time));
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE TRH_POHON >= '$start' AND TRH_POHON < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_qt = "SELECT * FROM QT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_ft = "SELECT * FROM FT WHERE TRH_DAF_HMLK >= '$start' AND TRH_DAF_HMLK < '$end' AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE TRH_WARTA >= '$start' AND TRH_WARTA < '$end' AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
          }elseif ($type_selected == 'MONTH') {
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(m, TRH_POHON) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_POHON) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(m, TRH_DAF_HMLK) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_rizab = "SELECT * FROM RIZAB WHERE DATEPART(m, TRH_WARTA) = DATEPART(m, DATEADD(m, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(m, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
          }
		  // Carian Year
		  elseif ($type_selected == 'YEAR') {
            $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(YEAR, TRH_POHON) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_qt = "SELECT * FROM QT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_ft = "SELECT * FROM FT WHERE DATEPART(YEAR, TRH_DAF_HMLK) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_DAF_HMLK) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
            $sql_rezab = "SELECT * FROM RIZAB WHERE DATEPART(YEAR, TRH_WARTA) = DATEPART(YEAR, DATEADD(YEAR, 0, '$date_selected')) AND DATEPART(yyyy, TRH_WARTA) = DATEPART(yyyy, DATEADD(YEAR, 0, '$date_selected')) AND STATUS_TERKINI = 2 AND NAMA_NEGERI = '$login_state' AND KEMENTERIAN = '$kementerian_selected'";
          }
        }
      }else{
        $sql_permohonan = "SELECT * FROM PERMOHONAN WHERE NAMA_NEGERI = '$login_state' AND STATUS_TERKINI = 2";
        $sql_qt = "SELECT * FROM QT WHERE NAMA_NEGERI = '$login_state' AND STATUS_TERKINI = 2";
        $sql_ft = "SELECT * FROM FT WHERE NAMA_NEGERI = '$login_state'";
        $sql_rizab = "SELECT * FROM RIZAB WHERE NAMA_NEGERI = '$login_state' AND STATUS_TERKINI = 2";
      }
    }

    $permohonan = sqlsrv_query($conn, $sql_permohonan, array(), array( "Scrollable" => 'static' ));
    $permohonan_count = sqlsrv_num_rows($permohonan);
    $qt = sqlsrv_query($conn, $sql_qt, array(), array( "Scrollable" => 'static' ));
    $qt_count = sqlsrv_num_rows($qt);
    $ft = sqlsrv_query($conn, $sql_ft, array(), array( "Scrollable" => 'static' ));
    $ft_count = sqlsrv_num_rows($ft);
    $rizab = sqlsrv_query($conn, $sql_rizab, array(), array( "Scrollable" => 'static' ));
    $rizab_count = sqlsrv_num_rows($rizab);
    

  ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="row top_tiles">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Bilangan charting dalam proses mengikut status</h2>
				  

                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
				
					<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats3" style="color: white;">
                <div class="icon"><i class="fa fa-file-powerpoint-o"></i></div>
                 <div class="count"><?php echo $permohonan_count; ?></div>
                <h3>Permohonan</h3>
              </div>
            </div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats2" style="color: white;">
                <div class="icon"><i class="fa fa-clipboard"></i></div>
                <div class="count"><?php echo $qt_count; ?></div>
                <h3>QT</h3>
                  <!-- <p>For the current month.</p> -->
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats1" style="color: white;">
                <div class="icon"><i class="fa fa-gavel"></i></div>
                <div class="count"><?php echo $ft_count; ?></div>
                <h3>FT</h3>
                <!-- <p>Lorem ipsum psdea itgum rixt.</p> -->
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats4" style="color: white;">
                <div class="icon"><i class="fa fa-hand-lizard-o"></i></div>
                <div class="count"><?php echo $rizab_count; ?></div>
                <h3>Rizab</h3>
                <!-- <p>Lorem ipsum psdea itgum rixt.</p> -->
              </div>
            </div>
          </div>

                <label for="type" style="margin-left: 1em;">Jumlah keseluruhan: 
				<?php echo $permohonan_count + $qt_count + $ft_count + $rizab_count ?>
				</label>   

                </div>
              </div>
            </div>

          <div class="clearfix"></div>

          <div class="row">  
            <!-- Service graph -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Paparan graf charting mengikut kategori</h2>
				  
                  <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id="echart_service" style="height:350px;"></div>

                </div>
              </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Paparan mengikut carian</h2>
                  <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div style="height:350px;">
				  
				<!--	KEMENTERIAN : <1?php echo $_POST['kementerian'] ?> <br>
					TARIKH : <1?php echo $_POST['datepicker'] ?> <br>
					CARIAN MENGIKUT : <1?php echo $_POST['type'] ?> -->
				  
				  
				  
				  <br><br>
                    <form method="POST" action="index.php?action=Analysis" onsubmit="return validateForm()">
                      <label for="type" style="margin-left: 1em;">Kategori: </label>
                      <select name="txtservice" class="has-padding" style="height: 30px;" id="category">
                        <option value="PERMOHONAN" <?php echo (isset($_POST['txtservice']) && $_POST['txtservice'] == 'PERMOHONAN') ? 'selected="selected"' : ''; ?>>PERMOHONAN</option>
                        <option value="QT" <?php echo (isset($_POST['txtservice']) && $_POST['txtservice'] == 'QT') ? 'selected="selected"' : ''; ?>>QT</option>
                        <option value="FT" <?php echo (isset($_POST['txtservice']) && $_POST['txtservice'] == 'FT') ? 'selected="selected"' : ''; ?>>FT</option>
                        <option value="RIZAB" <?php echo (isset($_POST['txtservice']) && $_POST['txtservice'] == 'RIZAB') ? 'selected="selected"' : ''; ?>>RIZAB</option>                         
                      </select><br><br>
                      <label for="kementerian" style="margin-left: 1em;">Kementerian: </label>
                      <select name="kementerian" class="has-padding" style="height: 30px; width: 300px;" id="kementerian">
                        <option value="-1">All</option>
                      </select><br><br>
                      <?php
                      if (strcmp($_SESSION['category'], "Super Admin")==0 || strcmp($_SESSION['category'], "Admin")==0 || strcmp($_SESSION['category'], "Guest")==0) {
                      ?>
                        <label for="signin-state" style="margin-left: 1em;">Negeri</label>
                        <select name="txtstate" class="has-padding" style="height: 30px;" id="negeri">
                          <option value="-1" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == '-1') ? 'selected="selected"' : ''; ?>>All</option>
                          <option value="Johor" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Johor') ? 'selected="selected"' : ''; ?>>Johor</option>
                          <option value="Kedah" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Kedah') ? 'selected="selected"' : ''; ?>>Kedah</option>
                          <option value="Kelantan" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Kelantan') ? 'selected="selected"' : ''; ?>>Kelantan</option>
                          <option value="Melaka" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Melaka') ? 'selected="selected"' : ''; ?>>Melaka</option>
                          <option value="Negeri Sembilan" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Negeri Sembilan') ? 'selected="selected"' : ''; ?>>Negeri Sembilan</option>
                          <option value="Pahang" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Pahang') ? 'selected="selected"' : ''; ?>>Pahang</option>
                          <option value="Pulau Pinang" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Pulau Pinang') ? 'selected="selected"' : ''; ?>>Pulau Pinang</option>
                          <option value="Perak" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Perak') ? 'selected="selected"' : ''; ?>>Perak</option>
                          <option value="Perlis" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Perlis') ? 'selected="selected"' : ''; ?>>Perlis</option>
                          <option value="Sabah" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Sabah') ? 'selected="selected"' : ''; ?>>Sabah</option>
                          <option value="Sarawak" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Sarawak') ? 'selected="selected"' : ''; ?>>Sarawak</option>
                          <option value="Selangor" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Selangor') ? 'selected="selected"' : ''; ?>>Selangor</option>
                          <option value="Terengganu" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Terengganu') ? 'selected="selected"' : ''; ?>>Terengganu</option>
                          <option value="Kuala Lumpur" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Kuala Lumpur') ? 'selected="selected"' : ''; ?>>Kuala Lumpur</option>
                          <option value="Labuan" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Labuan') ? 'selected="selected"' : ''; ?>>Labuan</option>
                          <option value="Putrajaya" <?php echo (isset($_POST['txtstate']) && $_POST['txtstate'] == 'Putrajaya') ? 'selected="selected"' : ''; ?>>Putrajaya</option>
                        </select>
                      <?php
                      }else{
                      ?>
                        <label for="signin-state" style="margin-left: 1em;">Negeri</label>
                        <select name="txtstate" class="has-padding" style="height: 30px;" id="negeri">
                            <option value="<?php echo $_SESSION['state']; ?>"><?php echo $_SESSION['state']; ?></option>
                        </select>
                      <?php
                      }
                      ?>
                      <br><br>
                      <label for="type" style="margin-left: 1em;">Tarikh: </label>
                      <input type="text" name="date" id="datepicker" style="height: 30px;"><br><br>
                      <label for="type" style="margin-left: 1em;">Carian mengikut: </label>
                      <select name="type" class="has-padding" style="height: 30px;" id="type">
                        <option value="-1">All</option>
                      <?php
                        $type = array("DAY", "WEEK", "MONTH", "YEAR");
                        foreach($type as $item){
                        ?>
                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                        <?php
                        }
                      ?>
                      </select><br><br>
                      <input name="special" type="submit" value="Submit" style="float: right;">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
		  
		 <!--?php if(echo $_POST['kementerian'] != '-1') { ?--> 

          <div class="row">  
            <!-- bar chart -->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Kementerian </h2>
                  <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id="graph_bar_kementerian" style="width:100%; height:350px;"></div>
				  <!--div id="curtain"><span>Chart is loading...</span-->
                </div>
              </div>
            </div>
            <!-- /bar charts -->
          </div>
		  
		 <!--?php } ?--> 
		  
		  
          <?php
          if (strcmp($_SESSION['category'], "Super Admin")==0 || strcmp($_SESSION['category'], "Admin")==0 || strcmp($_SESSION['category'], "Guest")==0) {
          ?>
          <div class="row"> 
          <?php
          }else{
          ?>
          <div class="row" style="display: none;"> 
          <?php
          }
          ?> 
            <!-- bar chart -->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Negeri</h2>
                  <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id="graph_bar_negeri" style="width:100%; height:350px;"></div>
                </div>
              </div>
            </div>
            <!-- /bar charts -->
          </div>

          <!-- PERMOHONAN -->
          <div class="row">  
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <?php
                  if (isset($_REQUEST['special']))
                  {
                    ?>
                    <h2>Charting Di <?php echo $state; ?> Dalam Proses <?php echo $_POST['txtservice']; ?> </h2>
                    <?php
                  }else{
                    ?>
                    <h2>Charting Di <?php echo $state; ?> Dalam Proses Pemohonan</h2>
                    <?php
                  }
                  ?>
                  <ul class="nav navbar-right panel_toolbox">
                    <li style="padding-right: 50px; margin-top: 3px;">
                      <div>
                      </div>
                    </li>
                    <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li> -->
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="datatable-buttons" class="table table-striped table-bordered">
                  <?php
                  if (isset($_REQUEST['special']))
                  {
                    $service_selected = $_POST['txtservice'];
                    
                    if (strcmp($service_selected, "QT")==0) {
                      $q = sqlsrv_query($conn, $sql_qt);
                      ?>
                      <thead>
                        <tr>
						  <th>No</th>
                          <th>Projek</th>
                          <th>Daerah</th>
                          <th>Mukim</th>
                          <th>Seksyen</th>
                          <th>Kementerian</th>
                          <th>Jenis Hakmilik</th>
                          <th>No Hakmilik Sementara</th>
						  <!--th>Tarikh Hakmilik</th-->
                          <th>No PT</th>
                          <th>Luas(h)</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
					  $NO = 0;
                      while( $row = sqlsrv_fetch_array( $q, SQLSRV_FETCH_ASSOC ) ) {
								if($row['GUNA_TANAH'] == NULL || $row['GUNA_TANAH'] == ''){$GUNATANAH = 'TIADA MAKLUMAT';}
								else{$GUNATANAH = $row['GUNA_TANAH'];}
								if($row['NAMA_SEKSYEN'] == NULL || $row['NAMA_SEKSYEN'] == ''){$NAMA_SEKSYEN = 'TIADA MAKLUMAT';}
								else{$NAMA_SEKSYEN = $row['NAMA_SEKSYEN'];}
								if($row['KEMENTERIAN'] == NULL || $row['KEMENTERIAN'] == ''){$KEMENTERIAN = 'TIADA MAKLUMAT';}
								else{$KEMENTERIAN = $row['KEMENTERIAN'];}
								if($row['LUAS_HA'] == NULL || $row['LUAS_HA'] == ''){$KELUASAN = 'TIADA MAKLUMAT';}
								else{$KELUASAN = $row['LUAS_HA'];}
                        $NAMA_DAERAH  = $row['NAMA_DAERAH'];
                        $NAMA_MUKIM  = $row['NAMA_MUKIM'];
                       // $NAMA_SEKSYEN  = $row['NAMA_SEKSYEN'];
                       // $KEMENTERIAN  = $row['KEMENTERIAN'];
                        $JENIS_HMLK  = $row['JENIS_HMLK'];
                        $HMLK_SMTARA  = $row['HMLK_SMTARA'];
						//$TRH_DAF_HMLK  = $row['TRH_DAF_HMLK'];
                        $NO_PT  = $row['NO_PT'];
                       // $KELUASAN  = $row['LUAS_HA'];
						            $NO = $NO + 1;
                      ?>
                      <tr>
                        <td><?php echo $NO;?></td>
						<td><?php echo $GUNATANAH;?></td>
                        <td><?php echo $NAMA_DAERAH;?></td>
                        <td><?php echo $NAMA_MUKIM;?></td>
                        <td><?php echo $NAMA_SEKSYEN;?></td>
                        <td><?php echo $KEMENTERIAN;?></td>
                        <td><?php echo $JENIS_HMLK;?></td>
                        <td><?php echo $HMLK_SMTARA;?></td>
						<!--td><1?php echo $TRH_DAF_HMLK;?></td-->
                        <td><?php echo $NO_PT;?></td>
                        <td><?php echo $KELUASAN;?></td>
                      </tr>
                      <?php
                      }
                      ?>
                      </tbody>
                      <?php
                    }else{
                      if (strcmp($service_selected, "FT")==0) {
                        $f = sqlsrv_query($conn, $sql_ft);
                      ?>
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Projek</th>
                            <th>Daerah</th>
                            <th>Mukim</th>
                            <th>Seksyen</th>
                            <th>Kementerian</th>
                            <th>Jenik Hakmilik</th>
                            <th>No Hakmilik Tetap</th>
                            <th>No Lot</th>
                            <th>PA</th>
                            <th>Luas(h)</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
						            $NO = 0;
                        while( $row = sqlsrv_fetch_array( $f, SQLSRV_FETCH_ASSOC ) ) {
            					if($row['GUNA_TANAH'] == NULL || $row['GUNA_TANAH'] == ''){$GUNATANAH = 'TIADA MAKLUMAT';}
								else{$GUNATANAH = $row['GUNA_TANAH'];}
								if($row['NAMA_SEKSYEN'] == NULL || $row['NAMA_SEKSYEN'] == ''){$NAMA_SEKSYEN = 'TIADA MAKLUMAT';}
								else{$NAMA_SEKSYEN = $row['NAMA_SEKSYEN'];}
								if($row['KEMENTERIAN'] == NULL || $row['KEMENTERIAN'] == ''){$KEMENTERIAN = 'TIADA MAKLUMAT';}
								else{$KEMENTERIAN = $row['KEMENTERIAN'];}
								if($row['LUAS_HA'] == NULL || $row['LUAS_HA'] == ''){$KELUASAN = 'TIADA MAKLUMAT';}
								else{$KELUASAN = $row['LUAS_HA'];}
                          $NAMA_DAERAH  = $row['NAMA_DAERAH'];
                          $NAMA_MUKIM  = $row['NAMA_MUKIM'];
                         // $NAMA_SEKSYEN  = $row['NAMA_SEKSYEN'];
                         // $KEMENTERIAN  = $row['KEMENTERIAN'];
                          $JENIS_HMLK  = $row['JENIS_HMLK'];
                          $HMLK_TETAP  = $row['HMLK_TETAP'];
                          $NO_LOT  = $row['NO_LOT'];
                          $NO_PA  = $row['NO_PA'];
                         // $KELUASAN  = $row['LUAS_HA'];
						              $NO = $NO + 1;
                        ?>
                        <tr>
						  <td><?php echo $NO;?></td>
                          <td><?php echo $GUNATANAH;?></td>
                          <td><?php echo $NAMA_DAERAH;?></td>
                          <td><?php echo $NAMA_MUKIM;?></td>
                          <td><?php echo $NAMA_SEKSYEN;?></td>
                          <td><?php echo $KEMENTERIAN;?></td>
                          <td><?php echo $JENIS_HMLK;?></td>
                          <td><?php echo $HMLK_TETAP;?></td>
                          <td><?php echo $NO_LOT;?></td>
                          <td><?php echo $NO_PA;?></td>
                          <td><?php echo $KELUASAN;?></td>
                        </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                        <?php
                      }else{
                        if (strcmp($service_selected, "RIZAB")==0) {
                          $w = sqlsrv_query($conn, $sql_rizab);
                        ?>
                          <thead>
                            <tr>
							  <th>No</th>
                              <th>Projek</th>
                              <th>Daerah</th>
                              <th>Mukim</th>
                              <th>Seksyen</th>
                              <th>Kementerian</th>
                              <th>No Warta</th>
                              <!--th>Tarikh Warta</th-->
                              <th>Luas(h)</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php
						   $NO = 0;
                          while( $row = sqlsrv_fetch_array( $w, SQLSRV_FETCH_ASSOC ) ) {
								if($row['GUNA_TANAH'] == NULL || $row['GUNA_TANAH'] == ''){$GUNATANAH = 'TIADA MAKLUMAT';}
								else{$GUNATANAH = $row['GUNA_TANAH'];}
								if($row['NAMA_SEKSYEN'] == NULL || $row['NAMA_SEKSYEN'] == ''){$NAMA_SEKSYEN = 'TIADA MAKLUMAT';}
								else{$NAMA_SEKSYEN = $row['NAMA_SEKSYEN'];}
								if($row['KEMENTERIAN'] == NULL || $row['KEMENTERIAN'] == ''){$KEMENTERIAN = 'TIADA MAKLUMAT';}
								else{$KEMENTERIAN = $row['KEMENTERIAN'];}
								if($row['LUAS_HA'] == NULL || $row['LUAS_HA'] == ''){$KELUASAN = 'TIADA MAKLUMAT';}
								else{$KELUASAN = $row['LUAS_HA'];}
                            $NAMA_DAERAH  = $row['NAMA_DAERAH'];
                            $NAMA_MUKIM  = $row['NAMA_MUKIM'];
                            //$NAMA_SEKSYEN  = $row['NAMA_SEKSYEN'];
                           // $KEMENTERIAN  = $row['KEMENTERIAN'];
                            $NO_WARTA  = $row['NO_WARTA'];
							//$TRH_WARTA  = $row['TRH_WARTA'];
                            $NO_PA  = $row['NO_PA'];
                            //$KELUASAN  = $row['LUAS_HA'];
							              $NO = $NO + 1;
                          ?>
                          <tr>
							<td><?php echo $NO;?></td>
                            <td><?php echo $GUNATANAH;?></td>
                            <td><?php echo $NAMA_DAERAH;?></td>
                            <td><?php echo $NAMA_MUKIM;?></td>
                            <td><?php echo $NAMA_SEKSYEN;?></td>
                            <td><?php echo $KEMENTERIAN;?></td>
                            <td><?php echo $NO_WARTA;?></td>
                            <!--td><1?php echo $TRH_WARTA;?></td-->
                            <td><?php echo $KELUASAN;?></td>
                          </tr>
                          <?php
                          }
                          ?>
                          </tbody>
                          <?php
                        }else{
                          ?>
                          <thead>
                            <tr>
							  <th>No</th>
                              <th>Projek</th>
                              <th>Daerah</th>
                              <th>Mukim</th>
                              <th>Seksyen</th>
                              <th>Kementerian</th>
							  <!--th>Tarikh Pohon</th-->
                              <th>Luas(h)</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php
                          $p = sqlsrv_query($conn, $sql_permohonan);
						  $NO = 0;
                          while( $row = sqlsrv_fetch_array( $p, SQLSRV_FETCH_ASSOC ) ) {
								
                            $NAMA_DAERAH  = $row['NAMA_DAERAH'];
                            $NAMA_MUKIM  = $row['NAMA_MUKIM'];	
							//$TRH_POHON  = $row['TRH_POHON'];
                          //$NAMA_SEKSYEN  = $row['NAMA_SEKSYEN'];
						  //$KEMENTERIAN  = $row['KEMENTERIAN'];
                          //$KELUASAN  = $row['LUAS_HA'];
								if($row['GUNA_TANAH'] == NULL || $row['GUNA_TANAH'] == ''){$GUNATANAH = 'TIADA MAKLUMAT';}
								else{$GUNATANAH = $row['GUNA_TANAH'];}
								if($row['NAMA_SEKSYEN'] == NULL || $row['NAMA_SEKSYEN'] == ''){$NAMA_SEKSYEN = 'TIADA MAKLUMAT';}
								else{$NAMA_SEKSYEN = $row['NAMA_SEKSYEN'];}
								if($row['KEMENTERIAN'] == NULL || $row['KEMENTERIAN'] == ''){$KEMENTERIAN = 'TIADA MAKLUMAT';}
								else{$KEMENTERIAN = $row['KEMENTERIAN'];}
								if($row['LUAS_HA'] == NULL || $row['LUAS_HA'] == ''){$KELUASAN = 'TIADA MAKLUMAT';}
								else{$KELUASAN = $row['LUAS_HA'];}
							$NO = $NO + 1;
                          ?>
                          <tr>
							<td><?php echo $NO;?></td>
                            <td><?php echo $GUNATANAH;?></td>
                            <td><?php echo $NAMA_DAERAH;?></td>
                            <td><?php echo $NAMA_MUKIM;?></td>
                            <td><?php echo $NAMA_SEKSYEN;?></td>
                            <td><?php echo $KEMENTERIAN;?></td>
							<!--td><1?php echo $TRH_POHON;?></td-->
                            <td><?php echo $KELUASAN;?></td>
                          </tr>
                          <?php
                          }
                          ?>
                          </tbody>
                          <?php
                        }
                      }
                    }
                  }else{
                  ?>
                    <thead>
                      <tr>
						<th>No</th>
                        <th>Projek</th>
                        <th>Daerah</th>
                        <th>Mukim</th>
                        <th>Seksyen</th>
                        <th>Kementerian</th>
						<!--th>Tarikh Pohon</th-->
                        <th>Luas(h)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      // include('../../sql_server_db.php');
                      if (strcmp($_SESSION['category'], "Super Admin")==0 || strcmp($_SESSION['category'], "Admin")==0 || strcmp($_SESSION['category'], "Guest")==0) {
                        $sql_permoh = "SELECT * FROM PERMOHONAN WHERE STATUS_TERKINI = 2";
                      }else{
                        $login_state = $_SESSION['state'];
                        $login_state = strtoupper($login_state);
                        $sql_permoh = "SELECT * FROM PERMOHONAN WHERE NAMA_NEGERI = '$login_state' AND STATUS_TERKINI = 2";
                      }
                      $p = sqlsrv_query($conn, $sql_permoh);
					            $NO = 0;
                      while( $row = sqlsrv_fetch_array( $p, SQLSRV_FETCH_ASSOC ) ) {
								if($row['GUNA_TANAH'] == NULL || $row['GUNA_TANAH'] == ''){$GUNATANAH = 'TIADA MAKLUMAT';}
								else{$GUNATANAH = $row['GUNA_TANAH'];}
								if($row['NAMA_SEKSYEN'] == NULL || $row['NAMA_SEKSYEN'] == ''){$NAMA_SEKSYEN = 'TIADA MAKLUMAT';}
								else{$NAMA_SEKSYEN = $row['NAMA_SEKSYEN'];}
								if($row['KEMENTERIAN'] == NULL || $row['KEMENTERIAN'] == ''){$KEMENTERIAN = 'TIADA MAKLUMAT';}
								else{$KEMENTERIAN = $row['KEMENTERIAN'];}
								if($row['LUAS_HA'] == NULL || $row['LUAS_HA'] == ''){$KELUASAN = 'TIADA MAKLUMAT';}
								else{$KELUASAN = $row['LUAS_HA'];}
                        $NAMA_DAERAH  = $row['NAMA_DAERAH'];
                        $NAMA_MUKIM  = $row['NAMA_MUKIM'];
						//$TRH_POHON  = $row['TRH_POHON'];
                       // $NAMA_SEKSYEN  = $row['NAMA_SEKSYEN'];
                       // $KEMENTERIAN  = $row['KEMENTERIAN'];
                       // $KELUASAN  = $row['LUAS_HA'];
						        $NO = $NO + 1;
                      ?>
                      <tr>
						<td><?php echo $NO;?></td>
                        <td><?php echo $GUNATANAH;?></td>
                        <td><?php echo $NAMA_DAERAH;?></td>
                        <td><?php echo $NAMA_MUKIM;?></td>
                        <td><?php echo $NAMA_SEKSYEN;?></td>
                        <td><?php echo $KEMENTERIAN;?></td>
						<!--td><1?php echo $TRH_POHON;?></td-->
                        <td><?php echo $KELUASAN;?></td>
                      </tr>
                      <?php
                      }
                      ?>
                    </tbody>
                  <?php
                  }
                  ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- /page content -->
  