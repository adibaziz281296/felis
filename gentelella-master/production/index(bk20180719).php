<?php
session_start();
if(!isset($_SESSION['username']))
{
    header('Location:../../Login/');
}


$username = $_SESSION['username'];
if (strcmp($_SESSION['category'], "Super Admin")==0) {
  $no = $_SESSION['no'];
}

// include('../../connect_db.php');
include('../../sql_server_db.php');
$stmt70 = "SELECT name, phone_no, state, email, profile_pic, CONVERT(nvarchar(50),[Last_time_seen],120) as [Last_time_seen], password FROM login WHERE username='$username'";
$res70 = sqlsrv_query( $conn, $stmt70);
if( $res70 === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

while($row70 = sqlsrv_fetch_array( $res70, SQLSRV_FETCH_ASSOC )){
  $name = $row70['name'];
  $phone = $row70['phone_no'];
  // $state = $row70['state'];
  // $address = $row70['address'];
  $email = $row70['email'];
  $profile_pic = $row70['profile_pic'];
  $last_login = $row70['Last_time_seen'];
  $password = $row70['password'];
}
$state = $_SESSION['state'];
// Online users
$query = "UPDATE login SET Last_time_seen = GETDATE() WHERE username = '$username'";
$res = sqlsrv_query( $conn, $query );
if( $res === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

$sql10 = "SELECT email FROM login WHERE Last_time_seen > DATEADD(minute, -1, GETDATE())";
$res10 = sqlsrv_query($conn, $sql10, array(), array( "Scrollable" => 'static' ));
if( $res10 === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}
$count = sqlsrv_num_rows($res10);

// Number of sign in per month
$stmt2 = "SELECT month, no, year FROM no_login_per_month";
$res12 = sqlsrv_query( $conn, $stmt2);
if( $res12 === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

$login_no = array();
while($row2 = sqlsrv_fetch_array( $res12, SQLSRV_FETCH_ASSOC )){
  $login_no[] = $row2['no'];
}

// Total Sign in for current month
$stmt3 = "SELECT month, no, year FROM no_login_per_month WHERE month = MONTH(GETDATE())";
$res3 = sqlsrv_query( $conn, $stmt3 );
if( $res3 === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

while($row3 = sqlsrv_fetch_array( $res3, SQLSRV_FETCH_ASSOC )){
  $cur_month_log_no = $row3['no'];
}

// Today's Visits
$stmt4 = "SELECT email FROM login WHERE Last_time_seen >= dateadd(day,datediff(day,0,GETDATE()),0)";
// $res4 = sqlsrv_query( $conn , $stmt4);
// if( $res4 === false) {
//   die( print_r( sqlsrv_errors(), true) );
// }

$res4 = sqlsrv_query($conn, $stmt4, array(), array( "Scrollable" => 'static' ));
if( $res4 === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

$today_visit = sqlsrv_num_rows($res4);
// while($row4 = sqlsrv_fetch_array( $res4, SQLSRV_FETCH_ASSOC )){
//   $today_visit = $row4['visits_today'];
// }

// Total sign up

$stmt5 = "SELECT email FROM login";
$res5 = sqlsrv_query($conn, $stmt5, array(), array( "Scrollable" => 'static' ));
if( $res5 === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}
$total_sign_up = sqlsrv_num_rows($res5);
// while($row5 = sqlsrv_fetch_array( $res5, SQLSRV_FETCH_ASSOC )){
//   $total_sign_up = $row5['sign_up'];
// }

// Number of sign in per day
$stmt16 = "SELECT day, no, month FROM no_login_per_day";
$res16 = sqlsrv_query( $conn, $stmt16 );
if( $res16 === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

$login_no1 = array();
while($row16 = sqlsrv_fetch_array( $res16, SQLSRV_FETCH_ASSOC )){
  $login_no1[] = $row16['no'];
  $m = $row16['month'];
}

// Number of sign in per state
//$in = 0;
if (isset($_REQUEST['months']))
{
  $month_selected = $_POST['txtmonth'];
  $stmt7 = "SELECT id, state, month, year, no FROM no_login_per_state WHERE month = '$month_selected' AND year = YEAR(GETDATE())";
  $res7 = sqlsrv_query( $conn, $stmt7 );
  if( $res7 === false) {
    die( print_r( sqlsrv_errors(), true) );
    echo "<script> console.log('PHP: wrong!!!');</script>";
  }
  $res11 = sqlsrv_query($conn, $stmt7, array(), array( "Scrollable" => 'static' ));
  if( $res11 === false) {
    die( print_r( sqlsrv_errors(), true) );
    echo "<script> console.log('PHP: wrong!!!');</script>";
  }

  $in = sqlsrv_num_rows($res11);
  $login_no7 = array();
  $momthly_state = array();
  while($row7 = sqlsrv_fetch_array( $res7, SQLSRV_FETCH_ASSOC )){
    $login_no7[] = $row7['no'];
    $momthly_state[] = $row7['state'];
    //$in++;
  }
}
else
{
  $stmt7 = "SELECT id, state, month, year, no FROM no_login_per_state WHERE month = MONTH(GETDATE()) AND year = YEAR(GETDATE())";
  $res7 = sqlsrv_query( $conn, $stmt7 );
  if( $res7 === false) {
    die( print_r( sqlsrv_errors(), true) );
    echo "<script> console.log('PHP: wrong!!!');</script>";
  }
  $res11 = sqlsrv_query($conn, $stmt7, array(), array( "Scrollable" => 'static' ));
  if( $res11 === false) {
    die( print_r( sqlsrv_errors(), true) );
    echo "<script> console.log('PHP: wrong!!!');</script>";
  }
  $in = sqlsrv_num_rows($res11);
  $login_no7 = array();
  $momthly_state = array();
  while($row7 = sqlsrv_fetch_array( $res7, SQLSRV_FETCH_ASSOC )){
    $login_no7[] = $row7['no'];
    $momthly_state[] = $row7['state'];
    //$in++;
  }
}
$states = array();
$stmt888 = "SELECT NAMA_NEGERI FROM tblrujnegeri_1";
$result888 = sqlsrv_query( $conn, $stmt888 );

while($row = sqlsrv_fetch_array( $result888, SQLSRV_FETCH_ASSOC )){
  $states[] = $row['NAMA_NEGERI'];
}

// $states = array("Johor", "Kedah", "Kelantan", "Malacca", "Negeri Sembilan", "Pahang", "Penang", "Perak", "Perlis", "Sabah", "Sarawak", "Selangor", "Terengganu", "WP Kuala Lumpur", "WP Labuan", "WP Putrajaya");
for ($t=0; $t < $in; $t++) {
  foreach (array_keys($states, $momthly_state[$t]) as $key) {
    unset($states[$key]);
  }
}

$states = array_values($states);

for ($z=$in; $z < 16; $z++) { 
  $login_no7[] = 0;
  $momthly_state[$z] = $states[$z-$in];
}


// $AA = 5;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FeLIS</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
    <!-- Dropzone.js -->
    <link href="../vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
	
	<link href="css/elements.css" rel="stylesheet">
    <style type="text/css">
	
	 /* Paste this css to your style sheet file or under head tag */
	/* This only works with JavaScript, 
	if it's not present, don't show loader */
	.no-js #loader { display: none;  }
	.js #loader { display: block; position: absolute; left: 100px; top: 0; }
	.se-pre-con {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url(images/loader-64x/Preloader_1.gif) center no-repeat #fff;
	}	
	
	
	
      .logout-message 
      {
        padding: 0 25px 25px;
        border-radius: 10px;
        text-align: center;
        color: #fff;
        background-color: rgba(26,36,47,0.9);
      }

      .logout-message h3 
      {
        margin: 15px 0;
      }

      .logout-message p 
      {
        margin: 0 0 25px;
      }

      .text-green {
        color: #16a085;
      }

      .btn-green 
      {
        border-color: #15987e;
        color: #fff;
        background-color: #16a085;
      }

      .btn-green:hover,
      .btn-green:focus,
      .btn-green:active,
      .btn-green.active,
      .open .dropdown-toggle.btn-green 
      {
        border-color: #138871;
        color: #fff;
        background-color: #149077;
      }

      .btn-green.disabled,
      .btn-green[disabled],
      fieldset[disabled] .btn-green,
      .btn-green.disabled:hover,
      .btn-green[disabled]:hover,
      fieldset[disabled] .btn-green:hover,
      .btn-green.disabled:focus,
      .btn-green[disabled]:focus,
      fieldset[disabled] .btn-green:focus,
      .btn-green.disabled:active,
      .btn-green[disabled]:active,
      fieldset[disabled] .btn-green:active,
      .btn-green.disabled.active,
      .btn-green[disabled].active,
      fieldset[disabled] .btn-green.active 
      {
        border-color: #2eaa91;
        background-color: #39ae97;
      }
      #leftPanel{
        background-color:#172D44;
        color:#fff;
        text-align: center;
      }

      #rightPanel{
        min-height:415px;
      }

      /* Credit to bootsnipp.com for the css for the color graph */
      .colorgraph {
        height: 5px;
        border-top: 0;
        background: #c4e17f;
        border-radius: 5px;
        background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
      }
    </style>
  </head>

  <div class="se-pre-con"></div>
  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <?php  require("LeftBox.php"); ?>
          </div>
        </div>
        <?php  require("HeaderBox.php"); ?>
        <?php  require("Modules.php") ?>
        <!-- footer content -->
        <?php
        if ($action!="WebGIS" && $action!="WebGIS-temp") {
        ?>
          <footer>
            <div class="pull-right">
              2016 &copy; GIS MyeTaPP System Admin
            </div>
            <div class="clearfix"></div>
          </footer>
        <?php
        }
        ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <?php
      if ($action == "main" || $action == "Analysis") {
      ?>
        <!-- Chart.js -->
        <script src="../vendors/Chart.js/dist/Chart_modify.js"></script>
        <!-- ECharts -->
        <script src="../vendors/echarts/dist/echarts.min.js"></script>

        <script src="../vendors/echarts/map/js/world.js"></script>
    <?php
    }
    ?>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Upload Image.js -->
    <script src="../vendors/uploadimage.js/upload-image.js"></script>
    
    
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
	<!-- calendar -->
    <link rel="stylesheet" href="../vendors/calendar/jquery-ui.css">
    <!-- <script src="../vendors/calendar/jquery-1.10.2.js"></script> -->
    <script src="../vendors/calendar/jquery-ui.js"></script>
    <!-- Custom Theme Scripts -->
    <?php
    if ($action == "Notification" || $action == "UserProfile" || $action == "AdminList" || $action == "AdminDetail" || $action == "StaffList" || $action == "StaffDetail" || $action == "RegisterStaff"){
    ?>
      <script src="../build/js/custom.min.js"></script>
    <?php
    }
    ?>
	
	<!-- Loading Page -->
	<script type="text/javascript">
	
	//paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
	
    </script>
	
    <!-- Datatables -->
    <script type="text/javascript">
      $(document).ready(function(){
        $.ajax({
          type: 'post',
          url: 'notificationAlert.php',
          data: {status: "Not Active"}
        }).success(function(data){
          $('#alerts').html(data);
        });
      });
    </script>
    <script type="text/javascript">
      $(document).on('click', '#read-notifications', function(e) {
        $.ajax({
            type: 'post',
            url: 'readNotification.php',
            data: {status: "Not Active"},
        }).success(function(data) {
            $('#alerts').html(data);// do what you need to do as notifications were read.
        });
      });
    </script>
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                // {
                //   extend: "copy",
                //   className: "btn-sm"
                // },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();

      });
    </script>
    <!-- /Datatables -->
    <!-- Full screen -->
    <script type="text/javascript">
      function toggleFullscreen(elem) {
        elem = elem || document.documentElement;
        if (!document.fullscreenElement && !document.mozFullScreenElement &&
          !document.webkitFullscreenElement && !document.msFullscreenElement) {
          if (elem.requestFullscreen) {
            elem.requestFullscreen();
          } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
          } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
          } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
          }
        } else {
          if (document.exitFullscreen) {
            document.exitFullscreen();
          } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
          } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
          } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
          }
        }
      }

      document.getElementById('fullscreen').addEventListener('click', function() {
        toggleFullscreen();
      });

      // document.getElementById('exampleImage').addEventListener('click', function() {
      //   toggleFullscreen(this);
      // });
    </script>
    <!-- /Full screen -->
    <?php
    if ($action=="main") {
    ?>
    <!-- lineChart -->
    <script>
          Chart.defaults.global.legend = {
            enabled: false
          };

          // Line chart
          var ctx = document.getElementById("lineChart");
          var x = new Array();
          x = '<?php echo json_encode($login_no); ?>'
          // for (var i = 0; i <= 11; i++) {
          //   eval("dynamic" + i + " = x[i]");
          // }
          var jan = '<?php echo $login_no[0]; ?>'
          var feb = '<?php echo $login_no[1]; ?>'
          var mar = '<?php echo $login_no[2]; ?>'
          var apr = '<?php echo $login_no[3]; ?>'
          var may = '<?php echo $login_no[4]; ?>'
          var jun = '<?php echo $login_no[5]; ?>'
          var jul = '<?php echo $login_no[6]; ?>'
          var aug = '<?php echo $login_no[7]; ?>'
          var sep = '<?php echo $login_no[8]; ?>'
          var oct = '<?php echo $login_no[9]; ?>'
          var nov = '<?php echo $login_no[10]; ?>'
          var dec = '<?php echo $login_no[11]; ?>'
          // alert(x);
          var lineChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
              datasets: [{
                label: "Users",
                backgroundColor: "rgba(38, 185, 154, 0.31)",
                borderColor: "rgba(38, 185, 154, 0.7)",
                pointBorderColor: "rgba(38, 185, 154, 0.7)",
                pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointBorderWidth: 10,
                data: [jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec]
              }]
            },
        });

          // Bar chart
        var ctx = document.getElementById("mybarChart");
        var x = new Array();
          x = '<?php echo json_encode($login_no1); ?>'
          // for (var i = 0; i <= 11; i++) {
          //   eval("dynamic" + i + " = x[i]");
          // }
          var day1 = '<?php echo $login_no1[0]; ?>'
          var day2 = '<?php echo $login_no1[1]; ?>'
          var day3 = '<?php echo $login_no1[2]; ?>'
          var day4 = '<?php echo $login_no1[3]; ?>'
          var day5 = '<?php echo $login_no1[4]; ?>'
          var day6 = '<?php echo $login_no1[5]; ?>'
          var day7 = '<?php echo $login_no1[6]; ?>'
          var day8 = '<?php echo $login_no1[7]; ?>'
          var day9 = '<?php echo $login_no1[8]; ?>'
          var day10 = '<?php echo $login_no1[9]; ?>'
          var day11 = '<?php echo $login_no1[10]; ?>'
          var day12 = '<?php echo $login_no1[11]; ?>'
          var day13 = '<?php echo $login_no1[12]; ?>'
          var day14 = '<?php echo $login_no1[13]; ?>'
          var day15 = '<?php echo $login_no1[14]; ?>'
          var day16 = '<?php echo $login_no1[15]; ?>'
          var day17 = '<?php echo $login_no1[16]; ?>'
          var day18 = '<?php echo $login_no1[17]; ?>'
          var day19 = '<?php echo $login_no1[18]; ?>'
          var day20 = '<?php echo $login_no1[19]; ?>'
          var day21 = '<?php echo $login_no1[20]; ?>'
          var day22 = '<?php echo $login_no1[21]; ?>'
          var day23 = '<?php echo $login_no1[22]; ?>'
          var day24 = '<?php echo $login_no1[23]; ?>'
          var day25 = '<?php echo $login_no1[24]; ?>'
          var day26 = '<?php echo $login_no1[25]; ?>'
          var day27 = '<?php echo $login_no1[26]; ?>'
          var day28 = '<?php echo $login_no1[27]; ?>'
          var day29 = '<?php echo $login_no1[28]; ?>'
          var day30 = '<?php echo $login_no1[29]; ?>'
          var day31 = '<?php echo $login_no1[30]; ?>'

        var mybarChart = new Chart(ctx, {
          type: 'bar',
          data: {
            <?php
            if ($m == 01 || $m == 03 || $m == 05 || $m == 07 || $m == 08 || $m == 10 || $m == 12)
            {
            ?>
              labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
            <?php
            }
            else
            {
              if ($m == 04 || $m == 06 || $m == 09 || $m == 11) 
              {
              ?>
                labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
              <?php
              }
              else
              {
              ?>
              labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29"],
            <?php
            }
          }
          ?>
              datasets: [{
                label: "Users",
                backgroundColor: "rgba(3, 88, 106, 0.3)",
                borderColor: "rgba(3, 88, 106, 0.70)",
                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(151,187,205,1)",
                pointBorderWidth: 10,
                <?php
                if ($m == 01 || $m == 03 || $m == 05 || $m == 07 || $m == 08 || $m == 10 || $m == 12) 
                {
                ?>
                data: [day1,day2,day3,day4,day5,day6,day7,day8,day9,day10,day11,day12,day13,day14,day15,day16,day17,day18,day19,day20,day21,day22,day23,day24,day25,day26,day27,day28,day29,day30,day31]
                <?php
                }
                else
                {
                  if ($m == 04 || $m == 06 || $m == 09 || $m == 11) 
                  {
                  ?>
                  data: [day1,day2,day3,day4,day5,day6,day7,day8,day9,day10,day11,day12,day13,day14,day15,day16,day17,day18,day19,day20,day21,day22,day23,day24,day25,day26,day27,day28,day29,day30]
                  <?php
                  }
                  else
                  {
                  ?>
                  data: [day1,day2,day3,day4,day5,day6,day7,day8,day9,day10,day11,day12,day13,day14,day15,day16,day17,day18,day19,day20,day21,day22,day23,day24,day25,day26,day27,day28,day29]
                  <?php
                  }
                }
                ?>
              }]
          },

          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

        var theme = {
          color: [
              '#FF00FF', '#008000', '#e98000', '#0000FF',
              '#808080', '#FF0000', '#FFFF00', '#00FF00',
              '#008080', '#800080', '#800000', '#C0C0C0',
              '#00FFFF', '#808000', '#000080', '#97c2cc'
          ],

          title: {
              itemGap: 8,
              textStyle: {
                  fontWeight: 'normal',
                  color: '#408829'
              }
          },

          dataRange: {
              color: ['#1f610a', '#97b58d']
          },

          toolbox: {
              color: ['#408829', '#408829', '#408829', '#408829']
          },

          tooltip: {
              backgroundColor: 'rgba(0,0,0,0.5)',
              axisPointer: {
                  type: 'line',
                  lineStyle: {
                      color: '#408829',
                      type: 'dashed'
                  },
                  crossStyle: {
                      color: '#408829'
                  },
                  shadowStyle: {
                      color: 'rgba(200,200,200,0.3)'
                  }
              }
          },

          dataZoom: {
              dataBackgroundColor: '#eee',
              fillerColor: 'rgba(64,136,41,0.2)',
              handleColor: '#408829'
          },
          grid: {
              borderWidth: 0
          },

          categoryAxis: {
              axisLine: {
                  lineStyle: {
                      color: '#408829'
                  }
              },
              splitLine: {
                  lineStyle: {
                      color: ['#eee']
                  }
              }
          },

          valueAxis: {
              axisLine: {
                  lineStyle: {
                      color: '#408829'
                  }
              },
              splitArea: {
                  show: true,
                  areaStyle: {
                      color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
                  }
              },
              splitLine: {
                  lineStyle: {
                      color: ['#eee']
                  }
              }
          },
          timeline: {
              lineStyle: {
                  color: '#408829'
              },
              controlStyle: {
                  normal: {color: '#408829'},
                  emphasis: {color: '#408829'}
              }
          },

          k: {
              itemStyle: {
                  normal: {
                      color: '#68a54a',
                      color0: '#a9cba2',
                      lineStyle: {
                          width: 1,
                          color: '#408829',
                          color0: '#86b379'
                      }
                  }
              }
          },
          map: {
              itemStyle: {
                  normal: {
                      areaStyle: {
                          color: '#ddd'
                      },
                      label: {
                          textStyle: {
                              color: '#c12e34'
                          }
                      }
                  },
                  emphasis: {
                      areaStyle: {
                          color: '#99d2dd'
                      },
                      label: {
                          textStyle: {
                              color: '#c12e34'
                          }
                      }
                  }
              }
          },
          force: {
              itemStyle: {
                  normal: {
                      linkStyle: {
                          strokeColor: '#408829'
                      }
                  }
              }
          },
          chord: {
              padding: 4,
              itemStyle: {
                  normal: {
                      lineStyle: {
                          width: 1,
                          color: 'rgba(128, 128, 128, 0.5)'
                      },
                      chordStyle: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          }
                      }
                  },
                  emphasis: {
                      lineStyle: {
                          width: 1,
                          color: 'rgba(128, 128, 128, 0.5)'
                      },
                      chordStyle: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          }
                      }
                  }
              }
          },
          gauge: {
              startAngle: 225,
              endAngle: -45,
              axisLine: {
                  show: true,
                  lineStyle: {
                      color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                      width: 8
                  }
              },
              axisTick: {
                  splitNumber: 10,
                  length: 12,
                  lineStyle: {
                      color: 'auto'
                  }
              },
              axisLabel: {
                  textStyle: {
                      color: 'auto'
                  }
              },
              splitLine: {
                  length: 18,
                  lineStyle: {
                      color: 'auto'
                  }
              },
              pointer: {
                  length: '90%',
                  color: 'auto'
              },
              title: {
                  textStyle: {
                      color: '#333'
                  }
              },
              detail: {
                  textStyle: {
                      color: 'auto'
                  }
              }
          },
          textStyle: {
              fontFamily: 'Arial, Verdana, sans-serif'
          }
      };
        
        
        var echartPie = echarts.init(document.getElementById('echart_pie'), theme);
        
        
          var A1 = '<?php echo $login_no7[0]; ?>';
          var A2 = '<?php echo $login_no7[1]; ?>';
          var A3 = '<?php echo $login_no7[2]; ?>';
          var A4 = '<?php echo $login_no7[3]; ?>';
          var A5 = '<?php echo $login_no7[4]; ?>';
          var A6 = '<?php echo $login_no7[5]; ?>';
          var A7 = '<?php echo $login_no7[6]; ?>';
          var A8 = '<?php echo $login_no7[7]; ?>';
          var A9 = '<?php echo $login_no7[8]; ?>';
          var A10 = '<?php echo $login_no7[9]; ?>';
          var A11 = '<?php echo $login_no7[10]; ?>';
          var A12 = '<?php echo $login_no7[11]; ?>';
          var A13 = '<?php echo $login_no7[12]; ?>';
          var A14 = '<?php echo $login_no7[13]; ?>';
          var A15 = '<?php echo $login_no7[14]; ?>';
          var A16 = '<?php echo $login_no7[15]; ?>';

          var B1 = '<?php echo $momthly_state[0]; ?>';
          var B2 = '<?php echo $momthly_state[1]; ?>';
          var B3 = '<?php echo $momthly_state[2]; ?>';
          var B4 = '<?php echo $momthly_state[3]; ?>';
          var B5 = '<?php echo $momthly_state[4]; ?>';
          var B6 = '<?php echo $momthly_state[5]; ?>';
          var B7 = '<?php echo $momthly_state[6]; ?>';
          var B8 = '<?php echo $momthly_state[7]; ?>';
          var B9 = '<?php echo $momthly_state[8]; ?>';
          var B10 = '<?php echo $momthly_state[9]; ?>';
          var B11 = '<?php echo $momthly_state[10]; ?>';
          var B12 = '<?php echo $momthly_state[11]; ?>';
          var B13 = '<?php echo $momthly_state[12]; ?>';
          var B14 = '<?php echo $momthly_state[13]; ?>';
          var B15 = '<?php echo $momthly_state[14]; ?>';
          var B16 = '<?php echo $momthly_state[15]; ?>';

      echartPie.setOption({
        tooltip: {
          trigger: 'item',
          formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: [{
          width: '10%',
          x: '60%',
          y: 'middle',
          data: [B1, B2, B3, B4, B5, B6, B7, B8]
        },{
          width: '10%',
          x: '80%',
          y: 'middle',
          data: [B9, B10, B11, B12, B13, B14, B15, B16]
        }],
        toolbox: {
          show: true,
          feature: {
            magicType: {
              show: true,
              type: ['pie', 'funnel'],
              option: {
                funnel: {
                  x: '25%',
                  width: '50%',
                  funnelAlign: 'left',
                  max: 1548
                }
              }
            },
            restore: {
              show: true,
              title: "Restore"
            },
            saveAsImage: {
              show: true,
              title: "Save Image"
            }
          }
        },
        calculable: true,
        series: [{
          name: 'Negeri',
          type: 'pie',
          radius: '55%',
          center: ['30%', '60%'],
          data: [{
            value: A1,
            name: B1
          }, {
            value: A2,
            name: B2
          }, {
            value: A3,
            name: B3
          }, {
            value: A4,
            name: B4
          }, {
            value: A5,
            name: B5
          }, {
            value: A6,
            name: B6
          }, {
            value: A7,
            name: B7
          }, {
            value: A8,
            name: B8
          }, {
            value: A9,
            name: B9
          }, {
            value: A10,
            name: B10
          }, {
            value: A11,
            name: B11
          }, {
            value: A12,
            name: B12
          }, {
            value: A13,
            name: B13
          }, {
            value: A14,
            name: B14
          }, {
            value: A15,
            name: B15
          }, {
            value: A16,
            name: B16
          }]
        }]
      });

    </script>
    <!-- /lineChart -->
  <?php
  }
  ?>
	<script type="text/javascript">
      // Validating Empty Field
      function register_staff() {
        if (document.getElementById('name').value == "")
        {
          alert("Please insert staff name");
        }
        else
        {
          if (document.getElementById('phone_no').value == "")
          {
            alert("Please insert staff phone number");
          }
          else
          {
            if (document.getElementById('email').value == "")
            {
              alert("Please insert staff email address");
            }
            else
            {
              if (document.getElementById('address').value == "")
              {
                alert("Please insert staff address");
              }
              else
              {
                if (document.getElementById('username').value == "")
                {
                  alert("Please insert staff username");
                }
                else
                {
                  var phone = document.getElementById('phone_no').value;
                  var area1 = phone.substring(0, 3);
                  var area2 = phone.substring(0, 4);
                  var area3 = phone.substring(0, 1);
                  if (area1 == "+60") {
                    var extension = phone.substring(3);
                    if (extension.length == 9 || extension.length == 10)
                    {
                      var regExpObj = /^(?:0|\(?\+60\)?\s?|0060\s?)[0-9]{9}/;
                      if(regExpObj.exec(phone) == null)
                      {
                        alert("Not a valid phone number");
                      }
                      else
                      {
                        var x = document.getElementById('email').value;
                        var atpos = x.indexOf("@");
                        var dotpos = x.lastIndexOf(".");
                        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
                            alert("Not a valid e-mail address");
                        }
                        else
                        {
                          document.getElementById('form1').submit();
                        }
                      }
                    }
                    else
                    {
                      alert("Not a valid phone number");
                    }
                  }
                  else
                  {
                    if (area2 == "0060") {
                      var extension = phone.substring(4);
                      if (extension.length == 9 || extension.length == 10)
                      {
                        var regExpObj = /^(?:0|\(?\+60\)?\s?|0060\s?)[0-9]{9}/;
                        if(regExpObj.exec(phone) == null)
                        {
                          alert("Not a valid phone number");
                        }
                        else
                        {
                          var x = document.getElementById('email').value;
                          var atpos = x.indexOf("@");
                          var dotpos = x.lastIndexOf(".");
                          if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
                              alert("Not a valid e-mail address");
                          }
                          else
                          {
                            document.getElementById('form1').submit();
                          }
                        }
                      }
                      else
                      {
                        alert("Not a valid phone number");
                      }
                    }
                    else
                    {
                      if (area3 == "0") {
                        var extension = phone.substring(1);
                        if (extension.length == 9 || extension.length == 10)
                        {
                          var regExpObj = /^(?:0|\(?\+60\)?\s?|0060\s?)[0-9]{9}/;
                          if(regExpObj.exec(phone) == null)
                          {
                            alert("Not a valid phone number");
                          }
                          else
                          {
                            var x = document.getElementById('email').value;
                            var atpos = x.indexOf("@");
                            var dotpos = x.lastIndexOf(".");
                            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
                                alert("Not a valid e-mail address");
                            }
                            else
                            {
                              document.getElementById('form1').submit();
                            }
                          }
                        }
                        else
                        {
                          alert("Not a valid phone number");
                        }
                      }
                      else
                      {
                        alert("Not a valid phone number");
                      }
                    } 
                  }
                }
              }
            }
          }
        }
      }

      function check_empty() {
        var password = '<?php echo $password; ?>';
        if (document.getElementById('password').value == password) {
          document.getElementById('form1').submit();
        } else {
          if (document.getElementById('password').value == "") {
            alert("Insert Password!!!");
          }
          else{
          alert("Invalid Password!!!");
          }
        }
        document.getElementById('password').value = "";
      }
      function check_empty1() {
        var password = '<?php echo $password; ?>';
        if (document.getElementById('password1').value == password) {
          document.getElementById('form2').submit();
        } else {
          if (document.getElementById('password1').value == "") {
            alert("Insert Password!!!");
          }
          else{
          alert("Invalid Password!!!");
          }
        }
        document.getElementById('password1').value = "";
      }
      //Function To Display Popup
      function div_show() {
        if (document.getElementById('name').value == "")
        {
          alert("Please insert your name");
        }
        else
        {
          if (document.getElementById('phone_no').value == "")
          {
            alert("Please insert your phone number");
          }
          else
          {
            if (document.getElementById('email').value == "")
            {
              alert("Please insert your email address");
            }
            else
            {
              var phone = document.getElementById('phone_no').value;
              var area1 = phone.substring(0, 3);
              var area2 = phone.substring(0, 4);
              var area3 = phone.substring(0, 1);
              if (area1 == "+60") {
                var extension = phone.substring(3);
                if (extension.length == 9 || extension.length == 10)
                {
                  var regExpObj = /^(?:0|\(?\+60\)?\s?|0060\s?)[0-9]{9}/;
                  if(regExpObj.exec(phone) == null)
                  {
                    alert("Not a valid phone number");
                  }
                  else
                  {
                    var x = document.getElementById('email').value;
                    var atpos = x.indexOf("@");
                    var dotpos = x.lastIndexOf(".");
                    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
                        alert("Not a valid e-mail address");
                    }
                    else
                    {
                      document.getElementById('abc').style.display = "block";
                    }
                  }
                }
                else
                {
                  alert("Not a valid phone number");
                }
              }
              else
              {
                if (area2 == "0060") {
                  var extension = phone.substring(4);
                  if (extension.length == 9 || extension.length == 10)
                  {
                    var regExpObj = /^(?:0|\(?\+60\)?\s?|0060\s?)[0-9]{9}/;
                    if(regExpObj.exec(phone) == null)
                    {
                      alert("Not a valid phone number");
                    }
                    else
                    {
                      var x = document.getElementById('email').value;
                      var atpos = x.indexOf("@");
                      var dotpos = x.lastIndexOf(".");
                      if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
                          alert("Not a valid e-mail address");
                      }
                      else
                      {
                        document.getElementById('abc').style.display = "block";
                      }
                    }
                  }
                  else
                  {
                    alert("Not a valid phone number");
                  }
                }
                else
                {
                  if (area3 == "0") {
                    var extension = phone.substring(1);
                    if (extension.length == 9 || extension.length == 10)
                    {
                      var regExpObj = /^(?:0|\(?\+60\)?\s?|0060\s?)[0-9]{9}/;
                      if(regExpObj.exec(phone) == null)
                      {
                        alert("Not a valid phone number");
                      }
                      else
                      {
                        var x = document.getElementById('email').value;
                        var atpos = x.indexOf("@");
                        var dotpos = x.lastIndexOf(".");
                        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
                            alert("Not a valid e-mail address");
                        }
                        else
                        {
                          document.getElementById('abc').style.display = "block";
                        }
                      }
                    }
                    else
                    {
                      alert("Not a valid phone number");
                    }
                  }
                  else
                  {
                    alert("Not a valid phone number");
                  }
                } 
              }
            }
          }
        }
      }

      function div_show1() {
        if (document.getElementById('new_password').value == "")
        {
          alert("Please insert new password");
        }
        else
        {
          if (document.getElementById('cnew_password').value == "")
          {
            alert("Please insert confirm new password");
          }
          else
          {
            if (document.getElementById('new_password').value !== document.getElementById('cnew_password').value) {
              alert("The new password does not match");
            }
            else
            {
              document.getElementById('abcd').style.display = "block";
            }

          }
        }
      }
      //Function to Hide Popup
      function div_hide(){
        document.getElementById('abc').style.display = "none";
      }
      function div_hide1(){
        document.getElementById('abcd').style.display = "none";
      }
    </script>
    <?php
    if ($action=="Analysis") {
    ?>
	<!-- morris.js -->
    <script src="../vendors/raphael/raphael.min.js"></script>
    <script src="../vendors/morris.js/morris.min.js"></script>
    <script type="text/javascript">
      var kategori = $('#category').val();
      var negeri = $('#negeri').val();
      // alert(negeri);

      $('#kementerian').html('');
      $('#kementerian')
        .append($("<option></option>")
        .attr("value",'-1')
        .text('All'));
      var kem = [];
      var kem1 = [];
      var kem_no = [];
      var data = [];
      var neg = [];
      var neg_no = [];
      var data1 = [];

      kementerian();
      
      function kementerian(){
        var info = $.post('https://www.gis.myetapp.gov.my/myetappgis/gentelella-master/production/all_kementerian.php', { kategori: kategori, negeri: negeri});
      
        info.done(function(json_data) {
          var info_json = $.parseJSON(json_data);
          $.each(info_json, function(key, val){
            kem.push(val);
            $('#kementerian')
              .append($("<option></option>")
              .attr("value",val)
              .text(val));
            if (val == 'JABATAN PERDANA MENTERI') {
              val = 'JPM';
            }
            if (val == 'KEMENTERIAN KESIHATAN') {
              val = 'MOH';
            }
            if (val == 'KEMENTERIAN PELAJARAN') {
              val = 'MOE';
            }
            if (val == 'KEMENTERIAN PENGANGKUTAN') {
              val = 'MOT';
            }
            if (val == 'KEMENTERIAN PERTAHANAN') {
              val = 'MOD';
            }
            if (val == 'KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI') {
              val = 'MOA';
            }
            if (val == 'KEMENTERIAN PERUMAHAN DAN KERAJAAN TEMPATAN') {
              val = 'KPKT';
            }
            if (val == 'KEMENTERIAN TENAGA, TEKNOLOGI HIJAU DAN AIR') {
              val = 'KETTHA';
            }
            if (val == 'KEMENTERIAN SUMBER ASLI DAN ALAM SEKITAR') {
              val = 'NRE';
            }
            if (val == 'KEMENTERIAN KERJARAYA') {
              val = 'KKR';
            }
            if (val == 'KEMENTERIAN SAINS, TEKNOLOGI DAN INOVASI') {
              val = 'MOSTI';
            }
            if (val == 'KEMENTERIAN KEBUDAYAAN. KESENIAN DAN WARISAN') {
              val = 'JKKN';
            }
            if (val == 'KEMENTERIAN KEWANGAN') {
              val = 'MOF';
            }
            if (val == 'KEMENTERIAN PENGAJIAN TINGGI') {
              val = 'KPT';
            }
            if (val == 'KEMENTERIAN KEMAJUAN LUAR BANDAR DAN WILAYAH') {
              val = 'KKLW';
            }
            if (val == 'KEMENTERIAN HAL EHWAL DALAM NEGERI') {
              val = 'MOHA';
            }
            if (val == 'KEMENTERIAN KESELAMATAN DALAM NEGERI') {
              val = 'KKDN';
            }
            if (val == 'KEMENTERIAN WILAYAH PERSEKUTUAN') {
              val = 'KWP';
            }
            if (val == 'KEMENTERIAN PERUMAHAN DAN KERAJAAN TEMPATAN') {
              val = 'KPKT';
            }
            if (val == 'KEMENTERIAN BELIA DAN SUKAN') {
              val = 'KBS';
            }
            if (val == 'KEMENTERIAN PEMBANGUNAN WANITA. KELUARGA DAN MASYARAKAT' || val == 'KEMENTERIAN PEMBANGUNAN WANITA DAN KELUARGA') {
              val = 'KPWKM';
            }
            if (val == 'KEMENTERIAN TENAGA, AIR & KOMUNIKASI') {
              val = 'KTAK';
            }
            if (val == 'KEMENTERIAN PENERANGAN') {
              val = 'KKMM';
            }
            if (val == 'KEMENTERIAN PERDAGANGAN ANTARABANGSA DAN INDUSTRI') {
              val = 'MITI';
            }
            if (val == 'KEMENTERIAN PERDAGANGAN DALAM NEGERI DAN HAL EHWAL PENGGUNA') {
              val = 'KPDNKK';
            }
            if (val == 'KEMENTERIAN PELANCONGAN') {
              val = 'MOTAC';
            }
            if (val == 'KEMENTERIAN PERUSAHAAN, PERLADANGAN DAN KOMUDITI') {
              val = 'KPPK';
            }
            if (val == 'KEMENTERIAN SUMBER MANUSIA') {
              val = 'MOHR';
            }
            if (val == 'KEMENTERIAN LUAR NEGERI') {
              val = 'KLN';
            }
            if (val == 'KEMENTERIAN PEMBANGUNAN, USAHAWAN DAN KOPERASI') {
              val = 'MECD';
            }

            kem1.push(val);
          });
          kementerian_no(kem);
        });
      }
      
      function kementerian_no(val){
        var info_no = $.post('https://www.gis.myetapp.gov.my/myetappgis/gentelella-master/production/kementerian_no.php', { kategori: kategori, kementerian: val, negeri: negeri });
        info_no.done(function(json_data1) {
          var info_no_json = $.parseJSON(json_data1);
          $.each(info_no_json, function(key1, val1){
            kem_no.push(val1);
          });
          for(i=0;i<kem_no.length;i++){
            data.push({device: kem1[i], geekbench: kem_no[i]});
          }
          Morris.Bar({
            element: 'graph_bar_kementerian',
            data: data,
            xkey: 'device',
            ykeys: ['geekbench'],
            labels: ['KEMENTERIAN'],
            xLabelMargin: 10,
            barRatio: 0.4,
            barColors: function (row, series, type) {
              if (type === 'bar') {
                var blue = Math.ceil(255 * row.y / this.ymax);
                return 'rgb(0,102,'+ blue +')';
              }
              else {
                return '#000';
              }
            },
            xLabelAngle: 75,
            hideHover: 'auto',
            resize: true
          });
          all_negeri();
        });
      }


      function all_negeri(){
        var info_negeri = $.post('https://www.gis.myetapp.gov.my/myetappgis/gentelella-master/production/all_negeri.php', { kategori: kategori});
      
        info_negeri.done(function(json_data2) {
          var info_negeri_json = $.parseJSON(json_data2);
          $.each(info_negeri_json, function(key2, val2){
            neg.push(val2);
          });
          negeri_no(neg);
        });
      }

      function negeri_no(val){
        var info_negeri_no = $.post('https://www.gis.myetapp.gov.my/myetappgis/gentelella-master/production/negeri_no.php', { kategori: kategori, negeri: val});
        info_negeri_no.done(function(json_data3) {
          var info_negeri_no_json = $.parseJSON(json_data3);
          $.each(info_negeri_no_json, function(key3, val3){
            neg_no.push(val3);
          });
          for(i=0;i<neg_no.length;i++){
            data1.push({device: neg[i], geekbench: neg_no[i]});
          }
          Morris.Bar({
            element: 'graph_bar_negeri',
            data: data1,
            xkey: 'device',
            ykeys: ['geekbench'],
            labels: ['NEGERI'],
            xLabelMargin: 100,
            barRatio: 0.4,
            barColors: function (row, series, type) {
              if (type === 'bar') {
                var blue = Math.ceil(255 * row.y / this.ymax);
                return 'rgb(0,102,'+ blue +')';
              }
              else {
                return '#000';
              }
            },
            xLabelAngle: 75,
            hideHover: 'auto',
            resize: true
          });
          $('#graph_bar_negeri').find('text').attr('font-size', '6px');
        });
      }
        

      $('#category').change(function () {
        var kategori = $('#category').val();
        var negeri = $('#negeri').val();
        // alert(negeri);

        $('#kementerian').html('');
        $('#kementerian')
          .append($("<option></option>")
          .attr("value",'-1')
          .text('All'));
        var info = $.post('https://www.gis.myetapp.gov.my/myetappgis/gentelella-master/production/all_kementerian.php', { kategori: kategori, negeri: negeri});

        info.done(function(json_data) {
          var info_json = $.parseJSON(json_data);

          $.each(info_json, function(key, val){
            $('#kementerian')
              .append($("<option></option>")
              .attr("value",val)
              .text(val));
          });
        });
      })

      function validateForm(){
        var date = $('#datepicker').val();
        var type = $('#type').val();

        if ((date == '' && type == '-1') || (date != '' && type != '-1')) {
          return true;
        }else{
          alert('Sila tandakan Tarikh/Carian mengikut untuk membuat carian !!!');
          return false;
        }
      }
    </script>
    <script type="text/javascript">

      var theme = {
          color: [
			  //  ft, qt, rizab, permohonan
			  '#26B99A', '#d9534f', '#f0ad4e', '#3498DB'
          ],

          title: {
              itemGap: 8,
              textStyle: {
                  fontWeight: 'normal',
                  color: '#408829'
              }
          },

          dataRange: {
              color: ['#1f610a', '#97b58d']
          },

          toolbox: {
              color: ['#408829', '#408829', '#408829', '#408829']
          },

          tooltip: {
              backgroundColor: 'rgba(0,0,0,0.5)',
              axisPointer: {
                  type: 'line',
                  lineStyle: {
                      color: '#408829',
                      type: 'dashed'
                  },
                  crossStyle: {
                      color: '#408829'
                  },
                  shadowStyle: {
                      color: 'rgba(200,200,200,0.3)'
                  }
              }
          },

          dataZoom: {
              dataBackgroundColor: '#eee',
              fillerColor: 'rgba(64,136,41,0.2)',
              handleColor: '#408829'
          },
          grid: {
              borderWidth: 0
          },

          categoryAxis: {
              axisLine: {
                  lineStyle: {
                      color: '#408829'
                  }
              },
              splitLine: {
                  lineStyle: {
                      color: ['#eee']
                  }
              }
          },

          valueAxis: {
              axisLine: {
                  lineStyle: {
                      color: '#408829'
                  }
              },
              splitArea: {
                  show: true,
                  areaStyle: {
                      color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
                  }
              },
              splitLine: {
                  lineStyle: {
                      color: ['#eee']
                  }
              }
          },
          timeline: {
              lineStyle: {
                  color: '#408829'
              },
              controlStyle: {
                  normal: {color: '#408829'},
                  emphasis: {color: '#408829'}
              }
          },

          k: {
              itemStyle: {
                  normal: {
                      color: '#68a54a',
                      color0: '#a9cba2',
                      lineStyle: {
                          width: 1,
                          color: '#408829',
                          color0: '#86b379'
                      }
                  }
              }
          },
          map: {
              itemStyle: {
                  normal: {
                      areaStyle: {
                          color: '#ddd'
                      },
                      label: {
                          textStyle: {
                              color: '#c12e34'
                          }
                      }
                  },
                  emphasis: {
                      areaStyle: {
                          color: '#99d2dd'
                      },
                      label: {
                          textStyle: {
                              color: '#c12e34'
                          }
                      }
                  }
              }
          },
          force: {
              itemStyle: {
                  normal: {
                      linkStyle: {
                          strokeColor: '#408829'
                      }
                  }
              }
          },
          chord: {
              padding: 4,
              itemStyle: {
                  normal: {
                      lineStyle: {
                          width: 1,
                          color: 'rgba(128, 128, 128, 0.5)'
                      },
                      chordStyle: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          }
                      }
                  },
                  emphasis: {
                      lineStyle: {
                          width: 1,
                          color: 'rgba(128, 128, 128, 0.5)'
                      },
                      chordStyle: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          }
                      }
                  }
              }
          },
          gauge: {
              startAngle: 225,
              endAngle: -45,
              axisLine: {
                  show: true,
                  lineStyle: {
                      color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                      width: 8
                  }
              },
              axisTick: {
                  splitNumber: 10,
                  length: 12,
                  lineStyle: {
                      color: 'auto'
                  }
              },
              axisLabel: {
                  textStyle: {
                      color: 'auto'
                  }
              },
              splitLine: {
                  length: 18,
                  lineStyle: {
                      color: 'auto'
                  }
              },
              pointer: {
                  length: '90%',
                  color: 'auto'
              },
              title: {
                  textStyle: {
                      color: '#333'
                  }
              },
              detail: {
                  textStyle: {
                      color: 'auto'
                  }
              }
          },
          textStyle: {
              fontFamily: 'Arial, Verdana, sans-serif'
          }
      };

      var echartService = echarts.init(document.getElementById('echart_service'), theme);

      var permohonan_count = '<?php echo $permohonan_count; ?>';
      var qt_count = '<?php echo $qt_count; ?>';
      var ft_count = '<?php echo $ft_count; ?>';
      var rezab_count = '<?php echo $rizab_count; ?>';

      echartService.setOption({
        tooltip: {
          trigger: 'item',
          formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: [{
          x: 'center',
          y: 'bottom',
			data: ["PERMOHONAN", "QT", "FT", "RIZAB"]
        }],
        toolbox: {
          show: true,
          feature: {
            magicType: {
              show: true,
              type: ['pie', 'funnel'],
              option: {
                funnel: {
                  x: '25%',
                  width: '50%',
                  funnelAlign: 'left',
                  max: 1548
                }
              }
            },
            restore: {
              show: true,
              title: "Restore"
            },
            saveAsImage: {
              show: true,
              title: "Save Image"
            }
          }
        },
        calculable: true,
        series: [{
          name: 'Service',
          type: 'pie',
          radius: '55%',
          center: ['55%', '48%'],
          data: [{
            value: qt_count,
            name: "QT"
          }, {
            value: ft_count,
            name: "FT"
          }, {
            value: rezab_count,
            name: "RIZAB"
          }, {
            value: permohonan_count,
            name: "PERMOHONAN"
          }]
        }]
      });
    </script>
	<script>
      $(function() {
        $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
      });
    </script>
    <?php
    }
    ?>
  </body>
</html>