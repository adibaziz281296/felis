<?php
//session_start();
$state = $_SESSION['state'];
$username = $_SESSION['username'];
$category = $_SESSION['category'];
// echo $state;
include('../sql_server_db.php');

$sql = "SELECT * FROM location WHERE state = '$state'";

$result = sqlsrv_query( $conn, $sql);
if( $result === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

while($row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )){	
	$latitude = $row['latitude'];
	$longitude = $row['longitude'];
	$zoom = $row['zoom'];
}


$phpToJsVars = array(
  'value1' => $latitude,
  'value2' => $longitude,
  'value3' => $zoom
   );
?>
<!DOCTYPE html>
<html>
  <head>
  	<title>MyeTaPP-GIS</title>
    <meta charset="utf-8">
    <!--The viewport meta tag is used to improve the presentation and behavior of the samples 
      on iOS devices-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	
	<!-- Bootstrap core CSS -->
	<link href="assets/css/gis/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Custom styles for this template -->
	<link href="assets/css/gis/simple-sidebar.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="toc/2.10/src/agsjs/css/agsjs.css" />
	
	
	
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//esri.github.io/bootstrap-map-js/src/css/bootstrapmap.css">


	<style>
		
		#slideout {				
			position: fixed;
			top:20% !important; 
			right: 20px;
			z-index: 50;
			width: 35px;
			padding: 12px 0;
			text-align: center;
			-webkit-transition-duration: 0.3s;
			-moz-transition-duration: 0.3s;
			-o-transition-duration: 0.3s;
			transition-duration: 0.3s;
			-webkit-border-radius: 0 5px 5px 0;
			-moz-border-radius: 0 5px 5px 0;
			border-radius: 0 5px 5px 0;
		}
		#slideout_inner {
			position: fixed;
			top:20% !important;
			right: -250px;
			width: 220px;
			height: 150px;
			-webkit-transition-duration: 0.3s;
			-moz-transition-duration: 0.3s;
			-o-transition-duration: 0.3s;
			transition-duration: 0.3s;
			text-align: left;
			-webkit-border-radius: 0 0 5px 0;
			-moz-border-radius: 0 0 5px 0;
			border-radius: 0 0 5px 0;
		}
		
          .show:focus + .hide {display: inline; }
          .show:focus + .hide + #slideout_inner {display: block;}

		#slideout:hover {
			right: 225px; <!-- gap antara icon & slider -->
		}
		#slideout:hover #slideout_inner {
			right: 0px; <!-- jarak dr kanan -->
		}
		
		#slideout_inner:focus{
			right: 0px; <!-- jarak dr kanan -->
			display: block;
		}
		

		</style>
	<link rel="stylesheet" type="text/css" href="css/accordian.css" />  

	
    <link rel="stylesheet" href="http://js.arcgis.com/3.14/dijit/themes/claro/claro.css">
    <link rel="stylesheet" href="http://js.arcgis.com/3.14/esri/css/esri.css" />
	
	
<script>
  $(function() {
    $( "#accordion" ).accordion({
      heightStyle: "content"
    });
	
	$('.disabled .ui-accordion-header').addClass('ui-state-disabled').on('click', function () {
        return false;
     });
  });
  
    
  	function confirmAction(){
		
      var confirmed = confirm("Are you sure you want to delete this project?");
      return confirmed;
	}
	
	function confirmEdit(){
		
      var confirmed = confirm("Are you sure you want to edit this project?");
      return confirmed;
	}
	
	function newProject()
	{
		
		
		if(confirm('Are you sure you want to create a new project?')==true)
		{			
			window.location = "http://localhost/geoiskandar/IRDA_online_edit2/online_edit.php?project_id=0";
			return true;
		}
		else
		{
			return false;
		} 

	}
	
	
	
  	function confirmSave(){
      		alert("Your project has been successfully saved.");
	}
	
	function usr_logout2()
	{
		
		
		if(confirm('Have you save your project? Save your project before logout')==true)
		{
			window.location = "logout.php";
			return true;
		}
		else
		{
			return false;
		} 

		
		
	
	}
  
</script> 	
    
    <style>
    	@import url("http://js.arcgis.com/3.10/js/dojo/dijit/themes/claro/claro.css");
		  @import url("http://archive.dojotoolkit.org/nightly/dojotoolkit/dojox/layout/resources/FloatingPane.css"); 
		  @import url("http://archive.dojotoolkit.org/nightly/dojotoolkit/dojox/layout/resources/ResizeHandle.css");	
		  @import url("http://ajax.googleapis.com/ajax/libs/dojo/1.6/dojox/grid/enhanced/resources/claro/EnhancedGrid.css");
      html,body{height:100%;width:100%;margin:0;overflow:hidden;}
      #map{
        padding:0;
      }
      .templatePicker {
        border: none;
      }

      .dj_ie .infowindow .window .top .right .user .content { position: relative; }
      .dj_ie .simpleInfoWindow .content {position: relative;}
	  
	  .dgrid { border: none; height: 100%; }
      .dgrid-column-0 {
        width: 35px;
      }
      .dgrid-row-odd {
        background: #FFFDF3;
      }
      td div img:hover {
        cursor: pointer;
      }
    </style>
    <!-- Custom info popup style to overwrite the default info popup style -->
    <link rel="stylesheet" type="text/css" href="assets/css/gis/modernGrey.css">
	
	<!-- Custom widget style to overwrite the default widget style -->
	<link rel="stylesheet" type="text/css" href="assets/css/gis/widget.css">
	
	<!--for uploading flood report file -->
	<script language="javascript" src="report/funcs/val.js"></script>
    <script>
		var dojoConfig = {
		isDebug: true,
		parseOnLoad: true,
		foo: "bar"
		};
	</script> 
	<!-- Load dojo and provide config via data attribute for TOC -->
	<script type="text/javascript">
		// Registers the correct location of the "demo" package so we can load Dojo
		var dojoConfig = { 
			paths: {
				//if you want to host on your own server, download and put in folders then use path like: 
				agsjs: location.pathname.replace(/\/[^/]+$/, '') + '/toc/2.10/src/agsjs' 
				
			}
		};      
	</script>
    <script type="text/javascript">
	var phpVars = { 
	<?php 
	  foreach ($phpToJsVars as $key => $value) {
	    echo '  ' . $key . ': ' . '"' . $value . '",' . "\n";  
	  }
	?>
	};
	</script>
	<script src="//ajax.googleapis.com/ajax/libs/dojo/1.6.2/dojo/dojo.xd.js"></script>

	<!-- Reference for StreetView -->
	<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp" type="text/javascript"></script>  -->
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyDEfWbEK3OTIRtSJhxCW_AmerJQOu9asdw" type="text/javascript"></script>
	
    <!-- Reference the ArcGIS API for JavaScript -->
    <script src="http://js.arcgis.com/3.14/"></script>
  </head>
  <body class="claro">
  
  	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
		  <!-- <ul class="nav navbar-nav navbar-right">
            <li><a ref="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="login-btn"><i class="fa fa-user white"></i>&nbsp;&nbsp;Daftar Masuk</a></li>
          </ul> -->	  
          <ul class="nav navbar-nav">
		   	<li class="dropdown">
				<a  href="#" role="button"  id="basic_legend" style="font-size: 13px;"><i class="fa fa-bars icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Legend Map</a>
			</li>
          </ul>
		  <input type="hidden" id="username" type="hidden" value="<?php echo $username; ?>" name="username"/><br/>  
		  <input type="hidden" id="state" value="<?php echo $state; ?>" name="state"/><br/>  
        </div>
      </div>
    </nav> 

    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#"><img src="assets/img/arrow.png" width="32px" height="29px" alt="Tutup petunjuk peta"></a>
                </li>
				<li>
					<a href="#">Online Services</a>
                    <div id="tocDiv"></div>
                </li>
				<br>
            </ul>
			
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
		<div id="main" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design:'headline'" style="width:100%;height:100%;">
		  <!--?php if ($category == 'Admin'){ ?>
		  <div data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'left'" style="width: 100px;overflow:hidden;">
	        <div id="grid"></div>
	      </div>
		   <1?php } ?-->
		
	      <?php if ($category == 'Staff'){ ?>
		  <div data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'left'" style="width: 100px;overflow:hidden;">
	        <div id="templateDiv"></div>
	        <div id="editorDiv"></div>
	      </div>
		  <?php } ?>
	      <div data-dojo-type="dijit/layout/ContentPane" id="map" data-dojo-props="region:'center'">
			<div id="HomeButton"></div>
			<div id="LocateButton"></div>
			<div id="EraseButton" title="Erase graphic"></div>
			<div id="BasemapToggle"></div>
			<span id="coordinate_info" style="color: black;"></span>
			<img id="loadingImg" src="loader/loader.gif" style="position:absolute; right:612px; top:356px; z-index:100;" />
		  </div>
			<?php if ($category == 'Staff'){ ?>
				<div class="panel panel-primary panel-fixed">
					<div class="panel-heading">
					  <h3 class="panel-title">Feedback Form</h3>
					</div>
					<div class="panel-body">
						<form name="coordinate" method="post" onSubmit="return valSubmit(this, 'online_edit.php');">
						<div class="form-group">
							<label for="formGroupExampleInput">Title:</label>
							<textarea  style="font-size:10px;" class="form-control" type="text" id="title" name="title" value="" cols="30" rows="4" maxlength="100" autocomplete="on" class="input" onkeyup="showChar(this);" onblur="showChar(this);"></textarea>	
						  </div>
						  
						  <div class="form-group">
							<input name="submit" style="font-size:10px; font-weight:bold" id="submitbtn" type="submit" value="SUBMIT" align="center" width="40px;" class="button" onclick="return confirmSave()"/>   
							<!--button id="btnSolveRoute" class="btn btn-success">Save</button>
							<button id="btnClearRoutes" class="btn btn-default">Clear</button-->
						  </div>
						  <div id="userMessage">Send your feedback here.</div>
						   </form> 
					 
					</div>
				  </div>
				<?php } ?>
	    </div>
		
<!--         <div id="page-content-wrapper">
            <!-- <div class="container-fluid-2"> -->
                <!-- <div class="row"> -->
                    <!-- <div class="col-lg-12">  
					<div id="map"></div>	
                        <!-- <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a> -->
                    <!-- </div> -->
                <!-- </div> -->
            <!-- </div> 
       </div>  -->
        <!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
	
	<?php if ($category == 'Admin'){ ?>
	<div id="slideout">
				<a href="#reset" class="show"><img src="src/icons/i_widget.png"  /></a> 
				<div id="slideout_inner">	
					<div id="contain">
					
						<!--div id="legendDiv"></div-->
					
						<div class="accordion">
							<!-- SEARCH KM POINT FORM -->
							<div id="kmpoint">
								<a href="#kmpoint" class="tab">Legend</a>
								<div class="content">
									<div data-dojo-type="dijit/layout/ContentPane" style="width:200px; height:185px; overflow:auto;">
										
<form name="coordinate" method="post" onSubmit="return valSubmit(this, 'online_edit.php');">
				
				<input id="xmin" name="xmin"  type="hidden">
                <input id="ymin" name="ymin"  type="hidden">
                <input id="xmax" name="xmax"  type="hidden">
                <input id="ymax" name="ymax"  type="hidden">
				
            	<label>
                <span style="font-size:10px; font-weight:bold"><font color="#000000;">Author :</font></span>
				</label>
				<label style="font-size:10px; font-weight:bold"><font color="#000000"><?php echo $username ?></font>
                </label>
                <p>

<script type="text/javascript">

var max=0;
function showChar(obj)
{
var obj3=document.getElementById("char");
if(max==0)
{
max=obj3.firstChild.nodeValue*1;
}
len=obj.value.length;
var cur=max*1;
cur=cur-len;
if(cur<0)
{
var obj2=document.forms[0].elements['comm'];
var str=obj2.value.substring(0,max*1);
obj2.value=str;
showChar(obj2,max);
return false;
}
else
{
var obj2=document.getElementById('char');
var str=document.createTextNode(cur);
obj2.replaceChild(str,obj2.firstChild);
return true;
}
}

function showChar2(obj)
{
var obj3=document.getElementById("char2");
if(max==0)
{
max=obj3.firstChild.nodeValue*1;
}
len=obj.value.length;
var cur=max*1;
cur=cur-len;
if(cur<0)
{
var obj2=document.forms[0].elements['comm'];
var str=obj2.value.substring(0,max*1);
obj2.value=str;
showChar(obj2,max);
return false;
}
else
{
var obj2=document.getElementById('char2');
var str=document.createTextNode(cur);
obj2.replaceChild(str,obj2.firstChild);
return true;
}
}

function showChar3(obj)
{
var obj3=document.getElementById("char3");
if(max==0)
{
max=obj3.firstChild.nodeValue*1;
}
len=obj.value.length;
var cur=max*1;
cur=cur-len;
if(cur<0)
{
var obj2=document.forms[0].elements['comm'];
var str=obj2.value.substring(0,max*1);
obj2.value=str;
showChar(obj2,max);
return false;
}
else
{
var obj2=document.getElementById('char3');
var str=document.createTextNode(cur);
obj2.replaceChild(str,obj2.firstChild);
return true;
}
}

function showChar4(obj)
{
var obj3=document.getElementById("char4");
if(max==0)
{
max=obj3.firstChild.nodeValue*1;
}
len=obj.value.length;
var cur=max*1;
cur=cur-len;
if(cur<0)
{
var obj2=document.forms[0].elements['comm'];
var str=obj2.value.substring(0,max*1);
obj2.value=str;
showChar(obj2,max);
return false;
}
else
{
var obj2=document.getElementById('char4');
var str=document.createTextNode(cur);
obj2.replaceChild(str,obj2.firstChild);
return true;
}
}

</script>
				<label>
                <span style="font-size:10px; font-weight:bold"><font color="#000000">Project Name *:</font></span><br>
                <textarea  style="font-size:10px;" type="text" id="title" name="title" value="" cols="30" rows="2" maxlength="100" autocomplete="on" class="input" onkeyup="showChar(this);" onblur="showChar(this);"></textarea>
                </label>
				<label>
				<p>
                <span style="font-size:10px; font-weight:bold"><font color="#000000">Description :</font></span><br>
                <textarea  style="font-size:10px;" type="text" id="des" name="des" value="" cols="30" rows="2" maxlength="100" autocomplete="on" class="input" onkeyup="showChar2(this);" onblur="showChar2(this);"></textarea>
                </label>
				<p>
               
              <input name="submit" style="font-size:10px; font-weight:bold" id="submitbtn" type="submit" value="SUBMIT" align="center" width="40px;" class="button" onclick="return confirmSave()"/>                         
                    
          </form> 	
										
										
	
									</div>
								</div>
							</div>
							<!-- SEARCH BLOCK VALVE FORM -->
							<div id="blockvalve">
								<a href="#blockvalve" class="tab">Basemap</a>
								<div class="content">
									  <div data-dojo-type="dijit/layout/ContentPane" style="width:200px; height:185px; overflow:auto;">
											<div id="basemapGallery"></div>
									  </div>					
								</div>
							</div>

						
						</div>
					</div>	
				</div>
			</div>
	<?php } ?>		
			

    <!-- =========== Bootstrap core JavaScript =========== -->
    <!-- Placed at the end of the document so the pages load faster -->	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/css/gis/bootstrap.min.js"></script>
    
    <?php if ($category == 'Admin'){ ?>
	<script type="text/javascript" src="map_admin.js"></script>
	<?php } ?>
	<?php if ($category == 'Staff'){ ?>
	<script type="text/javascript" src="map_staff.js"></script>
	<?php } ?>

    	<!-- Reference the Query/Search JavaScript functions -->
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>