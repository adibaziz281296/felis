// =============================================================================================
//								VALIDATE UPLOAD FILE FORM
// =============================================================================================
function jsUpload(field){    
		var re_text =/\.zip|\.rar/i;  
		var filename = field.value;    
		
		/* Checking file type */    
		if (filename.search(re_text) == -1) {         
			alert("Pastikan format fail adalah betul\n(fail .zip yang mengandungi fail .shp & berkaitan).");  
			inputHolder.innerHTML = "";
			inputHolder.innerHTML ="<input name=\"userfile[]\" type=\"file\" multiple onChange=\"jsUpload(this)\">";
			//field.focus(); 
			return false;    
		}    
		
		return true;
}

function val_add(form, succurl) {
	  var theMessage = "Sila lengkapkan maklumat berikut: \n--------------------------------------------\n";
	  var noErrors = theMessage
	  
	  if (form.report_name.value =="") {
		  theMessage = theMessage + "\n -> Tajuk Laporan";
	  }
	  
	  if (form.fileSelect.value =="") {
		  theMessage = theMessage + "\n -> Fail untuk dimuatnaik ";
	  }


	  // END CHECKING
	  if (theMessage == noErrors) {
			  document.form.action=succurl; 
	  } 
	  else {
		  alert(theMessage);
		  return false;
	  }
} // close func



// =============================================================================================
//								DATA: VALIDATE EDIT FORM
// =============================================================================================
function val_edit(form, succurl) {
	var theMessage = "Sila lengkapkan maklumat berikut: \n--------------------------------------------\n";
	var noErrors = theMessage
	
	if (form.tajuk.value =="") {
		theMessage = theMessage + "\n -> Tajuk ";
	}
	
	if (form.kataKunci.value =="") {
		theMessage = theMessage + "\n -> Kata Kunci ";
	}
	
	var listCheck = form.statID.selectedIndex;
	if (form.statID.options[listCheck].value=="-") {
			theMessage = theMessage + "\n -> Status ";
	}
	
	// END CHECKING
	if (theMessage == noErrors) {
			document.form.action=succurl; 
	} 
	else {
		alert(theMessage);
		return false;
	}
}

// =============================================================================================
//								OTHER FUNCTIONS
// =============================================================================================

function openPage(url) {
	window.location = url;
	}
	
	function wopen(url, name) {
		var win = window.open(url,
			name, 
				'width=' + screen.width  + ', height=' + screen.height + ', ' +
				'location=no, menubar=no, status=no, toolbar=no, ' +
				'scrollbars=yes, resizable=yes, directories=no');

		win.moveTo(0, 0);
		win.resizeTo(screen.width, screen.height);
		win.focus();
	}
	

function allRec(form) { 
	var succurl = 'list.php';
	
	form.kword_cat.value = 0;
	form.kword_dae.value = 0;
	form.kword_desc.value = "";
	
	document.form.action="list.php?kcat=0&kdae=0&kdesc=";
	document.form.submit();
}

function allReset(form) { 
	form.kword_cat.value = 0;
	form.kword_dae.value = 0;
	form.kword_desc.value = "";
}