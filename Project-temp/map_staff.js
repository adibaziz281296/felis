
var ipaddress = 'www.gis.myetapp.gov.my';
//"dojo.parser" -- Required if you have dojo widgets (aka. dijits)in your HTML markup 

var username = document.getElementById("username").value; 
var state = document.getElementById("negeri").value;  



$("#basic_legend").click(function() {
  $("#wrapper").toggleClass("toggled");
  return false;
});

$(".sidebar-brand").click(function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
});

$("#menu-toggle").click(function(e) {
	$("#wrapper").toggleClass("toggled");
});

var map,symbol, geomTask;
var gviewClient, Pano, Glocator;
var viewPoint;	
var viewStatus = false;
var viewAddress = "";
      
    require([

        "esri/map", 
        "esri/tasks/GeometryService",
        "esri/geometry/Extent",
        "esri/toolbars/edit",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "agsjs/dijit/TOC",
		"esri/toolbars/draw",
        "esri/toolbars/edit",
		"esri/request",
		"esri/config",
		
		"dojo/on",
        "dojo/dom",
		"esri/dijit/HomeButton",
		"esri/geometry/webMercatorUtils",
		"esri/dijit/Scalebar",
		"esri/dijit/LocateButton",
		"esri/dijit/BasemapToggle", 
		"esri/layers/ImageParameters",
		
        "esri/layers/ArcGISTiledMapServiceLayer",
        "esri/layers/FeatureLayer",
        "esri/InfoTemplate", "esri/tasks/QueryTask",
        "esri/tasks/query",
        "esri/urlUtils",
        "esri/geometry/Point",
        "esri/tasks/IdentifyTask",
        "esri/tasks/IdentifyParameters",
        "esri/Color",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/dijit/Popup",	
		"esri/dijit/PopupTemplate",				
		"esri/symbols/SimpleFillSymbol",
		"esri/symbols/SimpleLineSymbol",

        "esri/dijit/editing/Editor",
        "esri/dijit/editing/TemplatePicker",

        "esri/request",
        "esri/config",
        "dojo/i18n!esri/nls/jsapi",

        "dojo/_base/array", "dojo/_base/connect", "dojo/parser", "dojo/keys", "dojo/dom-class", "dojo/dom-construct",

        "dijit/layout/BorderContainer", "dijit/layout/ContentPane",  
        "dojo/domReady!"
      ], function(

        Map, GeometryService, Extent, Edit,  ArcGISDynamicMapServiceLayer, TOC, Draw, Edit, esriRequest, esriConfig, on, dom, HomeButton, webMercatorUtils, Scalebar, LocateButton, BasemapToggle, ImageParameters,
        ArcGISTiledMapServiceLayer, FeatureLayer, InfoTemplate, QueryTask, Query,
        urlUtils, Point, IdentifyTask, IdentifyParameters,
        Color, SimpleMarkerSymbol, Popup, PopupTemplate, 
        SimpleFillSymbol, SimpleLineSymbol, 
        Editor, TemplatePicker,
        esriRequest,
        esriConfig, jsapiBundle,
        arrayUtils, conn, parser, keys, domClass, domConstruct
      ) {
        parser.parse();     

        // ---------------------- GLOBAL VARIABLES DECLARATION -------------------------------
		var identifyParams, identifyTask, tb;
		var center = [108.75,5.391]; // lon, lat
		var clickmap;
		var app = {};
		var dynaLayer1, dynaLayer2, dynaLayer3, dynaLayer4;
      	app.map = null; app.printer = null;

      	// ---------------------- INFO POPUP PROPS -------------------------------
					var popup = new Popup({ fillSymbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
											new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
											new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25]))
					}, domConstruct.create("div"));
 	
		// popup theme
        //dojo.addClass(popup.domNode, "modernGrey");
		domClass.add(popup.domNode, "dark");

        // snapping is enabled for this sample - change the tooltip to reflect this
        jsapiBundle.toolbars.draw.start = jsapiBundle.toolbars.draw.start +  "<br>Press <b>ALT</b> to enable snapping";
       
        // refer to "Using the Proxy Page" for more information:  https://developers.arcgis.com/javascript/jshelp/ags_proxy.html
        //esriConfig.defaults.io.proxyUrl = "/proxy/"; 

        //This service is for development and testing purposes only. We recommend that you create your own geometry service for use within your applications. 
        //esriConfig.defaults.geometryService = new GeometryService("http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
        esriConfig.defaults.geometryService = new GeometryService("https://"+ipaddress+"/arcgis/rest/services/Utilities/Geometry/GeometryServer");
  		// var latitude = phpVars["value1"];
		// var longitude = phpVars["value2"];
		// var zoomin = phpVars["value3"];

		var latitude = localStorage.getItem('latt');
		var longitude = localStorage.getItem('lonn');
		var zoomin = localStorage.getItem('zoomm');

		// var latitude = 103.791096;
		// var longitude = 1.547069;
		// var zoomin = 16;

		var userCategory = phpVars["value4"];
		var stateCode = phpVars["value5"];
		
		loading = dom.byId("loadingImg");  //loading image. id  

        map = new Map("map", { 
          	center: [latitude, longitude],
			zoom: zoomin,
			basemap: "hybrid",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false*/
			infoWindow: popup,
			showAttribution: false
        });
		
        //add boundaries and place names 
        // var labels = new ArcGISTiledMapServiceLayer("http://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer");
        // map.addLayer(labels);
		
		// ---------------------- DEFINE SERVICES -------------------------------	
		//esriConfig.defaults.io.proxyUrl = "/proxy/";
		esriConfig.defaults.io.proxyUrl = "../proxy/PHP/proxy.php";
        esriConfig.defaults.io.alwaysUseProxy = false;
		
		// SETUP PROXY for Buffer, Measure
	//	esriConfig.defaults.io.proxyUrl = 'dist/proxy-1.1.0/PHP/proxy.php';
	//	esriConfig.defaults.io.alwaysUseProxy = false;
//			esriConfig.defaults.io.corsDetection = false; // to get rid of HTTPS (share services in applications that utilize HTTPS)
		esriConfig.defaults.io.corsEnabledServers.push("https://www.gis.myetapp.gov.my/myetappgis");
		esriConfig.defaults.io.corsEnabledServers.push("https://www.gis.myetapp.gov.my");

		//Use the ImageParameters to set map service layer definitions and map service visible layers before adding to the client map.
		var imageParameters = new ImageParameters();

		//ImageParameters.layerDefinitions takes an array.  The index of the array corresponds to the layer id.
		//In the sample below an element is added in the array at 3, 4, and 5 indexes.
		//Those array elements correspond to the layer id within the remote ArcGISDynamicMapServiceLayer
		var layerDefs = [];

		if (userCategory == 'Staff') {
			layerDefs[0] = "STATUS_TERKINI='2' and KOD_NEGERI ='"+stateCode+"'";
			layerDefs[1] = "STATUS_TERKINI='2' and KOD_NEGERI ='"+stateCode+"'";
			layerDefs[2] = "KOD_NEGERI ='"+stateCode+"'";
			layerDefs[3] = "STATUS_TERKINI='2' and KOD_NEGERI ='"+stateCode+"'";
		}else{
			layerDefs[0] = "STATUS_TERKINI='2'";
			layerDefs[1] = "STATUS_TERKINI='2'";
			layerDefs[2] = "STATUS_TERKINI='3'";
			layerDefs[3] = "STATUS_TERKINI='2'";
		}
        imageParameters.layerDefinitions = layerDefs;
		  
		//I want layers 5,4, and 3 to be visible
		imageParameters.layerIds = [0, 1, 2, 3];
		imageParameters.layerOption = ImageParameters.LAYER_OPTION_SHOW;
		imageParameters.transparent = true;


		dynaLayer1 = new ArcGISDynamicMapServiceLayer("https://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/sani/1MyMap/MapServer", {
			opacity: 0.8
		});

		/* dynaLayer2 = new ArcGISDynamicMapServiceLayer("http://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer", {
		opacity: 0.8
		}); */

		//construct ArcGISDynamicMapServiceLayer with imageParameters from above
		dynaLayer2 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer",{
			opacity: 0.8,
			"imageParameters": imageParameters
		});

		dynaLayer3 = new ArcGISDynamicMapServiceLayer("https://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/jalan_jkr/Rangkaian_Jalan_JKR/MapServer", {
			opacity: 0.8
		});

		dynaLayer4 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG/ndcdb/MapServer", {
			opacity: 0.8
		});

		dynaLayer5 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/indexmap/MapServer", {
			opacity: 0.8
		});

		dynaLayer6 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/sempadan/MapServer", {
			opacity: 0.8
		});

		dynaLayer7 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/feedback3/MapServer", {
			opacity: 0.8
		});

		map.on('layers-add-result', function(evt){

            // overwrite the default visibility of service.
            // TOC will honor the overwritten value.
			
		   // SET LAYER VISIBILITY ================================================
			dynaLayer1.setVisibility(false); // to default unchecked
			dynaLayer3.setVisibility(false); // to default unchecked
			dynaLayer4.setVisibility(false); // to default unchecked
			dynaLayer5.setVisibility(false); // to default unchecked
			dynaLayer6.setVisibility(false); // to default unchecked
			dynaLayer7.setVisibility(false); // to default unchecked

			dynaLayer1.setVisibleLayers([]);
			dynaLayer3.setVisibleLayers([]);
			dynaLayer2.setVisibleLayers([0,1,2,3]);
			dynaLayer4.setVisibleLayers([]);
			dynaLayer5.setVisibleLayers([]);
			dynaLayer6.setVisibleLayers([]);
			dynaLayer7.setVisibleLayers([]);
            //try {
			toc = new TOC({
				map: map,
				layerInfos: [
				/*{
				  layer: featLayer1,
				  title: "FeatureLayer1"
				},*/

				{
				  layer: dynaLayer2,
				  title: "Charting",
				  //collapsed: false, // whether this root layer should be collapsed initially, default false.
				  slider: true // whether to display a transparency slider.
				},

				// {
				//   layer: dynaLayer1,
				//   title: "1 Malaysia Map"
				//   //collapsed: false, // whether this root layer should be collapsed initially, default false.
				//   //slider: false // whether to display a transparency slider.
				// },
									
				// {
				//   layer: dynaLayer3,
				//   title: "JKR Road"
				//   //collapsed: false, // whether this root layer should be collapsed initially, default false.
				//   //slider: false // whether to display a transparency slider.
				// },
				{
				  layer: dynaLayer5,
				  title: "Index Map"
				  //collapsed: false, // whether this root layer should be collapsed initially, default false.
				  //slider: false // whether to display a transparency slider.
				},
				{
				  layer: dynaLayer4,
				  title: "Lot NDCDB"
				  //collapsed: false, // whether this root layer should be collapsed initially, default false.
				  //slider: false // whether to display a transparency slider.
				},
				{
				  layer: dynaLayer6,
				  title: "Sempadan"
				  //collapsed: false, // whether this root layer should be collapsed initially, default false.
				  //slider: false // whether to display a transparency slider.
				}
				// {
				//   layer: dynaLayer7,
				//   title: "feedback"
				//   //collapsed: false, // whether this root layer should be collapsed initially, default false.
				//   //slider: false // whether to display a transparency slider.
				// }	
				]
			}, 'tocDiv');
			toc.startup();

			//} catch (e) {  alert(e); }
		});
		map.addLayers([dynaLayer7, dynaLayer6 ,dynaLayer4 , dynaLayer5 ,dynaLayer2, dynaLayer1, dynaLayer3]);

		var responsePoints = new FeatureLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/feedback3/FeatureServer/0",{
         	mode: FeatureLayer.MODE_ONDEMAND, 
          	outFields: ['*']
        });

        var responsePolyline = new FeatureLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/feedback3/FeatureServer/1",{
          	mode: FeatureLayer.MODE_ONDEMAND, 
         	outFields: ['*']
        });

        var responsePolygon = new FeatureLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/feedback3/FeatureServer/2",{
          	mode: FeatureLayer.MODE_ONDEMAND, 
         	outFields: ['*']
        });

		responsePolygon.setDefinitionExpression("USERNAME='username' AND STATE='state'");
		responsePolyline.setDefinitionExpression("USERNAME='username' AND STATE='state'");
		responsePoints.setDefinitionExpression("USERNAME='username' AND STATE='state'");
		
		map.addLayers([responsePolygon, responsePolyline, responsePoints]);    

        map.on("layers-add-result", initEditor);
		
		var coords, zoomLevel;  
		var urlObject = esri.urlToObject(document.location.href);  
	
		// Infowindows
		map.infoWindow.resize(550, 250);	
		
		// Resize map
		map.on('resize', function (evt) {
			var pnt = evt.target.extent.getCenter();
			setTimeout(function () { evt.target.centerAt(pnt);}, 500);
		});

		/////////////////////////////////////////////////////////

		//Use the ImageParameters to set map service layer definitions and map service visible layers before adding to the client map.
   //        var imageParameters = new ImageParameters();

   //        //ImageParameters.layerDefinitions takes an array.  The index of the array corresponds to the layer id.
   //        //In the sample below an element is added in the array at 3, 4, and 5 indexes.
   //        //Those array elements correspond to the layer id within the remote ArcGISDynamicMapServiceLayer
   //        var layerDefs = [];
   //        layerDefs[0] = "STATUS_TERKINI='2'";
   //        layerDefs[1] = "STATUS_TERKINI='2'";
   //        layerDefs[2] = "STATUS_TERKINI='2'";
		 // // layerDefs[3] = "STATUS_TERKINI='2'";
   //        imageParameters.layerDefinitions = layerDefs;
		  
		 //  //I want layers 5,4, and 3 to be visible
   //        imageParameters.layerIds = [0, 1, 2];
   //        imageParameters.layerOption = ImageParameters.LAYER_OPTION_SHOW;
   //        imageParameters.transparent = true;

			//    dynaLayer1 = new ArcGISDynamicMapServiceLayer("http://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/sani/1MyMap/MapServer", {
   //              opacity: 0.8
   //            });
			  
			//   dynaLayer2 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer", {
   //              opacity: 0.8,
			// 	"imageParameters": imageParameters
   //            });
			  
			//   dynaLayer3 = new ArcGISDynamicMapServiceLayer("http://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/jalan_jkr/Rangkaian_Jalan_JKR/MapServer", {
   //              opacity: 0.8
   //            });
			  
             
			//   map.on('layers-add-result', function(evt){
   //              // overwrite the default visibility of service.
   //              // TOC will honor the overwritten value.
				
			// 	   // SET LAYER VISIBILITY ================================================
			// 		 dynaLayer1.setVisibility(false); // to default unchecked
			// 		 dynaLayer3.setVisibility(false); // to default unchecked
	
   //                 dynaLayer1.setVisibleLayers([]);
			// 	   dynaLayer3.setVisibleLayers([]);
			// 	   dynaLayer2.setVisibleLayers([]);
   //              //try {
   //                toc = new TOC({
   //                  map: map,
   //                  layerInfos: [
	
			// 		{
   //                    layer: dynaLayer2,
   //                    title: "Charting",
   //                    //collapsed: false, // whether this root layer should be collapsed initially, default false.
   //                    slider: true // whether to display a transparency slider.
   //                  },
					
			// 		{
   //                    layer: dynaLayer1,
   //                    title: "1 Malaysia Map"
   //                    //collapsed: false, // whether this root layer should be collapsed initially, default false.
   //                    //slider: false // whether to display a transparency slider.
   //                  },
										
			// 		{
   //                    layer: dynaLayer3,
   //                    title: "JKR Road"
   //                    //collapsed: false, // whether this root layer should be collapsed initially, default false.
   //                    //slider: false // whether to display a transparency slider.
   //                  }					
			// 		]
   //                }, 'tocDiv');
   //                toc.startup();
         
			
   //              //} catch (e) {  alert(e); }
   //            });
   //            map.addLayers([dynaLayer2, dynaLayer1, dynaLayer3]);



/////////////////////////////////////////////////////////	

		
		
		

		function initEditor(evt) {
		
		  console.log("initEditor", evt);
          // var map = this;
          var currentLayer = null;
          var layers = arrayUtils.map(evt.layers, function(result) {
            return result.layer;
          });
          console.log("layers", layers);
		  

			 responsePolygon.on("before-apply-edits", function(evt) {
			  var toAdd = evt.adds;
			  dojo.forEach(toAdd, function(add) {
		
				if(add.attributes.STATE === null){
					  add.attributes.STATE = state;
					}
				});
			  });
			  
			  responsePolygon.on("before-apply-edits", function(evt) {
			  var toAdd = evt.adds;
			  dojo.forEach(toAdd, function(add) {
		
				if(add.attributes.USERNAME === null){
					  add.attributes.USERNAME = username;
					}
				});
			  });
			  
			  responsePolyline.on("before-apply-edits", function(evt) {
			  var toAdd = evt.adds;
			  dojo.forEach(toAdd, function(add) {
		
				if(add.attributes.STATE === null){
					  add.attributes.STATE = state;
					}
				});
			  });
			  
			  responsePolyline.on("before-apply-edits", function(evt) {
			  var toAdd = evt.adds;
			  dojo.forEach(toAdd, function(add) {
		
				if(add.attributes.USERNAME === null){
					  add.attributes.USERNAME = username;
					}
				});
			  });
			  
			  responsePoints.on("before-apply-edits", function(evt) {
			  var toAdd = evt.adds;
			  dojo.forEach(toAdd, function(add) {
		
				if(add.attributes.STATE === null){
					  add.attributes.STATE = state;
					}
				});
			  });
			  
			  responsePoints.on("before-apply-edits", function(evt) {
			  var toAdd = evt.adds;
			  dojo.forEach(toAdd, function(add) {
		
				if(add.attributes.USERNAME === null){
					  add.attributes.USERNAME = username;
					}
				});
			  });
			
			
          var templatePicker = new TemplatePicker({
            featureLayers: layers,
            grouping: true,
            rows: "auto",
            //column: 3
          }, "templateDiv");
          templatePicker.startup();

          var layers = arrayUtils.map(evt.layers, function(result) {
            return { 
			featureLayer: result.layer,
			'fieldInfos':[
				{'fieldName':'COMMENT','label':'Feedback','stringFieldOption': 'textarea','visible': false,'isEditable':true},
				{'fieldName':'STATE','label':'JKPTG','isEditable':false},
				{'fieldName':'USERNAME','label':'Send by','isEditable':false}
			  ]
			};
          });
		  
		  
          var settings = {
            map: map,
            templatePicker: templatePicker,
            layerInfos: layers,
            toolbarVisible: true,
            createOptions: {
              polylineDrawTools:[ Editor.CREATE_TOOL_FREEHAND_POLYLINE ],
              polygonDrawTools: [ Editor.CREATE_TOOL_FREEHAND_POLYGON,
                Editor.CREATE_TOOL_CIRCLE,
                Editor.CREATE_TOOL_TRIANGLE,
                Editor.CREATE_TOOL_RECTANGLE
              ]
            },
            toolbarOptions: {
              reshapeVisible: true
            }
          };

          var params = {settings: settings};    
          var myEditor = new Editor(params,'editorDiv');
          //define snapping options
          var symbol = new SimpleMarkerSymbol(
            SimpleMarkerSymbol.STYLE_CROSS, 
            15, 
            new SimpleLineSymbol(
              SimpleLineSymbol.STYLE_SOLID, 
              new Color([255, 0, 0, 0.5]), 
              5
            ), 
            null
          );
          map.enableSnapping({
            snapPointSymbol: symbol,
            tolerance: 20,
            snapKey: keys.ALT
          });
    
          myEditor.startup();
		  
        }
		

		
		// ---------------------- Show Loading ----------------------
        on(map, "update-start", showLoading);
		on(map, "update-end", hideLoading);
		
		 function showLoading() {
			esri.show(loading);
			map.disableMapNavigation();
			map.hideZoomSlider();
		  }

		 function hideLoading(error) {
			esri.hide(loading);
			map.enableMapNavigation();
			map.showZoomSlider();
		  } 
		  
		 // ---------------------- Get Center Point ---------------------- 
		function getCenterPoint()
		{
		  return map.extent.getCenter();
		}

		map.on("extent-change", function(){
		  
		  var point = getCenterPoint();
		  
		  var newPoint = webMercatorUtils.webMercatorToGeographic(point);
		  
		  console.log("current map center point is x: " + point.getLatitude() + ", y: " + point.getLongitude());
		  console.log("current map center is x: " + newPoint.x + ", y: " + newPoint.y);
		  
		  document.getElementById("getx").value = newPoint.x; 
		  document.getElementById("gety").value = newPoint.y;
		  
		});
 
		  
		  
		 // ---------------------- Activate HOME button ----------------------
		var home = new HomeButton({ map: map}, "HomeButton");      	
		home.title = "This is the new title"; 
		home.startup();	

		map.on("load", function() {
          //after map loads, connect to listen to mouse move & drag events
          map.on("mouse-move", showCoordinates);
          map.on("mouse-drag", showCoordinates);
	
        });	
		
		// ---------------------- ERASE button ----------------------
		on(dom.byId("EraseButton"), "click", function(){
			if(map){ 
				map.graphics.clear(); 

			}
        }); 
		
		// ---------------------- LOCATE/Geolocation button ----------------------
		geoLocate = new LocateButton({
			map: map
		}, "LocateButton");
		geoLocate.startup();

		// ---------------------- Show Coordinates ----------------------
        function showCoordinates(evt) {
			//the map is in web mercator but display coordinates in geographic (lat, long)
			var mp = webMercatorUtils.webMercatorToGeographic(evt.mapPoint);
			//display mouse coordinates
			dom.byId("coordinate_info").innerHTML = "X: "+ mp.x.toFixed(6) + ", Y: " + mp.y.toFixed(6);
        }
		
		// ---------------------- SCALEBAR --------------------------------
		var scalebar = new Scalebar({
			map: map,
			attachTo: 'bottom-left',
			scalebarStyle: 'line',
			scalebarUnit: 'dual'
		});

		
		// ------------------------------- OVERVIEW MAP-------- -----------------------
		var overviewMapDijit = new OverviewMap({
			map: map,
			attachTo: "bottom-right",
			/*color:"#fdf874",*/
			height : 200,
			width : 225,
			opacity: .40
        });
		
        overviewMapDijit.startup();
		
		// ---------------------- BASEMAP TOGGLE ---------------------
		var toggle = new BasemapToggle({
			map: map,
			basemap: "osm"
		}, "BasemapToggle");
     
		toggle.startup();
		
      });