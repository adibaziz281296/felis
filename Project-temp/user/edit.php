<?php
	include("../../assets/conn/sql_server.php");
	
	# RETRIEVE USER'S INFO	-------------------------------------------------------------------------------
	$userName = $_GET['uname'];
	$sql_usr = "SELECT *
				FROM PENGGUNA_SISTEM u, AGENSI a
				WHERE u.agensiID = a.agensiID
				AND idSistem = '$userName'"; 
	$params_usr = array();
	$options_usr =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_usr = sqlsrv_query($conn, $sql_usr , $params_usr, $options_usr );	
  
	if( $stmt_usr === false ) { print( print_r( sqlsrv_errors() ) ); }
	
	$row_usr = sqlsrv_fetch_array( $stmt_usr, SQLSRV_FETCH_ASSOC);

	# RETRIEVE AGENCY LIST	-------------------------------------------------------------------------------
	$sql = "SELECT *
			FROM agensi
			WHERE agensiID <> 'admin'
			ORDER BY agensiNama"; 
	$params = array();
	$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt = sqlsrv_query($conn, $sql , $params, $options );	
  
	if( $stmt === false ) { print( print_r( sqlsrv_errors() ) ); }
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Kemaskini Maklumat Pengguna</title>               
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
                        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                 
	<script language="javascript" src="funcs/val.js"></script>		
    </head>
    <body>           
            <!-- PAGE CONTENT -->
            <div class="page-content">                             
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" name="form" onSubmit="return val_edit(this, 'edit_val.php')" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Kemaskini Maklumat Pengguna</h3>   
                                </div>
                                <!--<div class="panel-body">
                                    <p>This is non libero bibendum, scelerisque arcu id, placerat nunc. Integer ullamcorper rutrum dui eget porta. Fusce enim dui, pulvinar a augue nec, dapibus hendrerit mauris. Praesent efficitur, elit non convallis faucibus, enim sapien suscipit mi, sit amet fringilla felis arcu id sem. Phasellus semper felis in odio convallis, et venenatis nisl posuere. Morbi non aliquet magna, a consectetur risus. Vivamus quis tellus eros. Nulla sagittis nisi sit amet orci consectetur laoreet. Vivamus volutpat erat ac vulputate laoreet. Phasellus eu ipsum massa.</p>
                                </div> -->
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Nama Pegawai</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="fullName" value="<?php echo $row_usr['penggunaNama']; ?>"/>
                                            </div>                                            
                                            <span class="help-block">Seperti di dalam MyKad</span>
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">No. MyKad</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="icNo" value="<?php echo $row_usr['icNo']; ?>"/>
												<input type="hidden" class="form-control" name="userName" value="<?php echo $row_usr['idSistem']; ?>"/>
                                            </div>                                            
                                            <span class="help-block">No MyKad/Kad Pengenalan tanpa tanda '-'</span>
                                        </div>
                                    </div>
									
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Agensi</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" name="levelName">
                                                <option value="0">Pilih Agensi</option>
											<?php																						  
											  while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) { 	
											?>
                                                <option value="<?php echo $row['agensiID'] ?>"  <?php if (!(strcmp($row['agensiID'], $row_usr['agensiID']))) { echo "SELECTED";}?>><?php echo $row['agensiNama'] ?></option>
											<?php } ?>
                                            </select>
                                            <span class="help-block">Agensi pengguna bertugas</span>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="panel-footer">
                                    <!-- <button type="submit" class="btn btn-default">Bersihkan Borang</button> -->                                   
                                    <button type="submit" class="btn btn-info pull-right">Kemaskini</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->             
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>                
        <!-- END PLUGINS -->
        
        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <!-- END THIS PAGE PLUGINS -->       
        
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/settings.js"></script>
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->                   
    </body>
</html>






