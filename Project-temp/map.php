<?php
//session_start();
include('../sql_server_db.php');

$state = $_SESSION['state'];
$username = $_SESSION['username'];
$category = $_SESSION['category'];
$state_code = $_SESSION['state_code'];

// $state = "JOHOR";
// $username = "zul";
// $state_code = "01";
// $category = "Staff";
// $date = date("Y-m-d");

function datetime()
{
	return date("d-m-Y");
}

$sql = "SELECT * FROM location WHERE state = '$state_code'";

$result = sqlsrv_query( $conn, $sql);
if( $result === false) {
  die( print_r( sqlsrv_errors(), true) );
  echo "<script> console.log('PHP: wrong!!!');</script>";
}

while($row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )){	
	$latitude = $row['latitude'];
	$longitude = $row['longitude'];
	$zoom = $row['zoom'];
}


$phpToJsVars = array(
  'value1' => $latitude,
  'value2' => $longitude,
  'value3' => $zoom,
  'value4' => $category,
  'value5' => $state_code
   );
   
// Submit feedback to JKPTG Federal (Admin)   
if (isset($_POST['submit']))
{

	$title=$_POST['title'];
	$desc=$_POST['desc']; 	

	$coorx = $_POST['getx'];
	$coory = $_POST['gety'];

	
					
		# Get user record
		$sql_projek = "INSERT INTO MAKLUMBALAS ([tajuk],[penerangan],[username],[ic],[state],[coorx],[coory],[date]) VALUES 
		('$title' ,'$desc','$username','$ic','$state','$coorx','$coory','$date')";   
		$params_projek = array();
		$options_projek =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql_projek , $params_projek, $options_projek );
		

		$sql5 = "SELECT TOP 1 * FROM [jkptg].[dbo].[MAKLUMBALAS] WHERE username = '".$username."'  ORDER BY project_id DESC"; 
		$params5 = array(); 
		$options5 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt5 = sqlsrv_query($conn, $sql5 , $params5, $options5 );	
		
			
		# Get value from the $sql - table name & column/info to display
		$row5 = sqlsrv_fetch_array( $stmt5, SQLSRV_FETCH_ASSOC);
		$project_id = $row5['PROJECT_ID'];  


		
		$point_projek = "UPDATE POINT SET PROJECT_ID = '".$project_id."' WHERE PROJECT_ID IS NULL AND USERNAME = '".$username."'";
		$params_projek2 = array();
		$options_projek2 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt2 = sqlsrv_query($conn, $point_projek , $params_projek2, $options_projek2 );
		
		 
		$line_projek = "UPDATE POLYLINE SET PROJECT_ID = '".$project_id."' WHERE PROJECT_ID IS NULL AND USERNAME = '".$username."'";
		$params_projek3 = array();
		$options_projek3 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt3 = sqlsrv_query($conn, $line_projek , $params_projek3, $options_projek3 ); 
		
		$polygon_projek = "UPDATE POLYGON SET PROJECT_ID = '".$project_id."' WHERE PROJECT_ID IS NULL AND USERNAME = '".$username."'";
		$params_projek4 = array();
		$options_projek4 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt4 = sqlsrv_query($conn, $polygon_projek , $params_projek4, $options_projek4 ); 

		$sqlCheck = "SELECT TOP 1 * FROM MAKLUMBALAS WHERE USERNAME = '".$username."' ORDER BY project_id DESC"; 
		$paramsCheck = array();
		$optionsCheck =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmtCheck = sqlsrv_query($conn, $sqlCheck , $paramsCheck, $optionsCheck );
		$rowCheck = sqlsrv_fetch_array( $stmtCheck, SQLSRV_FETCH_ASSOC);
		
		
		$message = 'Your feedback has been send to JKPTG federal for approval/rejected.';
		echo "<SCRIPT>alert('$message');</SCRIPT>";

}   
   
?>
<!DOCTYPE html>
<html>
  <head>
  	<title>MyeTaPP-GIS</title>
    <meta charset="utf-8">
    <!--The viewport meta tag is used to improve the presentation and behavior of the samples 
      on iOS devices-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	
	<!-- Bootstrap core CSS -->
	<link href="assets/css/gis/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Custom styles for this template -->
	<link href="assets/css/gis/simple-sidebar.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="toc/2.10/src/agsjs/css/agsjs.css" />
		
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//esri.github.io/bootstrap-map-js/src/css/bootstrapmap.css">

	<style>
		
		#slideout {				
			position: fixed;
			top:20% !important; 
			right: 20px;
			z-index: 50;
			width: 35px;
			padding: 12px 0;
			text-align: center;
			-webkit-transition-duration: 0.3s;
			-moz-transition-duration: 0.3s;
			-o-transition-duration: 0.3s;
			transition-duration: 0.3s;
			-webkit-border-radius: 0 5px 5px 0;
			-moz-border-radius: 0 5px 5px 0;
			border-radius: 0 5px 5px 0;
		}
		#slideout_inner {
			position: fixed;
			top:20% !important;
			right: -250px;
			width: 220px;
			height: 150px;
			-webkit-transition-duration: 0.3s;
			-moz-transition-duration: 0.3s;
			-o-transition-duration: 0.3s;
			transition-duration: 0.3s;
			text-align: left;
			-webkit-border-radius: 0 0 5px 0;
			-moz-border-radius: 0 0 5px 0;
			border-radius: 0 0 5px 0;
		}
		
          .show:focus + .hide {display: inline; }
          .show:focus + .hide + #slideout_inner {display: block;}

		#slideout:hover {
			right: 225px; <!-- gap antara icon & slider -->
		}
		#slideout:hover #slideout_inner {
			right: 0px; <!-- jarak dr kanan -->
		}
		
		#slideout_inner:focus{
			right: 0px; <!-- jarak dr kanan -->
			display: block;
		}
		
		#map .atiTextAreaField {  
		   height: 100px;  
		   width: 260px;  
		  } 
		
	</style>
	<link rel="stylesheet" type="text/css" href="css/accordian.css" />  	
    <link rel="stylesheet" href="https://js.arcgis.com/3.14/dijit/themes/claro/claro.css">
    <link rel="stylesheet" href="https://js.arcgis.com/3.14/esri/css/esri.css" />
	
<script>

	function doSubmit(){ 

		 var x = document.forms["borangFeedback"]["title"].value;
		 var y = document.forms["borangFeedback"]["desc"].value;
		 
			if (x == "") {
				alert("Title must be filled out");
				return false;
			}
			
			if (y == "") {
				alert("Description must be filled out");
				return false;
			}
			
			else {
				$('#borangFeedback').submit();
			}	
	}
 
</script> 	

		
	<style>
		 @import url("https://js.arcgis.com/3.10/js/dojo/dijit/themes/claro/claro.css");
		 @import url("css/FloatingPane.css"); 
		 @import url("css/ResizeHandle.css");	
		 @import url("css/EnhancedGrid.css");   
   		  
      html,body
	  {
		  height:100%;
		  width:100%;
		  margin:0;
		  overflow:hidden;
	  }
	  
      #map{
        padding:0;
		 max-width: 100%;
		  overflow-x: hidden;
		  max-height: 100%;
		  overflow-y: hidden;
      }
      .templatePicker {
        border: none;
      }

      .dj_ie .infowindow .window .top .right .user .content { position: relative; }
      .dj_ie .simpleInfoWindow .content {position: relative;}
	  
	  .dgrid { border: none; height: 100%; }
      .dgrid-column-0 {
        width: 35px;
      }
      .dgrid-row-odd {
        background: #FFFDF3;
      }
      td div img:hover {
        cursor: pointer;
      }
    </style>
    <!-- Custom info popup style to overwrite the default info popup style -->
    <link rel="stylesheet" type="text/css" href="assets/css/gis/modernGrey.css">
	
	<!-- Custom widget style to overwrite the default widget style -->
	<link rel="stylesheet" type="text/css" href="assets/css/gis/widget.css">
	
	<!--for uploading flood report file -->
	<script language="javascript" src="report/funcs/val.js"></script>
    <script>
		var dojoConfig = {
		isDebug: true,
		parseOnLoad: true,
		foo: "bar"
		};
	</script> 
	<!-- Load dojo and provide config via data attribute for TOC -->
	<script type="text/javascript">
		// Registers the correct location of the "demo" package so we can load Dojo
		var dojoConfig = { 
			paths: {
				//if you want to host on your own server, download and put in folders then use path like: 
				agsjs: location.pathname.replace(/\/[^/]+$/, '') + '/toc/2.10/src/agsjs' 
				
			}
		};      
	</script>
    <script type="text/javascript">
	var phpVars = { 
	<?php 
	  foreach ($phpToJsVars as $key => $value) {
	    echo '  ' . $key . ': ' . '"' . $value . '",' . "\n";  
	  }
	?>
	};
	</script>
	<script src="//ajax.googleapis.com/ajax/libs/dojo/1.6.2/dojo/dojo.xd.js"></script>

	<!-- Reference for StreetView -->
	<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp" type="text/javascript"></script>  -->
	<script src="https://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyDEfWbEK3OTIRtSJhxCW_AmerJQOu9asdw" type="text/javascript"></script>
	
    <!-- Reference the ArcGIS API for JavaScript -->
    <script src="https://js.arcgis.com/3.14/"></script>
  </head>
  <body class="claro">  
  	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
		  <!-- <ul class="nav navbar-nav navbar-right">
            <li><a ref="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="login-btn"><i class="fa fa-user white"></i>&nbsp;&nbsp;Daftar Masuk</a></li>
          </ul> -->	  
          <ul class="nav navbar-nav">
		   	<!--li>
				<a  href="#" role="button"  id="basic_legend" style="font-size: 13px;"><i class="fa fa-bars icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Legend Map</a>
			</li-->
			
			
			<!--li>
				<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="state"><i class="fa fa-map icon-blue"></i>&nbsp;&nbsp;State</a></li>
			</li-->
			<li class="dropdown">
				<a  href="#" role="button"  id="basic_legend" style="font-size: 13px;"><i class="fa fa-bars icon-blue" style="font-size: 19px;"></i>&nbsp;&nbsp;Legend Map</a>
			</li>
			<li class="right">
				<a  href="#" role="button" onClick="window.location.reload()" style="font-size: 13px;" title="Refresh Page"><i class="fa fa-refresh" aria-hidden="true" style="font-size: 19px;"></i></a>
			</li>
          </ul>
		  <input type="hidden" id="username" value="<?php echo $username; ?>" name="username"/><br/>  
		  <input type="hidden" id="negeri" value="<?php echo $state; ?>" name="negeri"/><br/> 
			
			
        </div>
      </div>
    </nav> 

    <div id="wrapper">
	
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#"><img src="assets/img/arrow.png" width="32px" height="29px" alt="Tutup petunjuk peta"></a>
                </li>
				<li>
					<a href="#">Table of Content</a>
                    <div id="tocDiv"></div>
                </li>
				<br>
            </ul>			
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
		<div id="main" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design:'headline'" style="width:100%;height:100%;">
		  <!--?php if ($category == 'Admin'){ ?>
		  <div data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'left'" style="width: 100px;overflow:hidden;">
	        <div id="grid"></div>
	      </div>
		   <1?php } ?-->
		  <?php if ($category == 'Super Admin' || $category == 'Admin' || $category == 'Guest'){ ?> 		  
		  <div id="map">
			<div id="HomeButton"></div>
			<div id="LocateButton"></div>
			<div id="EraseButton" title="Erase graphic"></div>
			<div id="BasemapToggle"></div>
			<span id="coordinate_info" style="color:orange; text-shadow:1px 1px 1px #000;"></span>
			<img id="loadingImg" src="loader/loader.gif" style="position:absolute; right:612px; top:356px; z-index:100;" />
		  </div>
		  <?php } ?>
		
	      <?php if ($category == 'Staff'){ ?>
		  <div data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'left'" style="width: 95px;overflow:hidden;">
	        <div id="templateDiv"></div>
	        <div id="editorDiv"></div>
	      </div>
		  
		  <div data-dojo-type="dijit/layout/ContentPane" id="map" data-dojo-props="region:'center'" style="overflow:hidden;">

			<div id="HomeButton"></div>
			<div id="LocateButton"></div>
			<div id="EraseButton" title="Erase graphic"></div>
			<div id="BasemapToggle"></div>
			<span id="coordinate_info" style="color:orange; text-shadow:1px 1px 1px #000;"></span>
			<img id="loadingImg" src="loader/loader.gif" style="position:absolute; right:612px; top:356px; z-index:100;" />
		  </div>
		 
				<div class="panel panel-primary panel-fixed">
					<div class="panel-heading">
					  <h3 class="panel-title">Feedback Form</h3>
					</div>
					<div class="panel-body">
						<form name="borangFeedback" method="post" onSubmit="return doSubmit();" action="#">
						<div class="form-group">
						
							<input name="getx" type="hidden" id="getx"/>	
							<input name="gety" type="hidden" id="gety"/>
						
							<label for="formGroupExampleInput">Title:</label>
							<textarea  style="font-size:10px;" class="form-control" type="text" id="title" name="title" value="" cols="30" rows="1" maxlength="100" autocomplete="on" class="input" onkeyup="showChar(this);" onblur="showChar(this);"></textarea>	
						  </div>
						  
						  <div class="form-group">
							<label for="formGroupExampleInput">Description:</label>
							<textarea  style="font-size:10px;" class="form-control" type="text" id="desc" name="desc" value="" cols="30" rows="4" maxlength="100" autocomplete="on" class="input" onkeyup="showChar(this);" onblur="showChar(this);"></textarea>	
						  </div>
						  
						  <div class="form-group">
							<input name="submit" style="font-size:10px; font-weight:bold" id="submitbtn" type="submit" value="SUBMIT" align="center" width="40px;" class="button" />   
							<!--button id="btnSolveRoute" class="btn btn-success">Save</button>
							<button id="btnClearRoutes" class="btn btn-default">Clear</button-->
						  </div>
						  <div id="userMessage">Send your feedback here.</div>
						   </form> 
					 
					</div>
				  </div>
				<?php } ?>
	    </div>
		
<!--         <div id="page-content-wrapper">
            <!-- <div class="container-fluid-2"> -->
                <!-- <div class="row"> -->
                    <!-- <div class="col-lg-12">  
					<div id="map"></div>	
                        <!-- <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a> -->
                    <!-- </div> -->
                <!-- </div> -->
            <!-- </div> 
       </div>  -->
        <!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->

	
	<?php if ($category == 'Super Admin' || $category == 'Admin' || $category == 'Guest'){ ?>
	<div id="slideout">
				<a href="#reset" class="show"><img src="src/icons/feedback.png" width="42" height="42" /></a> 
				<div id="slideout_inner">	
					<div id="contain">
					
						<!--div id="legendDiv"></div-->					
						<div class="accordion">
							<!-- SEARCH KM POINT FORM -->
							<div id="kmpoint">
								
								<a href="#kmpoint" class="tab">Feedback List</a>
								<div class="content">
									<div id="parentDiv" data-dojo-type="dijit/layout/ContentPane" style="width:200px; height:185px; overflow:auto;">
										 <ul style="list-style-type:disc">

										  <li>
										  
										  <script>
											function disableButton(bawa,nama,value){
											
							
											document.getElementById("lala"+bawa).disabled = true;
											
											 $.ajax({
											  type: 'post',
											  dataType: 'json',
											  url: 'updatedata.php?nama='+nama+'&project_id='+value,
											   success: function(data){
													$.each( data, function( i, item ) {	
													 if(item.bawa == 'success'){
											
														}
												  });
												 }
											}); 		
											}
										  
										  
										  </script>
										
									
									
									
										<label style="font-size:10px; font-weight:bold">
											<?php 
							
											$bil = 1;
											$sql8 = "SELECT * FROM MAKLUMBALAS"; 
											$params8 = array();
											$options8 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
											$stmt8 = sqlsrv_query($conn, $sql8 , $params8, $options8 );
											while ($row8 = sqlsrv_fetch_array( $stmt8, SQLSRV_FETCH_ASSOC)){ ?>
												<?php echo $bil; ?>)&nbsp;
												Tajuk : <b><?php echo $row8['tajuk']?><b><br>&nbsp;&nbsp;&nbsp;&nbsp; JKPTG : <?php echo $row8['state']?><br>
												&nbsp;&nbsp;&nbsp;&nbsp; Username : <?php echo $row8['username']?><br>
												&nbsp;&nbsp;&nbsp;&nbsp;<input id="<?php echo $bil; ?>" class="clickMe" type="image" src="assets/img/zoom3.png" width="25" height="25" title="Zoom"><!--button id="<1?php echo $bil; ?>" class="clickMe">Zoom</button-->
												<?php if ($row8['flag'] == 0) : ?>
												 <button type="button" class="btn btn-warning btn-xs" id="lala<?php echo $bil; ?>" onclick="disableButton(<?php echo $bil; ?>,'<?php echo $row8['username']; ?>',<?php echo $row8['project_id'] ?>)">Acknowledged</button>
												  <?php else: ?>
												  <button type="button" class="btn btn-default btn-xs" disabled="disabled">Acknowledged</button>
												<?php endif; ?>
												
												<input name="coory" type="hidden" id="coory<?php echo $bil; ?>" value="<?php echo $row8['coory']; ?>"/>	
												<input name="coorx" type="hidden" id="coorx<?php echo $bil; ?>" value="<?php echo $row8['coorx']; ?>"/>	
												<br><br>               
											<?php $bil++; } ?>					
											</label>
											<input name="feedid" type="hidden" id="feedid" value="<?php echo $bil; ?>"/>
												</li>
											</ul> 
									</div>
								</div>
							</div>
							<!-- SEARCH BLOCK VALVE FORM -->
							<!--div id="blockvalve">
								<a href="#blockvalve" class="tab">Kedah</a>
								<div class="content">
									  <div data-dojo-type="dijit/layout/ContentPane" style="width:200px; height:185px; overflow:auto;">
											<div id="basemapGallery"></div>
									  </div>					
								</div>
							</div-->

						
						</div>
					</div>	
				</div>
			</div>
	<?php } ?>
	
	<?php if ($category == 'Staff'){ ?>
	<div id="slideout">
				<a href="#reset" class="show"><img src="src/icons/feedback.png" width="42" height="42" /></a> 
				<div id="slideout_inner">	
					<div id="contain">
					
						<!--div id="legendDiv"></div-->					
						<div class="accordion">
							<!-- SEARCH KM POINT FORM -->
							<div id="kmpoint">
								
								<a href="#kmpoint" class="tab">Feedback List</a>
								<div class="content">
									<div id="parentDiv" data-dojo-type="dijit/layout/ContentPane" style="width:200px; height:185px; overflow:auto;">
										 <ul style="list-style-type:disc">

										  <li>
										
									
									
									
										<label style="font-size:10px; font-weight:bold">
											<?php 
							
											$bil = 1;
											$sql8 = "SELECT * FROM MAKLUMBALAS WHERE username = '$username' AND state = '$state'"; 
											$params8 = array(); 
											$options8 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
											$stmt8 = sqlsrv_query($conn, $sql8 , $params8, $options8 );
											while ($row8 = sqlsrv_fetch_array( $stmt8, SQLSRV_FETCH_ASSOC)){ ?>
												<?php echo $bil; ?>)&nbsp;
												Tajuk : <b><?php echo $row8['tajuk']?><b><br>&nbsp;&nbsp;&nbsp;&nbsp; Status : <?php if($row8['flag'] == 1) echo 'Federal acknowledge'; else echo 'Waiting for acknowledge'; ?><br>
												&nbsp;&nbsp;&nbsp;&nbsp;<!--button id="<1?php echo $bil; ?>" class="clickMe">Zoom</button-->
												<input name="coory" type="hidden" id="coory<?php echo $bil; ?>" value="<?php echo $row8['coory']; ?>"/>	
												<input name="coorx" type="hidden" id="coorx<?php echo $bil; ?>" value="<?php echo $row8['coorx']; ?>"/>	
												<br><br>               
											<?php $bil++; } ?>					
											</label>
											<input name="feedid" type="hidden" id="feedid" value="<?php echo $bil; ?>"/>
												</li>
											</ul> 
									</div>
								</div>
							</div>
							<!-- SEARCH BLOCK VALVE FORM -->
							<!--div id="blockvalve">
								<a href="#blockvalve" class="tab">Kedah</a>
								<div class="content">
									  <div data-dojo-type="dijit/layout/ContentPane" style="width:200px; height:185px; overflow:auto;">
											<div id="basemapGallery"></div>
									  </div>					
								</div>
							</div-->

						
						</div>
					</div>	
				</div>
			</div>
	<?php } ?>

			
    <!-- =========== Bootstrap core JavaScript =========== -->
    <!-- Placed at the end of the document so the pages load faster -->	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/css/gis/bootstrap.min.js"></script>
    
    <?php if ($category == 'Super Admin' || $category == 'Admin' || $category == 'Guest'){ ?>
	<script type="text/javascript" src="map_admin.js"></script>
	<?php } ?>
	<?php if ($category == 'Staff'){ ?>
	<script type="text/javascript" src="map_staff.js"></script>
	<?php } ?>

    <!-- Reference the Query/Search JavaScript functions -->
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>