// alert(localStorage.getItem('latt'));
var ipaddress = 'www.gis.myetapp.gov.my';
//"dojo.parser" -- Required if you have dojo widgets (aka. dijits)in your HTML markup 

$("#basic_legend").click(function() {
        $("#wrapper").toggleClass("toggled");
  return false;
});

$(".sidebar-brand").click(function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
});

$("#menu-toggle").click(function(e) {
	$("#wrapper").toggleClass("toggled");
});



var map,symbol, geomTask;
var gviewClient, Pano, Glocator;
var viewPoint;	
var viewStatus = false;
var viewAddress = "";
      
    require([

        "esri/map", 
        "esri/tasks/GeometryService",
        "esri/geometry/Extent",
        "esri/toolbars/edit",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "agsjs/dijit/TOC",
		"esri/dijit/Bookmarks",
		"esri/request",
		"esri/config",

		
		"dojo/on",
        "dojo/dom",
		"esri/dijit/HomeButton",
		"esri/geometry/webMercatorUtils",
		"esri/dijit/Scalebar",
		"esri/dijit/LocateButton",
		"esri/dijit/BasemapToggle", 
		
		
        "esri/layers/ArcGISTiledMapServiceLayer",
        "esri/layers/FeatureLayer",
        "esri/InfoTemplate", "esri/tasks/QueryTask",
        "esri/tasks/query",
        "esri/urlUtils",
        "esri/geometry/Point",
        "esri/tasks/IdentifyTask",
        "esri/tasks/IdentifyParameters",
        "esri/Color",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/dijit/Popup",	
		"esri/dijit/PopupTemplate",				
		"esri/symbols/SimpleFillSymbol",
		"esri/symbols/SimpleLineSymbol",

        "esri/dijit/editing/Editor",
        "esri/dijit/editing/TemplatePicker",

        "esri/request",
        "esri/config",
        "dojo/i18n!esri/nls/jsapi",

        "dojo/_base/array", "dojo/_base/connect", "dojo/parser", "dojo/keys", "dojo/dom-class", "dojo/dom-construct",

        "dijit/layout/BorderContainer", "dijit/layout/ContentPane",  
        "dojo/domReady!"
      ], function(
        Map, GeometryService, Extent, Edit,  ArcGISDynamicMapServiceLayer, TOC, Bookmarks, esriRequest, esriConfig, on, dom, HomeButton, webMercatorUtils, Scalebar, LocateButton, BasemapToggle,
        ArcGISTiledMapServiceLayer, FeatureLayer, InfoTemplate, QueryTask, Query,
        urlUtils, Point, IdentifyTask, IdentifyParameters,
        Color, SimpleMarkerSymbol, Popup, PopupTemplate, 
        SimpleFillSymbol, SimpleLineSymbol, 
        Editor, TemplatePicker,
        esriRequest,
        esriConfig, jsapiBundle,
        arrayUtils, conn, parser, keys, domClass, domConstruct
      ) {
        parser.parse();     

        // ---------------------- GLOBAL VARIABLES DECLARATION -------------------------------
		var identifyParams, identifyTask, tb;		
		var routeTask, routeParams, routes = [];
        var stopSymbol, barrierSymbol, routeSymbols;
        var mapOnClick_addStops_connect, mapOnClick_addBarriers_connect;
		var clickmap;
		var app = {};
      	app.map = null; app.printer = null;  

      	// ---------------------- INFO POPUP PROPS -------------------------------
        var popup = new Popup({ fillSymbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
											new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
											new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25]))
					}, domConstruct.create("div"));
		
 	
		// popup theme
        //dojo.addClass(popup.domNode, "modernGrey");
		 domClass.add(popup.domNode, "dark");

        // snapping is enabled for this sample - change the tooltip to reflect this
        jsapiBundle.toolbars.draw.start = jsapiBundle.toolbars.draw.start +  "<br>Press <b>ALT</b> to enable snapping";
       
        // refer to "Using the Proxy Page" for more information:  https://developers.arcgis.com/javascript/jshelp/ags_proxy.html
        //esriConfig.defaults.io.proxyUrl = "/proxy/"; 

        //This service is for development and testing purposes only. We recommend that you create your own geometry service for use within your applications. 
        esriConfig.defaults.geometryService = new GeometryService("https://"+ipaddress+"/arcgis/rest/services/Utilities/Geometry/GeometryServer");
		
        // var latitude = phpVars["value1"];
		// var longitude = phpVars["value2"];
		//var zoomin = phpVars["value3"];
		// var zoomin = 6;

		var latitude = localStorage.getItem('latt');
		var longitude = localStorage.getItem('lonn');
		var zoomin = localStorage.getItem('zoomm');
		
		loading = dom.byId("loadingImg");  //loading image. id  


        map = new Map("map", { 
          	center: [latitude, longitude],
			zoom: zoomin,
			basemap: "hybrid",
			logo: false,
			slider: true, /* to hide default zoom in zoom out button set to false*/
			infoWindow: popup,
			showAttribution: false
        });
		
		var template = new InfoTemplate();
        template.setTitle("Permintaan Kemaskini Maklumat Tanah PTP");
        template.setContent("Maklumbalas: ${COMMENT} <br> JKPTG: ${STATE} <br> Dihantar oleh : ${USERNAME}");

        //add a layer to the map
        var featureLayer0 = new FeatureLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/feedback3/MapServer/0", {
          mode: FeatureLayer.MODE_ONDEMAND,
          infoTemplate:template,
          outFields: ["*"]
        });
		
       // map.addLayer(featureLayer);
		
		//add a layer to the map
        var featureLayer1 = new FeatureLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/feedback3/MapServer/1", {
          mode: FeatureLayer.MODE_ONDEMAND,
          infoTemplate:template,
          outFields: ["*"]
        });
		
       // map.addLayer(featureLayer);
		
		//add a layer to the map
        var featureLayer2 = new FeatureLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/feedback3/MapServer/2", {
          mode: FeatureLayer.MODE_ONDEMAND,
          infoTemplate:template,
          outFields: ["*"]
        });
		
        //map.addLayer(featureLayer);
		map.addLayers([featureLayer0, featureLayer1, featureLayer2]);

        map.infoWindow.resize(150, 75);
		
 

		// ---------------------- DEFINE SERVICES -------------------------------	
			//esriConfig.defaults.io.proxyUrl = "/proxy/";
		esriConfig.defaults.io.proxyUrl = "../proxy/PHP/proxy.php";
        esriConfig.defaults.io.alwaysUseProxy = false;
		
			// SETUP PROXY for Buffer, Measure
		//	esriConfig.defaults.io.proxyUrl = 'dist/proxy-1.1.0/PHP/proxy.php';
		//	esriConfig.defaults.io.alwaysUseProxy = false;
//			esriConfig.defaults.io.corsDetection = false; // to get rid of HTTPS (share services in applications that utilize HTTPS)
			esriConfig.defaults.io.corsEnabledServers.push("https://www.gis.myetapp.gov.my/myetappgis");
			esriConfig.defaults.io.corsEnabledServers.push("https://www.gis.myetapp.gov.my");

		

		var coords, zoomLevel;  
		var urlObject = esri.urlToObject(document.location.href);  
	
		// Infowindows
		map.infoWindow.resize(550, 250);	
		//map.addLayers([BasicLayer, FloodInfoLayer, RainfallStationLayer, FloodModellingLayer]);
		
		
		// Resize map
		map.on('resize', function (evt) {
			var pnt = evt.target.extent.getCenter();
			setTimeout(function () { evt.target.centerAt(pnt);}, 500);
		});


		/////////////////////////////////////////////////////////

			   dynaLayer1 = new ArcGISDynamicMapServiceLayer("http://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/sani/1MyMap/MapServer", {
                opacity: 0.8
              });
			  
			  dynaLayer2 = new ArcGISDynamicMapServiceLayer("https://"+ipaddress+"/arcgis/rest/services/JKPTG4/charting/MapServer", {
                opacity: 0.8
              });
			  
			  dynaLayer3 = new ArcGISDynamicMapServiceLayer("http://1malaysiamap.mygeoportal.gov.my/arcgis/rest/services/jalan_jkr/Rangkaian_Jalan_JKR/MapServer", {
                opacity: 0.8
              });
			  
             
			  map.on('layers-add-result', function(evt){
                // overwrite the default visibility of service.
                // TOC will honor the overwritten value.
				
				   // SET LAYER VISIBILITY ================================================
					 dynaLayer1.setVisibility(false); // to default unchecked
					 dynaLayer3.setVisibility(false); // to default unchecked
	
                   dynaLayer1.setVisibleLayers([]);
				   dynaLayer3.setVisibleLayers([]);
				   dynaLayer2.setVisibleLayers([]);
                //try {
                  toc = new TOC({
                    map: map,
                    layerInfos: [
					/*{
                      layer: featLayer1,
                      title: "FeatureLayer1"
                    },*/
					
					{
                      layer: dynaLayer2,
                      title: "Charting",
                      //collapsed: false, // whether this root layer should be collapsed initially, default false.
                      slider: true // whether to display a transparency slider.
                    }
					
					// {
     //                  layer: dynaLayer1,
     //                  title: "1 Malaysia Map"
     //                  //collapsed: false, // whether this root layer should be collapsed initially, default false.
     //                  //slider: false // whether to display a transparency slider.
     //                },
										
					// {
     //                  layer: dynaLayer3,
     //                  title: "JKR Road"
     //                  //collapsed: false, // whether this root layer should be collapsed initially, default false.
     //                  //slider: false // whether to display a transparency slider.
     //                }					
					]
                  }, 'tocDiv');
                  toc.startup();
         
			
                //} catch (e) {  alert(e); }
              });
              map.addLayers([dynaLayer2, dynaLayer1, dynaLayer3]);


/////////////////////////////////////////////////////////

	// ---------------------- Show Loading ----------------------
        on(map, "update-start", showLoading);
		on(map, "update-end", hideLoading);
		
		 function showLoading() {
			esri.show(loading);
			map.disableMapNavigation();
			map.hideZoomSlider();
		  }

		 function hideLoading(error) {
			esri.hide(loading);
			map.enableMapNavigation();
			map.showZoomSlider();
		  } 
	

        var myObject = {
            id: "myObject",
            onClick: function(evt){
				acknowledge(this.id);
            }
        };
        var div = dom.byId("parentDiv");
        on(div, ".clickMe:click", myObject.onClick);


	 function acknowledge(val)
	  {  
	  
			var lat_value = document.getElementById("coory"+val).value  
			var long_value = document.getElementById("coorx"+val).value 
			
			xmin =  98.947096;
			ymin =  0.358901;
			xmax = 119.689284;
			ymax = 8.414430;
			

			
				// check for values
				if ( (long_value >= xmin && long_value <= xmax ) && (lat_value >= ymin && lat_value <= ymax ) ) {

					var mp = new Point(long_value, lat_value);  
					map.centerAndZoom(mp, 16);
				}
				
				else {
					alert ("Please enter valid X and Y values");
				}
		
		}	
		  
		 // ---------------------- Activate HOME button ----------------------
		var home = new HomeButton({ map: map}, "HomeButton");      	
		home.title = "This is the new title"; 
		home.startup();	

		map.on("load", function() {
          //after map loads, connect to listen to mouse move & drag events
          map.on("mouse-move", showCoordinates);
          map.on("mouse-drag", showCoordinates);
	
        });	
		
		// ---------------------- ERASE button ----------------------
		on(dom.byId("EraseButton"), "click", function(){
			if(map){ 
				map.graphics.clear(); 

			}
        }); 
		
		// ---------------------- LOCATE/Geolocation button ----------------------
		geoLocate = new LocateButton({
			map: map
		}, "LocateButton");
		geoLocate.startup();

		// ---------------------- Show Coordinates ----------------------
        function showCoordinates(evt) {
			//the map is in web mercator but display coordinates in geographic (lat, long)
			var mp = webMercatorUtils.webMercatorToGeographic(evt.mapPoint);
			//display mouse coordinates
			dom.byId("coordinate_info").innerHTML = "X: "+ mp.x.toFixed(6) + ", Y: " + mp.y.toFixed(6);
        }
		
		// ---------------------- SCALEBAR --------------------------------
		var scalebar = new Scalebar({
			map: map,
			attachTo: 'bottom-left',
			scalebarStyle: 'line',
			scalebarUnit: 'dual'
		});
		

		// ------------------------------- OVERVIEW MAP-------- -----------------------
		var overviewMapDijit = new OverviewMap({
			map: map,
			attachTo: "bottom-right",
			/*color:"#fdf874",*/
			height : 200,
			width : 225,
			opacity: .40
        });
		
        overviewMapDijit.startup();
		
		// ---------------------- BASEMAP TOGGLE ---------------------
		var toggle = new BasemapToggle({
			map: map,
			basemap: "osm"
		}, "BasemapToggle");
      
		toggle.startup();
		
		// ---------------------- BOOKMARK ---------------------
		// Create the bookmark widget
          // specify "editable" as true to enable editing
          var bookmarks = new Bookmarks({
            map: map,
            bookmarks: [],
            editable: true
          }, "bookmarks");

          // Bookmark data objects
          var bookmarkJSON = {
            first: {
              "extent": {
                "xmin": -12975151.579395358,
                "ymin": 3993919.9969406975,
                "xmax": -12964144.647322308,
                "ymax": 4019507.292159126,
                "spatialReference": {
                  "wkid": 102100,
                  "latestWkid": 3857
                }
              },
              "name": "Palm Springs, CA"
            },
            second: {
              "extent": {
                "xmin": -13052123.666878553,
                "ymin": 4024962.9850527253,
                "xmax": -13041116.734805504,
                "ymax": 4050550.280271154,
                "spatialReference": {
                  "wkid": 102100,
                  "latestWkid": 3857
                }
              },
              "name": "Redlands, California"
            },
            third: {
              "extent": {
                "xmin": -13048836.874662295,
                "ymin": 3844839.127898948,
                "xmax": -13037829.942589246,
                "ymax": 3870426.4231173764,
                "spatialReference": {
                  "wkid": 102100,
                  "latestWkid": 3857
                }
              },
              "name": "San Diego, CA"
            },
          };

          // Add bookmarks to the widget
          Object.keys(bookmarkJSON).forEach(function (bookmark){
            bookmarks.addBookmark(bookmarkJSON[bookmark]);
          });
    

		

      });// close function
	  
	
		
		
	  