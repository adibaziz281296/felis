<?php 
# ============================= GET CONNECTED TO DATABASE ============================= 
include("../../assets/conn/sql_server.php"); 

	// AUDIT TRAIL OPEN 
	// include("../../common/auditTrail.php"); //call audit trail function
#======================================================================================

//if(isset($_POST['submit']))
//{

	$icNo = $_POST["icNo"];
	
	# Get Timestamp
	date_default_timezone_set('Asia/Kuala_Lumpur');
	$now = date('Y-m-d H:i:s');
	
	
	# Check if victim already register or not, using CHECKIN field
	$sql_vic = "SELECT * FROM MANGSAMASUK WHERE noIC LIKE ?"; 
	$params_vic = array($icNo);
	$options_vic =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_vic = sqlsrv_query($conn, $sql_vic, $params_vic, $options_vic );	
	$row_vic = sqlsrv_fetch_array( $stmt_vic, SQLSRV_FETCH_ASSOC);
	$row_count_vic = sqlsrv_num_rows( $stmt_vic ); 
	
	$sql_previc = "SELECT * FROM MANGSABANJIR WHERE noIC LIKE ?"; 
	$params_previc = array($icNo);
	$options_previc =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_previc = sqlsrv_query($conn, $sql_previc, $params_previc, $options_previc );	
	$row_previc = sqlsrv_fetch_array( $stmt_previc, SQLSRV_FETCH_ASSOC);
	$row_count_previc = sqlsrv_num_rows( $stmt_previc ); 
	
	
	if( $stmt_vic === false ) { print( print_r( sqlsrv_errors() ) ); }
									
	if($row_count_vic > 0) {
		echo '<script language = "Javascript">';
		echo 'alert("Mangsa ini telah berdaftar \nSila lihat maklumat mangsa tersebut.");';
		echo 'window.location = "info.php?vicIcNo='.$icNo.'"';
		echo '</script>';
	} else if($row_count_previc > 0) {
		echo '<script language = "Javascript">';
		echo 'alert("Rekod pradaftar ditemui ini.\nSila lengkapkan maklumat mangsa tersebut.");';
		echo 'window.location = "add_bypre.php?vicIcNo='.$icNo.'"';
		echo '</script>';
	}
	else 
	{	?>							
				<script language = "Javascript">
					alert("Mangsa banjir ini belum didaftarkan.")
					window.location = 'add.php';
				</script>
<?php } ?>