// =============================================================================================
//								VALIDATE CHECK USER FORM
// =============================================================================================
function val_check(form, succurl) { 

	var theMessage = "Sila lengkapkan maklumat berikut: \n--------------------------------------------\n";
	var noErrors = theMessage;

	if (form.icNo.value=="") {
		theMessage = theMessage + "\n -> No. Kad Pengenalan ";
	}
	
// If all completed, check the entered values one by one
	if (theMessage == noErrors) {
	
		var numericExpression = /^[0-9]+$/;
	
			if (form.icNo.value.length < 12){
				  alert("No. MyKad/Kad Pengenalan tidak lengkap");
				  form.icNo.focus();	  
				  return false;
			}
			else if(!form.icNo.value.match(numericExpression)){
				alert("Sila Masukkan No. MyKad/Kad Pengenalan dengan format yang betul");
				form.icNo.focus();
				return false;
			}			
			else { // bila semuanya dah oke
				document.form.action=succurl;
			}
	} 
	
	else {
		// If errors were found, show alert message
		alert(theMessage);
		return false;
	}
}// End

// =============================================================================================
//								VALIDATE ADD VICTIM FORM
// =============================================================================================

function getOptions() {
			make=document.getElementById("state").value; //replace with where you actually get the make from
			//alert(make);
			var xmlRequest = new XMLHttpRequest();
			xmlRequest.onreadystatechange=function(){
				if (xmlRequest.readyState==4 && xmlRequest.status==200) {
					document.getElementById("tableContainerDistrict").innerHTML = xmlRequest.responseText;
					//someSelector is the ID of your <select> tag.
				}
			}
			xmlRequest.open("GET","getOptions_district.php?state="+make,true);
			xmlRequest.send();
		}
		
		function getOptionsMukim() {
			make=document.getElementById("district").value; //replace with where you actually get the make from
			//alert(make);
			var xmlRequest = new XMLHttpRequest();
			xmlRequest.onreadystatechange=function(){
				if (xmlRequest.readyState==4 && xmlRequest.status==200) {
					document.getElementById("tableContainerMukim").innerHTML = xmlRequest.responseText;
					//someSelector is the ID of your <select> tag.
				}
			}
			xmlRequest.open("GET","getOptions_mukim.php?district="+make,true);
			xmlRequest.send();
		}
		
		function getOptionsEvacuation() { 
			make=document.getElementById("mukim").value; //replace with where you actually get the make from
			//alert(make);
			var xmlRequest = new XMLHttpRequest();
			xmlRequest.onreadystatechange=function(){
				if (xmlRequest.readyState==4 && xmlRequest.status==200) {
					document.getElementById("tableContainerEvacuation").innerHTML = xmlRequest.responseText;
					//someSelector is the ID of your <select> tag.
				}
			}
			xmlRequest.open("GET","getOptions_evacuation.php?mukim="+make,true);
			xmlRequest.send();
		}
		
		
function val_add2(form, succurl) { alert("masuk sini");
alert("sini ok");
	var theMessage = "Sila lengkapkan maklumat berikut: \n--------------------------------------------\n";
	var noErrors = theMessage;

	/*if (form.fullName.value=="") {
		theMessage = theMessage + "\n -> Nama Mangsa";
	}
	
	alert("sini ok");

	/*if (form.icNo.value=="") {
		theMessage = theMessage + "\n -> No. Kad Pengenalan ";
	}
	
	if (form.umur.value=="") {
		theMessage = theMessage + "\n -> Umur ";
	}
	
	var listCheck = form.sex.selectedIndex;
	if (form.sex.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Jantina ";
	}
	
	var listCheck = form.race.selectedIndex;
	if (form.race.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Bangsa ";
	}

	var listCheck = form.status.selectedIndex;
	if (form.status.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Status OKU ";
	}

	if (form.no_tel.value=="") {
		theMessage = theMessage + "\n -> No. Telefon ";
	}
	
	var listCheck = form.state.selectedIndex;
	if (form.state.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Negeri ";
	}
	
	var listCheck = form.district.selectedIndex;
	if (form.district.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Daerah ";
	}
	
	var listCheck = form.mukim.selectedIndex;
	if (form.mukim.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Mukim ";
	}
	
	var listCheck = form.evacuation.selectedIndex;
	if (form.evacuation.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Pusat Penempatan Banjir ";
	}
	
	
// If all completed, check the entered values one by one
	if (theMessage == noErrors) {
	
		var numericExpression = /^[0-9]+$/;
	
			if (form.icNo.value.length < 12){
				  alert("No. MyKad/Kad Pengenalan tidak lengkap");
				  form.icNo.focus();	  
				  return false;
			}
			else if(!form.icNo.value.match(numericExpression)){
				alert("Sila Masukkan No. MyKad/Kad Pengenalan dengan format yang betul");
				form.icNo.focus();
				return false;
			}
			else if(!form.umur.value.match(numericExpression)){
				alert("Sila Masukkan umur dengan format yang betul");
				form.umur.focus();
				return false;
			}
			else { // bila semuanya dah oke
				document.form.action=succurl;
			}
	} 
	
	else {
		// If errors were found, show alert message
		alert(theMessage);
		return false;
	}*/
}// End



// =============================================================================================
//								DATA: VALIDATE EDIT FORM
// =============================================================================================
function val_edit(form, succurl) {
	var theMessage = "Sila lengkapkan maklumat berikut: \n--------------------------------------------\n";
	var noErrors = theMessage

	if (form.fullName.value=="") {
		theMessage = theMessage + "\n -> Nama Pegawai";
	}

	if (form.icNo.value=="") {
		theMessage = theMessage + "\n -> No. Kad Pengenalan ";
	}
	
	var listCheck = form.levelName.selectedIndex;
	if (form.levelName.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Agensi Pengguna ";
	}
	
	// END CHECKING
	if (theMessage == noErrors) {
			document.form.action=succurl; 
	} 
	else {
		alert(theMessage);
		return false;
	}
}

// =============================================================================================
//								DATA: VALIDATE EDIT FORM
// =============================================================================================
function val_del(form, succurl) {
	var confirmSet = confirm ("Anda pasti untuk menghapuskan maklumat pengguna ini?");
	
	if(confirmSet == true){
			document.form.action=succurl; 
	} 
	else {
		return false;
	}
}


// =============================================================================================
//								OTHER FUNCTIONS
// =============================================================================================

function openPage(url) {
	window.location = url;
	}
	
	function wopen(url, name) {
		var win = window.open(url,
			name, 
				'width=' + screen.width  + ', height=' + screen.height + ', ' +
				'location=no, menubar=no, status=no, toolbar=no, ' +
				'scrollbars=yes, resizable=yes, directories=no');

		win.moveTo(0, 0);
		win.resizeTo(screen.width, screen.height);
		win.focus();
	}
	

function allRec(form) { 
	var succurl = 'list.php';
	
	form.kword_cat.value = 0;
	form.kword_dae.value = 0;
	form.kword_desc.value = "";
	
	document.form.action="list.php?kcat=0&kdae=0&kdesc=";
	document.form.submit();
}

function allReset(form) { 
	form.kword_cat.value = 0;
	form.kword_dae.value = 0;
	form.kword_desc.value = "";
}