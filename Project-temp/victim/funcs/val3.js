// =============================================================================================
//								VALIDATE CHECK USER FORM
// =============================================================================================
function val_check(form, succurl) { 

	var theMessage = "Sila lengkapkan maklumat berikut: \n--------------------------------------------\n";
	var noErrors = theMessage;

	if (form.icNo.value=="") {
		theMessage = theMessage + "\n -> No. Kad Pengenalan ";
	}
	
// If all completed, check the entered values one by one
	if (theMessage == noErrors) {
	
		var numericExpression = /^[0-9]+$/;
	
			if (form.icNo.value.length < 12){
				  alert("No. MyKad/Kad Pengenalan tidak lengkap");
				  form.icNo.focus();	  
				  return false;
			}
			else if(!form.icNo.value.match(numericExpression)){
				alert("Sila Masukkan No. MyKad/Kad Pengenalan dengan format yang betul");
				form.icNo.focus();
				return false;
			}			
			else { // bila semuanya dah oke
				document.form.action=succurl;
			}
	} 
	
	else {
		// If errors were found, show alert message
		alert(theMessage);
		return false;
	}
}// End

// =============================================================================================
//								VALIDATE ADD VICTIM FORM
// =============================================================================================

function val_add(form, succurl) { 

	document.form.action=succurl;

	
}// End



// =============================================================================================
//								DATA: VALIDATE EDIT FORM
// =============================================================================================
function val_edit(form, succurl) {
	var theMessage = "Sila lengkapkan maklumat berikut: \n--------------------------------------------\n";
	var noErrors = theMessage

	if (form.fullName.value=="") {
		theMessage = theMessage + "\n -> Nama Pegawai";
	}

	if (form.icNo.value=="") {
		theMessage = theMessage + "\n -> No. Kad Pengenalan ";
	}
	
	var listCheck = form.levelName.selectedIndex;
	if (form.levelName.options[listCheck].value=="0") {
		theMessage = theMessage + "\n -> Agensi Pengguna ";
	}
	
	// END CHECKING
	if (theMessage == noErrors) {
			document.form.action=succurl; 
	} 
	else {
		alert(theMessage);
		return false;
	}
}

// =============================================================================================
//								DATA: VALIDATE EDIT FORM
// =============================================================================================
function val_del(form, succurl) {
	var confirmSet = confirm ("Anda pasti untuk menghapuskan maklumat pengguna ini?");
	
	if(confirmSet == true){
			document.form.action=succurl; 
	} 
	else {
		return false;
	}
}


// =============================================================================================
//								OTHER FUNCTIONS
// =============================================================================================

function openPage(url) {
	window.location = url;
	}
	
	function wopen(url, name) {
		var win = window.open(url,
			name, 
				'width=' + screen.width  + ', height=' + screen.height + ', ' +
				'location=no, menubar=no, status=no, toolbar=no, ' +
				'scrollbars=yes, resizable=yes, directories=no');

		win.moveTo(0, 0);
		win.resizeTo(screen.width, screen.height);
		win.focus();
	}
	

function allRec(form) { 
	var succurl = 'list.php';
	
	form.kword_cat.value = 0;
	form.kword_dae.value = 0;
	form.kword_desc.value = "";
	
	document.form.action="list.php?kcat=0&kdae=0&kdesc=";
	document.form.submit();
}

function allReset(form) { 
	form.kword_cat.value = 0;
	form.kword_dae.value = 0;
	form.kword_desc.value = "";
}