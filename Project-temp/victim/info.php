<?php
	include("../../assets/conn/sql_server.php");

	# RETRIEVE VICTIM INFO	-------------------------------------------------------------------------------
	$icNo = $_GET['vicIcNo'];
	//echo $icNo;
	
	/*$sql_vic = "SELECT * FROM MANGSAMASUK m, MANGSABANJIR b
				WHERE m.noIC LIKE b.noIC
				AND m.noIC LIKE ?"; */
	$sql_vic = "SELECT * FROM MANGSAMASUK
				WHERE noIC LIKE ?"; 			
	$params_vic = array($icNo);
	$options_vic =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_vic = sqlsrv_query($conn, $sql_vic, $params_vic, $options_vic );	
	$row_vic = sqlsrv_fetch_array( $stmt_vic, SQLSRV_FETCH_ASSOC);
	
	if( $stmt_vic === false ) { print( print_r( sqlsrv_errors() ) ); }
	
	$sql_previc = "SELECT * FROM MANGSAMASUK m, MANGSABANJIR b
				WHERE m.noIC LIKE b.noIC
				AND m.noIC LIKE ?"; 
	$params_previc = array($icNo);
	$options_previc  =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt_previc  = sqlsrv_query($conn, $sql_previc, $params_previc, $options_previc );	
	$row_previc  = sqlsrv_fetch_array( $stmt_previc , SQLSRV_FETCH_ASSOC);
	$row_count_previc = sqlsrv_num_rows( $stmt_previc ); 
	
	if( $stmt_previc  === false ) { print( print_r( sqlsrv_errors() ) ); }
	
	if($row_count_previc > 0) $pusat_pradaftar = $row_vic['pusat_pemindahan'];
	else $pusat_pradaftar = 'Tiada';
	
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Maklumat Mangsa Banjir Berdaftar</title>               
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
                        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                   
    </head>
    <body>           
            <!-- PAGE CONTENT -->
            <div class="page-content">                             
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" name="form"  method="post" action="list.php">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Maklumat Mangsa Banjir Berdaftar</h3>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Nama</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="fullName" value="<?php echo $row_vic['mangsaNama']; ?>" />
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">No. MyKad/Kad Pengenalan</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="icNo" value="<?php echo $row_vic['noIC']; ?>" />
                                            </div>                                            
                                        </div>
                                    </div>
									
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Jantina</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="icNo" value="<?php echo $row_vic['jantina']; ?>" />
                                            </div>                                            
                                        </div>
                                    </div>

									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Umur</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $row_vic['umur']; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Bangsa</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $row_vic['bangsa']; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $row_vic['status']; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">No. Tel</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $row_vic['no_tel']; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Mukim</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $row_vic['mukim']; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Daerah</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $row_vic['daerah']; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Negeri</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $row_vic['negeri']; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Pusat Penempatan Pra Daftar</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $pusat_pradaftar ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<hr/>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Pusat Penempatan Sementara</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo $row_vic['lokasi_masuk']; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Tarikh & Masa Daftar</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo date_format($row_vic['tarikh_masuk'], 'd/m/Y H:i:s'); ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									
									<?php if($row_vic['tarikh_keluar'] >= $row_vic['tarikh_masuk']) { ?>
									
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Tarikh & Masa Keluar</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="userName" value="<?php echo date_format($row_vic['tarikh_keluar'], 'd/m/Y H:i:s'); ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
									
									<?php } ?>
                                                                       
                                </div>
                                <div class="panel-footer">
									<button type="submit" class="btn btn-info pull-right">Senarai Mangsa Berdaftar</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->             
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>                
        <!-- END PLUGINS -->
        
        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <!-- END THIS PAGE PLUGINS -->       
        
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/settings.js"></script>
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->                   
    </body>
</html>






