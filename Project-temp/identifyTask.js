// ---------------------- IDENTIFY TASK --------------------------------------	
function runIdentifies(evt) {  
	identifyResults = [];  
	idPoint = evt.mapPoint;  
	var layers = dojo.map(map.layerIds, function (layerId) {  
		return map.getLayer(layerId);  
	});  
	layers = dojo.filter(layers, function (layer) {  
		if (layer.visibleLayers[0] !== -1) {  
			return layer.getImageUrl && layer.visible  
		}  
	}); //Only dynamic layers have the getImageUrl function. Filter so you only query visible dynamic layers  
	var tasks = dojo.map(layers, function (layer) {  
		return new IdentifyTask(layer.url);  
	}); //map each visible dynamic layer to a new identify task, using the layer url  
	var defTasks = dojo.map(tasks, function (task) {  
		return new dojo.Deferred();  
	}); //map each identify task to a new dojo.Deferred  
	var params = createIdentifyParams(layers, evt);  

	var promises = [];  

	for (i = 0; i < tasks.length; i++) {  
		promises.push(tasks[i].execute(params[i])); //Execute each task  
	}  

	var allPromises = new All(promises);  
	allPromises.then(function (r) { showIdentifyResults(r, tasks); });  
}		

function showIdentifyResults(r, tasks) {  
	var results = [];  
	var taskUrls = [];  
	r = dojo.filter(r, function (result) {  
		return r[0];  
	});  
	for (i = 0; i < r.length; i++) {  
		results = results.concat(r[i]);  
		for (j = 0; j < r[i].length; j++) {  
			taskUrls = taskUrls.concat(tasks[i].url);  
		}  
	}  
	results = dojo.map(results, function (result, index) {  
		var feature = result.feature;  
		var layerName = result.layerName;  
		var serviceUrl = taskUrls[index];  
		feature.attributes.layerName = result.layerName;  

		/*var template = new InfoTemplate("", "Service Url: " + serviceUrl + "<br/><br/>Layer name: " + result.layerName + "<br/><br/> Object Id: ${OBJECTID}");  
		feature.setInfoTemplate(template); */
		
		switch(layerName){	
			case 'Kecemasan':
					var info_emergency = new esri.InfoTemplate();
					info_emergency.setTitle("Pusat Tindakan Kecemasan");
					info_emergency.setContent(
											"<table id='info'>" +
												"<tr><td><b>NAMA</b></td><td><b>: </b>${Nama}</td></tr>" +
												"<tr><td><b>KATEGORI</b></td><td><b>: </b>${Kategori}</td></tr>" +
												"<tr><td><b>DAERAH</b></td><td><b>: </b>${Daerah}</td></tr>" + 
												"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
												"<tr><td colspan='2' align='center'><img src='../assets/img/Kecemasan/${Gambar2}' width='100%' height='20%'/></td></tr>" +
											"</table>" 
					 ); 
					 feature.setInfoTemplate(info_emergency);   			
			break;
			
			case 'Penempatan Banjir':
					var info_evacuation = new esri.InfoTemplate();
					info_evacuation.setTitle("Penempatan Banjir");
					info_evacuation.setContent(
											"<table id='info'>" +
												"<tr><td><b>NAMA</b></td><td><b>: </b>${nama}</td></tr>" +
												"<tr><td><b>KAPASITI</b></td><td><b>: </b>${kapasiti}</td></tr>" +
												"<tr><td><b>JENIS</b></td><td><b>: </b>${jenis}</td></tr>" +
												"<tr><td><b>MUKIM</b></td><td><b>: </b>${mukim}</td></tr>" +
												"<tr><td><b>DAERAH</b></td><td><b>: </b>${daerah}</td></tr>" +
												"<tr><td><b>SENARAI MANGSA BANJIR</b></td><td><b>: </b><a href='${HYPERLINK}' target='blank'>HYPERLINK</a></td></tr>" +
												"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
												"<tr><td colspan='2' align='center'><img src='../assets/img/Point Of Interest/${photos2}' width='100%' height='20%'</td></tr>" +
											"</table>" 
					 ); 
					 
					 feature.setInfoTemplate(info_evacuation);   			
			break;	
			
			
			case 'Stesen Hujan':
					var info_rainStation = new esri.InfoTemplate();
					info_rainStation.setTitle("Stesen Hujan");
					info_rainStation.setContent(
											"<table id='info'>" +
												"<tr><td><b>NAMA</b></td><td><b>: </b>${Nama}</td></tr>" +
												"<tr><td><b>ID Stesen</b></td><td><b>: </b>${ID_Stesen}</td></tr>" +
												"<tr><td><b>Status</b></td><td><b>: </b>${Status}</td></tr>" +
											"</table>" 
					 ); 
					 
					 feature.setInfoTemplate(info_rainStation);   			
			break;
			
			case 'Point of Interest':
					var info_poi = new esri.InfoTemplate();
					info_poi.setTitle("Point of Interest 3");
					info_poi.setContent(
											"<table id='info'>" +
												"<tr><td><b>NAMA</b></td><td><b>: </b>${NAME}</td></tr>" +
												"<tr><td><b>JENIS</b></td><td><b>: </b>${TYPE}</td></tr>" +
												"<tr><td><b>KATEGORI</b></td><td><b>: </b>${CATEGORY}</td></tr>" +
												"<tr><td><b>DAERAH</b></td><td><b>: </b>${DISTRICT}</td></tr>" +
											"</table>" 
					 ); 
					 
					 feature.setInfoTemplate(info_poi);   			
			break;		

			case 'Sungai':
					var info_sungai = new esri.InfoTemplate();
					info_sungai .setTitle("Jalan");
					info_sungai .setContent(
											"<table id='info'>" +
												"<tr><td><b>NAMA</b></td><td><b>: </b>${Nama}</td></tr>" +
												"<tr><td><b>DAERAH</b></td><td><b>: </b>${Daerah}</td></tr>" +
												"<tr><td><b>JENIS</b></td><td><b>: </b>${Jenis}</td></tr>" +
												"<tr><td><b>PANJANG</b></td><td><b>: </b>${Shape_Length}</td></tr>" +
											"</table>" 
					 ); 
					 
					 feature.setInfoTemplate(info_sungai);   			
			break;
			
			case 'Jalanraya':
					var info_jalan = new esri.InfoTemplate();
					info_jalan .setTitle("Jalan");
					info_jalan .setContent(
											"<table id='info'>" +
												"<tr><td><b>NAMA</b></td><td><b>: </b>${Nama}</td></tr>" +
												"<tr><td><b>JENIS</b></td><td><b>: </b>${Jenis}</td></tr>" +
												"<tr><td><b>PANJANG</b></td><td><b>: </b>${Shape_Length}</td></tr>" +
											"</table>" 
					 ); 
					 
					 feature.setInfoTemplate(info_jalan );   			
			break;	
			
			case 'Sempadan Mukim':
					var info_mukim = new esri.InfoTemplate();
					info_mukim.setTitle("Sempadan Mukim");
					info_mukim.setContent(
											"<table id='info'>" +
												"<tr><td><b>NAMA</b></td><td><b>: </b>${Mukim}</td></tr>" +
											"</table>" 
					 ); 
					 
					 feature.setInfoTemplate(info_mukim);   			
			break;
			
			case 'Sempadan Daerah':
					// 37. DISTRICT POPUP INFO: Specify fields & Content
					var info_district = new esri.InfoTemplate();
					info_district.setTitle("Sempadan Daerah");
					info_district.setContent(
											"<table id='info'>" +
												"<tr><td><b>NAMA</b></td><td><b>: </b>${NAME}</td></tr>" +
											"</table>" 
					 ); 
					 
					 feature.setInfoTemplate(info_district);   			
			break;	

			case 'Sempadan Negeri':
					// 38. STATE POPUP INFO: Specify fields & Content
					var info_state = new esri.InfoTemplate();
					info_state.setTitle("Sempadan Negeri");
					info_state.setContent(
											"<table id='info'>" +
												"<tr><td><b>NEGERI</b></td><td><b>: </b>${STATE}</td></tr>" +
												"<tr><td><b>PERIMETER</b></td><td><b>: </b>${PERIMETER}</td></tr>" +
												"<tr><td><b>LUAS (HEKTAR)</b></td><td><b>: </b>${HECTARES}</td></tr>" +	
												"<tr><td><b>X Coor</b></td><td><b>: </b>${Xcoor}</td></tr>" +
												"<tr><td><b>Y Coor</b></td><td><b>: </b>${Ycoor}</td></tr>" +
											"</table>"  
					 ); 
					 
					 feature.setInfoTemplate(info_state);   			
			break; 	
			
			default:
					var template = new esri.InfoTemplate("${*}");
					template.setTitle("Maklumat "+ layerName);
					feature.setInfoTemplate(template);
			break;
			
		} //close switch
		
		

		var resultGeometry = feature.geometry;  
		var resultType = resultGeometry.type;  
		return feature;  
	});  

	if (results.length === 0) {  
		map.infoWindow.clearFeatures();  
	} else {  
		map.infoWindow.setFeatures(results);  
	}  
	map.infoWindow.show(idPoint);  
	return results;  
}  		

function createIdentifyParams(layers, evt) {  
	var identifyParamsList = [];  
	identifyParamsList.length = 0;  
	dojo.forEach(layers, function (layer) {  
		var idParams = new esri.tasks.IdentifyParameters();  
		idParams.width = map.width;  
		idParams.height = map.height;  
		idParams.geometry = evt.mapPoint;  
		idParams.mapExtent = map.extent;  
		idParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_VISIBLE;  
		var visLayers = layer.visibleLayers;  
		if (visLayers !== -1) {  
			var subLayers = [];  
			for (var i = 0; i < layer.layerInfos.length; i++) {  
				if (layer.layerInfos[i].subLayerIds == null)  
					subLayers.push(layer.layerInfos[i].id);  
			}  
			idParams.layerIds = subLayers;  
		} else {  
			idParams.layerIds = [];  
		}  
		idParams.tolerance = 3;  
		idParams.returnGeometry = true;  
		identifyParamsList.push(idParams);  
	});  
	return identifyParamsList;  
}			