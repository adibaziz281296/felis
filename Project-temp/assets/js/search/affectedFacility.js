/**
 * @author Fadhilah, Nov 2015
 * Search Facility Centre Props
 */
var prevfeature = null;
 
// Create Facility Centre result grid
var gridFacility_Structure =[[
	// declare your structure here
	{name: 'Name', field: 'NAME', width: '40%'},
	{name: 'Type', field: 'TYPE'},
	{name: 'Category', field: 'CATEGORY'},
	{name: 'State', field: ''},
	{name: 'City', field: 'DISTRICT'},
]];

// Facility Centre: REGION Dropdown 			
function populateList_state_facility(results) {
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.STATE
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_facility").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		
// Facility Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_facility(results) {
	dijit.byId("select_district_facility").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DISTRICT;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_facility").set('store', store);
}

// Facility Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Facility Centre: Execute Search
function executeQueryTask_AffectedFacility() { console.log("execute task");
	// Check if all values have been selected
	var state = dojo.byId('select_state_facility').value;
	var district = dojo.byId('select_district_facility').value;
	var facility_name = dojo.byId('facility_name').value;
	
	console.log(state);
	console.log(district);
	console.log(facility_name);
	
	var kes;
	
	if(state == "Select state..." && district == "Select district..." && facility_name == "") kes = 1; // none
	else if(state != "Select state..." && district == "Select district..." && facility_name == "") kes = 2; // state
	else if(state != "Select state..." && district != "Select district..." && facility_name == "") kes = 3; // state & district
	else if(state != "Select state..." && district == "Select district..." && facility_name != "") kes = 4; // state & facility
	else if(state != "Select state..." && district != "Select district..." && facility_name != "") kes = 5; // ALL
	else kes = 'default';
	
	console.log(kes);
	
	switch (kes){
		case 1:
			query_AffectedFacility.where = "NAME <> ''";
		break;
		
		case 2:
			//query_AffectedFacility.where = "STATE = '" + state  + "'";
			query_AffectedFacility.where = "NAME <> ''";
		break;
		
		case 3:
			query_AffectedFacility.where = "DISTRICT = '" + district + "'";
		break;
		
		case 4:
			//query_AffectedFacility.where = "STATE = '" + state  + "' AND NAME LIKE '%" + facility_name + 
			query_AffectedFacility.where = "NAME LIKE '%" + facility_name + "%'";
		break;
		
		case 5:
			//query_AffectedFacility.where = "STATE = '" + state  + "' AND DISTRICT = '" + district + "' AND NAME LIKE '%" + facility_name + "%'";
			query_AffectedFacility.where = "DISTRICT = '" + district + "' AND NAME LIKE '%" + facility_name + "%'";
		break;
		
		default:
			query_AffectedFacility.where = "NAME <> ''";
		break;		
	}
	
	// Execute query
	queryTask_AffectedFacility.execute(query_AffectedFacility,showResults_AffectedFacility);
	showResultpane_AffectedFacility();
} 

function showResultpane_AffectedFacility() {  
			dijit.byId('searchResultFacility').show();
			
			dijit.byId('searchFormEvacuation').hide(); 
			dijit.byId('searchFormEmergency').hide();
			dijit.byId('searchFormFacility').hide();
			dijit.byId('searchFormVictim').hide();
}

// Facility Centre: Show Search Result			
function showResults_AffectedFacility(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_AffectedFacility = dijit.byId("gridFacility");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_AffectedFacility.set("noDataMessage", 'Sorry, there are no results.');
		grid_AffectedFacility.setStore(clearStore); 					
	}
	
	else {								    			    
		prevfeature = null;
	
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_AffectedFacility = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_AffectedFacility.push(feature.attributes);  
		
			//Set the infoTemplate.
			var info_poi = new esri.InfoTemplate();
			info_poi.setTitle("Point of Interest");
			info_poi.setContent(
									"<table id='info'>" +
										"<tr><td><b>NAME</b></td><td><b>: </b>${NAME}</td></tr>" +
										"<tr><td><b>TYPE</b></td><td><b>: </b>${TYPE}</td></tr>" +
										"<tr><td><b>CATEGORY</b></td><td><b>: </b>${CATEGORY}</td></tr>" +
										"<tr><td><b>CITY</b></td><td><b>: </b>${DISTRICT}</td></tr>" +
									"</table>" 
			 ); 			         			      
			graphic.setInfoTemplate(info_poi); 
			//console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gFacility = evt.graphic;
          map.infoWindow.setContent(gFacility.getContent());
          map.infoWindow.setTitle(gFacility.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_AffectedFacility = {
			identifier : "OBJECTID", //This field needs to have unique values
			label : "OBJECTID", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_AffectedFacility
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_AffectedFacility,
				clearOnClose: true
		});
												
		grid_AffectedFacility.set("store", store);
		
		// Clear previous search results
		if(grid_AffectedFacility.store.save) { grid_AffectedFacility.store.save();}
		grid_AffectedFacility.store.close();				
		grid_AffectedFacility._refresh(); // or grid.store.fetch();								
		grid_AffectedFacility.filter();
		
		grid_AffectedFacility.on("rowClick", onRowClickHandler_grid_AffectedFacility);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_AffectedFacility(evt){
	console.log('prevfeature = '+ prevfeature);	
	var grid = dijit.byId("gridFacility");				
	var clickedId = grid.getItem(evt.rowIndex).objectid_12;
	console.log('clickedId = ' + clickedId);
	var selectedId;	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.objectid_12 == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;			
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_AffectedFacility(){
	dojo.byId('select_state_facility').value = "Select state...";
	dojo.byId('select_district_facility').value = "Select district...";
	dojo.byId('facility_name').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	clearQueryTask("gridFacility");
	dijit.byId('searchResultFacility').hide();
	
}