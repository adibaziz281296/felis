/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;
 
// Create Victim Centre result grid
var gridProject_Structure =[[
	// declare your structure here
	{name: 'KEGUNAAN TANAH', field: 'KEGUNAAN_TANAH', width: '30%'},
	{name: 'STATE', field: 'NEGERI'},
	{name: 'CITY', field: 'DAERAH'},	
	{name: 'SUB DISTRICT', field: 'MUKIM'}
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_project(results) {
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.NEGERI
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_project").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		
// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_project(results) {
	dijit.byId("select_district_project").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DAERAH;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_project").set('store', store);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_project(results) {
	dijit.byId("select_mukim_project").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukim = feature.attributes.MUKIM;
		if (!testVals[mukim]) {
			testVals[mukim] = true;
			values.push({name:mukim, id:mukim});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_project").set('store', store);
}

// Victim Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Victim Centre: Execute Search
function executeQueryTask_Project() { console.log("execute task search Project");
	// Check if all values have been selected
	var service = dojo.byId('select_service_project').value;
	if (service != 'Select service...') {
		var state = dojo.byId('select_state_project').value;
		var district = dojo.byId('select_district_project').value;
		var mukim = dojo.byId('select_mukim_project').value;
		var tanah = dojo.byId('tanah').value;
		
		console.log(state);
		console.log(district);
		console.log(mukim);
		console.log(tanah);

		var kes;
		
		if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tanah == "") kes = 1; // state
		else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && tanah == "") kes = 2; // state, district
		else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && tanah == "") kes = 3; // state, district, mukim
		else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && tanah != "") kes = 4; // state, district, mukim, tanah
		else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tanah != "") kes = 5; // state, tanah
		else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && tanah != "") kes = 6; // state, district, tanah
		else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tanah != "") kes = 7; // tanah
		else kes = 'default';
		
		console.log("kes = "+ kes);
		
		switch (kes){
			
			case 1:
				query_Project.where = "NEGERI = '" + state  + "' AND KEGUNAAN_TANAH <> ''";
			break;
			
			case 2:
				query_Project.where = "NEGERI = '" + state  + "'  AND daerah = '" + district + "'  AND KEGUNAAN_TANAH <> ''";
			break;
			
			case 3:
				query_Project.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "'  AND KEGUNAAN_TANAH <> ''";
			break;

			case 4:
				query_Project.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND KEGUNAAN_TANAH LIKE '%" + tanah + "%'";
			break;

			case 5:
				query_Project.where = "NEGERI = '" + state  + "' AND KEGUNAAN_TANAH LIKE '%" + tanah + "%'";
			break;

			case 6:
				query_Project.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND KEGUNAAN_TANAH LIKE '%" + tanah + "%'";
			break;

			case 7:
				query_Project.where = "KEGUNAAN_TANAH LIKE '%" + tanah + "%'";
			break;
			
			default:
				query_Project.where = "KEGUNAAN_TANAH <> ''";
			break;		
		}
		
		console.log(query_Project.where);

		// Execute query
		queryTask_Project.execute(query_Project, showResults_Project);
		showResultpane_Project();
	}
	else
	{
		alert('Please select service!!!');
	}
} 

function showResultpane_Project() {  
			dijit.byId('searchResultProject').show();

			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
}

// Project: Show Search Result			
function showResults_Project(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Project = dijit.byId("gridProject");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Project.set("noDataMessage", 'Sorry, there are no results.');
		grid_Project.setStore(clearStore); 	
		console.log("I'M HERE");				
	}
	
	else {								    			    
		prevfeature = null;
		console.log("I'M HERE TOO");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_Project = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_Project.push(feature.attributes);  
		
			//Set the infoTemplate.
			// var bil_mangsa_list = feature.attributes.bil_mangsa_daftar;
			// var turun_senarai = '';
			// if (bil_mangsa_list > 0) turun_senarai = "<a href='assets/flood_report/victim_list.xls' target='blank'>Download list</a>";
			// else turun_senarai = "-Tiada-";
				
			var info_project = new esri.InfoTemplate();
			info_project.setTitle("Project");
			info_project.setContent("No. Fail Pemohonan: ${KPKT} <br> Projek: ${CATATAN} <br> Tarikh Permohonan: ${PERMOHONAN} <br> Tanah Asal : ${TANAH_ASAL} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${SEMENTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNITLUAS} <br> Status : ${HAKMILIK1} <br> NEGERI: ${NEGERI} <br> DAERAH: ${DAERAH} <br> MUKIM : ${MUKIM} <br> SEKSYEN : ${SEKSYEN} <br> LOT : ${LOT} <br> UPI : ${UPI} <br> PA : ${PA}");

			// info_project.setContent(
			// 						"<table id='info'>" +
			// 							"<tr><td><b>LOT</b></td><td><b>: </b>${LOT}</td></tr>" +
			// 							"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
			// 						"</table>" 
			//  ); 
			graphic.setInfoTemplate(info_project); 
			//console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gProject = evt.graphic;
          map.infoWindow.setContent(gProject.getContent());
          map.infoWindow.setTitle(gProject.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_Project = {
			identifier : "OBJECTID_1", //This field needs to have unique values
			label : "OBJECTID_1", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Project
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Project,
				clearOnClose: true
		});
												
		grid_Project.set("store", store);
		
		// Clear previous search results
		if(grid_Project.store.save) { grid_Project.store.save();}
		grid_Project.store.close();				
		grid_Project._refresh(); // or grid.store.fetch();								
		grid_Project.filter();
		
		grid_Project.on("rowClick", onRowClickHandler_grid_Project);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Project(evt){
	var grid = dijit.byId("gridProject");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID_1;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID_1 == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_Project(){
	dojo.byId('select_service_project').value = "Select service...";
	dojo.byId('select_state_project').value = "Select state...";
	dojo.byId('select_district_project').value = "Select district...";
	dojo.byId('select_mukim_project').value = "Select sub district...";
	dojo.byId('tanah').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridProject");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridProject"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultProject').hide();
	
}