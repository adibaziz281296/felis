/**
 * @author Fadhilah, Nov 2015
 * Functions that access by all search functions
 */
 var center = [108.75,5.391]; // lon, lat
 var zoom = 5;
 
  
 // Get feature extent & zoom to the feature
 function getFeatureExtent(feature) { 
	// Create symbol for selected feature
	var clickPolygonSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 0]), 1), new dojo.Color([22, 22, 248, 0.6]));
	
	var clickPolylineSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_NULL, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 255, 0]), 2), new dojo.Color([0, 0, 0, 0]));
	
	/*var clickPointSymbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE, 10, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([22, 229, 248]), 2), new dojo.Color([22, 22, 248]));*/
	var clickPointSymbol = new esri.symbol.PictureMarkerSymbol("../assets/img/map_pin_hlight.png", 16, 36);

	// Reset color
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
		currentGraphic.setSymbol(symbolSearch);
	} 
	
	console.log(feature.geometry.type);
	switch (feature.geometry.type) {
		case "polygon":
			feature.setSymbol(clickPolygonSymbol);
			xmin = feature.geometry.getExtent().xmin - 0;
			ymin = feature.geometry.getExtent().ymin - 0;
			xmax = feature.geometry.getExtent().xmax + 0;
			ymax = feature.geometry.getExtent().ymax + 0;
		break;
			
		case "polyline":
			feature.setSymbol(clickPolylineSymbol);
			xmin = feature.geometry.getExtent().xmin - 300;
			ymin = feature.geometry.getExtent().ymin - 300;
			xmax = feature.geometry.getExtent().xmax + 300;
			ymax = feature.geometry.getExtent().ymax + 300;
		break;	
					   
		case "point": 					
			feature.setSymbol(clickPointSymbol);
			xmin = feature.geometry.x;
			ymin = feature.geometry.y;
			xmax = feature.geometry.x;
			ymax = feature.geometry.y;	
		break;
	}
	
	// Zoom to the clicked grid
	switch (feature.geometry.type) {
		case "polygon":
			map.setExtent(feature.geometry.getExtent());
		break;
			
		case "polyline":
			map.setExtent(feature.geometry.getExtent());
		break;	
					   
		case "point": 					
			map.setZoom(15);
			map.centerAt(feature.geometry); 
		break;
	}
}

function clearQueryTask(gridDiv) {	
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});			
		
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	//alert(gridDiv);
				
	// Remove previous search results
	var grid = dijit.byId(gridDiv); 
	grid.set("noDataMessage", 'Tiada item untuk dipaparkan.');				
	grid.setStore(clearStore);
	//map.centerAndZoom(center, zoom);	
}

function clearQueryTask_Coordinate() {			
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
				
	// Remove previous search results
	document.getElementById("latitude").value = "";
	document.getElementById("longitude").value = "";
	map.centerAndZoom(center, zoom);	
}