/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;
 
// Create Victim Centre result grid
var gridSementara_Structure =[[
	// declare your structure here
	{name: 'SEMENTARA', field: 'SEMENTARA', width: '30%'},
	{name: 'PTNO', field: 'PTNO'},
	{name: 'STATE', field: 'NEGERI'},
	{name: 'CITY', field: 'DAERAH'},	
	{name: 'SUB DISTRICT', field: 'MUKIM'}
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_sementara(results) {
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.NEGERI
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_sementara").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		
// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_sementara(results) {
	dijit.byId("select_district_sementara").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DAERAH;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_sementara").set('store', store);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_sementara(results) {
	dijit.byId("select_mukim_sementara").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukim = feature.attributes.MUKIM;
		if (!testVals[mukim]) {
			testVals[mukim] = true;
			values.push({name:mukim, id:mukim});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_sementara").set('store', store);
}

// Victim Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Victim Centre: Execute Search
function executeQueryTask_Sementara() { console.log("execute task search Hak Milik Sementara");
	// Check if all values have been selected
	var state = dojo.byId('select_state_sementara').value;
	var district = dojo.byId('select_district_sementara').value;
	var mukim = dojo.byId('select_mukim_sementara').value;
	var sementara = dojo.byId('sementara').value;
		
	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(sementara);

	var kes;
		
	if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && sementara == "") kes = 1; // state
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && sementara == "") kes = 2; // state, district
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && sementara == "") kes = 3; // state, district, mukim
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && sementara != "") kes = 4; // state, district, mukim, sementara
	else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && sementara != "") kes = 5; // state, sementara
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && sementara != "") kes = 6; // state, district, sementara
	else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && sementara != "") kes = 7; // sementara
	else kes = 'default';
		
	console.log("kes = "+ kes);
		
	switch (kes){
			
		case 1:
			query_Sementara.where = "NEGERI = '" + state  + "' AND SEMENTARA <> ''";
		break;
			
		case 2:
			query_Sementara.where = "NEGERI = '" + state  + "'  AND daerah = '" + district + "' AND SEMENTARA <> ''";
		break;
			
		case 3:
			query_Sementara.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND SEMENTARA <> ''";
		break;

		case 4:
			query_Sementara.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND KEGUNAAN_TANAH LIKE '%" + sementara + "%'";
		break;

		case 5:
			query_Sementara.where = "NEGERI = '" + state  + "' AND KEGUNAAN_TANAH LIKE '%" + sementara + "%'";
		break;

		case 6:
			query_Sementara.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND KEGUNAAN_TANAH LIKE '%" + sementara + "%'";
		break;

		case 7:
			query_Sementara.where = "KEGUNAAN_TANAH LIKE '%" + sementara + "%'";
		break;
			
		default:
			query_Sementara.where = "SEMENTARA <> ''";
		break;		
	}
		
	console.log(query_Sementara.where);

	// Execute query
	queryTask_Sementara.execute(query_Sementara, showResults_Sementara);
	showResultpane_Sementara();
} 

function showResultpane_Sementara() {  
			dijit.byId('searchResultSementara').show();

			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
}

// Sementara: Show Search Result			
function showResults_Sementara(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Sementara = dijit.byId("gridSementara");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Sementara.set("noDataMessage", 'Sorry, there are no results.');
		grid_Sementara.setStore(clearStore); 	
		console.log("I'M HERE");				
	}
	
	else {								    			    
		prevfeature = null;
		console.log("I'M HERE TOO");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_Sementara = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_Sementara.push(feature.attributes);  
		
			//Set the infoTemplate.
			// var bil_mangsa_list = feature.attributes.bil_mangsa_daftar;
			// var turun_senarai = '';
			// if (bil_mangsa_list > 0) turun_senarai = "<a href='assets/flood_report/victim_list.xls' target='blank'>Download list</a>";
			// else turun_senarai = "-Tiada-";
				
			var info_sementara = new esri.InfoTemplate();
			info_sementara.setTitle("Hak Milik Sementara");
			info_sementara.setContent("No. Fail Pemohonan: ${KPKT} <br> Projek: ${CATATAN} <br> Tarikh Permohonan: ${PERMOHONAN} <br> Tanah Asal : ${TANAH_ASAL} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${SEMENTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNITLUAS} <br> Status : ${HAKMILIK1} <br> NEGERI: ${NEGERI} <br> DAERAH: ${DAERAH} <br> MUKIM : ${MUKIM} <br> SEKSYEN : ${SEKSYEN} <br> LOT : ${LOT} <br> UPI : ${UPI} <br> PA : ${PA}");

			// info_sementara.setContent(
			// 						"<table id='info'>" +
			// 							"<tr><td><b>LOT</b></td><td><b>: </b>${LOT}</td></tr>" +
			// 							"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
			// 						"</table>" 
			//  ); 
			graphic.setInfoTemplate(info_sementara); 
			//console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gSementara = evt.graphic;
          map.infoWindow.setContent(gSementara.getContent());
          map.infoWindow.setTitle(gSementara.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_Sementara = {
			identifier : "OBJECTID_1", //This field needs to have unique values
			label : "OBJECTID_1", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Sementara
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Sementara,
				clearOnClose: true
		});
												
		grid_Sementara.set("store", store);
		
		// Clear previous search results
		if(grid_Sementara.store.save) { grid_Sementara.store.save();}
		grid_Sementara.store.close();				
		grid_Sementara._refresh(); // or grid.store.fetch();								
		grid_Sementara.filter();
		
		grid_Sementara.on("rowClick", onRowClickHandler_grid_Sementara);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Sementara(evt){
	var grid = dijit.byId("gridSementara");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID_1;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID_1 == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_Sementara(){
	dojo.byId('select_state_sementara').value = "Select state...";
	dojo.byId('select_district_sementara').value = "Select district...";
	dojo.byId('select_mukim_sementara').value = "Select sub district...";
	dojo.byId('sementara').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridSementara");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridSementara"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultSementara').hide();
	
}