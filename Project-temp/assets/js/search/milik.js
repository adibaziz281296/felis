/**
 * @author Fadhilah, May 2016
 * Search Victim Centre Props
 */
var prevfeature = null;
 
// Create Victim Centre result grid
var gridMilik_Structure =[[
	// declare your structure here
	{name: 'TETAP', field: 'TETAP', width: '30%'},
	{name: 'LOTNO', field: 'LOTNO'},
	{name: 'STATE', field: 'NEGERI'},
	{name: 'CITY', field: 'DAERAH'},	
	{name: 'SUB DISTRICT', field: 'MUKIM'}
]];

// Victim Centre: REGION Dropdown 			
function populateList_state_milik(results) {
	var state;
	var values = [];
	var testVals={};

	//Loop through the QueryTask results and populate an array with the unique values
	var features = results.features;
	dojo.forEach (features, function(feature) { 
		state = feature.attributes.NEGERI
		//console.log(state);
		if (!testVals[state]) { 
			testVals[state] = true;
			values.push({name:state, id:state});
		}
	});
	
	//Create a ItemFileReadStore and use it for the comboBox's data source
	var dataItems = {
		identifier: "name", //bukan identifier: "OBJECTID" sbb ni nk create list
		label: "name",
		items: values
	};	

	dataItems.items.push({ name: 'Select state...', id: '0A' });
	//dataItems.items.push({ name: 'Seremban', id: 'Seremban' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	//var select = document.getElementById("year");
	
	dijit.byId("select_state_milik").set('store', store);
	
	/*  var myDijit = new dijit.form.ComboBox({
          store : store,
		  sort:[{attribute:'STATE', ascending:true}]
        });
        dojo.byId("my").appendChild(myDijit.domNode); */
/* 	values.sort(function(a, b){
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0;
	})
	
	var sel = document.getElementById('select_state_evacuation2'); // find the drop down
	for (var i in values) {
		console.log(values[i]);
		var item = values[i];
		sel.options[sel.options.length] = new Option(item.id, item.name);
    } */

}

		
// Victim Centre: DISTRICT Dropdown by STATE dropdown
function populateList_district_byState_milik(results) {
	dijit.byId("select_district_milik").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		district = feature.attributes.DAERAH;
		if (!testVals[district]) {
			testVals[district] = true;
			values.push({name:district, id:district});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_district_milik").set('store', store);
}

// Victim Centre: MUKIM Dropdown by STATE & DISTRICTdropdown
function populateList_mukim_byStateDistrict_milik(results) {
	dijit.byId("select_mukim_milik").reset(); 
	
	var mainline;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		mukim = feature.attributes.MUKIM;
		if (!testVals[mukim]) {
			testVals[mukim] = true;
			values.push({name:mukim, id:mukim});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select sub district...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_mukim_milik").set('store', store);
}

// Victim Centre: Initial No. KP Dropdown 
/* function populateList_KMPoint_NoKP(results) {
	var nokp;
	var values = [];
	var testVals={};			
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "name", 
		label: "name",
		items: values
	};	
			
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
} */

// KM Point: NO. KP Dropdown by REGION dropdown
/* function populateList_KMPoint_NoKPbyRegion(results) {
	dijit.byId("select_nokp_kmpoint").reset(); 
	
	var nokp;
	var values = [];
	var testVals={};
	
	var features = results.features;
	dojo.forEach (features, function(feature) {
		nokp = feature.attributes.NO_KP;
		if (!testVals[nokp]) {
			testVals[nokp] = true;
			values.push({name:nokp, id:nokp});				
		}
	});
		
	var dataItems = {
		identifier: "id",
		label: "name",
		items: values
	};	
	
	dataItems.items.push({ name: 'Select KM Point...', id: '0A' });
	
	var store = new dojo.data.ItemFileReadStore({data:dataItems});
	dijit.byId("select_nokp_kmpoint").set('store', store);
}
 */
// Victim Centre: Execute Search
function executeQueryTask_Milik() { console.log("execute task search Hak Milik");
	// Check if all values have been selected
	var state = dojo.byId('select_state_milik').value;
	var district = dojo.byId('select_district_milik').value;
	var mukim = dojo.byId('select_mukim_milik').value;
	var tetap = dojo.byId('milik').value;
		
	console.log(state);
	console.log(district);
	console.log(mukim);
	console.log(tetap);

	var kes;
		
	if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tetap == "") kes = 1; // state
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && tetap == "") kes = 2; // state, district
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && tetap == "") kes = 3; // state, district, mukim
	else if(state != "Select state..." && district != "Select district..." && mukim != "Select sub district..." && tetap != "") kes = 4; // state, district, mukim, tetap
	else if(state != "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tetap != "") kes = 5; // state, tetap
	else if(state != "Select state..." && district != "Select district..." && mukim == "Select sub district..." && tetap != "") kes = 6; // state, district, tetap
	else if(state == "Select state..." && district == "Select district..." && mukim == "Select sub district..." && tetap != "") kes = 7; // tetap
	else kes = 'default';
		
	console.log("kes = "+ kes);
		
	switch (kes){
			
		case 1:
			query_Milik.where = "NEGERI = '" + state  + "' AND TETAP <> ''";
		break;
			
		case 2:
			query_Milik.where = "NEGERI = '" + state  + "'  AND daerah = '" + district + "' AND TETAP <> ''";
		break;
			
		case 3:
			query_Milik.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND TETAP <> ''";
		break;

		case 4:
			query_Milik.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND MUKIM = '" + mukim + "' AND TETAP LIKE '%" + tetap + "%'";
		break;

		case 5:
			query_Milik.where = "NEGERI = '" + state  + "' AND TETAP LIKE '%" + tetap + "%'";
		break;

		case 6:
			query_Milik.where = "NEGERI = '" + state  + "'  AND DAERAH = '" + district + "' AND TETAP LIKE '%" + tetap + "%'";
		break;

		case 7:
			query_Milik.where = "TETAP LIKE '%" + tetap + "%'";
		break;
			
		default:
			query_Milik.where = "TETAP <> ''";
		break;		
	}
		
	console.log(query_Milik.where);

	// Execute query
	queryTask_Milik.execute(query_Milik, showResults_Milik);
	showResultpane_Milik();
} 

function showResultpane_Milik() {  
			dijit.byId('searchResultMilik').show();

			dijit.byId('searchFormLot').hide();
			dijit.byId('searchFormProject').hide();
			dijit.byId('searchFormSementara').hide();
			dijit.byId('searchFormMilik').hide();
}

// Milik: Show Search Result			
function showResults_Milik(featureSet) {
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid_Milik = dijit.byId("gridMilik");				
	
	// Remove all graphics on the maps graphics layer
	map.graphics.clear();
	
	// Remove previous search results
	if (featureSet.features.length == 0) {
		grid_Milik.set("noDataMessage", 'Sorry, there are no results.');
		grid_Milik.setStore(clearStore); 	
		console.log("I'M HERE");				
	}
	
	else {								    			    
		prevfeature = null;
		console.log("I'M HERE TOO");
		//Performance enhancer - assign featureSet array to a single variable.
		var resultFeatures = featureSet.features;
		
		// Create items array to be added to store's data
		var items_Milik = []; // all items to be stored in data store
	
		//Loop through each feature returned
		dojo.forEach(featureSet.features, function (feature) {  
			var graphic = feature;  
			graphic.setSymbol(symbolSearch);  
			map.graphics.add(graphic);  
			items_Milik.push(feature.attributes);  
		
			//Set the infoTemplate.
			// var bil_mangsa_list = feature.attributes.bil_mangsa_daftar;
			// var turun_senarai = '';
			// if (bil_mangsa_list > 0) turun_senarai = "<a href='assets/flood_report/victim_list.xls' target='blank'>Download list</a>";
			// else turun_senarai = "-Tiada-";
				
			var info_milik = new esri.InfoTemplate();
			info_milik.setTitle("Hak Milik");
			info_milik.setContent("No. Fail Pemohonan: ${KPKT} <br> Projek: ${CATATAN} <br> Tarikh Permohonan: ${PERMOHONAN} <br> Tanah Asal : ${TANAH_ASAL} <br> Kementerian : ${KEMENTERIAN} <br> No Hak Milik Sementara : ${SEMENTARA} <br> Keluasan : ${KELUASAN} <br> Unit Luas : ${UNITLUAS} <br> Status : ${HAKMILIK1} <br> NEGERI: ${NEGERI} <br> DAERAH: ${DAERAH} <br> MUKIM : ${MUKIM} <br> SEKSYEN : ${SEKSYEN} <br> LOT : ${LOT} <br> UPI : ${UPI} <br> PA : ${PA}");

			// info_sementara.setContent(
			// 						"<table id='info'>" +
			// 							"<tr><td><b>LOT</b></td><td><b>: </b>${LOT}</td></tr>" +
			// 							"<tr><td><b>&nbsp;</b></td><td>&nbsp;</td></tr>" +
			// 						"</table>" 
			//  ); 
			graphic.setInfoTemplate(info_milik); 
			//console.log(feature.attributes);						
		}); // close loop forEach
		
		dojo.connect(map.graphics, "onMouseOut", function(evt) {
          var gMilik = evt.graphic;
          map.infoWindow.setContent(gMilik.getContent());
          map.infoWindow.setTitle(gMilik.getTitle());
          map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
        });
		
									
		//Create data object to be used in store
		var data_Milik = {
			identifier : "OBJECTID_1", //This field needs to have unique values
			label : "OBJECTID_1", //Name field for display. Not pertinent to a grid but may be used elsewhere.
			items : items_Milik
		};
		 
		//Create data store and bind to grid.
		var store = new dojo.data.ItemFileWriteStore({
				data: data_Milik,
				clearOnClose: true
		});
												
		grid_Milik.set("store", store);
		
		// Clear previous search results
		if(grid_Milik.store.save) { grid_Milik.store.save();}
		grid_Milik.store.close();				
		grid_Milik._refresh(); // or grid.store.fetch();								
		grid_Milik.filter();
		
		grid_Milik.on("rowClick", onRowClickHandler_grid_Milik);				
		var resultExtent = esri.graphicsExtent(resultFeatures);
		map.setExtent(resultExtent);	    
		//map.centerAndZoom(center, zoom);		
	} 
}

//Zoom to the feature when the user clicks a row		
function onRowClickHandler_grid_Milik(evt){
	var grid = dijit.byId("gridMilik");				
	var clickedId = grid.getItem(evt.rowIndex).OBJECTID_1;
	var selectedId;
	// alert(clickedId);	    
	for (var i = 0, il = map.graphics.graphics.length; i < il; i++) {
		var currentGraphic = map.graphics.graphics[i];
			if ((currentGraphic.attributes) && currentGraphic.attributes.OBJECTID_1 == clickedId) {
				selectedId = currentGraphic;
				break;
			}
	}
				   
	var feature = selectedId;
	console.log('selected id= ' + clickedId);	
	// Get extent then zoom in
	getFeatureExtent(feature);
}
				


function clearSearch_Milik(){
	dojo.byId('select_state_milik').value = "Select state...";
	dojo.byId('select_district_milik').value = "Select district...";
	dojo.byId('select_mukim_milik').value = "Select sub district...";
	dojo.byId('milik').value = "";
	//dojo.byId('select_nokp_kmpoint').value = "Select KM Point...";
	
	//clearQueryTask("gridMilik");
	var clearStore = new dojo.data.ItemFileReadStore({data: {  identifier: "",  items: []}});
	var grid = dijit.byId("gridMilik"); 
	grid.set("noDataMessage", 'There are no items to display.');	
	grid.setStore(clearStore);
	dijit.byId('searchResultMilik').hide();
	
}